from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from core import views

urlpatterns = [

    url(r'^rubric_list/$', views.rubric_list, name="rubric_list"),
    url(r'^postulate/$', views.postulate, name="postulate"),
    url(r'^project_details/(?P<pk>\d+)$', views.tesista_details, name="tesista_details"),
    url(r'^agregar_cloud/$', views.agregar_cloud, name="agregar_cloud"),
    url(r'^modificar_cloud/$', views.modificar_cloud, name="modificar_cloud"),
    url(r'^get_cloud/$', views.get_cloud, name="get_cloud"),
    url(r'^planning/rubric_details/(?P<pk>\d+)$', views.rubric_details, name="rubric_details"),
    url(r'^planning/rubric_delete/(?P<pk>\d+)$', views.rubric_delete, name="rubric_delete"),
    url(r'^tesistas/$', views.tesistas, name="tesistas"),
    url(r'^profile/$', views.profile, name="profile"),
    url(r'^upload_photo/$', views.upload_photo, name="upload_photo"),
    url(r'^postulaciones/$', views.postulaciones, name="postulaciones"),
    url(r'^planning/add_rubric/$', views.rubric_add, name='planning_rubric_add'),
    url(r'^planning/rubric_create/$', views.rubric_create, name='planning_rubric_create'),
    url(r'^planning/delete_evaluation$', views.delete_evaluation, name="delete_evaluation"),
    url(r'^planning/delete_item$', views.delete_item, name="delete_item"),
    url(r'^planning/add_topic_week$', views.add_topic_week, name="add_topic_week"),
    url(r'^planning/add_presentation_item$', views.add_presentation_item, name="add_presentation_item"),
    url(r'^planning/add_item$', views.item_create, name="item_create"),
    url(r'^planning/add_evaluation$', views.evaluation_create, name="evaluation_create"),
    url(r'^project_evaluation/$', views.project_evaluation, name="project_evaluation"),
    url(r'^evaluation_details/(?P<pk>\d+)/session/(?P<session_num>\d+)$', views.evaluation_details, name="evaluation_details"),
    url(r'^create_evaluation$', views.create_evaluation, name='create_evaluation'),
    url(r'^create_evaluation_hito$', views.create_evaluation_hito, name='create_evaluation_hito'),
    url(r'^upload_date$', views.upload_date, name='upload_date'),
    url(r'^teacher_postulate$', views.teacher_postulate, name='teacher_postulate'),
    url(r'^postulate_detail_project/$', views.postulate_detail_project, name="postulate_detail_project"),
    url(r'^list_message/$', views.list_message, name="list_message"),
    url(r'^rubric_activate/$', views.rubric_activate, name="rubric_activate"),
    url(r'^rubric_deactivate/$', views.rubric_deactivate, name="rubric_deactivate"),
    url(r'^get_invites/$', views.get_invites, name="get_invites"),
    url(r'^show_message_modal/$', views.show_message_modal, name="show_message_modal"),
    url(r'^message_Info/$', views.message_Info, name="message_Info"),


]
