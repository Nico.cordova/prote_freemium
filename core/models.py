#-*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import get_object_or_404
from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
from projects.models import Project
from django.utils import timezone

STATUS_MAIL_TYPE_CHOICES = [
    ('AC', 'Active'),
    ('DC', 'Desactive')
]

STATUS_HITOEVALUATION_CHOICES = [
    ('BO', 'Borrador'),
    ('FI', 'Finalizado')
]

EVALUATION_TYPE_CHOICES = [
    ('OB', 'Objetivo'),
    ('AT', 'Actividad'),
    ('AV', 'Avance')
]

USERPROFILE_TYPE_CHOICES = [
    ('AD','Administrador'),
    ('IN','Académico'),
    ('EE','Encargado de Empresa')
]

@python_2_unicode_compatible
class Carreer(models.Model):
    career_pk = models.IntegerField(blank=True,null=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "%s" % (self.name)


@python_2_unicode_compatible
class UserProfile(models.Model):
	api_user = models.IntegerField(blank=True,null=True)
	user = models.OneToOneField(User,null=True,blank=True)
	rut = models.CharField(max_length=30,blank=True,null=True)
	max_projects = models.IntegerField(default=9)
	name = models.CharField(max_length=100,blank=True,null=True)
	img = models.ImageField(upload_to="profilePic", default="../Static/img/profiles/image.png")

	user_type = models.CharField(max_length=2,
									choices=USERPROFILE_TYPE_CHOICES,
									default="AC")

	def __str__(self):
		return self.name

	def get_amount_projects(self):
		return len(self.guide_teacher.all())

class Student(models.Model):
	api_user = models.IntegerField(blank=True,null=True)
	user = models.OneToOneField(User, on_delete=models.CASCADE,null=True,blank=True)
	rut = models.CharField(max_length=30,blank=True,null=True)
	name = models.CharField(max_length=100,blank=True,null=True)
	project = models.ForeignKey(Project,null=True,blank=True)
	actual_subject = models.CharField(max_length=50,null=True,blank=True)
	img = models.ImageField(upload_to="profilePic",default="../Static/img/profiles/image.png")
	carreers = models.CharField(max_length=30,null=True,blank=True)
	def __str__(self):
		return "%s" % (self.name)

class Session_start(models.Model):
    carreer = models.IntegerField()
    beginning = models.DateField()
    period = models.CharField(max_length=2,null=True,blank=True)
    last_mod = models.DateTimeField(default=timezone.now)

@python_2_unicode_compatible
class Rubric(models.Model):
	name = models.CharField(max_length=60)
	author = models.ForeignKey(User, null=True)
	date = models.DateTimeField(blank=True, null=True)
	desem = models.IntegerField(blank=True,null=True)
	pond_desem = models.IntegerField(default=0)
	carreer = models.IntegerField(blank=True, null=True)
	subject = models.CharField(max_length=50,blank=True, null=True)
	description = models.CharField(max_length=300, null=True, blank=True)
	status = models.CharField(max_length=2,
								choices=STATUS_MAIL_TYPE_CHOICES,
								default="DC")

	def __str__(self):
		return self.name


class Subject(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=20)
    carreer = models.IntegerField()


@python_2_unicode_compatible
class Session(models.Model):
    rubric = models.ForeignKey(Rubric,blank=True,null=True)
    number = models.IntegerField()

    def __str__(self):
        return self.rubric.name + str(self.number)

class Hito(models.Model):
	ponderation = models.IntegerField(default=0,blank=True)
	final = models.IntegerField(default=0)	
	session = models.OneToOneField(Session,null=True,related_name="hito")
	tech_pond = models.IntegerField(default=33.3)
	mem_pond = models.IntegerField(default=33.3)
	pres_pond = models.IntegerField(default=33.3)
	total_technique_percentage = models.IntegerField(default=0)
	total_presentation_percentage = models.IntegerField(default=0)
	total_memory_percentage = models.IntegerField(default=0)

class Technique(models.Model):
    hito = models.ForeignKey(Hito, null=True)
    item = models.CharField(max_length=300, blank=True, null=True)
    percentage = models.IntegerField()

class Memory(models.Model):
    hito = models.ForeignKey(Hito, null=True)
    item = models.CharField(max_length=300, blank=True, null=True)
    percentage = models.IntegerField()

class Topic(models.Model):
    name = models.CharField(max_length=300,blank=True,null=True)
    hito = models.ForeignKey(Hito,null=True)

class SubTopic(models.Model):
    topic = models.ForeignKey(Topic,null=True)
    name = models.CharField(max_length=300,blank=True,null=True)
    percentage = models.IntegerField()

class Week(models.Model):
    topic = models.CharField(max_length=200,null=True,blank=True)
    session = models.OneToOneField(Session,null=True,related_name='week')
    total_objetive_percentage = models.IntegerField(default=0)
    total_progress_percentage = models.IntegerField(default=0)
    total_activity_percentage = models.IntegerField(default=0)

class Evaluation(models.Model):
    name = models.CharField(max_length=300, blank=True, null=True)
    percentage = models.IntegerField()
    week = models.ForeignKey(Week, null=True)
    type_evaluation = models.CharField(max_length=2,
                                       choices=EVALUATION_TYPE_CHOICES,
                                       default="OB",
                                      )

class ProjectEvaluation(models.Model):
    date = models.DateTimeField(default=timezone.now,blank=True)
    project = models.ForeignKey(Project,blank=True,null=True)
    session = models.ForeignKey(Session,blank=True,null=True)
    status = models.CharField(max_length=2,choices=STATUS_HITOEVALUATION_CHOICES,default="BO")
    comment_progress = models.TextField(max_length=300)
    comment_activity = models.TextField(max_length=300)
    comment_accomplishment = models.TextField(max_length=300)
    objetive_qualification = models.FloatField(default=0)
    activity_qualification = models.FloatField(default=0)
    progress_qualification = models.FloatField(default=0)
    session_qualification = models.FloatField(default=0)


class ObjetiveEvaluation(models.Model):
    project_evaluation = models.ForeignKey(ProjectEvaluation, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation,null=True,blank=True)
    nota = models.DecimalField(max_digits=2,decimal_places=1,null=True,blank=True)

class ActivityEvaluation(models.Model):
    project_evaluation = models.ForeignKey(ProjectEvaluation, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation,null=True,blank=True)
    nota = models.DecimalField(max_digits=2,decimal_places=1,null=True,blank=True)
    
class ProgressEvaluation(models.Model):
    project_evaluation = models.ForeignKey(ProjectEvaluation, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation,null=True,blank=True)
    nota = models.DecimalField(max_digits=2,decimal_places=1,null=True,blank=True)
    
class ProjectHitoEvaluation(models.Model):
    date = models.DateTimeField(default=timezone.now,blank=True)
    project = models.ForeignKey(Project,blank=True,null=True)
    session = models.ForeignKey(Session,blank=True,null=True)
    status = models.CharField(max_length=2,choices=STATUS_HITOEVALUATION_CHOICES,default="BO")
    technique_qualification = models.FloatField(default=0)
    presentation_qualification = models.FloatField(default=0)
    memory_qualification = models.FloatField(default=0)
    hito_qualification = models.FloatField(default=0)
    comment_progress = models.TextField(max_length=300)
    comment_activity = models.TextField(max_length=300)
    comment_accomplishment = models.TextField(max_length=300)

class MemoryEvaluation(models.Model):
    project_hito_evaluation = models.ForeignKey(ProjectHitoEvaluation, blank=True, null=True)
    memory = models.ForeignKey(Memory, blank=True,null=True)
    nota = models.DecimalField(max_digits=2,decimal_places=1,null=True,blank=True)

class TechniqueEvaluation(models.Model):
    project_hito_evaluation = models.ForeignKey(ProjectHitoEvaluation, blank=True, null=True)
    technique = models.ForeignKey(Technique,blank=True,null=True)
    nota = models.DecimalField(max_digits=2,decimal_places=1,null=True,blank=True)

class PresentationEvaluation(models.Model):
    project_hito_evaluation = models.ForeignKey(ProjectHitoEvaluation, blank=True, null=True)
    presentation = models.ForeignKey(SubTopic,blank=True,null=True)
    nota = models.DecimalField(max_digits=2,decimal_places=1,null=True,blank=True)
