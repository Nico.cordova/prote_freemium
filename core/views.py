# -*- coding:utf-8 -*-
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import PermissionDenied
from core.models import *
from django.db.models import Q
from django.utils import timezone
from operator import itemgetter
from projects.models import MinutaRetro,MinutaValidation,Minuta,Teacher_Postulation, Glosa, Project, Postulation, Feedback
import datetime
import json
import math
import os
import unidecode
from unidecode import unidecode
import base64
import requests
import ast
from django.contrib.staticfiles.templatetags.staticfiles import static
from core.forms import *
from datetime import date
import sys



@login_required(login_url='/')
def index(request):
	data = {}
	template_name = "index.html"
	if request.session["type"] == "DS" or request.session["type"] == "SE" or request.session["type"] == "EE":
		return HttpResponseRedirect(reverse_lazy('index_practicas'))
	if request.session["type"] == "DC":
		post_data = {"carreer": 1}
		header = {"Content-Type": 'application/x-www-form-urlencoded',
                "Authorization": "Token " + request.session["token"]}
		response = requests.post(
            "http://supreme.webtic.cl/get/get_count_teacher/", headers=header, data=post_data)
		content = response.content
		content = ast.literal_eval(content)

		log_current = 0
		log_total = 0
		students_on_projects = 0
		students_total = 0
		teacher_on_projects = 0
		teachers_total = content['number_teachers']
		active_projects = 0
		projects_total = 0
		projects_pending = []
		students = Student.objects.all()
		projects = Project.objects.all()
		projects_total = projects.count()
		students_total = students.count()
		total_reuniones = len(Minuta.objects.filter(status="AP").all())
		data['total_reuniones'] = total_reuniones 

		minutas_pendientes = Minuta.objects.filter(Q(status="PE")|Q(status="EM")|Q(status="RM"))
		data['minutas_pendientes'] = minutas_pendientes.all() 
		aux_doc = []
		for p in projects:
			if p.status == "EC":
				students_on_projects += Student.objects.filter(project=p).count()
				active_projects += 1
				if p.guide_teacher not in aux_doc:
					teacher_on_projects += 1
					aux_doc.append(p.guide_teacher)
					
			if p.status == "ER":
				projects_pending.append(p)

		data['log_current'] = log_current
		data['log_total'] = log_total
		data['students_on_projects'] = students_on_projects
		data['students_total'] = students_total
		data['teacher_on_projects'] = teacher_on_projects
		data['teachers_total'] = teachers_total
		data['active_projects'] = active_projects
		data['projects_total'] = projects_total
		data['projects_pending'] = projects_pending

	elif request.session["type"] == "AL":
		student_data = get_object_or_404(Student, api_user=request.session["user_pk"])
		feedbacks = student_data.ms_receipt.all()


		data['latest_messages'] = feedbacks if feedbacks.count() != 0 else [{"description":"No hay mensajes disponibles", "author":" "}]
		if student_data.project is not None:
			beginning = Session_start.objects.get(carreer=student_data.project.carreer,period=student_data.project.period).beginning
			time_diff = datetime.date.today() - beginning
			time_diff = time_diff.total_seconds()
			sesion_actual = math.ceil(time_diff/60/60/24/7)
			data['project_name'] = student_data.project
			data['current_session'] = int(sesion_actual)
		else:
			data['project_name'] = "Sin proyecto"
			data['current_session'] = "--"

		data['project_teacher_name'] = student_data.project.guide_teacher if student_data.project is not None else "Sin profesor guía"

		minuta_st = Student.objects.get(user = request.user)
		try:
			if MinutaValidation.objects.filter(student=minuta_st,validation_status="PE").exists():
				minuta = MinutaValidation.objects.filter(student=minuta_st,validation_status="PE").all()[0]
				data['minuta_val'] = minuta
			else:
				data['minuta_val'] = None

		except IndexError:
			data['minuta_val'] = None

	elif request.session["type"] == "DO":
		teacher_data = get_object_or_404(UserProfile, api_user=request.session["user_pk"])
		projects = teacher_data.guide_teacher.all()
		my_students = 0
		for x in projects:
			my_students += x.student_set.all().count()
		students_total = Student.objects.all().count()

		my_projects = projects.filter(status="EC")
		projects_total = Project.objects.all().count()

		feedbacks = teacher_data.m_receipt.all()
		data['latest_messages'] = feedbacks if feedbacks.count() != 0 else [{"description":"No hay mensajes disponibles", "author":" "}]

		data["my_students"] = my_students
		data["students_total"] = students_total
		data["my_projects"] = my_students
		data["projects_total"] = students_total

	return render(request, template_name, data)

@login_required(login_url='/')
def project_evaluation(request):
	data = {}
	try:
		project = get_object_or_404(Project, pk=request.POST["project_pk"])
		if request.POST["session"] == "0":
			beginning = Session_start.objects.get(carreer=project.carreer).beginning
			time_diff = datetime.date.today() - beginning
			time_diff = time_diff.total_seconds()
			sesion_actual = math.ceil(time_diff/60/60/24/7)
		else:
			sesion_actual = request.POST["session"]

		data["url"] = "/dashboard/evaluation_details/"+str(project.pk)+"/session/"+str(sesion_actual)
		return JsonResponse(data)

	except:
		return JsonResponse({"mensaje":"ERROR"})

@login_required(login_url='/')
def evaluation_details(request,pk,session_num):
	data = {}
	project = Project.objects.get(pk=pk)
	student = project.student_set.all()[0]
	# beginning = Session_start.objects.get(carreer=project.carreer).beginning
	# time_diff = datetime.date.today() - beginning
	# time_diff = time_diff.total_seconds()
	# sesion_actual = math.ceil(time_diff/60/60/24/7)
	rubric = Rubric.objects.get(subject=student.actual_subject)

	session = rubric.session_set.get(number=int(session_num))
	
	if ProjectHitoEvaluation.objects.filter(session=session,project=project).exists() or ProjectEvaluation.objects.filter(session=session,project=project).exists():
		return reverse_lazy('project_current')
	else:
		template_name = "project_evaluation.html"
		data['session'] = session
		data['project'] = project
		return render(request,template_name,data)

@login_required(login_url='/')
def rubric_add(request):
    template_name = 'create_rubric.html'
    form = RubricForm()
    post_data = {"carreers": 0}

    header = {"Content-Type": 'application/json',
              "Authorization": "Token " + request.session["token"]}
    response = requests.post(
        "http://supreme.webtic.cl/get/get_carrers/", headers=header, data=post_data)
    content = response.content
    content = ast.literal_eval(content)
    html = ""
    for x in content["carreers"]:
        for z in [x][0].keys():
            html += "<option value='" + x[z][1] + "'>" + x[z][0] + "</option>"
    return render(request, template_name, {'forms': form, 'html': html})

@login_required(login_url='/')
def rubric_activate(request):
    subject = request.POST['subject']
    rubricpk = request.POST['rubric_pk']
    rubric = get_object_or_404(Rubric,pk=int(rubricpk))
    rubric.subject = subject
    rubric.save()

    response = {}
    response["message"] = "activado con exito"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

@login_required(login_url='/')
def rubric_deactivate(request):
	rubricpk = request.POST['pk']
	rubric = get_object_or_404(Rubric,pk=rubricpk)
	rubric.subject = None
	rubric.save()
	response = {}
	response["message"] = "desactivado con exito"
	return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

@login_required(login_url='/')
def rubric_create(request):
	response = {}
	cont = 1
	sessions = json.loads(request.POST['sessions'])
	name = request.POST['title']
	description = request.POST['description']
	carreer = 1
	rubrica = Rubric.objects.create(name=name,
									date=datetime.datetime.now(),
									author=request.user,
									desem=1 if request.POST["desem"] == "true" else 0, 
									description=description,
									carreer=carreer,
									)

	rubrica.save()
	response['pk'] = rubrica.pk

	i = 0
	for x in sessions:
		session_obj = Session.objects.create(number=cont,
											 rubric=rubrica)
		session_obj.save()
		if x == "s":
			week = Week.objects.create(session=session_obj)
			week.save()
		elif x == "h":
			hito = Hito.objects.create(session=session_obj,
									   ponderation=request.POST['ponderacion-'+str(i)])
			hito.save()
			i+=1

		session_obj.save()
		cont += 1
	
	if "desem_pond" in request.POST:
		rubrica.pond_desem = request.POST['desem_pond']
		rubrica.save()

	hito.final = 1
	hito.save()
	response['mensaje'] = "Rubrica Creada"
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
	)

def login_user(request):
	logout(request)
	username = password = ''
	if request.POST:
		request.session["h"] = 0
		username = request.POST["username"]
		password = request.POST["password"]
		post_data = {
			'username': username,
			'password': request.POST["password"]
		}
		response = requests.post(
			"http://supreme.webtic.cl/rest-auth/login/", data=post_data)
		content = response.content
		content = ast.literal_eval(content)
		if "key" in content:
			post_data = {'username': username}
			header = {"Content-Type": 'application/json',
					  "Authorization": "Token " + content["key"]}
			response_1 = requests.get(
				'http://supreme.webtic.cl/rest-auth/user/', headers=header, data=post_data)
			content_1 = ast.literal_eval(response_1.content)
			request.session["name"] = content_1["first_name"] + \
				" " + content_1["last_name"]
			request.session["token"] = content["key"]
			header = {"Content-Type": 'application/x-www-form-urlencoded',
					  'Authorization': "Token " + content["key"]}
			post_data = {'user_pk': content_1['pk'],
						 'period': datetime.date.today().year}
			response_2 = requests.post(
				'http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
			content_2 = ast.literal_eval(response_2.content)
			content_2 = content_2['user_info']
			request.session["user_pk"] = content_1['pk']
			request.session["username"] = username
			request.session["rut"] = content_2['user_rut']
			request.session["type"] = content_2['user_type']
			if content_1['first_name'][0] == " ":
				first = content_1['first_name'][1:].split(" ")
			else:
				first = content_1['first_name'].split(" ")

			if content_1["last_name"][0] == " ":
				last = content_1["last_name"][1:].split(" ")
			else:
				last = content_1["last_name"].split(" ")

			full_name = ""
			for name in first:
				full_name += name.capitalize()+" "

			for name in last:
				full_name += name.capitalize()+" "

			if not User.objects.filter(username=username).exists():
				n_usuario = User.objects.create_user(
					username=username,
					password=password
					)
				n_usuario.save()
				n_usuario.first_name = content_1['first_name']
				n_usuario.last_name = content_1['last_name']
				n_usuario.save()

				if request.session["type"] == "AL":
					try:
						student = Student.objects.get(
							rut=content_2["user_rut"])
						student.api_user = content_1["pk"]
						student.user = n_usuario
						print content_2['subject_actual']
						student.actual_subject = content_2['subject_actual']
						student.save()

						if student.project != None:
							request.session["project"] = student.project.pk
						else:
							request.session["project"] = "None"

					except:
						student = Student.objects.create(
							api_user=content_1['pk'],
							user=n_usuario,
							actual_subject=content_2["subject_actual"],
							name=full_name,
							carreers=content_2["carreer_pk"],
							rut=content_2['user_rut'],
						)
						student.save()
						request.session["project"] = "None"
				else:
					try:
						perfil = UserProfile.objects.get(
							rut=content_2['user_rut'])
						perfil.api_user = content_1["pk"]
						perfil.user = n_usuario
					except:
						perfil = UserProfile.objects.create(
							rut=content_2['user_rut'],
							name=full_name,
							api_user=content_1['pk'],
							user=n_usuario,
						)
					perfil.save()

				user = authenticate(
					username=username,
					password=password
				)
				login(request, user)

				return HttpResponseRedirect(reverse_lazy('index'))
			else:
				user = authenticate(
					username=username,
					password=password
				)
				login(request, user)
				if request.session["type"] == "AL":
					try:
						student = Student.objects.get(
							rut=content_2["user_rut"])
						student.api_user = content_1["pk"]
						student.user = user
						print content_2['subject_actual']
						student.actual_subject = content_2['subject_actual']
						student.save()
						
						if student.project != None:
							request.session["project"] = student.project.pk
						else:
							request.session["project"] = "None"

					except:
						student = Student.objects.create(
							api_user=content_1['pk'],
							user=user,
							name=full_name,
							carreers=content_2["carreer_pk"],
							rut=content_2['user_rut'],
						)
						student.save()
						request.session["project"] = "None"
				else:
					try:
						perfil = UserProfile.objects.get(
							rut=content_2['user_rut'])
						perfil.api_user = content_1["pk"]
						perfil.user = user
					except:
						perfil = UserProfile.objects.create(
							rut=content_2['user_rut'],
							name=full_name,
							api_user=content_1['pk'],
							user=user,
						)
					perfil.save()
				return HttpResponseRedirect(reverse_lazy('index'))

	data = {}
	template_name = "login.html"
	return render(request, template_name, data)


@login_required(login_url='/')
def rubric_list(request):
	rubrics = Rubric.objects.all()
	rubrics = rubrics[::-1]
	template_name = 'rubric_list.html'

	rubrics_f = []
	for rubric in rubrics:
		if Carreer.objects.filter(career_pk=int(rubric.carreer)).exists():
			carreer = Carreer.objects.get(career_pk=rubric.carreer)
			str_f = carreer.name
		else:
			header = {"Content-Type": 'application/x-www-form-urlencoded',
					'Authorization': "Token " + request.session["token"]}
			post_data = {'carreer': rubric.carreer}

			response = requests.post(
					'http://supreme.webtic.cl/get/Carreer_Data/', headers=header, data=post_data)

			content = json.loads(response.content)
			str_f = content["nombre"]
			carreer = Carreer.objects.create(name=str_f,career_pk=rubric.carreer)
			carreer.save()
		if rubric.subject:
			s_name = Subject.objects.get(code=rubric.subject).name
			rubrics_f.append([rubric,str_f,s_name])
		else:
			rubrics_f.append([rubric,str_f])

	return render(request, template_name, {'rubrics':rubrics_f})

@login_required(login_url='/')
def teacher_postulate(request):
	teacher = UserProfile.objects.get(user=request.user)
	if len(teacher.guide_teacher.all()) < 15:
		projects = Project.objects.filter(status="ED")
		output = []
		for project in projects:
			if not Teacher_Postulation.objects.filter(project=project).filter(teacher=teacher).exists():
				output.append(project)
			else:
				pass

		data = {"projects": output}
		html = "teacher_postulate.html"
		return render(request, html, data)
	else:
		return HttpResponseRedirect(reverse_lazy("index"))

@login_required(login_url='/')
def postulate(request):
    if request.session["project"] == "None":
		student = Student.objects.get(user=request.user)
		projects = Project.objects.filter(status="AC")
		output = []
		for project in projects:
			if not Postulation.objects.filter(project=project).filter(student=student).exists():
				if project.student_set.exists():
					if student.actual_subject == project.student_set.all()[0].actual_subject:
						output.append(project)
					else:
						pass
				else:
					output.append(project)
			else:
				pass

		data = {"projects": output}
		template_name = "postulate.html"
		return render(request, template_name, data)
    else:
        return HttpResponseRedirect(reverse_lazy("index"))

@login_required(login_url='/')
def postulate_detail_project(request):
    if request.POST["info"]:
		project = Project.objects.get(pk=request.POST['info'])
		postulations = project.postulation_set.filter(status='PE')
		html_postulations = ''
		for p in postulations:
			html_postulations += '<li class="list-group-item ">'\
                                    '<span class="pull-left"><div class="circle md md-bg-pink-50 m-r-10"></div></span>'\
                                    '<div class="list-group-item-body">'\
                                        '<div class="list-group-item-heading">' + p.student.name + '</div>'\
                                        '<div class="list-group-item-text">Fecha de postulacion: ' + unicode(p.creation_date.strftime('%d/%m/%Y')) + '</div>'\
                                    '</div>'\
                                 '</li>'

		students = Student.objects.filter(project=project)
		html_students = ''
		for s in students:
			html_students += '<li class="list-group-item ">'\
                                '<span class="pull-left"><div class="circle md md-bg-pink-50 m-r-10"></div></span>'\
                                '<div class="list-group-item-body">'\
									'<div class="list-group-item-heading">' + s.name + '</div>'\
									'<div class="list-group-item-text">Inscrito</div>'\
                                '</div>'\
                             '</li>'

		return JsonResponse({
            "project": {
				"name":project.name,
				"description":project.description,
			},
            "postulations":html_postulations,
            "students":html_students
        })
    else:
        raise PermissionDenied


@login_required(login_url='/')
def tesista_details(request, pk): # HAY QUE REVISAR EL FUNCIONAMIENTO DE ESTE VIEW AL HABER ELIMINADOS LA INTERACCION CON ABET
	project = get_object_or_404(Project, pk=pk)
	data = {}
	t_tag = '<span class="label label-info">%s</span>'
	teachers = UserProfile.objects.filter(user_type="IN").exclude(user=request.user).exclude(id__in=[obj.id for obj in project.shared_teachers.all()]).all() if project.shared_teachers.count() < 3 else False 
	data["teachers"] = teachers
	shared_teachers = project.shared_teachers.all() if project.shared_teachers.count() != 0 else False
	data['shared_teachers'] = shared_teachers 
	if Carreer.objects.filter(career_pk=int(project.carreer)).exists():
		carreer = Carreer.objects.get(career_pk=project.carreer)
		str_f = carreer.name
	else:
		header = {"Content-Type": 'application/x-www-form-urlencoded',
				'Authorization': "Token " + request.session["token"]}
		post_data = {'carreer': project.carreer}

		response = requests.post(
				'http://supreme.webtic.cl/get/Carreer_Data/', headers=header, data=post_data)

		content = json.loads(response.content)
		str_f = content["nombre"]
		carreer = Carreer.objects.create(name=str_f,career_pk=project.carreer)
		carreer.save()

	tags = ""
	for tag in project.tags.all():
		tags += t_tag % (tag.value)
	if project.student_set.exists():
		subject = project.student_set.all()[0].actual_subject
		subject_name = Subject.objects.get(code=subject).name
		rubric = Rubric.objects.filter(subject=subject).exists()
		if rubric:
			rubric=1
		else:
			rubric=0
		data["rubric"] = rubric
		try:
			rubric = Rubric.objects.get(subject=subject)
			sessions = Session.objects.filter(rubric=rubric).order_by('number')
			evaluations = []
			for session in sessions:
				if session.projecthitoevaluation_set.filter(project=project):
					evaluations.append([session.projecthitoevaluation_set.get(project=project),1])
				elif session.projectevaluation_set.filter(project=project):
					evaluations.append([session.projectevaluation_set.get(project=project),0])

			cant_semanas = len(rubric.session_set.all())
			data["cant_semanas"] = cant_semanas
			data["evaluations"] = evaluations
		except Rubric.DoesNotExist:
			data["rubric"] = 0
	else:
		subject = "Sin Asignatura"
		subject_name = ""
	data["subject"] = subject
	data["subject_name"] = subject_name
	minutas = Minuta.objects.filter(project=project).all()
	hoy = timezone.now()
	data["minutas_pendientes"] = []
	data['retro_pendientes'] = []
	for minuta in minutas:
		if request.session["type"] == "AL":
			try:
				retro = MinutaRetro.objects.get(minuta=minuta,read=0)
				data["retro_pendientes"].append(retro)
			except MinutaRetro.DoesNotExist:
				pass

		if (minuta.reunion_date - hoy).total_seconds() < 0 and not minuta.minutaevidence_set.exists():
			if minuta.status == "ER":
				minuta.status = "EM"
				minuta.save()
			elif minuta.status == "PE":
				validations = minuta.minutavalidation_set.all()
				for validation in validations:
					validation.validation_status = "NR"
					validation.save()
				minuta.status="EM"
				minuta.save()
				 				
		if minuta.status == "RM":
			data["minutas_pendientes"].append(minuta)
	if len(data['minutas_pendientes']) == 0:
		data["minutas_pendientes"] = False
	if len(data['retro_pendientes']) == 0:
		data["retro_pendientes"] = False

	data["info"] = [project, unidecode(tags), str_f]
	if Session_start.objects.filter(carreer=project.carreer).exists():
		#startClass = Session_start.objects.get(carreer=project.carreer).beginning Obtención del dato, no necesario a implementar
		#pero lo dejo por si acaso es útil en algún momento
		data["startClass"] = 1
	else:
		data["startClass"] = 0
	data["minutas"] = minutas
	data["project"] = project

	if Minuta.objects.filter(status="PE",project=project).exists():
		minuta = Minuta.objects.filter(status="PE",project=project)[0]
		data["minuta"] = minuta
		data["validations"] = MinutaValidation.objects.filter(minuta=minuta).all()
		data["val_flag"] = True
		for validation in data["validations"]:
			if validation.validation_status == "PE":
				data['val_flag'] = False
	else:
		data["minuta"] = False

	if request.session["type"] == "AL":
		try:
			if MinutaValidation.objects.filter(student=Student.objects.get(user=request.user),validation_status="PE").exists():
				solicitud = MinutaValidation.objects.filter(student=Student.objects.get(user=request.user),validation_status="PE")[0]
				if solicitud.minuta.status == "PE":
					data["solicitud"] = solicitud
				else:
					data["solicitud"] = False
			else:
				data['solicitud'] = False
		except MinutaValidation.DoesNotExist:
			data["solicitud"] = False

	template_name = "projects/tesista_details.html"
	return render(request, template_name, data)

@login_required(login_url='/')
def agregar_cloud(request):
	pk = request.POST["pk_project"]
	project = Project.objects.get(pk = int(pk))

	project.cloud = request.POST["cloud"]
	project.save()

	return redirect('tesista_details', pk)

def get_cloud(request):
	response = {}
	pk = request.POST["pk"]
	project = Project.objects.get(pk = int(pk))

	response["pk"] = project.pk
	response["cloud"] = project.cloud

	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
	)

@login_required(login_url='/')
def modificar_cloud(request):
	response = {}

	pk = request.POST["pk"]
	project = Project.objects.get(pk = int(pk))

	project.cloud = request.POST["cloud"]
	project.save()

	response["pk"] = project.pk


	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
	)


@login_required(login_url='/')
def tesistas(request):
    data = {}
    user_profile = UserProfile.objects.get(user=request.user)
    projects = Project.objects.filter(guide_teacher=user_profile)
    output = []

    for project in projects:
        students = []
        for student in project.student_set.all():
			header = {"Content-Type": 'application/x-www-form-urlencoded','Authorization': "Token " + request.session["token"]}
			post_data = {'rut': student.rut}
			response = requests.post('http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
			content = ast.literal_eval(response.content)
			content = content['user_info']
			if content['user_first_name'][0] == " ":
				first = content['user_first_name'][1:].split(" ")
			else:
				first = content['user_first_name'].split(" ")

			if content["user_last_name"][0] == " ":
				last = content['user_last_name'][1:].split(" ")
			else:
				last = content['user_last_name'].split(" ")
			student = ""
			for name in first:
			    student += name.capitalize()+" "

			for name in last:
				student += name.capitalize()+" "

			students.append(student)
        output.append([project, students])

    data["projects"] = output
    template_name = "projects/tesistas.html"

    return render(request, template_name, data)

#@login_required(login_url='/')
def profile(request):
	data = {}
	template_name = "perfil.html"
	post_data = {"username": request.session['username']}
	header = {"Content-Type": 'application/x-www-form-urlencoded',
              "Authorization": "Token " + request.session["token"]}
	response = requests.post("http://supreme.webtic.cl/get/get_careers/", headers=header, data=post_data)
	dic = {"\u00ed":"i","\u00f3":"o","\u00e1":"a"}
	content = response.content
	content = ast.literal_eval(content)
	perfil_carreers = []
	for career in content['careers']:
		if Carreer.objects.filter(career_pk=int(career['info'])).exists():
			str_f = Carreer.objects.get(career_pk=int(career['info'])).name
			perfil_carreers.append(str_f)
	data = {"perfil_carreers":perfil_carreers}
	projects = Project.objects.all()
	sessions = Session_start.objects.all()
	data["projects"] = []
	data["sessions"] = []
	for project in projects:
		if Carreer.objects.filter(career_pk=int(project.carreer)).exists():
			carreer = Carreer.objects.get(career_pk=project.carreer)
		else:
			header = {"Content-Type": 'application/x-www-form-urlencoded',
					'Authorization': "Token " + request.session["token"]}
			post_data = {'carreer': project.carreer}
			response = requests.post(
					'http://supreme.webtic.cl/get/Carreer_Data/', headers=header, data=post_data)
			content = json.loads(response.content)
			str_f = content["nombre"]
			carreer = Carreer.objects.create(name=str_f,career_pk=project.carreer)
			carreer.save()
		data['projects'].append([project,carreer.name])
	for session in sessions:
		if Carreer.objects.filter(career_pk=int(session.carreer)).exists():
			carreer = Carreer.objects.get(career_pk=session.carreer)
		else:
			dic = {"\u00ed":"i","\u00f3":"o","\u00e1":"a"}
			header = {"Content-Type": 'application/x-www-form-urlencoded',
					'Authorization': "Token " + request.session["token"]}
			post_data = {'carreer': session.carreer}
			response = requests.post(
					'http://supreme.webtic.cl/get/Carreer_Data/', headers=header, data=post_data)
			content = json.loads(response.content)
			str_f = content["nombre"]
			for key in dic:
				if key in str_f:
					str_f = str_f.replace(key,dic[key])
			carreer = Carreer.objects.create(name=str_f,career_pk=session.carreer)
			carreer.save()
		data['sessions'].append([session,carreer.name])
	try:
		perfil = UserProfile.objects.get(user=request.user)
		feedbacks = Feedback.objects.filter(receipt=perfil).order_by("-date")
		for feedback in feedbacks:
			feedback.read = 1
			feedback.save()
		proyectos = Project.objects.filter(guide_teacher=perfil, status='EC')
		contador_pre = 0
		contador_adv = 0
		for proyecto in proyectos:
			students = proyecto.student_set.all()
			if students[0].actual_subject[0] == "A":
				contador_adv += 1
			else:
				contador_pre += 1

		data["contador_adv"] = contador_adv			
		data["contador_pre"] = contador_pre
		if contador_adv != 0 and contador_pre != 0:
			data['barra_adv'] = int(float(contador_adv) / (contador_adv + contador_pre) * 100)
			data['barra_pre'] = int(float(contador_pre) / (contador_adv + contador_pre) * 100)
		data['total_proyectos'] = contador_adv + contador_pre
	except UserProfile.DoesNotExist:
		perfil = Student.objects.get(user=request.user)
		feedbacks = Feedback.objects.filter(receipt_student=perfil).order_by("-date")
		for feedback in feedbacks:
			feedback.read = 1
			feedback.save()

	projects = len(Project.objects.filter(status="EC").all())
	data["perfil"] = perfil
	if bool(perfil.img):
		data["foto"] = perfil.img.url
	else:
		data["foto"] = static('img/profiles/large_05.png')
	data["feedbacks"] = feedbacks
	data["projects"] = projects
	
	return render(request, template_name, data)

@login_required(login_url='/')
def upload_photo(request):
    rut = request.session["rut"]
    upload_photo = {}
    template_name = "perfil.html"
    if request.POST:
        photo = request.FILES['foto']
        user_data = UserProfile.objects.get(rut=rut)
        user_data.img = photo
        upload_photo['img'] = user_data.img.url
        user_data.save()
        return HttpResponse(json.dumps(upload_photo), content_type='application/json')
    return render(request, template_name, {})

@login_required(login_url='/')
def postulaciones(request):
    data = {}
    perfil = UserProfile.objects.get(user=request.user)
    if request.session["type"] == "DC" or request.session["type"] == "SA":
		postulations = Postulation.objects.all()
		teacher_postulations = Teacher_Postulation.objects.all()
		data["teacher_postulations"] = teacher_postulations
    else:
		postulations = Postulation.objects.filter(project__guide_teacher=perfil)
		teacher_postulations = Teacher_Postulation.objects.filter(teacher=perfil).exclude(author=perfil)
		data["teacher_postulations"] = teacher_postulations

    data["postulations"] = postulations
    template_name = "postulaciones.html"
    return render(request, template_name, data)

@login_required(login_url='/')
def rubric_details(request,pk):
	template_name = 'rubric_details.html'
	rubric_obj = get_object_or_404(Rubric, pk=int(pk))
	sessions = Session.objects.filter(rubric=rubric_obj)
	
	return render(request, template_name, {'sessions':sessions,
											'rubric':rubric_obj})
@login_required(login_url='/')
def upload_date(request):
	date = request.POST['date']
	carreer_pk = request.POST['carrera']
	period = request.POST['period']
	if not Session_start.objects.filter(carreer=int(carreer_pk),period=period).exists():
		if Carreer.objects.filter(career_pk=int(carreer_pk)).exists():
			carreer = Carreer.objects.get(career_pk=carreer_pk)
		else:
			dic = {"\u00ed":"i","\u00f3":"o","\u00e1":"a"}

			header = {"Content-Type": 'application/x-www-form-urlencoded',
					'Authorization': "Token " + request.session["token"]}
			post_data = {'carreer': carreer_pk}
			response = requests.post(
					'http://supreme.webtic.cl/get/Carreer_Data/', headers=header, data=post_data)
			content = json.loads(response.content)
			str_f = content["nombre"]
			n_carreer = Carreer.objects.create(name=str_f,career_pk=carreer_pk)
			n_carreer.save()

		n_session_start = Session_start.objects.create(carreer=carreer_pk,beginning=date,period=period)
		n_session_start.save()
		response = {"mensaje":"Exito"}
		return HttpResponse(
			json.dumps(response),
			content_type="application/json"
			)
	else:
		return JsonResponse({'mensaje':"Error, Ya existe un inicio registrado en ese periodo."})

@login_required(login_url='/')
def delete_evaluation(request):
    response = {}
    evaluation = get_object_or_404(Evaluation, pk=request.POST['evaluation_pk'])
    week_obj = evaluation.week
    response['week_pk'] = evaluation.week.pk
    response['type'] = evaluation.type_evaluation
    if response['type'] == "OB":
        week_obj.total_objetive_percentage -= evaluation.percentage
        response['percentage'] = week_obj.total_objetive_percentage
    elif response['type'] == "AV":
        week_obj.total_progress_percentage -= evaluation.percentage
        response['percentage'] = week_obj.total_progress_percentage
    elif response['type'] == "AT":
        week_obj.total_activity_percentage -= evaluation.percentage
        response['percentage'] = week_obj.total_activity_percentage
    evaluation.delete()
    week_obj.save()
    response['evaluation_pk'] = request.POST['evaluation_pk']
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

@login_required(login_url='/')
def delete_item(request):
    response = {}
    if request.POST['option'] == "technique":
        technique = get_object_or_404(Technique, pk=request.POST['pk'])
        technique.hito.total_technique_percentage -= technique.percentage
        technique.hito.save()
        response['total_percentage'] = technique.hito.total_technique_percentage
        response['option'] = "TE"
        response['hito_pk'] = technique.hito.pk
        technique.delete()
    elif request.POST['option'] == "memory":
        memory = get_object_or_404(Memory, pk=request.POST['pk'])
        memory.hito.total_memory_percentage -= memory.percentage
        memory.hito.save()
        response['total_percentage'] = memory.hito.total_memory_percentage
        response['option'] = "ME"
        response['hito_pk'] = memory.hito.pk
        memory.delete()
    elif request.POST['option'] == "presentation":
        subtopic = get_object_or_404(SubTopic, pk=request.POST['pk'])
        subtopic.topic.hito.total_presentation_percentage -= subtopic.percentage
        subtopic.delete()
        subtopic.topic.hito.save()
    return HttpResponse(
            json.dumps(response),
            content_type="application/json"
            )

@login_required(login_url='/')
def add_topic_week(request):
    week = get_object_or_404(Week, pk=request.POST['week_pk'])
    week.topic = request.POST['week_topic']
    response = {}
    response['week_pk'] = week.pk
    response['week_topic'] = week.topic
    week.save()
    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

@login_required(login_url='/')
def add_presentation_item(request):
    hito_obj = get_object_or_404(Hito, pk=request.POST['hito_pk'])
    topics = Topic.objects.filter(hito = hito_obj)
    exist_topic = 0
    response = {}
    for x in topics:
        if x.name == request.POST['topic']:
            exist_topic = x.pk
            topic_obj = get_object_or_404(Topic, pk=exist_topic)
    if exist_topic == 0:
        topic_obj = Topic.objects.create(hito=hito_obj,
                                    name=request.POST['topic'])
        topic_obj.save()
    subtopic = SubTopic.objects.create(topic = topic_obj,
                                      name=request.POST['subtopic'],
                                      percentage=int(request.POST['percentage']))
    sum_total = hito_obj.total_presentation_percentage + subtopic.percentage
    if sum_total > 100 or subtopic.percentage < 0:
        response['error'] = 0
        subtopic.delete()
        if exist_topic == 0:
            topic_obj.delete()
        return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )
    hito_obj.total_presentation_percentage = sum_total
    hito_obj.save()
    response['total_percentage'] = hito_obj.total_presentation_percentage
    response['topic_name'] = topic_obj.name
    response['topic_pk'] = topic_obj.pk
    response['hito_pk'] = request.POST['hito_pk']
    response['subtopic_percentage'] = subtopic.percentage
    response['subtopic_pk'] = subtopic.pk
    response['subtopic_name'] = subtopic.name
    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

@login_required(login_url='/')
def item_create(request):
    response = {}
    hito_obj = get_object_or_404(Hito, pk=request.POST['hito_pk'])
    if request.POST['option'] == "memoria":
        memory = Memory.objects.create(hito=hito_obj,
                                      item=request.POST['item'],
                                      percentage = int(request.POST['percentage']))
        total_sum = hito_obj.total_memory_percentage + memory.percentage
        if total_sum > 100 or total_sum < 0 :
            response['error'] = 0
            memory.delete()
            return HttpResponse(
                json.dumps(response),
                content_type="application/json"
                )
        hito_obj.total_memory_percentage = total_sum
        hito_obj.save()
        memory.save()
        response['total_percentage'] = hito_obj.total_memory_percentage
        response['item_name'] = memory.item
        response['item_pk'] = memory.pk
        response['item_percentage'] = memory.percentage
        response['option'] = "memory"

    elif request.POST['option'] == "tecnica":
        technique = Technique.objects.create(hito=hito_obj,
                                            item=request.POST['item'],
                                            percentage = int(request.POST['percentage']))
        total_sum = hito_obj.total_technique_percentage + technique.percentage
        if total_sum > 100 or total_sum < 0 :
            response['error'] = 0
            technique.delete()
            return HttpResponse(
                json.dumps(response),
                content_type="application/json"
                )
        hito_obj.total_technique_percentage = total_sum
        hito_obj.save()
        technique.save()
        response['total_percentage'] = hito_obj.total_technique_percentage
        response['item_name'] = technique.item
        response['item_pk'] = technique.pk
        response['item_percentage'] = technique.percentage
        response['option'] = "technique"

    response['hito_pk'] = hito_obj.pk
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

@login_required(login_url='/')
def evaluation_create(request):
    week_obj = get_object_or_404(Week, pk=request.POST['week_pk'])
    evaluation = Evaluation.objects.create(week=week_obj,
                                           name=request.POST['name'],
                                           percentage=int(request.POST['percentage']))
    response = {}
    if request.POST['option'] == "actividad":
        evaluation.type_evaluation = "AT"
        week_obj.total_activity_percentage += evaluation.percentage
        if week_obj.total_activity_percentage > 100:
            response['error'] = 0
            evaluation.delete()
            return HttpResponse(
                                json.dumps(response),
                                content_type="application/json"
                                )
        else:
            evaluation.save()
            response['total_percentage'] = week_obj.total_activity_percentage
            week_obj.save()
    elif request.POST['option'] == "avance":
        evaluation.type_evaluation = "AV"
        week_obj.total_progress_percentage += evaluation.percentage
        if week_obj.total_progress_percentage > 100:
            response['error'] = 0
            evaluation.delete()
            return HttpResponse(
                                json.dumps(response),
                                content_type="application/json"
                                )
        else:
            evaluation.save()
            response['total_percentage'] = week_obj.total_progress_percentage
            week_obj.save()
    elif request.POST['option'] == "objetivo":
        evaluation.type_evaluation = "OB"
        week_obj.total_objetive_percentage += evaluation.percentage
        if week_obj.total_objetive_percentage > 100 or evaluation.percentage < 0:
            response['error'] = 0
            evaluation.delete()
            return HttpResponse(
                                json.dumps(response),
                                content_type="application/json"
                                )
        else:
            evaluation.save()
            response['total_percentage'] = week_obj.total_objetive_percentage
            week_obj.save()
    response['week_pk'] = week_obj.pk
    response['evaluation_type'] = evaluation.type_evaluation
    response['evaluation_name'] = evaluation.name
    response['evaluation_pk'] = evaluation.pk
    response['evaluation_percentage'] = evaluation.percentage
    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

@login_required(login_url='/')
def rubric_delete(request, pk):
    rubric = get_object_or_404(Rubric, pk=pk)
    rubric.delete()
    return HttpResponseRedirect(reverse_lazy('rubric_list'))

@login_required(login_url='/')
def create_evaluation(request):
	response = {}
	finished_flag = True
	len_obj = request.POST['len_obj']
	len_obj = int(len_obj)
	print "Len objetivos ", len_obj 
	len_act = request.POST['len_act']
	len_act = int(len_act)
	print "Len actividades ", len_act 

	len_pro = request.POST['len_pro']
	len_pro = int(len_pro)
	print "Len pro ", len_pro 
	option = False
	project_obj = get_object_or_404(Project,pk=request.POST['project_pk'])
	session_obj = get_object_or_404(Session,pk=request.POST['session_pk'])
	project_evaluations = ProjectEvaluation.objects.filter(project=project_obj, session=session_obj)
	objetive_qualification = 0.0
	activity_qualification = 0.0
	progress_qualification = 0.0
	if len(project_evaluations) != 0:
			option = True
	if option == False:
		project_evaluation_obj = ProjectEvaluation.objects.create(
										date = datetime.datetime.now(),
										project = project_obj,
										session = session_obj,
										comment_progress = request.POST["comment_progress"],
										comment_activity = request.POST["comment_activity"],
										comment_accomplishment = request.POST["comment_accomplishment"]
										)
		for i in range(len_obj):
			evaluation_pk = "objetivos[" + str(i) + "][name]"
			evaluation_pk = request.POST[evaluation_pk]
			evaluation_pk = int(evaluation_pk)
			nota_post = "objetivos[" + str(i) + "][value]"
			if request.POST[nota_post] == "" and finished_flag == True:
				finished_flag = False
			nota_post = request.POST[nota_post] if request.POST[nota_post] != "" else "0"
			print "Esta es la nota objetivo: ", nota_post
			print "REplace: ", nota_post.replace(",",".")
			print "Nota final: ", float(nota_post.replace(",","."))
			evaluation_obj = get_object_or_404(Evaluation,pk=evaluation_pk)
			objetive_qualification = objetive_qualification + ((float(nota_post.replace(",","."))*float(evaluation_obj.percentage))/100)
			n_eval = ObjetiveEvaluation.objects.create(project_evaluation=project_evaluation_obj,
											evaluation = evaluation_obj,
											nota = float(nota_post.replace(",","."))
											)
			n_eval.save()
		project_evaluation_obj.objetive_qualification = objetive_qualification
		project_evaluation_obj.save()
		for x in range(len_act):
			evaluation_pk = "actividades[" + str(x) + "][name]"
			evaluation_pk = request.POST[evaluation_pk]
			evaluation_pk = int(evaluation_pk)
			nota_post = "actividades[" + str(x) + "][value]"
			if request.POST[nota_post] == "" and finished_flag == True:
				finished_flag = False
			nota_post = request.POST[nota_post] if request.POST[nota_post] != "" else "0"
			print "Esta es la nota actividad: ", nota_post			
			print "REplace: ", nota_post.replace(",",".")
			print "Nota final: ", float(nota_post.replace(",","."))
			evaluation_obj = get_object_or_404(Evaluation,pk=evaluation_pk)
			activity_qualification = activity_qualification + ((float(nota_post.replace(",","."))*float(evaluation_obj.percentage))/100)

			n_eval = ActivityEvaluation.objects.create(project_evaluation=project_evaluation_obj,
											evaluation = evaluation_obj,
											nota = float(nota_post.replace(",","."))
											)
			n_eval.save()
		project_evaluation_obj.activity_qualification = activity_qualification
		project_evaluation_obj.save()
		for z in range(len_pro):
			evaluation_pk = "avances[" + str(z) + "][name]"
			evaluation_pk = request.POST[evaluation_pk]
			evaluation_pk = int(evaluation_pk)
			nota_post = "avances[" + str(z) + "][value]"
			if request.POST[nota_post] == "" and finished_flag == True:
				finished_flag = False
			nota_post = request.POST[nota_post] if request.POST[nota_post] != "" else "0"

			print "Esta es la nota avances: ", nota_post			
			print "REplace: ", nota_post.replace(",",".")
			print "Nota final: ", float(nota_post.replace(",","."))
			evaluation_obj = get_object_or_404(Evaluation,pk=evaluation_pk)
			progress_qualification = progress_qualification + ((float(nota_post.replace(",","."))*float(evaluation_obj.percentage))/100)
			n_pevaluation = ProgressEvaluation.objects.create(
				project_evaluation=project_evaluation_obj,
				evaluation = evaluation_obj,
				nota = float(nota_post.replace(",","."))
				)
			n_pevaluation.save()

		project_evaluation_obj.progress_qualification = progress_qualification
		project_evaluation_obj.session_qualification = (progress_qualification + objetive_qualification +activity_qualification)/3
		project_evaluation_obj.save()
		if finished_flag:
			project_evaluation_obj.status = "FI"
			project_evaluation_obj.save()

		response["message"] = finished_flag
	else:
		response["message"] = "Error"
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def create_evaluation_hito(request):
	response = {}
	finished_flag = True
	len_mem = request.POST['len_mem']
	len_mem = int(len_mem)
	len_tech = request.POST['len_tech']
	len_tech = int(len_tech)
	len_pre = request.POST['len_pre']
	len_pre = int(len_pre)
	option = False
	project_obj = get_object_or_404(Project,pk=request.POST['project_pk'])
	session_obj = get_object_or_404(Session, pk=request.POST['session_pk'])
	project_evaluations = ProjectHitoEvaluation.objects.filter(project = project_obj, session=session_obj)
	technique_qualification = 0.0
	memory_qualification = 0.0
	presentation_qualification = 0.0
	if len(project_evaluations) != 0:
			option = True
	if option == False:
		project_evaluation_obj = ProjectHitoEvaluation.objects.create(
										date = datetime.datetime.now(),
										project = project_obj,
										session = session_obj,
										comment_progress = request.POST["comment_progress"],
										comment_activity = request.POST["comment_activity"],
										comment_accomplishment = request.POST["comment_accomplishment"]
										)
		project_evaluation_obj.save()

		for i in range(len_pre):
			evaluation_pk = "presentaciones[" + str(i) + "][name]"
			evaluation_pk = request.POST[evaluation_pk]
			evaluation_pk = int(evaluation_pk)
			nota_post = "presentaciones[" + str(i) + "][value]"
			if request.POST[nota_post] == "" and finished_flag == True:
				finished_flag = False
			nota_post = request.POST[nota_post] if request.POST[nota_post] != "" else "0"

			evaluation_obj = get_object_or_404(SubTopic,pk=evaluation_pk)
			presentation_qualification = presentation_qualification + ((float(nota_post.replace(",",".") if nota_post != "" else 0)*float(evaluation_obj.percentage))/100)
			PresentationEvaluation.objects.create(
											project_hito_evaluation=project_evaluation_obj,
											presentation = evaluation_obj,
											nota = float(nota_post.replace(",",".") if nota_post != "" else 0)
										)
		project_evaluation_obj.presentation_qualification = presentation_qualification
		project_evaluation_obj.save()
		for x in range(len_tech):
			evaluation_pk = "tecnicas[" + str(x) + "][name]"
			evaluation_pk = request.POST[evaluation_pk]
			evaluation_pk = int(evaluation_pk)
			nota_post = "tecnicas[" + str(x) + "][value]"
			if request.POST[nota_post] == "" and finished_flag == True:
				finished_flag = False
			nota_post = request.POST[nota_post] if request.POST[nota_post] != "" else "0"

			evaluation_obj = get_object_or_404(Technique,pk=evaluation_pk)
			technique_qualification = technique_qualification + ((float(nota_post.replace(",",".") if nota_post != "" else 0)*float(evaluation_obj.percentage))/100)
			TechniqueEvaluation.objects.create(project_hito_evaluation=project_evaluation_obj,
											technique = evaluation_obj,
											nota = float(nota_post.replace(",",".") if nota_post != "" else 0)
											)
		project_evaluation_obj.technique_qualification = technique_qualification
		project_evaluation_obj.save()
		for z in range(len_mem):
			evaluation_pk = "memorias[" + str(z) + "][name]"
			evaluation_pk = request.POST[evaluation_pk]
			evaluation_pk = int(evaluation_pk)
			nota_post = "memorias[" + str(z) + "][value]"
			if request.POST[nota_post] == "" and finished_flag == True:
				finished_flag = False
			nota_post = request.POST[nota_post] if request.POST[nota_post] != "" else "0"

			evaluation_obj = get_object_or_404(Memory,pk=evaluation_pk)
			memory_qualification = memory_qualification + ((float(nota_post.replace(",",".") if nota_post != "" else 0)*float(evaluation_obj.percentage))/100)
			MemoryEvaluation.objects.create(project_hito_evaluation=project_evaluation_obj,
											memory = evaluation_obj,
											nota = float(nota_post.replace(",",".") if nota_post != "" else 0)
											)
		project_evaluation_obj.memory_qualification = memory_qualification
		i = 0
		if technique_qualification != 0 :
			i += 1
		if presentation_qualification != 0 :
			i += 1
		if memory_qualification != 0 :
			i += 1
		
		project_evaluation_obj.hito_qualification = (technique_qualification + presentation_qualification + memory_qualification)/i
		project_evaluation_obj.save()
		if finished_flag:
			project_evaluation_obj.status = "FI"
			project_evaluation_obj.save()

		response["message"] = finished_flag
	else:
		response["message"] = "Error"
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def list_message(request):
    template_name = "list_message.html"
    data = {}
    if request.session["type"] == "DC" or request.session["type"] == "DO" or request.session["type"] == "SA" or request.session["type"] == "SE" or request.session["type"] == "DS" or request.session["type"] == "ME":
		perfil = UserProfile.objects.get(user=request.user)
		feedback_perfil = Feedback.objects.filter(receipt=perfil)
		for feedback in feedback_perfil:
			feedback.read = 1
			feedback.status = "ME"
			data["feedback_perfil"] = feedback_perfil		
    else:
    	perfil = Student.objects.get(user=request.user)
    	feedback_perfil = Feedback.objects.filter(receipt_student=perfil)
    	for feedback in feedback_perfil:
    		feedback.read = 1
    		feedback.status = "ME"
    		data["feedback_perfil"] = feedback_perfil

    return render(request, template_name, data)



@login_required(login_url='/')
def get_invites(request):
	receipts = []
	data = {}
	carreer_pk = request.POST['carreer_pk']
	project = Project.objects.get(pk=request.POST['projectpk'])
	cycle = "RE" if project.student_set.all()[0].actual_subject[0] != "A" else "AD"
	post_data = {
		"cycle": cycle,
		"carreer": carreer_pk
	}

	header = {"Content-Type": 'application/x-www-form-urlencoded',
			"Authorization": "Token " + request.session["token"]}
	response = requests.post(
		"http://supreme.webtic.cl/get/get_students_carreer/", headers=header, data=post_data)
	content = response.content
	content = ast.literal_eval(content)

	students = sorted(content["students"], key=itemgetter(0))

	post_data = {
		"carreer": carreer_pk
	}

	header = {"Content-Type": 'application/x-www-form-urlencoded',
				"Authorization": "Token " + request.session["token"]}

	response = requests.post(
		"http://supreme.webtic.cl/get/get_teachers_carreer/", headers=header, data=post_data)
	content = response.content
	content = ast.literal_eval(content)

	teachers = sorted(content["teachers"], key=itemgetter(0))
	for teacher in teachers:
		teacher.append("U")
		receipts.append(teacher)
	for student in students:
		student[0] = " ".join([name.capitalize() for name in student[0].split()])
		student.append("S")
		receipts.append(student)
	receipts = sorted(receipts, key=itemgetter(0))
	options = ""
	for receipt in receipts:
		if receipt[-1] == "S":
			options += "<option value='%s;%s;%s'>%s - Estudiante</option>" % (receipt[1],receipt[2],receipt[3],receipt[0])
		else:
			options += "<option value='%s;%s'>%s</option>" % (receipt[1],receipt[2],receipt[0])

	data["receipts"] = options

	return JsonResponse(data)

def show_message_modal(request):
	if request.POST:
		msg = get_object_or_404(Feedback, pk=int(request.POST['info']))
		response = {"mensaje":msg.description}
		print response
		return JsonResponse(response)

def message_Info(request):
    template_name = "mensajesEnviados.html"
    data = {}

    
    if request.session['type'] == "AL":
		perfil = Student.objects.get(user=request.user)
		feedbacks = Feedback.objects.filter(author_s=perfil,status="ME").all()
    else: 
    	perfil = UserProfile.objects.get(user=request.user)
    	feedbacks = Feedback.objects.filter(author=perfil,status="ME").all()


    data['feedback_profile'] = feedbacks
    return render(request, template_name, data)


