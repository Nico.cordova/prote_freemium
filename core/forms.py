#encoding:utf-8
from django import forms
from django.contrib.auth.models import User
from core.models import *
from localflavor.cl.forms import CLRutField
from django.utils.translation import ugettext as _
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget




class WeekForm(forms.Form):
    name = forms.CharField()
    percentage = forms.IntegerField(min_value=0,max_value=100)


class RubricForm(forms.Form):
    title = forms.CharField(label="Título")
    carreer = forms.IntegerField(label="Carrera")
    description = forms.CharField(label="Descripción",widget=forms.Textarea)
    cantidadsemanas = forms.IntegerField(label="Cantidad de Sesiones",min_value=0, max_value=20)
