""" Context proccesor for dashboard app """
from django.contrib.auth.models import User
from core.models import Session_start,UserProfile,Student,Session_start
from projects.models import Feedback
import datetime
def global_data(request):
	try:
		admin=User.objects.get(username="ADMIN")
	except User.DoesNotExist:
		admin=User.objects.create_user(username="ADMIN",password="valinor2")
		admin.save()

	data = {}
	admin = False
	if request.user.is_authenticated :
		current_sessions = {}
		for session in Session_start.objects.all():
			if session.carreer not in map(int,current_sessions.keys()):
				time_diff = datetime.date.today() - session.beginning
				time_diff = time_diff.total_seconds()
				sesion_actual = int(time_diff/60/60/24/7)
				current_sessions["%d"%(session.carreer)] = sesion_actual 

		project = "False"
		try:
			user_data = UserProfile.objects.get(user=request.user)
			unread_feedback = Feedback.objects.filter(receipt=user_data).filter(read=0)
			unread_feedback = unread_feedback.all()[:5]
			proyects = len(user_data.guide_teacher.all())
			data['max_proyects'] = proyects >= 15
			if user_data.user_type == "AD":
				admin = True

		except UserProfile.DoesNotExist:
			try:
				user_data = Student.objects.get(user=request.user)
				if user_data.project != None:
					project = user_data.project.pk
				else:
					project = "False"

				data['project_active'] = project
				unread_feedback = Feedback.objects.filter(receipt_student=user_data).filter(read=0)
				unread_feedback = unread_feedback.all()[:5]
			except:
				return data
		data["unread_feedback"] = unread_feedback
		data['admin'] = admin
		data['current_sessions'] = current_sessions
		data["photo"] = user_data.img
	return data	

