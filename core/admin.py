from django.contrib import admin
from core.models import *
# Register your models here.
admin.site.register(UserProfile)
admin.site.register(Student)
admin.site.register(Session)
admin.site.register(ActivityEvaluation)
admin.site.register(Evaluation)
admin.site.register(Hito)
admin.site.register(Session_start)
admin.site.register(Carreer)
admin.site.register(Rubric)
admin.site.register(Week)
admin.site.register(Subject)

@admin.register(ProjectEvaluation)
class ProjectEvaluationAdmin(admin.ModelAdmin):
    list_display = ('date', 'project', 'session', 'objetive_qualification','activity_qualification','progress_qualification','session_qualification')

@admin.register(ProjectHitoEvaluation)
class ProjectHitoEvaluationAdmin(admin.ModelAdmin):
    list_display = ('date', 'project', 'session', 'technique_qualification','presentation_qualification','memory_qualification','hito_qualification')