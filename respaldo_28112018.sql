-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: proteN
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add permission',3,'add_permission'),(8,'Can change permission',3,'change_permission'),(9,'Can delete permission',3,'delete_permission'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add carreer',7,'add_carreer'),(20,'Can change carreer',7,'change_carreer'),(21,'Can delete carreer',7,'delete_carreer'),(22,'Can add session_start',8,'add_session_start'),(23,'Can change session_start',8,'change_session_start'),(24,'Can delete session_start',8,'delete_session_start'),(25,'Can add subject',9,'add_subject'),(26,'Can change subject',9,'change_subject'),(27,'Can delete subject',9,'delete_subject'),(28,'Can add project evaluation',10,'add_projectevaluation'),(29,'Can change project evaluation',10,'change_projectevaluation'),(30,'Can delete project evaluation',10,'delete_projectevaluation'),(31,'Can add activity evaluation',11,'add_activityevaluation'),(32,'Can change activity evaluation',11,'change_activityevaluation'),(33,'Can delete activity evaluation',11,'delete_activityevaluation'),(34,'Can add memory evaluation',12,'add_memoryevaluation'),(35,'Can change memory evaluation',12,'change_memoryevaluation'),(36,'Can delete memory evaluation',12,'delete_memoryevaluation'),(37,'Can add sub topic',13,'add_subtopic'),(38,'Can change sub topic',13,'change_subtopic'),(39,'Can delete sub topic',13,'delete_subtopic'),(40,'Can add presentation evaluation',14,'add_presentationevaluation'),(41,'Can change presentation evaluation',14,'change_presentationevaluation'),(42,'Can delete presentation evaluation',14,'delete_presentationevaluation'),(43,'Can add technique',15,'add_technique'),(44,'Can change technique',15,'change_technique'),(45,'Can delete technique',15,'delete_technique'),(46,'Can add progress evaluation',16,'add_progressevaluation'),(47,'Can change progress evaluation',16,'change_progressevaluation'),(48,'Can delete progress evaluation',16,'delete_progressevaluation'),(49,'Can add rubric',17,'add_rubric'),(50,'Can change rubric',17,'change_rubric'),(51,'Can delete rubric',17,'delete_rubric'),(52,'Can add objetive evaluation',18,'add_objetiveevaluation'),(53,'Can change objetive evaluation',18,'change_objetiveevaluation'),(54,'Can delete objetive evaluation',18,'delete_objetiveevaluation'),(55,'Can add project hito evaluation',19,'add_projecthitoevaluation'),(56,'Can change project hito evaluation',19,'change_projecthitoevaluation'),(57,'Can delete project hito evaluation',19,'delete_projecthitoevaluation'),(58,'Can add memory',20,'add_memory'),(59,'Can change memory',20,'change_memory'),(60,'Can delete memory',20,'delete_memory'),(61,'Can add evaluation',21,'add_evaluation'),(62,'Can change evaluation',21,'change_evaluation'),(63,'Can delete evaluation',21,'delete_evaluation'),(64,'Can add week',22,'add_week'),(65,'Can change week',22,'change_week'),(66,'Can delete week',22,'delete_week'),(67,'Can add hito',23,'add_hito'),(68,'Can change hito',23,'change_hito'),(69,'Can delete hito',23,'delete_hito'),(70,'Can add user profile',24,'add_userprofile'),(71,'Can change user profile',24,'change_userprofile'),(72,'Can delete user profile',24,'delete_userprofile'),(73,'Can add student',25,'add_student'),(74,'Can change student',25,'change_student'),(75,'Can delete student',25,'delete_student'),(76,'Can add technique evaluation',26,'add_techniqueevaluation'),(77,'Can change technique evaluation',26,'change_techniqueevaluation'),(78,'Can delete technique evaluation',26,'delete_techniqueevaluation'),(79,'Can add topic',27,'add_topic'),(80,'Can change topic',27,'change_topic'),(81,'Can delete topic',27,'delete_topic'),(82,'Can add session',28,'add_session'),(83,'Can change session',28,'change_session'),(84,'Can delete session',28,'delete_session'),(85,'Can add outcome',29,'add_outcome'),(86,'Can change outcome',29,'change_outcome'),(87,'Can delete outcome',29,'delete_outcome'),(88,'Can add minuta',30,'add_minuta'),(89,'Can change minuta',30,'change_minuta'),(90,'Can delete minuta',30,'delete_minuta'),(91,'Can add abet evaluation',31,'add_abetevaluation'),(92,'Can change abet evaluation',31,'change_abetevaluation'),(93,'Can delete abet evaluation',31,'delete_abetevaluation'),(94,'Can add feedback',32,'add_feedback'),(95,'Can change feedback',32,'change_feedback'),(96,'Can delete feedback',32,'delete_feedback'),(97,'Can add outcome evaluation',33,'add_outcomeevaluation'),(98,'Can change outcome evaluation',33,'change_outcomeevaluation'),(99,'Can delete outcome evaluation',33,'delete_outcomeevaluation'),(100,'Can add criteria evaluation',34,'add_criteriaevaluation'),(101,'Can change criteria evaluation',34,'change_criteriaevaluation'),(102,'Can delete criteria evaluation',34,'delete_criteriaevaluation'),(103,'Can add abet',35,'add_abet'),(104,'Can change abet',35,'change_abet'),(105,'Can delete abet',35,'delete_abet'),(106,'Can add teacher_ postulation',36,'add_teacher_postulation'),(107,'Can change teacher_ postulation',36,'change_teacher_postulation'),(108,'Can delete teacher_ postulation',36,'delete_teacher_postulation'),(109,'Can add crit_info',37,'add_crit_info'),(110,'Can change crit_info',37,'change_crit_info'),(111,'Can delete crit_info',37,'delete_crit_info'),(112,'Can add project',38,'add_project'),(113,'Can change project',38,'change_project'),(114,'Can delete project',38,'delete_project'),(115,'Can add postulation',39,'add_postulation'),(116,'Can change postulation',39,'change_postulation'),(117,'Can delete postulation',39,'delete_postulation'),(118,'Can add minuta validation',40,'add_minutavalidation'),(119,'Can change minuta validation',40,'change_minutavalidation'),(120,'Can delete minuta validation',40,'delete_minutavalidation'),(121,'Can add abet rubric',41,'add_abetrubric'),(122,'Can change abet rubric',41,'change_abetrubric'),(123,'Can delete abet rubric',41,'delete_abetrubric'),(124,'Can add criteria',42,'add_criteria'),(125,'Can change criteria',42,'change_criteria'),(126,'Can delete criteria',42,'delete_criteria'),(127,'Can add tag',43,'add_tag'),(128,'Can change tag',43,'change_tag'),(129,'Can delete tag',43,'delete_tag'),(130,'Can add glosa',44,'add_glosa'),(131,'Can change glosa',44,'change_glosa'),(132,'Can delete glosa',44,'delete_glosa'),(133,'Can add task',45,'add_task'),(134,'Can change task',45,'change_task'),(135,'Can delete task',45,'delete_task'),(136,'Can add justificativo',46,'add_justificativo'),(137,'Can change justificativo',46,'change_justificativo'),(138,'Can delete justificativo',46,'delete_justificativo'),(139,'Can add publication',47,'add_publication'),(140,'Can change publication',47,'change_publication'),(141,'Can delete publication',47,'delete_publication'),(142,'Can add solicitud_ practica',48,'add_solicitud_practica'),(143,'Can change solicitud_ practica',48,'change_solicitud_practica'),(144,'Can delete solicitud_ practica',48,'delete_solicitud_practica'),(145,'Can add ausencia',49,'add_ausencia'),(146,'Can change ausencia',49,'change_ausencia'),(147,'Can delete ausencia',49,'delete_ausencia'),(148,'Can add empresa',50,'add_empresa'),(149,'Can change empresa',50,'change_empresa'),(150,'Can delete empresa',50,'delete_empresa'),(151,'Can add practica',51,'add_practica'),(152,'Can change practica',51,'change_practica'),(153,'Can delete practica',51,'delete_practica'),(154,'Can add week_ practica',52,'add_week_practica'),(155,'Can change week_ practica',52,'change_week_practica'),(156,'Can delete week_ practica',52,'delete_week_practica'),(157,'Can add retro',53,'add_retro'),(158,'Can change retro',53,'change_retro'),(159,'Can delete retro',53,'delete_retro'),(160,'Can add respuesta solicitud',54,'add_respuestasolicitud'),(161,'Can change respuesta solicitud',54,'change_respuestasolicitud'),(162,'Can delete respuesta solicitud',54,'delete_respuestasolicitud'),(163,'Can add comments',55,'add_comments'),(164,'Can change comments',55,'change_comments'),(165,'Can delete comments',55,'delete_comments'),(166,'Can add minuta retro',56,'add_minutaretro'),(167,'Can change minuta retro',56,'change_minutaretro'),(168,'Can delete minuta retro',56,'delete_minutaretro'),(169,'Can add obs_director',57,'add_obs_director'),(170,'Can change obs_director',57,'change_obs_director'),(171,'Can delete obs_director',57,'delete_obs_director'),(175,'Can add eval_ final',59,'add_eval_final'),(176,'Can change eval_ final',59,'change_eval_final'),(177,'Can delete eval_ final',59,'delete_eval_final'),(181,'Can add minuta evidence',61,'add_minutaevidence'),(182,'Can change minuta evidence',61,'change_minutaevidence'),(183,'Can delete minuta evidence',61,'delete_minutaevidence'),(184,'Can add minuta comment',62,'add_minutacomment'),(185,'Can change minuta comment',62,'change_minutacomment'),(186,'Can delete minuta comment',62,'delete_minutacomment'),(187,'Can add historical minuta evidence',63,'add_historicalminutaevidence'),(188,'Can change historical minuta evidence',63,'change_historicalminutaevidence'),(189,'Can delete historical minuta evidence',63,'delete_historicalminutaevidence');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$30000$YklhLnM5QPd3$9XI+a/DCKSPZuTKq7NESriDG2vGiyZiX4xDbiqxi1O8=',NULL,0,'ADMIN','','','',0,1,'2018-06-05 19:06:53.951556'),(2,'pbkdf2_sha256$30000$Aibkd1QDh15o$v3C1MUm4YrslaZ53le6jcmrpbh2nQgx4+wkpc0sEDC4=','2018-10-29 19:54:27.673301',0,'eduardo.quiroga','Eduardo','Quiroga Aguilera','',0,1,'2018-06-05 19:07:16.585499'),(3,'pbkdf2_sha256$30000$EAnl3TIeIWcU$QkRdTOLTN7e1oc4Z3dXKk44JHuR22gkLVmk89NjwpX4=','2018-09-05 13:52:28.749244',0,'alejandro.spichiger.stuardo','Alejandro','Spichiger Stuardo','',0,1,'2018-06-05 19:09:15.603626'),(4,'pbkdf2_sha256$30000$MlMiJNYTghoH$zJJMFddMenEyOHJ0VGxI8iGtHwPcBl+uBp7tUUHV1PM=','2018-10-29 19:58:12.806324',0,'f.falconsoto',' FRANCISCO J.','FALCON SOTO','',0,1,'2018-06-05 19:09:51.418159'),(5,'pbkdf2_sha256$30000$VhtIlf7dFHvT$ua5Spl7TGa0N1xzJAJxYDtD5wC5gCe4hJLLvcB31aTI=','2018-06-25 19:38:17.540678',0,'a.mendezjimenez',' ANIBAL ESTEBAN','MENDEZ JIMENEZ','',0,1,'2018-06-05 19:10:28.187106'),(6,'pbkdf2_sha256$30000$1XIpDkCrwTnx$R52AZ07+fROi9n0Lhbq+WytNm5wPrSwR7IKpw/syIlU=','2018-11-29 00:22:48.699619',1,'root','','','a@a.cl',1,1,'2018-06-05 19:41:50.955445'),(7,'pbkdf2_sha256$30000$Qrdb7lw1hgL8$rL/72MHiDxWaQIBG8gEUTUtl2XTVYuOpwMOMunvGG4o=','2018-08-23 18:19:20.390153',0,'rodrigo.caballero.vivanco','Rodrigo','Caballero Vivanco','',0,1,'2018-06-07 20:06:54.041574'),(9,'pbkdf2_sha256$30000$J0N66T6sIQqa$OtxBPVXWiFotTj1QsKZpKqe/KVSiVihu7Nhja0jBjNs=','2018-07-12 20:57:23.977885',0,'yonn.escalona.medina','Yonnys','Escalona Medina','',0,1,'2018-06-11 14:43:39.454863'),(10,'pbkdf2_sha256$30000$CT3zPQHhAp52$2ZNVoBVMEf/pOXdrl6UhLOSeC5dWfMK5WT46JHAzNGM=','2018-07-13 16:01:07.833267',0,'edgardo.fuentes.caceres','Edgardo','Fuentes Cáceres','',0,1,'2018-06-11 14:55:42.460086'),(11,'pbkdf2_sha256$30000$Z9mPhk4NfF02$M3hx92U6RC6UUIWdysNljK6wSeTEQQ3mcj8bM905ai8=','2018-07-03 21:04:43.656834',0,'edg.fuentesc','Edgardo','Fuentes','',0,1,'2018-07-03 14:53:48.354250'),(12,'pbkdf2_sha256$30000$Z6ODgAY16xNX$u+ed12XiDbknwe0oM9wrYwSCN8XLfEoKwFWSnETsVzU=','2018-07-13 20:36:39.959984',0,'a.cocasilva',' ADRIANA M.','COCA SILVA','',0,1,'2018-07-10 18:50:39.283239'),(13,'pbkdf2_sha256$30000$gp74s9RVo4nh$G5sxfYervck5/now4pqK0ec83s5UNshACL13z9FFqvw=','2018-07-11 15:02:42.225830',0,'j.gomezparga',' JORGE I.','GOMEZ PARGA','',0,1,'2018-07-11 15:02:42.152791'),(14,'pbkdf2_sha256$30000$azKu06K7flbb$Y43gR3PRg9I33bTAaaSHto9DAuUTZr6DqKpzhYx/6ak=','2018-07-11 15:03:32.321503',0,'J.villalobosjara',' JUAN C.','VILLALOBOS JARA','',0,1,'2018-07-11 15:03:32.245527'),(15,'pbkdf2_sha256$30000$8fRxV5oafzV9$krAEWD4sxklEDMFQTDJBIpDAnzXv0F6Yws86e1wAcdY=','2018-07-11 15:04:04.781440',0,'k.espinozapavez',' KATHERINE L.','ESPINOZA PAVEZ','',0,1,'2018-07-11 15:04:04.706322'),(16,'pbkdf2_sha256$30000$Joq5plWPSEVu$raX+99JVVxN9UwMh92UPe63z8KF/4DzTBrnVdzQyTGk=','2018-07-12 20:17:31.460705',0,'j.arredondoparedes',' JUAN R.','ARREDONDO PAREDES','',0,1,'2018-07-12 20:17:31.382997'),(17,'pbkdf2_sha256$30000$1NBcS7LO1im0$dTRdBUDwzA3ehWdPMCwaaoL90cp0hW/lx+xfcD88LeA=','2018-07-13 15:45:35.888937',0,'j.montanaressantis',' JOSE D.','MONTANARES SANTIS','',0,1,'2018-07-12 20:19:02.048241'),(18,'pbkdf2_sha256$30000$bofR3ebnFoTR$zR1yiOFAHgdT+vPSK92ACYY+J4YJoRoQxO/TXsaDJfw=','2018-08-06 23:25:08.237756',0,'carlos.neira.carrasco','Carlos','Neira Carrasco','',0,1,'2018-07-12 20:19:33.987227'),(19,'pbkdf2_sha256$30000$tzwZ7jekxiSQ$+yJDqEhavLToV0B8m85CvMODyzPWowEunG0S3TvGQes=','2018-07-12 20:58:50.610226',0,'j.muozcortes',' JULIO A.','MUNOZ CORTES','',0,1,'2018-07-12 20:58:50.536316'),(20,'pbkdf2_sha256$30000$f2hk7VjAIodK$1TStZExplFwGHqbZ6/PRtcu/7eppPlbsI6tmcCn3Iho=','2018-07-13 14:41:15.477812',0,'j.pozosaavedra',' JOSE L.','POZO SAAVEDRA','',0,1,'2018-07-13 14:41:15.400826'),(21,'pbkdf2_sha256$30000$RnqMrLsMVNJb$jOByrRNaKir/76DzuRXf6xJJQJOQSXY7mawa9kjTWlE=','2018-07-13 14:41:30.187094',0,'j.venegaszurita',' JUAN C.','VENEGAS ZURITA','',0,1,'2018-07-13 14:41:30.112838'),(22,'pbkdf2_sha256$30000$w874ZZxileS1$LIzZfNe1LmWYW9vrGbrN0qNTAIWeVEFSpvfz/090+I0=','2018-08-23 18:03:50.973441',0,'ingeniero.quiroga','Rodrigo','Donoso ','',0,1,'2018-07-13 15:55:56.874127'),(23,'pbkdf2_sha256$30000$11uvFJTi353U$eIbR7w43fE5lto3CrrGsm3aQpBOUsKBuH96WgFczvaI=','2018-07-18 20:50:30.984956',0,'f.olivaresbarros',' FERNANDO A.','OLIVARES BARROS','',0,1,'2018-07-18 20:50:30.909500'),(24,'pbkdf2_sha256$30000$5DW0GZS6PBun$DdE22u28ZBHVUPVqbHfa/pgWYGJlmLN5k3K4aRaBvKs=','2018-07-18 20:52:54.531835',0,'d.espinozamaldonado',' DIEGO I.','ESPINOZA MALDONADO','',0,1,'2018-07-18 20:51:35.561427'),(25,'pbkdf2_sha256$30000$XiF8i8UNAJNn$umuOdhOXeJMb0jZsl+vNavvawbRbaxzmQ8RcIepP5CM=','2018-10-29 19:52:59.588823',0,'hvidal','Haydee','Vidal Zambrano','',0,1,'2018-08-01 20:21:20.590007'),(26,'pbkdf2_sha256$30000$lt1iF503iJ99$mdrN+Sz4JlMoLFCXp8abb5MNYL6A9+0uC9uq6CXZ76Q=','2018-10-29 20:00:49.728761',0,'alicia.abarca','Alicia','Abarca','',0,1,'2018-08-01 20:22:55.791965');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_activityevaluation`
--

DROP TABLE IF EXISTS `core_activityevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_activityevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `evaluation_id` int(11) DEFAULT NULL,
  `project_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_activityevalua_evaluation_id_73521ef4_fk_core_evaluation_id` (`evaluation_id`),
  KEY `core_project_evaluation_id_5d10c534_fk_core_projectevaluation_id` (`project_evaluation_id`),
  CONSTRAINT `core_activityevalua_evaluation_id_73521ef4_fk_core_evaluation_id` FOREIGN KEY (`evaluation_id`) REFERENCES `core_evaluation` (`id`),
  CONSTRAINT `core_project_evaluation_id_5d10c534_fk_core_projectevaluation_id` FOREIGN KEY (`project_evaluation_id`) REFERENCES `core_projectevaluation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_activityevaluation`
--

LOCK TABLES `core_activityevaluation` WRITE;
/*!40000 ALTER TABLE `core_activityevaluation` DISABLE KEYS */;
INSERT INTO `core_activityevaluation` VALUES (1,5,39,1),(2,4,40,1),(3,5,41,1),(4,5,42,1),(5,40,44,2),(6,40,45,2),(7,0,46,2),(8,70,51,3),(9,20,52,3),(10,40,53,3),(11,40,58,4),(12,1,59,4),(13,60,68,5),(14,60,69,5),(15,4,76,6),(16,1,131,7),(17,1,132,7),(18,1,133,7),(19,1,139,8),(20,1,151,8),(21,1,147,9),(22,1,159,10),(23,1,167,11),(24,1,168,11),(25,1,194,12),(26,1,196,12),(27,1,211,15),(28,1,212,15),(29,1,218,16),(30,1,219,17),(31,1,221,17),(32,1,167,18),(33,1,168,18),(34,1,194,19),(35,1,196,19),(36,1,211,22),(37,1,212,22),(38,1,218,23),(39,1,219,24),(40,1,221,24),(41,1,94,26),(42,1,95,26),(43,1,110,27),(44,4,39,28),(45,4,40,28),(46,4,41,28),(47,4,42,28),(48,4,44,29),(49,4,45,29),(50,4,46,29),(51,5,39,30),(52,5,40,30),(53,5,41,30),(54,5,42,30),(55,4,44,31),(56,4,45,31),(57,4,46,31),(58,4,51,32),(59,4,52,32),(60,4,53,32),(61,5,58,33),(62,5,59,33),(63,4,68,34),(64,4,69,34),(65,5,76,35),(66,5,82,36),(67,5,88,37);
/*!40000 ALTER TABLE `core_activityevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_carreer`
--

DROP TABLE IF EXISTS `core_carreer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_carreer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `career_pk` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_carreer`
--

LOCK TABLES `core_carreer` WRITE;
/*!40000 ALTER TABLE `core_carreer` DISABLE KEYS */;
INSERT INTO `core_carreer` VALUES (1,1,'Ingeniería en Computación e Informática');
/*!40000 ALTER TABLE `core_carreer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_evaluation`
--

DROP TABLE IF EXISTS `core_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `type_evaluation` varchar(2) NOT NULL,
  `week_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_evaluation_week_id_4ed77828_fk_core_week_id` (`week_id`),
  CONSTRAINT `core_evaluation_week_id_4ed77828_fk_core_week_id` FOREIGN KEY (`week_id`) REFERENCES `core_week` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=307 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_evaluation`
--

LOCK TABLES `core_evaluation` WRITE;
/*!40000 ALTER TABLE `core_evaluation` DISABLE KEYS */;
INSERT INTO `core_evaluation` VALUES (39,'Elaborar los objetivos del proyecto',0,'AT',42),(40,'Métricas de los objetivos del proyecto',0,'AT',42),(41,'Los N porqués',0,'AT',42),(42,'Planificación al hito 1 que mitiga riesgo técnico ya identificado - Selección metodología desarrollo',0,'AT',42),(43,'Identificar la situación actual, sus problemas informáticos y describirlos claramente, utilizando técnicas y herramientas de la Ingeniería',0,'OB',42),(44,'Alternativas de solución y para la seleccionada implementación',0,'AT',43),(45,'Chequeo Ambiente de desarrollo',0,'AT',43),(46,'Mitigación riesgo técnico',0,'AT',43),(47,'Avance mitigación',40,'AV',43),(48,'cumplimiento compromisos pactados con profesor',30,'AV',43),(49,'Actividades prácticas',30,'AV',43),(50,'Formular una solución de ingeniería que dé respuesta a la problemática o necesidades definidas, justificando su elección mediante el estudio de diferentes alternativas (justificar la necesidad de un proyecto de Software desde la perspectiva del negocio del cliente).',0,'OB',43),(51,'Alternativas de solución y para la seleccionada implementación',0,'AT',44),(52,'Chequeo Ambiente de desarrollo',0,'AT',44),(53,'Mitigación riesgo técnico',0,'AT',44),(54,'Avance mitigación',40,'AV',44),(55,'cumplimiento compromisos pactados con profesor',30,'AV',44),(56,'Actividades prácticas',30,'AV',44),(57,'Formular una solución de ingeniería que dé respuesta a la problemática o necesidades definidas, justificando su elección mediante el estudio de diferentes alternativas (justificar la necesidad de un proyecto de Software desde la perspectiva del negocio del cliente).',0,'OB',44),(58,'Objetivos a requisitos de negocio, de requisitos de negocio a diseño, de diseño a código, de código  a pruebas ',0,'AT',45),(59,'Avance técnico acorde a planificación  y gestión de riesgos',0,'AT',45),(60,'Avance producto',60,'AV',45),(61,'Cumplimiento compromisos pactados con profesor',20,'AV',45),(62,'Actividades prácticas',20,'AV',45),(63,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado. ',0,'OB',45),(64,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',45),(68,'Elaborar al detalle documento de requisitos',0,'AT',46),(69,'Planificar alcance y tiempo',0,'AT',46),(70,'Avance producto',50,'AV',46),(71,'Cumplimiento compromisos pactados con profesor',30,'AV',46),(73,'Actividades prácticas',20,'AV',46),(74,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',46),(75,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',46),(76,'Planificando costos, calidad y comunicaciones',0,'AT',47),(77,'Avance producto',50,'AV',47),(78,'cumplimiento compromisos pactados con profesor',30,'AV',47),(79,'Actividades prácticas',20,'AV',47),(80,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',47),(81,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',47),(82,'Planificando riesgos, adquisiciones y recursos',0,'AT',48),(83,'Avance producto',50,'AV',48),(84,'Cumplimiento compromisos pactados con profesor',30,'AV',48),(85,'Actividades prácticas',20,'AV',48),(86,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',48),(87,'Administrar el proyecto acorde a una metodología de gestión de proyectos',0,'OB',48),(88,'Actualización del plan de dirección de proyecto',0,'AT',49),(89,'Avance producto',50,'AV',49),(90,'Cumplimiento compromisos pactados con profesor',30,'AV',49),(91,'Actividades prácticas',20,'AV',49),(92,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',49),(93,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',49),(94,'Problemática y objetivos versus necesidades',0,'AT',50),(95,'Trazabilidad objetivos a requisitos de negocio, de requisitos de negocio a diseño, de diseño a código, de código a pruebas  Avance técnico acorde a planificación y gestión de riesgos	',0,'AT',50),(96,'Avance producto',60,'AV',50),(97,'Cumplimiento compromisos pactados con profesor',20,'AV',50),(98,'Actividades prácticas',20,'AV',50),(99,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',50),(100,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',50),(101,'Validando factores de calidad McCall e ISO / IEC 9126 (plan de pruebas)',0,'AT',51),(102,'Refinamiento diseño detallado  Avance técnico acorde a planificación y gestión de riesgos',0,'AT',51),(103,'Avance producto',60,'AV',51),(105,'Cumplimiento compromisos pactados con profesor',20,'AV',51),(106,'Actividades prácticas',20,'AV',51),(107,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',51),(108,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',51),(109,'Seleccionar y justificar la elección de una arquitectura de Software.',0,'OB',51),(110,'Vistas arquitectónicas Avance técnico acorde a planificación y gestión de riesgos',0,'AT',52),(111,'Avance producto',60,'AV',52),(112,'Cumplimiento compromisos pactados con profesor',20,'AV',52),(113,'Actividades prácticas',20,'AV',52),(114,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',52),(115,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',52),(116,'Seleccionar y justificar la elección de una arquitectura de Software.',0,'OB',52),(117,'Fundamentos del proyecto de software Avance técnico acorde a planificación y gestión de riesgos',0,'AT',53),(118,'Avance producto',60,'AV',53),(119,'Cumplimiento compromisos pactados con profesor',20,'AV',53),(120,'Actividades prácticas',20,'AV',53),(121,'Administrar ejecución del desarrollo del producto mediante un modelo de desarrollo adecuado.',0,'OB',53),(122,'Administrar el proyecto acorde a una metodología de gestión de proyectos.',0,'OB',53),(123,'Seleccionar y justificar la elección de una arquitectura de Software.',0,'OB',53),(124,'Revisión del estado total del proyecto',0,'OB',54),(125,'Revisión total de proyecto',0,'OB',55),(126,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',58),(128,'Gestionar y controlar los cambios',0,'OB',58),(131,'Status del proyecto y demo de producto.',0,'AT',58),(132,'Entrega, revisión y aprobación plan de entregables para el semestre, considerando los tres hitos.',0,'AT',58),(133,'Revisión resultados prueba de diagnóstico',0,'AT',58),(134,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',59),(135,'Asegurar la calidad del proceso como del producto ',0,'OB',59),(137,'Gestionar y controlar los cambios',0,'OB',59),(139,'Evaluación de calidad de diseño a partir de métricas cuantitativas obtenidas de forma automática.',0,'AT',59),(140,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',59),(141,'Cumplimiento compromisos pactados con profesor',40,'AV',59),(147,'Evaluación de calidad de diseño a partir de métricas cuantitativas obtenidas de forma automática',0,'AT',60),(148,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar.',0,'OB',59),(149,'Asegurar la calidad del proceso como del producto',0,'OB',58),(150,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar.',0,'OB',58),(151,'Cálculo de métricas y análisis de desarrollo, a partir de metodología utilizada en proyecto.',0,'AT',59),(152,' Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',60),(153,'Cumplimiento compromisos pactados con profesor',20,'AV',60),(154,'Actividad evaluada',20,'AV',60),(155,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',60),(156,'Asegurar la calidad del proceso como del producto',0,'OB',60),(157,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar.',0,'OB',60),(158,'Gestionar y controlar los cambios',0,'OB',60),(159,'Construcción de reporte de pruebas asociado al hito actual, siguiendo la metodología usada en el proyecto.',0,'AT',61),(160,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',61),(161,'Cumplimiento compromisos pactados con profesor',20,'AV',61),(162,'Actividad evaluada',20,'AV',61),(163,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',61),(164,'Asegurar la calidad del proceso como del producto',0,'OB',61),(165,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',61),(166,'Gestionar y controlar los cambios',0,'OB',61),(167,'Postmortem del hito, a partir de comentarios presentación.',0,'AT',62),(168,'Propuesta de mejora en pos de hito siguiente.',0,'AT',62),(169,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',62),(170,'Cumplimiento compromisos pactados con profesor',20,'AV',62),(171,'Actividad evaluada',20,'AV',62),(172,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',62),(173,'Asegurar la calidad del proceso como del producto',0,'OB',62),(174,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar.',0,'OB',62),(175,'Gestionar y controlar los cambios',0,'OB',62),(178,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',63),(181,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',63),(182,'Asegurar la calidad del proceso como del producto',0,'OB',63),(183,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',63),(184,'Gestionar y controlar los cambios',0,'OB',63),(187,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',64),(188,'Cumplimiento compromisos pactados con profesor',40,'AV',64),(189,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',64),(190,'Asegurar la calidad del proceso como del producto',0,'OB',64),(191,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',64),(192,'Gestionar y controlar los cambios',0,'OB',64),(193,'Cumplimiento compromisos pactados con profesor',40,'AV',63),(194,'Construcción de reporte histórico de pruebas',0,'AT',63),(196,'Análisis de productividad',0,'AT',63),(197,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',65),(198,'Cumplimiento compromisos pactados con profesor',40,'AV',65),(200,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',65),(201,'Asegurar la calidad del proceso como del producto',0,'OB',65),(202,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',65),(203,'Gestionar y controlar los cambios',0,'OB',65),(204,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',66),(205,'Asegurar la calidad del proceso como del producto',0,'OB',66),(206,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',66),(207,'Gestionar y controlar los cambios',0,'OB',66),(208,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',66),(209,'Cumplimiento compromisos pactados con profesor',20,'AV',66),(210,'Actividad evaluada',20,'AV',66),(211,'Postmortem del hito, a partir de comentarios presentación',0,'AT',66),(212,'Propuesta de mejora en pos de hito siguiente',0,'AT',66),(213,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',60,'AV',67),(214,'Cumplimiento compromisos pactados con profesor',40,'AV',67),(215,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',67),(216,'Asegurar la calidad del proceso como del producto Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',67),(217,'Gestionar y controlar los cambios',0,'OB',67),(218,'Entrega draft memoria pre-empaste',0,'AT',67),(219,'Corrección y ajuste memoria completa',0,'AT',68),(220,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',80,'AV',68),(221,'Cumplimiento compromisos pactados con profesor',20,'AT',68),(222,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',68),(223,'Asegurar la calidad del proceso como del producto Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',68),(224,'Gestionar y controlar los cambios',0,'OB',68),(225,'Corrección y ajuste memoria completa',0,'AT',69),(226,'Avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',90,'AV',69),(227,'Cumplimiento compromisos pactados con profesor',10,'AV',69),(228,'Administrar y ejecutar el proyecto acorde a la planificación, replanificando justificadamente cuando corresponda',0,'OB',69),(229,'Asegurar la calidad del proceso como del producto',0,'OB',69),(230,'Construir la memoria en paralelo a la ejecución del proyecto acorde a estructura estándar',0,'OB',69),(231,'Gestionar y controlar los cambios',0,'OB',69),(232,'Revisión de status',0,'OB',70),(233,'Actividad formativa: cálculo de métricas y análisis de desarrollo, a partir de metodología utilizada en proyecto',0,'AT',71),(234,'Evaluación de calidad de diseño a partir de métricas cuantitativas obtenidas de forma automática.',0,'AT',71),(235,'Revisión de desempeño',70,'AV',71),(236,'cumplimiento compromisos pactados con profesor',30,'AV',71),(237,'Revisión de desempeño',70,'AV',72),(238,'cumplimiento compromisos pactados con profesor',30,'AV',72),(239,'Revisión de desempeño',70,'AV',73),(240,'cumplimiento compromisos pactados con profesor',30,'AV',73),(241,'Revisión de desempeño',70,'AV',74),(242,'cumplimiento compromisos pactados con profesor',30,'AV',74),(243,'Revisión de desempeño',70,'AV',75),(244,'cumplimiento compromisos pactados con profesor',30,'AV',75),(245,'Revisión de desempeño',80,'AV',76),(246,'cumplimiento compromisos pactados con profesor',20,'AV',76),(247,'avance producto de acuerdo a planificación',80,'AV',77),(248,'cumplimiento compromisos pactados con profesor',20,'AV',77),(249,'avance producto de acuerdo a planificación',90,'AV',78),(250,'cumplimiento compromisos pactados con profesor',10,'AV',78),(253,'item test 1',10,'OB',80),(254,'item test 2',10,'OB',80),(255,'item test 2',15,'OB',80),(256,'item test 3',20,'OB',80),(258,'item test 3',45,'OB',80),(259,'item test 5',40,'AV',80),(260,'dsadasasd',14,'OB',81),(261,'aaaaaaa',13,'AV',81),(262,'Definir profesor y tema',0,'AT',82),(263,'Justificar la necesidad de un proyecto de Software desde la perspectiva del negocio del cliente',0,'OB',82),(266,'Elaborar diagrama Ishikawa',50,'AT',94),(267,'Elaborar objetivos y métricas del proyecto',50,'AT',94),(268,'Justificar la necesidad de un proyecto de Software desde la perspectiva del negocio del cliente.',0,'OB',94),(269,'Selección de metodología de desarrollo de software',33,'AT',95),(270,'¿Cómo me planifico?',33,'AT',95),(271,'Elaborar resumen del proyecto',34,'AT',95),(272,'Alternativas de solución y para la seleccionada implementación',50,'AT',97),(273,'Instrumentalización de proyectos (apoyarse con Software necesario para iniciar, tales como herramientas para controlar versiones, para realizar pruebas unitarias, ambiente de desarrollo, seguimiento de cambios, entre otros)',50,'AT',97),(274,'Formular una solución de ingeniería que de respuesta a la problemática o necesidades definidas, justificando su elección mediante el estudio de diferentes alternativas.',0,'OB',97),(275,'Elaborar al detalle documento de requisitos',50,'AT',98),(276,'Planificar alcance y tiempo',50,'AT',98),(277,'Elicitar los requisitos del proyecto en base al dominio y a las necesidades del cliente.',0,'OB',98),(278,'Formalización arquitectura - Avance técnico acorde a planificación  y gestión de riesgos',50,'AT',100),(279,'pruebas unitarias y aceptación',50,'AT',100),(281,'Seleccionar y justificar la arquitectura definida si aplica.',0,'OB',100),(282,' Validando factores de calidad McCall e ISO / IEC 9126 (plan de pruebas)',50,'AT',101),(283,'refinamiento diseño detallado - Avance técnico acorde a planificación  y gestión de riesgos',50,'AT',101),(284,'Asegurar la calidad tanto del producto como de los procesos',0,'OB',101),(285,'Planificando riesgos, adquisiciones y recursos ',100,'AT',102),(286,'Dirigir, controlar y ejecutar el proyecto gestionando los riesgos.',0,'OB',102),(287,'Vistas arquitectónicas',100,'AT',104),(288,'Fundamentos del proyecto de software',100,'AT',105),(289,'evaluación de calidad de diseño a partir de métricas cuantitativas obtenidas de forma automática',100,'AT',106),(290,' avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',106),(291,'construcción de reporte histórico de pruebas. Análisis de productividad',100,'AT',107),(292,'vance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',107),(293,'construcción de reporte de pruebas asociado al hito actual, siguiendo la metodología usada en el proyecto',100,'AT',108),(294,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',108),(295,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',109),(296,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',110),(297,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',111),(298,'Revisión de status',0,'AT',115),(299,'Avance producto',100,'AV',116),(300,'Avance producto',100,'AV',117),(301,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AT',118),(302,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AT',119),(303,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',120),(304,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',121),(305,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AV',122),(306,'avance producto de acuerdo a planificación (incluye actividades de testing y control de cambios)',100,'AT',123);
/*!40000 ALTER TABLE `core_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_hito`
--

DROP TABLE IF EXISTS `core_hito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_hito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_technique_percentage` int(11) NOT NULL,
  `total_presentation_percentage` int(11) NOT NULL,
  `total_memory_percentage` int(11) NOT NULL,
  `session_id` int(11) DEFAULT NULL,
  `ponderation` int(11) NOT NULL,
  `final` int(11) NOT NULL,
  `mem_pond` int(11) NOT NULL,
  `pres_pond` int(11) NOT NULL,
  `tech_pond` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  CONSTRAINT `core_hito_session_id_0d1d89b5_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_hito`
--

LOCK TABLES `core_hito` WRITE;
/*!40000 ALTER TABLE `core_hito` DISABLE KEYS */;
INSERT INTO `core_hito` VALUES (11,100,100,100,56,0,0,33,33,33),(12,100,100,100,61,0,0,33,33,33),(13,100,100,100,67,0,0,33,33,33),(14,100,100,100,91,0,0,33,33,33),(15,100,100,100,96,0,0,33,33,33),(16,100,100,100,101,0,0,33,33,33),(17,0,0,0,106,0,0,33,33,33),(18,0,0,0,110,0,0,33,33,33),(19,0,0,0,114,0,0,33,33,33),(20,30,0,0,122,0,0,33,33,33),(21,0,0,0,129,0,0,33,33,33),(22,0,0,0,133,0,0,33,33,33),(23,0,0,0,136,0,0,33,33,33),(24,0,100,100,143,0,0,33,33,33),(25,100,80,100,147,0,0,33,33,33),(26,100,0,100,151,0,0,33,33,33),(27,100,100,100,157,0,0,33,33,33),(28,100,100,100,161,0,0,33,33,33),(29,100,95,100,165,0,0,33,33,33),(30,100,100,100,171,0,0,33,33,33),(31,100,100,100,175,0,0,33,33,33),(32,0,100,100,179,0,0,33,33,33);
/*!40000 ALTER TABLE `core_hito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_memory`
--

DROP TABLE IF EXISTS `core_memory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_memory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(300) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `hito_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_memory_hito_id_140d845b_fk_core_hito_id` (`hito_id`),
  CONSTRAINT `core_memory_hito_id_140d845b_fk_core_hito_id` FOREIGN KEY (`hito_id`) REFERENCES `core_hito` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_memory`
--

LOCK TABLES `core_memory` WRITE;
/*!40000 ALTER TABLE `core_memory` DISABLE KEYS */;
INSERT INTO `core_memory` VALUES (12,'Definición del problema',10,11),(13,'Objetivo General a corde al problema',5,11),(14,'Objetivos Específicos',10,11),(15,'Alcances Limitaciones',10,11),(16,'Alternativas de solución',15,11),(17,'Factibilidades',20,11),(18,'Diseño de alto nivel / esquema',5,11),(19,'Requerimientos (clasificar) Alto nivel',5,11),(20,'Plan de proyecto (incluir planificación otras métricas)',10,11),(21,'PostMortem',5,11),(22,'Trabajo Futuro	',5,11),(23,'Resumen e Introducción',5,12),(24,'Diseño de alto nivel / esquema',10,12),(25,'Requerimientos (clasificar) Alto nivel',15,12),(26,'Plan de proyecto (incluir planificación otras métricas)',5,12),(27,'Plan de Pruebas',5,12),(28,'Plan de Gestión de Configuración',5,12),(29,'Plan de Gestión de Cambios',5,12),(30,'Plan de Gestión de Riesgos',10,12),(31,'Ambiente de desarrollo, pruebas y producción',5,12),(32,'Análisis',10,12),(33,'Diseño detallado',10,12),(34,'Pruebas Desarrollador',5,12),(35,'Pruebas de Aceptación (al menos usabilidad)',5,12),(36,'PostMortem',3,12),(37,'Problemas Abiertos',2,12),(38,'Resumen e Introducción',10,13),(39,'Plan de proyecto (incluir planificación otras métricas)',5,13),(40,'Análisis',10,13),(41,'Diseño detallado',10,13),(42,'Arquitectura',15,13),(43,'Pruebas Desarrollador',5,13),(44,'Pruebas de Sistema (funcionales y de rendimiento)',5,13),(45,'Pruebas de Aceptación (al menos usabilidad)',5,13),(46,'Evidencias de Liberación del producto',5,13),(47,'Evidencias gestión de riesgos',5,13),(48,'Evidencias de Control de versiones',5,13),(49,'Evidencias de Control de cambios',5,13),(50,'PostMortem',5,13),(51,'Problemas Abiertos	',5,13),(52,'Trabajo Futuro	',5,13),(54,'Resumen e Introducción',5,14),(55,'Análisis',10,14),(56,'Diseño detallado',10,14),(57,'Arquitectura',10,14),(58,'Pruebas Desarrollador',7,14),(59,'Pruebas de Sistema (funcionales y de rendimiento)',7,14),(60,'Pruebas de Aceptación (al menos usabilidad)',7,14),(61,'Automatización (si corresponde) y regresión',7,14),(62,'Evidencias de Liberación del producto',5,14),(63,'Evidencias gestion de riesgos',5,14),(64,'Evidencias de Control de versiones',5,14),(65,'Evidencias de Control de cambios',5,14),(66,'PostMortem',7,14),(67,'Problemas Abiertos',5,14),(68,'Trabajo Futuro',5,14),(69,'Resumen e Introducción',5,15),(70,'Análisis',5,15),(71,'Diseño detallado',5,15),(72,'Arquitectura',5,15),(73,'Pruebas Desarrollador',5,15),(74,'Pruebas de Sistema (funcionales y de rendimiento)',10,15),(75,'Pruebas de Aceptación (al menos usabilidad)',10,15),(76,'Automatización (si corresponde) y regresión',10,15),(77,'Evidencias de Liberación del producto',5,15),(78,'Evidencias gestion de riesgos',5,15),(79,'Evidencias de Control de versiones',5,15),(80,'Evidencias de Control de cambios',5,15),(81,'PostMortem',15,15),(82,'Problemas Abiertos',5,15),(83,'Trabajo Futuro',5,15),(84,'Resumen e Introducción',5,16),(85,'Análisis',5,16),(86,'Diseño detallado',5,16),(87,'Arquitectura',5,16),(88,'Pruebas Desarrollador',5,16),(89,'Pruebas de Sistema (funcionales y de rendimiento)',10,16),(90,'Pruebas de Aceptación (al menos usabilidad)',10,16),(91,'Automatización (si corresponde) y regresión',10,16),(92,'Evidencias de Liberación del producto',5,16),(93,'Evidencias gestion de riesgos',5,16),(94,'Evidencias de Control de versiones',5,16),(95,'Evidencias de Control de cambios',5,16),(97,'PostMortem',15,16),(98,'Problemas Abiertos',5,16),(99,'Trabajo Futuro',5,16),(101,'Definición del problema',20,24),(102,'Objetivo General a corde al problema',20,24),(103,'Objetivos Específicos',20,24),(104,'Alcances Limitaciones',20,24),(105,'PostMortem',20,24),(106,'Resumen e Introducción',5,25),(107,'Alternativas de solución',10,25),(108,'Factibilidades',10,25),(109,'Diseño de alto nivel / esquema',10,25),(110,'Requerimientos (clasificar) Alto nivel',10,25),(111,'Plan de proyecto (incluir planificación otras métricas)',5,25),(112,'Plan de Pruebas',5,25),(113,'Plan de Gestión de Configuración',5,25),(114,'Plan de Gestión de Cambios',5,25),(115,'Plan de Gestión de Riesgos',5,25),(116,'Ambiente de desarrollo, pruebas y producción',5,25),(117,'Análisis',10,25),(118,'Diseño detallado',5,25),(119,'PostMortem',5,25),(120,'Trabajo Futuro',5,25),(121,'Resumen e Introducción',10,26),(122,'Análisis',10,26),(123,'Diseño detallado',10,26),(124,'Arquitectura',15,26),(125,'Pruebas Desarrollador',5,26),(126,'Pruebas de Sistema (funcionales y de rendimiento)',5,26),(127,'Pruebas de Aceptación (al menos usabilidad)',5,26),(128,'Evidencias de Liberación del producto',5,26),(129,'evidencias gestión de riesgos',5,26),(130,'Evidencias de Control de versiones',5,26),(131,'Evidencias de Control de cambios',5,26),(132,'PostMortem',5,26),(133,'Problemas Abiertos',5,26),(134,'Trabajo Futuro',5,26),(135,'Plan de proyecto (incluir planificación otras métricas)',5,26),(136,'Plan de proyecto (incluir planificación otras métricas)',5,27),(137,'Análisis',10,27),(138,'Diseño detallado',10,27),(139,'Arquitectura',15,27),(140,'Pruebas Desarrollador',5,27),(141,'Pruebas de Sistema (funcionales y de rendimiento)',5,27),(142,'Pruebas de Aceptación (al menos usabilidad)',5,27),(143,'Evidencias de Liberación del producto',5,27),(144,'evidencias gestión de riesgos',5,27),(145,'Evidencias de Control de versiones',5,27),(146,'Evidencias de Control de cambios',5,27),(147,'PostMortem',5,27),(148,'Problemas Abiertos',5,27),(149,'Trabajo Futuro',5,27),(150,'Objetivo General a corde al problema',5,27),(151,'Objetivos Específicos',5,27),(152,'Resumen e Introducción',10,28),(153,'Análisis',10,28),(154,'Diseño detallado',10,28),(155,'Arquitectura',15,28),(156,'Pruebas Desarrollador',5,28),(157,'Pruebas de Sistema (funcionales y de rendimiento)',5,28),(158,'Pruebas de Aceptación (al menos usabilidad)',5,28),(159,'Evidencias de Liberación del producto',5,28),(160,'evidencias gestión de riesgos',5,28),(161,'Evidencias de Control de versiones',5,28),(162,'Evidencias de Control de cambios',5,28),(163,'PostMortem',5,28),(164,'Problemas Abiertos',5,28),(165,'Trabajo Futuro',5,28),(166,'Plan de proyecto (incluir planificación otras métricas)',5,28),(167,'Plan de proyecto (incluir planificación otras métricas)',5,29),(168,'Análisis',10,29),(169,'Diseño detallado',10,29),(170,'Arquitectura',15,29),(171,'Pruebas Desarrollador',5,29),(172,'Pruebas de Sistema (funcionales y de rendimiento)',5,29),(173,'Pruebas de Aceptación (al menos usabilidad)',5,29),(174,'Evidencias de Liberación del producto',5,29),(175,'evidencias gestión de riesgos',5,29),(176,'Evidencias de Control de versiones',5,29),(177,'Evidencias de Control de cambios',5,29),(178,'PostMortem',5,29),(179,'Problemas Abiertos',5,29),(180,'Trabajo Futuro',5,29),(182,'Resumen e Introducción',10,29),(183,'Análisis',10,30),(184,'Diseño detallado',10,30),(185,'Arquitectura',10,30),(186,'Pruebas Desarrollador',7,30),(187,'Pruebas de Sistema (funcionales y de rendimiento)',7,30),(188,'Pruebas de Aceptación (al menos usabilidad)',7,30),(189,'Automatización (si corresponde) y regresión',7,30),(190,'Evidencias de Liberación del producto',5,30),(191,'evidencias gestión de riesgos',5,30),(192,'Evidencias de Control de versiones',5,30),(193,'Evidencias de Control de cambios',5,30),(194,'PostMortem',7,30),(195,'Problemas Abiertos',5,30),(196,'Trabajo Futuro',5,30),(197,'Resumen e Introducción',5,30),(198,'Resumen e Introducción',5,31),(199,'Análisis',5,31),(200,'Diseño detallado',5,31),(201,'Arquitectura',5,31),(202,'Pruebas Desarrollador',5,31),(203,'Pruebas de Sistema (funcionales y de rendimiento)',10,31),(204,'Pruebas de Aceptación (al menos usabilidad)',10,31),(205,'Automatización (si corresponde) y regresión',10,31),(206,'Evidencias de Liberación del producto',5,31),(207,'evidencias gestión de riesgos',5,31),(208,'Evidencias de Control de versiones',5,31),(209,'Evidencias de Control de cambios',5,31),(210,'PostMortem',15,31),(211,'Problemas Abiertos',5,31),(212,'Trabajo Futuro',5,31),(213,'Análisis',5,32),(214,'Diseño detallado',5,32),(215,'Arquitectura',5,32),(216,'Pruebas Desarrollador',5,32),(217,'Pruebas de Sistema (funcionales y de rendimiento)',10,32),(218,'Pruebas de Aceptación (al menos usabilidad)',10,32),(219,'Automatización (si corresponde) y regresión',10,32),(220,'Evidencias de Liberación del producto',5,32),(221,'evidencias gestión de riesgos',5,32),(222,'Evidencias de Control de versiones',5,32),(223,'Evidencias de Control de cambios',5,32),(224,'PostMortem',15,32),(225,'Problemas Abiertos',5,32),(226,'Trabajo Futuro',5,32),(227,'Resumen e Introducción',5,32);
/*!40000 ALTER TABLE `core_memory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_memoryevaluation`
--

DROP TABLE IF EXISTS `core_memoryevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_memoryevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `memory_id` int(11) DEFAULT NULL,
  `project_hito_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_memoryevaluation_memory_id_7274b4cc_fk_core_memory_id` (`memory_id`),
  KEY `D21a8bd94bdd99d89c1a72885b277fc6` (`project_hito_evaluation_id`),
  CONSTRAINT `D21a8bd94bdd99d89c1a72885b277fc6` FOREIGN KEY (`project_hito_evaluation_id`) REFERENCES `core_projecthitoevaluation` (`id`),
  CONSTRAINT `core_memoryevaluation_memory_id_7274b4cc_fk_core_memory_id` FOREIGN KEY (`memory_id`) REFERENCES `core_memory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_memoryevaluation`
--

LOCK TABLES `core_memoryevaluation` WRITE;
/*!40000 ALTER TABLE `core_memoryevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_memoryevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_objetiveevaluation`
--

DROP TABLE IF EXISTS `core_objetiveevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_objetiveevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `evaluation_id` int(11) DEFAULT NULL,
  `project_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_objetiveevalua_evaluation_id_e79a1fc6_fk_core_evaluation_id` (`evaluation_id`),
  KEY `core_project_evaluation_id_0c6a8656_fk_core_projectevaluation_id` (`project_evaluation_id`),
  CONSTRAINT `core_objetiveevalua_evaluation_id_e79a1fc6_fk_core_evaluation_id` FOREIGN KEY (`evaluation_id`) REFERENCES `core_evaluation` (`id`),
  CONSTRAINT `core_project_evaluation_id_0c6a8656_fk_core_projectevaluation_id` FOREIGN KEY (`project_evaluation_id`) REFERENCES `core_projectevaluation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_objetiveevaluation`
--

LOCK TABLES `core_objetiveevaluation` WRITE;
/*!40000 ALTER TABLE `core_objetiveevaluation` DISABLE KEYS */;
INSERT INTO `core_objetiveevaluation` VALUES (1,4,43,1),(2,60,50,2),(3,1,57,3),(4,40,63,4),(5,50,64,4),(6,40,74,5),(7,40,75,5),(8,40,80,6),(9,40,81,6),(10,1,126,7),(11,1,128,7),(12,1,149,7),(13,1,150,7),(14,1,134,8),(15,1,135,8),(16,1,137,8),(17,1,148,8),(18,1,155,9),(19,1,156,9),(20,1,157,9),(21,1,158,9),(22,1,163,10),(23,1,164,10),(24,1,165,10),(25,1,166,10),(26,1,172,11),(27,1,173,11),(28,1,174,11),(29,1,175,11),(30,1,181,12),(31,1,182,12),(32,1,183,12),(33,1,184,12),(34,1,189,13),(35,1,190,13),(36,1,191,13),(37,1,192,13),(38,1,200,14),(39,1,201,14),(40,1,202,14),(41,1,203,14),(42,1,204,15),(43,1,205,15),(44,1,206,15),(45,1,207,15),(46,1,215,16),(47,1,216,16),(48,1,217,16),(49,1,222,17),(50,1,223,17),(51,1,224,17),(52,1,172,18),(53,1,173,18),(54,1,174,18),(55,1,175,18),(56,1,181,19),(57,1,182,19),(58,1,183,19),(59,1,184,19),(60,1,189,20),(61,1,190,20),(62,1,191,20),(63,1,192,20),(64,1,200,21),(65,1,201,21),(66,1,202,21),(67,1,203,21),(68,1,204,22),(69,1,205,22),(70,1,206,22),(71,1,207,22),(72,1,215,23),(73,1,216,23),(74,1,217,23),(75,1,222,24),(76,1,223,24),(77,1,224,24),(78,1,99,26),(79,1,100,26),(80,1,114,27),(81,1,115,27),(82,1,116,27),(83,4,43,28),(84,4,50,29),(85,5,43,30),(86,4,50,31),(87,4,57,32),(88,5,63,33),(89,5,64,33),(90,4,74,34),(91,4,75,34),(92,5,80,35),(93,5,81,35),(94,5,86,36),(95,5,87,36),(96,5,92,37),(97,5,93,37);
/*!40000 ALTER TABLE `core_objetiveevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_presentationevaluation`
--

DROP TABLE IF EXISTS `core_presentationevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_presentationevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `presentation_id` int(11) DEFAULT NULL,
  `project_hito_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_presentationev_presentation_id_3f749e8a_fk_core_subtopic_id` (`presentation_id`),
  KEY `D0528c21d5036e2b84b5a5f7de6ca60b` (`project_hito_evaluation_id`),
  CONSTRAINT `D0528c21d5036e2b84b5a5f7de6ca60b` FOREIGN KEY (`project_hito_evaluation_id`) REFERENCES `core_projecthitoevaluation` (`id`),
  CONSTRAINT `core_presentationev_presentation_id_3f749e8a_fk_core_subtopic_id` FOREIGN KEY (`presentation_id`) REFERENCES `core_subtopic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_presentationevaluation`
--

LOCK TABLES `core_presentationevaluation` WRITE;
/*!40000 ALTER TABLE `core_presentationevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_presentationevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_progressevaluation`
--

DROP TABLE IF EXISTS `core_progressevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_progressevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `evaluation_id` int(11) DEFAULT NULL,
  `project_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_progressevalua_evaluation_id_9a5bb308_fk_core_evaluation_id` (`evaluation_id`),
  KEY `core_project_evaluation_id_5da9bba0_fk_core_projectevaluation_id` (`project_evaluation_id`),
  CONSTRAINT `core_progressevalua_evaluation_id_9a5bb308_fk_core_evaluation_id` FOREIGN KEY (`evaluation_id`) REFERENCES `core_evaluation` (`id`),
  CONSTRAINT `core_project_evaluation_id_5da9bba0_fk_core_projectevaluation_id` FOREIGN KEY (`project_evaluation_id`) REFERENCES `core_projectevaluation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_progressevaluation`
--

LOCK TABLES `core_progressevaluation` WRITE;
/*!40000 ALTER TABLE `core_progressevaluation` DISABLE KEYS */;
INSERT INTO `core_progressevaluation` VALUES (1,0,47,2),(2,70,48,2),(3,40,49,2),(4,40,54,3),(5,70,55,3),(6,50,56,3),(7,60,60,4),(8,30,61,4),(9,60,70,5),(10,50,71,5),(11,60,73,5),(12,6,77,6),(13,4,78,6),(14,4,79,6),(15,1,140,8),(16,1,141,8),(17,1,152,9),(18,1,153,9),(19,1,154,9),(20,1,160,10),(21,1,161,10),(22,1,162,10),(23,1,169,11),(24,1,170,11),(25,1,171,11),(26,1,178,12),(27,1,193,12),(28,1,187,13),(29,1,188,13),(30,1,197,14),(31,1,198,14),(32,1,208,15),(33,1,209,15),(34,1,210,15),(35,1,213,16),(36,1,214,16),(37,1,220,17),(38,1,169,18),(39,1,170,18),(40,1,171,18),(41,1,178,19),(42,1,193,19),(43,1,187,20),(44,1,188,20),(45,1,197,21),(46,1,198,21),(47,1,208,22),(48,1,209,22),(49,1,210,22),(50,1,213,23),(51,1,214,23),(52,1,220,24),(53,1,96,26),(54,1,97,26),(55,1,98,26),(56,1,111,27),(57,1,112,27),(58,1,113,27),(59,4,47,29),(60,4,48,29),(61,4,49,29),(62,4,47,31),(63,4,48,31),(64,4,49,31),(65,4,54,32),(66,4,55,32),(67,4,56,32),(68,5,60,33),(69,5,61,33),(70,55,62,33),(71,4,70,34),(72,4,71,34),(73,4,73,34),(74,5,77,35),(75,5,78,35),(76,5,79,35),(77,5,83,36),(78,5,84,36),(79,5,85,36),(80,5,89,37),(81,5,90,37),(82,5,91,37);
/*!40000 ALTER TABLE `core_progressevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_projectevaluation`
--

DROP TABLE IF EXISTS `core_projectevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_projectevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(6) NOT NULL,
  `comment_progress` longtext NOT NULL,
  `comment_activity` longtext NOT NULL,
  `comment_accomplishment` longtext NOT NULL,
  `objetive_qualification` double NOT NULL,
  `activity_qualification` double NOT NULL,
  `progress_qualification` double NOT NULL,
  `session_qualification` double NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_projectevaluatio_project_id_c1e8616b_fk_projects_project_id` (`project_id`),
  KEY `core_projectevaluation_session_id_dafbe573_fk_core_session_id` (`session_id`),
  CONSTRAINT `core_projectevaluatio_project_id_c1e8616b_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `core_projectevaluation_session_id_dafbe573_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_projectevaluation`
--

LOCK TABLES `core_projectevaluation` WRITE;
/*!40000 ALTER TABLE `core_projectevaluation` DISABLE KEYS */;
INSERT INTO `core_projectevaluation` VALUES (1,'2018-06-05 21:16:11.894074','bienvenido a su proyecto de título ','no se ha generado ninguna actividad ','no se ha generado ningún compromiso ',0,0,0,0,36,52,'BO'),(2,'2018-06-05 21:23:33.129156','','','',0,0,33,11,36,53,'BO'),(3,'2018-06-05 21:28:17.406462','va por buen camino, pero le falta las mitigaciones ','incumplimiento de ciertas areas como los son el ambiente de desarrollo y los riesgos ','comprometerse con las mitigaciones y ambiente de desarrollo',0,0,52,17.3333333333333,36,54,'BO'),(4,'2018-06-05 21:34:34.527733','se nota cierto retraso en algunas áreas claves como los riesgos y definiciones claras sobre la metodología de gestión ','no fue necesaria la revisión de actividades para esta semana, saludos cordiales ','aun no cumple con los riesgos ',0,0,0,0,36,55,'BO'),(5,'2018-06-05 21:40:19.504207','retomar el buen ritmo de semanas pasadas ','buen hacer en las actividades de esta semana ','aun no cumple con los riesgos ',0,0,57,19,36,57,'BO'),(6,'2018-06-05 21:49:38.514368','el proyecto va con buen ritmo, a pesar de trastear en diversos puntos. revíselos ','no se registran actividades esta semana ','arreglar sección bpmn están desactualizados',0,0,5,1.66666666666667,36,58,'BO'),(7,'2018-06-05 22:31:27.237002','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,0,0,52,87,'BO'),(8,'2018-06-05 22:33:32.347637','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,88,'BO'),(9,'2018-06-05 22:34:40.763994','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos. ',0,0,1,0.333333333333333,52,89,'BO'),(10,'2018-06-05 22:36:29.691563','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,90,'BO'),(11,'2018-06-05 22:46:43.890034','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,92,'BO'),(12,'2018-06-05 22:48:02.582329','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,93,'BO'),(13,'2018-06-05 22:49:31.105982','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,94,'BO'),(14,'2018-06-05 22:50:50.636590','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,95,'BO'),(15,'2018-06-05 22:52:33.816904','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,97,'BO'),(16,'2018-06-05 22:53:54.205537','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,52,98,'BO'),(17,'2018-06-05 23:01:00.427128','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0.2,0.8,0.333333333333333,52,99,'BO'),(18,'2018-06-05 23:03:52.269281','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,45,92,'BO'),(19,'2018-06-05 23:04:57.240880','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,45,93,'BO'),(20,'2018-06-05 23:07:40.766562','Alumno no registra información archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos.',0,0,1,0.333333333333333,45,94,'BO'),(21,'2018-06-05 23:10:46.729157','Alumno no registra información en los archivos adjuntos de su proyecto. ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,45,95,'BO'),(22,'2018-06-05 23:11:40.680281','Alumno no registra información en los archivos adjuntos de su proyecto.','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,45,97,'BO'),(23,'2018-06-05 23:12:39.613991','Alumno no registra información en los archivos adjuntos de su proyecto.','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,45,98,'BO'),(24,'2018-06-05 23:13:41.964276','Alumno no registra información en los archivos adjuntos de su proyecto.','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivo establecidos.',0,0.2,0.8,0.333333333333333,45,99,'BO'),(25,'2018-06-06 14:36:00.953828','Alumno no entrega información de los archivos adjuntos del proyecto.','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,0,0,43,54,'BO'),(26,'2018-06-06 14:39:58.575350','Alumno no tiene información de sus archivo adjunto del proyecto.','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,43,62,'BO'),(27,'2018-06-06 14:44:10.612196','Alumno no entrega información del proyecto en sus archivos adjuntos.','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos.',0,0,1,0.333333333333333,43,64,'BO'),(28,'2018-06-06 14:49:14.634973','Alumno entrega información del proyecto en el cual define la problemática del proyecto.','Alumno cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,0,0,43,52,'BO'),(29,'2018-06-06 15:04:29.855935','Alumno entrega información de sus objetivos específicos y la situación actual de  para el proyecto.  ','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno no cumple con sus objetivos establecidos. ',0,0,4,1.33333333333333,43,53,'BO'),(30,'2018-06-06 15:26:02.360818','Alumno entrega archivos donde establece los temas para el proyecto con sus objetivos. ','Alumno cumple con sus tareas asignadas para cada semana.','Alumno cumple con sus objetivos establecidos.',0,0,0,0,39,52,'BO'),(31,'2018-06-06 15:45:49.588319','Alumno entrega sus archivos del proyecto dando a conocer los temas a tratar y los objetivos para el proyecto proporcionando información mas detallada.','Alumno no cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,4,1.33333333333333,39,53,'BO'),(32,'2018-06-06 15:50:03.755574','Alumno entrega sus archivos del proyecto dando a conocer los temas a tratar, la metodología y el modelo de software para el proyecto, proporcionando información mas detallada.','Alumno cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,4,1.33333333333333,39,54,'BO'),(33,'2018-06-06 15:52:12.901040','Alumno entrega sus archivos del proyecto dando a conocer los temas a tratar, la metodología y el modelo de software para el proyecto, proporcionando información mas detallada.','Alumno cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,15,5,39,55,'BO'),(34,'2018-06-06 15:57:07.118095','Alumno entrega sus archivos del proyecto dando a conocer los temas a tratar, la metodología y el modelo de software y un plan de lacance para el proyecto, proporcionando información mas detallada.','Alumno cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,4,1.33333333333333,39,57,'BO'),(35,'2018-06-06 16:00:30.349405','Alumno entrega sus archivos del proyecto dando a conocer los temas a tratar, la metodología, el modelo de software y el plan de gestión de lacidad para el proyecto, proporcionando información mas detallada.','Alumno cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,5,1.66666666666667,39,58,'BO'),(36,'2018-06-06 16:17:10.627774',' Alumno entrega sus archivos del proyecto dando a conocer los temas a tratar, la metodología, el modelo de software y el plan de riegso para el proyecto, proporcionando información mas detallada.','Alumno cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,5,1.66666666666667,39,59,'BO'),(37,'2018-06-06 16:19:55.768990','Alumno entrega sus archivos del proyecto dando a conocer los temas a tratar, la metodología y el modelo de software para el proyecto, proporcionando información mas detallada para cada una de las distintas funcionalidades.','Alumno cumple con sus tareas asignadas para esta semana.','Alumno cumple con sus objetivos establecidos.',0,0,5,1.66666666666667,39,60,'BO');
/*!40000 ALTER TABLE `core_projectevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_projecthitoevaluation`
--

DROP TABLE IF EXISTS `core_projecthitoevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_projecthitoevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(6) NOT NULL,
  `technique_qualification` double NOT NULL,
  `presentation_qualification` double NOT NULL,
  `memory_qualification` double NOT NULL,
  `hito_qualification` double NOT NULL,
  `comment_progress` longtext NOT NULL,
  `comment_activity` longtext NOT NULL,
  `comment_accomplishment` longtext NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_projecthitoevalu_project_id_6e69d275_fk_projects_project_id` (`project_id`),
  KEY `core_projecthitoevaluatio_session_id_393c8bdf_fk_core_session_id` (`session_id`),
  CONSTRAINT `core_projecthitoevalu_project_id_6e69d275_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `core_projecthitoevaluatio_session_id_393c8bdf_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_projecthitoevaluation`
--

LOCK TABLES `core_projecthitoevaluation` WRITE;
/*!40000 ALTER TABLE `core_projecthitoevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_projecthitoevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_rubric`
--

DROP TABLE IF EXISTS `core_rubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_rubric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `carreer` int(11) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `desem` int(11),
  `pond_desem` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_rubric_author_id_bc332b9a_fk_auth_user_id` (`author_id`),
  CONSTRAINT `core_rubric_author_id_bc332b9a_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_rubric`
--

LOCK TABLES `core_rubric` WRITE;
/*!40000 ALTER TABLE `core_rubric` DISABLE KEYS */;
INSERT INTO `core_rubric` VALUES (4,'Proyecto de Título I - Sin ABET','2018-05-09 18:45:05.000000',1,'INS242','evaluación semana a semana','DC',2,NULL,0),(7,'Proyecto de Título - Sin ABET','2018-05-30 23:11:22.000000',1,'','Esto es una Prueba...','DC',2,NULL,0),(9,'Proyecto de Título II - Sin ABET','2018-06-02 16:37:45.000000',1,'INS244','Rúbrica de evaluación de Proyecto de Título II','DC',2,NULL,0),(10,'Proyecto de Título II - Advance - Sin ABET','2018-06-11 23:52:19.384283',1,NULL,'Rúbrica de evaluación Proyecto de Título II para el Programa Advance','DC',2,NULL,0),(12,'rubrica test','2018-06-28 19:06:16.875289',1,NULL,'una rubrica de test ','DC',2,NULL,0),(13,'Seminario Advance Sin ABET','2018-07-13 19:22:34.426754',1,NULL,'Rúbrica de evaluación para seminario de título en modalidad Advance. ','DC',2,NULL,0),(14,'Rúbrica Seminario Advance con ABET','2018-07-24 18:31:14.325662',1,'AINS2401','rúbrica de evaluación para seminario de título advance con ABET','DC',2,NULL,0),(15,'Proyecto de título I - Advance con ABET','2018-07-24 20:04:35.509192',1,'AINS2402','Proyecto de título I - Advance con ABET','DC',2,NULL,0),(16,'Proyecto de Título II - Advance - Sin ABET','2018-07-24 21:13:20.779165',1,'AINS3403','Proyecto de Título II - Advance - Sin ABET','DC',2,NULL,0);
/*!40000 ALTER TABLE `core_rubric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_session`
--

DROP TABLE IF EXISTS `core_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `abet` int(11) NOT NULL,
  `rubric_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_session_rubric_id_194d6c8a_fk_core_rubric_id` (`rubric_id`),
  CONSTRAINT `core_session_rubric_id_194d6c8a_fk_core_rubric_id` FOREIGN KEY (`rubric_id`) REFERENCES `core_rubric` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_session`
--

LOCK TABLES `core_session` WRITE;
/*!40000 ALTER TABLE `core_session` DISABLE KEYS */;
INSERT INTO `core_session` VALUES (52,1,0,4),(53,2,0,4),(54,3,0,4),(55,4,0,4),(56,5,0,4),(57,6,0,4),(58,7,0,4),(59,8,0,4),(60,9,0,4),(61,10,0,4),(62,11,0,4),(63,12,0,4),(64,13,0,4),(65,14,0,4),(66,15,0,4),(67,16,0,4),(68,17,0,4),(71,1,0,7),(87,1,0,9),(88,2,0,9),(89,3,0,9),(90,4,0,9),(91,5,0,9),(92,6,0,9),(93,7,0,9),(94,8,0,9),(95,9,0,9),(96,10,0,9),(97,11,0,9),(98,12,0,9),(99,13,0,9),(100,14,0,9),(101,15,0,9),(102,1,0,10),(103,2,0,10),(104,3,0,10),(105,4,0,10),(106,5,0,10),(107,6,0,10),(108,7,0,10),(109,8,0,10),(110,9,0,10),(111,10,0,10),(112,11,0,10),(113,12,0,10),(114,13,0,10),(120,1,0,12),(121,2,0,12),(122,3,1,12),(123,4,0,12),(124,5,0,12),(125,1,0,13),(126,2,0,13),(127,3,0,13),(128,4,0,13),(129,5,0,13),(130,6,0,13),(131,7,0,13),(132,8,0,13),(133,9,0,13),(134,10,0,13),(135,11,0,13),(136,12,0,13),(137,13,0,13),(138,14,0,13),(139,1,0,14),(140,2,0,14),(141,3,0,14),(142,4,0,14),(143,5,0,14),(144,6,0,14),(145,7,0,14),(146,8,0,14),(147,9,1,14),(148,10,0,14),(149,11,0,14),(150,12,0,14),(151,13,0,14),(152,14,0,14),(153,1,0,15),(154,2,0,15),(155,3,0,15),(156,4,0,15),(157,5,0,15),(158,6,0,15),(159,7,0,15),(160,8,0,15),(161,9,1,15),(162,10,0,15),(163,11,0,15),(164,12,0,15),(165,13,0,15),(166,14,0,15),(167,1,0,16),(168,2,0,16),(169,3,0,16),(170,4,0,16),(171,5,0,16),(172,6,0,16),(173,7,0,16),(174,8,0,16),(175,9,1,16),(176,10,0,16),(177,11,0,16),(178,12,0,16),(179,13,0,16),(180,14,1,16);
/*!40000 ALTER TABLE `core_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_session_start`
--

DROP TABLE IF EXISTS `core_session_start`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_session_start` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carreer` int(11) NOT NULL,
  `beginning` date NOT NULL,
  `last_mod` datetime(6) NOT NULL,
  `period` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_session_start`
--

LOCK TABLES `core_session_start` WRITE;
/*!40000 ALTER TABLE `core_session_start` DISABLE KEYS */;
INSERT INTO `core_session_start` VALUES (1,1,'2018-03-05','2018-06-05 21:07:50.000000','10'),(2,1,'2018-06-25','2018-07-24 20:54:48.511256','15');
/*!40000 ALTER TABLE `core_session_start` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_student`
--

DROP TABLE IF EXISTS `core_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_user` int(11) DEFAULT NULL,
  `rut` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `actual_subject` varchar(50) DEFAULT NULL,
  `img` varchar(100) NOT NULL,
  `carreers` varchar(30) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `core_student_project_id_6ca8238b_fk_projects_project_id` (`project_id`),
  CONSTRAINT `core_student_project_id_6ca8238b_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `core_student_user_id_666ccffd_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_student`
--

LOCK TABLES `core_student` WRITE;
/*!40000 ALTER TABLE `core_student` DISABLE KEYS */;
INSERT INTO `core_student` VALUES (17,1065,'18022940-K','Anibal Esteban Mendez Jimenez','INS242','Static/img/profiles/image.png','1-',54,5),(18,1034,'18626036-8','Francisco J. Falcon Soto ','INS242','../Static/img/profiles/image.png','1',35,4),(19,NULL,'19407179-5','Rodrigo A. Bustos De Las Riberas ','INS242','../Static/img/profiles/image.png','1',36,NULL),(20,NULL,'18676359-9','Carlos P. Fuentes Cortes ','INS242','../Static/img/profiles/image.png','1',38,NULL),(21,NULL,'19004709-1','Hugo E. Riquelme Valenzuela ','INS242','../Static/img/profiles/image.png','1',38,NULL),(22,548,'19668650-9','Fernando A. Olivares Barros ','INS242','../Static/img/profiles/image.png','1',39,23),(23,NULL,'16243457-8','Marco Antonio Lopez Garrido ','INS242','../Static/img/profiles/image.png','1',39,NULL),(24,NULL,'19632624-3','Ricardo I. Cesped San Martin ','INS242','../Static/img/profiles/image.png','1',40,NULL),(25,NULL,'19571033-3','Carlos G. Ramirez Cisternas ','INS242','../Static/img/profiles/image.png','1',41,NULL),(26,NULL,'18697836-6','Victor H. Vasquez Toledo ','INS242','../Static/img/profiles/image.png','1',42,NULL),(27,NULL,'19137373-1','Enzo D. Aranguiz Sanhueza ','INS242','../Static/img/profiles/image.png','1',43,NULL),(28,NULL,'18759919-9','Frances Alejandro Galleguillos Valdenegro ','INS242','../Static/img/profiles/image.png','1',44,NULL),(29,NULL,'17921245-5','Diego Ignacio Toro Mariangel ','INS244','../Static/img/profiles/image.png','1',45,NULL),(31,NULL,'18340302-8','Francisco Javier Pinto Romero ','INS244','../Static/img/profiles/image.png','1',52,NULL),(32,NULL,'16945549-K','Raul Christoffer Munoz Vega ','INS242','../Static/img/profiles/image.png','1',53,NULL),(35,NULL,'18378964-3','Felipe X. Penaloza Madrid ','INS242','../Static/img/profiles/image.png','1',57,NULL),(36,NULL,'19068311-7','Dario J. Prado Soto ','INS242','../Static/img/profiles/image.png','1',58,NULL),(37,NULL,'18457109-9','Cristian S. Ordonez Vargas ','INS242','../Static/img/profiles/image.png','1',59,NULL),(58,NULL,'18119157-0','Adrian H. Mallea Mursell ','INS244','../Static/img/profiles/image.png','1',88,NULL),(59,NULL,'10309951-K','Alberto A. Santis Jimenez ','AINS2401','../Static/img/profiles/image.png','1',88,NULL),(61,NULL,'22541657-5','Bayron Pershing Carrillo Pinares ','INS242','../Static/img/profiles/image.png','1',89,NULL),(62,NULL,'18954684-K','Carlos A. Mancilla Gonzalez ','INS242','../Static/img/profiles/image.png','1',89,NULL),(68,NULL,'18674064-5','Carlos A. Mardones Pino ','INS242','../Static/img/profiles/image.png','1',NULL,NULL),(74,102,'9974566-5','Jorge I. Gomez Parga ','AINS2401','../Static/img/profiles/image.png','1',104,13),(75,164,'16944861-2','Juan C. Villalobos Jara ','AINS2401','../Static/img/profiles/image.png','1',104,14),(76,93,'16264437-8','Katherine L. Espinoza Pavez ','AINS2401','../Static/img/profiles/image.png','1',104,15),(77,82,'22057279-K','Adriana M. Coca Silva ','AINS2401','../Static/img/profiles/image.png','1',105,12),(78,131,'15469088-3','Jose L. Pozo Saavedra ','AINS2401','../Static/img/profiles/image.png','1',105,20),(79,162,'18221761-1','Juan C. Venegas Zurita ','AINS2401','../Static/img/profiles/image.png','1',105,21),(80,NULL,'16123518-0','Patricio A. Retamales Vera ','AINS2401','../Static/img/profiles/image.png','1',107,NULL),(81,NULL,'17566410-6','Antonio A. Gavilan Carrasco ','AINS2401','../Static/img/profiles/image.png','1',108,NULL),(82,NULL,'26208827-8','Octavio J. Orozco Picon ','AINS2401','../Static/img/profiles/image.png','1',108,NULL),(83,NULL,'17663430-8','Pablo A. Villagra Macias ','AINS2401','../Static/img/profiles/image.png','1',108,NULL),(84,66,'12227499-3','Juan R. Arredondo Paredes ','AINS2401','../Static/img/profiles/image.png','1-',NULL,16),(85,117,'14108292-2','Jose D. Montanares Santis ','AINS2401','../Static/img/profiles/image.png','1-',109,17),(86,975,'15760652-2','Julio A. Munoz Cortes ','AINS2401','../Static/img/profiles/image.png','1-',NULL,19),(87,465,'19208774-0','Diego I. Espinoza Maldonado ','INS242','../Static/img/profiles/image.png','1-',112,24);
/*!40000 ALTER TABLE `core_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_subject`
--

DROP TABLE IF EXISTS `core_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(20) NOT NULL,
  `carreer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_subject`
--

LOCK TABLES `core_subject` WRITE;
/*!40000 ALTER TABLE `core_subject` DISABLE KEYS */;
INSERT INTO `core_subject` VALUES (1,'SEMINARIO DE TITULO','AINS2401',1),(2,'PROYECTO DE TITULO I','AINS2402',1),(3,'PROYECTO DE TITULO II','AINS3403',1),(4,'PROYECTO DE TITULO I','INS242',1),(5,'PROYECTO DE TITULO II','INS244',1),(6,'EXAMEN DE GRADO O TITULO','IET600',1);
/*!40000 ALTER TABLE `core_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_subtopic`
--

DROP TABLE IF EXISTS `core_subtopic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_subtopic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `topic_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_subtopic_topic_id_bd1bd532_fk_core_topic_id` (`topic_id`),
  CONSTRAINT `core_subtopic_topic_id_bd1bd532_fk_core_topic_id` FOREIGN KEY (`topic_id`) REFERENCES `core_topic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_subtopic`
--

LOCK TABLES `core_subtopic` WRITE;
/*!40000 ALTER TABLE `core_subtopic` DISABLE KEYS */;
INSERT INTO `core_subtopic` VALUES (7,'detalle del problema a abordar',30,9),(8,'Alternativas de solución',20,10),(10,'Gestión de tickets',10,12),(11,'Estadísticas',10,13),(12,'Definición de riesgos',10,14),(16,'Esquema de la solución',10,10),(17,'Planes de mitigación y contingencia',10,14),(19,'Evaluación',10,18),(20,'Alternativas de solución',5,19),(21,'Esquema de la solución',5,19),(22,'Satisface requisitos',20,20),(23,'Trazabilidad con especificaciones',10,20),(24,'Construcción del producto acorde a metodología',20,20),(25,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,20),(26,'Gestión de tickets',5,21),(27,'Estadísticas',5,21),(28,'Definición de riesgos',5,22),(29,'Evidencia de gestión de riesgos',10,22),(30,'Arquitectura de aplicación',20,23),(31,'Satisface requisitos',20,23),(32,'Trazabilidad con especificaciones',5,23),(33,'Construcción del producto acorde a metodología',20,23),(34,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',10,23),(35,'producto aceptado por cliente y postmortem acorde',10,23),(36,'Gestión de tickets',5,24),(37,'Estadísticas',5,24),(38,'Evidencia de gestión de riesgos',5,25),(39,'Problemática',30,26),(41,'Alternativas de solución',20,27),(43,'Esquema de la solución',10,27),(44,'Gestión de tickets',10,28),(45,'Estadísticas',10,28),(46,'Definición de riesgos',10,29),(47,'Planes de mitigación y contingencia',10,29),(48,'Problemática',10,30),(49,'Alternativas de solución',5,31),(50,'Esquema de la solución',5,31),(55,'Satisface requisitos',20,32),(56,'Trazabilidad con especificaciones',10,32),(57,'Construcción del producto acorde a metodología',20,32),(58,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,32),(59,'Gestión de tickets',5,33),(60,'Estadísticas',5,33),(61,'Definición de riesgos',5,34),(63,'Evidencia de gestión de riesgos',10,34),(64,'Arquitectura de aplicación',20,35),(65,'Satisface requisitos',20,35),(66,'Trazabilidad con especificaciones',5,35),(67,'Construcción del producto acorde a metodología',20,35),(68,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',10,35),(69,'Producto aceptado por cliente y postmortem acorde',10,35),(70,'Gestión de tickets',5,36),(71,'Estadísticas',5,36),(72,'Evidencia de gestión de riegos',5,37),(73,'',80,38),(74,'Alternativas de solución',20,39),(75,'',10,40),(76,'Alternativas de solución',10,40),(77,'Esquema de la solución',10,40),(78,'Satisface requisitos',10,41),(79,'Trazabilidad con especificaciones',10,41),(80,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,41),(81,'Gestión de control de cambios',5,42),(82,'Métricas de proyecto',5,42),(83,'Definición de riesgos',5,43),(84,'Planes de mitigación y contingencia',5,43),(85,'Planes de mitigación y contingenciaEvidencia de gestión de riesgos',5,43),(86,'Esquema de la solución',5,44),(87,'Arquitectura de aplicación',20,45),(88,'Satisface requisitos',20,45),(89,'Trazabilidad con especificaciones',5,45),(90,'Construcción del producto acorde a metodología',20,45),(91,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,45),(92,'producto aceptado por cliente y postmortem acorde',10,45),(93,'Gestión de control de cambios',5,46),(94,'Métricas de proyecto',5,46),(95,'Evidencia de gestión de riesgos',5,47),(96,'Esquema de la solución',7,48),(97,'Arquitectura de aplicación',20,49),(98,'Satisface requisitos',20,49),(99,'Trazabilidad con especificaciones',5,49),(100,'Construcción del producto acorde a metodología',20,49),(101,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,49),(102,'producto aceptado por cliente y postmortem acorde',10,49),(103,'Gestión de control de cambios',5,50),(104,'Métricas de proyecto',5,50),(106,'Evidencia de gestión de riesgos',3,52),(107,'Arquitectura de aplicación',20,53),(108,'Satisface requisitos',20,53),(109,'Trazabilidad con especificaciones',5,53),(110,'Construcción del producto acorde a metodología',20,53),(111,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,53),(112,'producto aceptado por cliente y postmortem acorde',10,53),(113,'Gestión de control de cambios',5,54),(114,'Métricas de proyecto',5,54),(115,'Evidencia de gestión de riesgos',5,55),(125,'',5,56),(126,'Alternativas de solución',7,57),(127,'Esquema de la solución',3,57),(128,'Arquitectura de aplicación',10,58),(130,'Satisface requisitos',30,58),(131,'Trazabilidad con especificaciones',3,58),(132,'Construcción del producto acorde a metodología',7,58),(133,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,58),(134,'Certificación de calidad (libera evidencia por funcionalidades y por unidad, libera a nivel de sistema, certifica aspectos de calidad frente a casos de carga real)',10,58),(135,'producto aceptado por cliente y postmortem acorde',5,58),(136,'Gestión de control de cambios',5,59),(138,'Definición de riesgos',1,60),(139,'Planes de mitigación y contingencia',1,60),(140,'Evidencia de gestión de riesgos',3,60),(141,'Métricas de proyecto',5,59),(142,'',5,61),(143,'Alternativas de solución',6,62),(144,'Esquema de la solución',4,62),(145,'Arquitectura de aplicación',10,63),(146,'Satisface requisitos',30,63),(147,'Trazabilidad con especificaciones',4,63),(148,'Construcción del producto acorde a metodología',6,63),(149,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',5,63),(150,'Certificación de calidad (libera evidencia por funcionalidades y por unidad, libera a nivel de sistema, certifica aspectos de calidad frente a casos de carga real)',10,63),(151,'producto aceptado por cliente y postmortem acorde',5,63),(152,'Gestión de control de cambios',4,64),(153,'Métricas de proyecto',6,64),(154,'Evidencia de gestión de riesgos',5,65),(155,'Alternativas de solución',5,66),(156,'Arquitectura de aplicación',5,67),(157,'Satisface requisitos',20,67),(158,'Construcción del producto acorde a metodología',10,67),(159,'Liberación del producto en los ambientes correspondientes acorde a definición y buenas prácticas y plan de entregables',10,67),(160,'Certificación de calidad (libera evidencia por funcionalidades y por unidad, libera a nivel de sistema, certifica aspectos de calidad frente a casos de carga real)',10,67),(161,'producto aceptado por cliente y postmortem acorde',20,67),(162,'Métricas de proyecto',10,68),(163,'Evidencia de gestión de riesgos',10,69);
/*!40000 ALTER TABLE `core_subtopic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_technique`
--

DROP TABLE IF EXISTS `core_technique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_technique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(300) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `hito_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_technique_hito_id_7eb542e5_fk_core_hito_id` (`hito_id`),
  CONSTRAINT `core_technique_hito_id_7eb542e5_fk_core_hito_id` FOREIGN KEY (`hito_id`) REFERENCES `core_hito` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_technique`
--

LOCK TABLES `core_technique` WRITE;
/*!40000 ALTER TABLE `core_technique` DISABLE KEYS */;
INSERT INTO `core_technique` VALUES (3,'Prototipo mitiga riesgo técnico',60,11),(4,'Preguntas técnicas',40,11),(5,'Producto desarrollado como se diseño',30,12),(6,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción?',30,12),(7,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes?',30,12),(8,'Producto liberado en ambiente de pruebas y/o producción',10,12),(10,'Producto desarrollado como se diseño',30,13),(11,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción?',30,13),(12,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes?',30,13),(14,'Producto liberado en ambiente de pruebas y/o producción',10,13),(15,'Producto desarrollado como se diseñó',40,14),(16,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción?',40,14),(17,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes?',10,14),(18,'Producto liberado en ambiente de pruebas y/o producción',10,14),(20,'Producto desarrollado como se diseñó',40,15),(21,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción?',40,15),(22,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes?',10,15),(23,'Producto liberado en ambiente de pruebas y/o producción',10,15),(24,'Producto desarrollado como se diseñó',40,16),(25,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción?',40,16),(26,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes?',10,16),(27,'Producto liberado en ambiente de pruebas y/o producción',10,16),(28,'asdasdasd',10,20),(29,'asdasdasd',10,20),(30,'asdasdasd',10,20),(31,'prototipo mitiga riesgo tecnico',60,25),(32,'preguntas técnicas',40,25),(33,'Producto desarrollado como se diseño',30,26),(34,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción)',30,26),(35,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes',30,26),(36,'Producto liberado en ambiente de pruebas y/o producción',10,26),(37,'Producto desarrollado como se diseño',30,27),(38,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción)',30,27),(39,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes',30,27),(40,'Producto liberado en ambiente de pruebas y/o producción',10,27),(41,'Producto desarrollado como se diseño',30,28),(42,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción)',30,28),(43,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes',30,28),(44,'Producto liberado en ambiente de pruebas y/o producción',10,28),(45,'Producto desarrollado como se diseño',30,29),(46,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción)',30,29),(47,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes',30,29),(48,'Producto liberado en ambiente de pruebas y/o producción',10,29),(49,'Producto desarrollado como se diseño',30,30),(50,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción)',30,30),(51,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes',30,30),(52,'Producto liberado en ambiente de pruebas y/o producción',10,30),(53,'Producto desarrollado como se diseño	',30,31),(54,'Las funcionalidades liberadas ¿hacen lo que deben hacer bajo condiciones similares al de producción)',30,31),(55,'¿Se pueden revisar en línea versiones actual y/o anteriores del producto junto a su release notes e installation notes',30,31),(56,'Producto liberado en ambiente de pruebas y/o producción',10,31);
/*!40000 ALTER TABLE `core_technique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_techniqueevaluation`
--

DROP TABLE IF EXISTS `core_techniqueevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_techniqueevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `project_hito_evaluation_id` int(11) DEFAULT NULL,
  `technique_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `D94b498250575119bb39d6eda5fd0994` (`project_hito_evaluation_id`),
  KEY `core_techniqueevaluat_technique_id_b3c0676f_fk_core_technique_id` (`technique_id`),
  CONSTRAINT `D94b498250575119bb39d6eda5fd0994` FOREIGN KEY (`project_hito_evaluation_id`) REFERENCES `core_projecthitoevaluation` (`id`),
  CONSTRAINT `core_techniqueevaluat_technique_id_b3c0676f_fk_core_technique_id` FOREIGN KEY (`technique_id`) REFERENCES `core_technique` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_techniqueevaluation`
--

LOCK TABLES `core_techniqueevaluation` WRITE;
/*!40000 ALTER TABLE `core_techniqueevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_techniqueevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_topic`
--

DROP TABLE IF EXISTS `core_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `hito_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_topic_hito_id_cc4fcfe8_fk_core_hito_id` (`hito_id`),
  CONSTRAINT `core_topic_hito_id_cc4fcfe8_fk_core_hito_id` FOREIGN KEY (`hito_id`) REFERENCES `core_hito` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_topic`
--

LOCK TABLES `core_topic` WRITE;
/*!40000 ALTER TABLE `core_topic` DISABLE KEYS */;
INSERT INTO `core_topic` VALUES (8,'Problemática	',11),(9,'Problemática',11),(10,'Valor agregado',11),(11,'Solución',11),(12,'Métricas',11),(13,'Estadísticas',11),(14,'Gestión de riesgos',11),(15,'Plan de riesgo',11),(16,'Esquema de la solución',11),(17,'Gestion de riesgos',11),(18,'Problemática',12),(19,'Valor agregado',12),(20,'Producto',12),(21,'Métricas',12),(22,'Gestión de riesgos',12),(23,'Producto',13),(24,'Métricas',13),(25,'Gestión de riesgos',13),(26,'Problemática',14),(27,'Valor agregado',14),(28,'Métricas',14),(29,'Gestión de riesgos',14),(30,'Problemática',15),(31,'Valor agregado',15),(32,'Producto',15),(33,'Métricas',15),(34,'Gestión de riesgos',15),(35,'Producto',16),(36,'Métricas',16),(37,'Gestión de riesgos',16),(38,'Problemática',24),(39,'Valor agregado',24),(40,'Problemática',25),(41,'Producto',25),(42,'Métricas',25),(43,'Gestión de riesgos',25),(44,'Valor agregado',27),(45,'Producto',27),(46,'Métricas',27),(47,'Gestión de riesgos',27),(48,'Valor agregado',28),(49,'Producto',28),(50,'Métricas',28),(52,'Gestión de riesgos',28),(53,'Producto',29),(54,'Métricas',29),(55,'Gestión de riesgos',29),(56,'Problemática',30),(57,'Valor agregado',30),(58,'Producto',30),(59,'Métricas',30),(60,'Gestión de riesgos',30),(61,'Problemática',31),(62,'Valor agregado',31),(63,'Producto',31),(64,'Métricas',31),(65,'Gestión de riesgos',31),(66,'Valor agregado',32),(67,'Producto',32),(68,'Métricas',32),(69,'Gestión de riesgos',32);
/*!40000 ALTER TABLE `core_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_userprofile`
--

DROP TABLE IF EXISTS `core_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_user` int(11) DEFAULT NULL,
  `rut` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `img` varchar(100) NOT NULL,
  `user_type` varchar(2) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `max_projects` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_userprofile_user_id_5141ad90_uniq` (`user_id`),
  KEY `core_userprofile_user_id_5141ad90_fk_auth_user_id` (`user_id`),
  CONSTRAINT `core_userprofile_user_id_5141ad90_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_userprofile`
--

LOCK TABLES `core_userprofile` WRITE;
/*!40000 ALTER TABLE `core_userprofile` DISABLE KEYS */;
INSERT INTO `core_userprofile` VALUES (1,2,'15021085-2','Eduardo Quiroga Aguilera ','profilePic/image.png','IN',2,9),(2,13,'14137981-K','Alejandro Spichiger Stuardo ','../Static/img/profiles/image.png','IN',3,9),(3,339,'16662072-4','Rodrigo Caballero Vivanco ','../Static/img/profiles/image.png','IN',7,9),(5,6,'23927409-9','Yonnys Escalona Medina ','../Static/img/profiles/image.png','IN',9,9),(6,379,'16615325-5','Edgardo Fuentes Cáceres ','../Static/img/profiles/image.png','IN',10,9),(7,1,'19431289-k','Marcelo Cavieres Davis','../Static/img/profiles/image.png','AD',6,9),(8,1195,'97897897-1','Marcelo Cavieres','../Static/img/profiles/image.png','AD',NULL,9),(9,NULL,'10492048-9','Carlos Abarzua Castro','../Static/img/profiles/image.png','IN',NULL,9),(10,NULL,'14546138-3','Carlos Mañan Piña','../Static/img/profiles/image.png','IN',NULL,9),(12,18,'14061700-8','Carlos Neira Carrasco ','../Static/img/profiles/image.png','IN',18,9),(13,NULL,'14008982-6','Rebeca Yañez Fuentes','../Static/img/profiles/image.png','IN',NULL,9),(14,1172,'21321-1','Edgardo Fuentes ','../Static/img/profiles/image.png','IN',11,9),(16,1192,'23468111-k','Rodrigo Donoso  ','../Static/img/profiles/image.png','IN',22,9),(17,3,'1241241-5','Haydee Vidal Zambrano ','../Static/img/profiles/image.png','AC',25,9),(18,1194,'134242341-1','Alicia Abarca ','../Static/img/profiles/image.png','AC',26,9);
/*!40000 ALTER TABLE `core_userprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_week`
--

DROP TABLE IF EXISTS `core_week`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_week` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(200) DEFAULT NULL,
  `total_objetive_percentage` int(11) NOT NULL,
  `total_progress_percentage` int(11) NOT NULL,
  `total_activity_percentage` int(11) NOT NULL,
  `session_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  CONSTRAINT `core_week_session_id_658f7705_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_week`
--

LOCK TABLES `core_week` WRITE;
/*!40000 ALTER TABLE `core_week` DISABLE KEYS */;
INSERT INTO `core_week` VALUES (42,'Elaboración de objetivos y métricas de proyectos',0,0,0,52),(43,'Enfoques del proyecto',0,100,0,53),(44,'Enfoques del proyecto',0,100,0,54),(45,'Trazabilidad del proyecto',0,100,0,55),(46,'Planificación y ejecución del proyecto',0,100,0,57),(47,'Gestión el proyecto de software',0,100,0,58),(48,'Retroalimentación y seguimiento',0,100,0,59),(49,'Planificación actualizada',0,100,0,60),(50,'Trazabilidad del proyecto	',0,100,0,62),(51,'Calidad arquitectónica de software	',0,100,0,63),(52,'Modelo de arquitectura de software',0,100,0,64),(53,'Integridad del enfoque arquitectónico',0,100,0,65),(54,'Revisión del proyecto',0,0,0,66),(55,'Revisión',0,0,0,68),(57,NULL,0,0,0,71),(58,'Introducción',0,0,0,87),(59,'Métricas de gestión Evaluación de calidad de diseño',0,100,0,88),(60,'Métricas de gestión Evaluación de calidad de diseño',0,100,0,89),(61,'Testing',0,100,0,90),(62,'Gestión de proyectos',0,100,0,92),(63,'Gestión de Pruebas',0,100,0,93),(64,'Gestión de Pruebas',0,100,0,94),(65,'Gestión de Pruebas',0,100,0,95),(66,'Gestión de Proyectos',0,100,0,97),(67,'Gestión de Proyectos',0,100,0,98),(68,'Gestión de Proyectos',0,80,20,99),(69,NULL,0,100,0,100),(70,'Introducción',0,0,0,102),(71,'Métricas de gestión Evaluación de calidad de diseño',0,100,0,103),(72,NULL,0,100,0,104),(73,NULL,0,100,0,105),(74,NULL,0,100,0,107),(75,NULL,0,100,0,108),(76,NULL,0,100,0,109),(77,NULL,0,100,0,111),(78,NULL,0,100,0,112),(79,NULL,0,0,0,113),(80,'testeo de tematica',100,40,0,120),(81,'asdasdasd',14,13,0,121),(82,'Presentación curso - Definición de tema',0,0,0,125),(83,NULL,0,0,0,126),(84,NULL,0,0,0,127),(85,NULL,0,0,0,128),(86,NULL,0,0,0,130),(87,NULL,0,0,0,131),(88,NULL,0,0,0,132),(89,NULL,0,0,0,134),(90,NULL,0,0,0,135),(91,NULL,0,0,0,137),(92,NULL,0,0,0,138),(93,'Presentación curso - Definición de tema',0,0,0,139),(94,'Recursos para la sistematización de problemas',0,0,100,140),(95,'Metodología de desarrollo de software y gestión de proyecto',0,0,100,141),(96,NULL,0,0,0,142),(97,'Enfoques del proyecto',0,0,100,144),(98,'Planificación y ejecución del proyecto',0,0,100,145),(99,NULL,0,0,0,146),(100,'Arquitectura de software',0,0,100,148),(101,'Arquitectura de software',0,0,100,149),(102,'Gestión del proyecto de software',0,0,100,150),(103,'Gestión de Proyectos',0,0,0,153),(104,'Modelamiento Arquitectura de Software',0,0,100,154),(105,'Integridad Enfoque arquitectónico',0,0,100,155),(106,'Integridad Enfoque arquitectónico',0,100,100,156),(107,'Evaluación calidad de diseño',0,100,100,158),(108,'Testing',0,100,100,159),(109,'Gestión de proyectos',0,100,0,160),(110,'Gestión de proyectos',0,100,0,162),(111,'Gestión de proyectos',0,100,0,163),(112,NULL,0,0,0,164),(113,'Semana Extra',100,100,100,166),(114,'Semana Extra',100,100,100,152),(115,'Gestión de Proyectos',0,0,0,167),(116,'Gestión de proyectos',0,100,0,168),(117,'Gestión de proyectos',0,100,0,169),(118,'Métricas de Gestión',0,0,100,170),(119,'Gestión de proyectos',0,0,100,172),(120,'Gestión de proyectos',0,100,0,173),(121,'Gestión de proyectos',0,100,0,174),(122,'Gestión de proyectos',0,100,0,176),(123,'Gestión de proyectos',0,0,100,177),(124,NULL,0,0,0,178),(125,NULL,0,0,0,180);
/*!40000 ALTER TABLE `core_week` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-06-05 19:43:00.194152','9','Proyecto de Título II - Sin ABET',2,'[{\"changed\": {\"fields\": [\"author\"]}}]',17,6),(2,'2018-06-05 19:43:04.626069','7','Proyecto de Título - Sin ABET',2,'[{\"changed\": {\"fields\": [\"author\"]}}]',17,6),(3,'2018-06-05 19:43:09.199170','4','Proyecto de Título I - Sin ABET',2,'[{\"changed\": {\"fields\": [\"author\"]}}]',17,6),(4,'2018-06-05 20:06:33.323188','2','Anibal Esteban Mendez Jimenez ',3,'',25,6),(5,'2018-06-05 20:06:33.331995','1','Francisco J. Falcon Soto ',3,'',25,6),(6,'2018-06-05 21:52:16.317099','44','Proceso de Administracion y Gestion Empresarial',2,'[{\"changed\": {\"fields\": [\"guide_teacher\"]}}]',38,6),(7,'2018-06-07 16:43:07.779818','126','ProjectEvaluation object',2,'[{\"changed\": {\"fields\": [\"comment_progress\", \"session_qualification\"]}}]',10,6),(8,'2018-06-28 17:03:09.134485','9','yonn.escalona.medina',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',4,6),(9,'2018-06-28 17:04:05.005293','9','yonn.escalona.medina',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',4,6),(10,'2018-07-10 17:41:19.057548','101','Integrador de gestion de entidades financieras',3,'',38,6),(11,'2018-07-10 17:54:44.948381','102','Integrador de gestion de entidades financieras',3,'',38,6),(12,'2018-07-10 18:03:56.187573','103','Integrador de gestion de entidades financieras',3,'',38,6),(13,'2018-07-11 16:53:55.943930','17','Anibal Esteban Mendez Jimenez',2,'[{\"changed\": {\"fields\": [\"name\", \"actual_subject\"]}}]',25,6),(14,'2018-07-13 05:05:59.408524','109','Gestion de estacionamientos para personas en situacion de discapacidad',2,'[{\"changed\": {\"fields\": [\"vacancy_total\", \"vacancy_available\", \"status\"]}}]',38,6),(15,'2018-07-13 05:06:05.505327','109','Gestion de estacionamientos para personas en situacion de discapacidad',2,'[]',38,6),(16,'2018-07-13 05:06:21.355766','108','Gestion de proyectos ABB',2,'[{\"changed\": {\"fields\": [\"vacancy_total\", \"vacancy_available\", \"status\"]}}]',38,6),(17,'2018-07-13 05:06:32.262300','107','Registro Clinico Electronico',2,'[{\"changed\": {\"fields\": [\"name\", \"vacancy_total\", \"vacancy_available\", \"status\"]}}]',38,6),(18,'2018-07-13 05:06:42.224325','106','Plataforma integrada de eventos registrables',2,'[]',38,6),(19,'2018-07-13 05:06:57.881355','105','Gestion automatizada de proyectos',2,'[{\"changed\": {\"fields\": [\"vacancy_total\", \"vacancy_available\"]}}]',38,6),(20,'2018-07-13 08:35:11.949083','5','Minuta object',3,'',30,6),(21,'2018-07-13 08:35:11.951537','4','Minuta object',3,'',30,6),(22,'2018-07-13 08:35:11.952934','3','Minuta object',3,'',30,6),(23,'2018-07-13 08:35:11.956962','2','Minuta object',3,'',30,6),(24,'2018-07-13 08:35:11.959144','1','Minuta object',3,'',30,6),(25,'2018-07-13 16:55:44.650105','1','MinutaEvidence object',3,'',61,6),(26,'2018-07-13 17:15:54.770633','6','Minuta object',2,'[{\"changed\": {\"fields\": [\"reunion_theme\", \"external_visit\", \"status\"]}}]',30,6),(27,'2018-07-18 19:21:16.651434','110','Sistema de retroalimentacion inmediata',2,'[]',38,6),(28,'2018-07-18 19:21:24.902455','110','Sistema de retroalimentación inmediata',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(29,'2018-07-18 19:21:43.686951','109','Gestión de estacionamientos para personas en situación de discapacidad',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(30,'2018-07-18 19:25:34.960596','108','Gestión de proyectos ABB',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(31,'2018-07-18 19:30:10.259271','107','Registro Clínico Electrónico',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(32,'2018-07-18 19:30:22.443666','105','Gestión automatizada de proyectos',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(33,'2018-07-18 19:30:32.237037','104','Integrador de gestión de entidades financieras',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(34,'2018-07-18 19:30:46.622970','58','Plataforma de generación de certificados',2,'[{\"changed\": {\"fields\": [\"name\", \"description\"]}}]',38,6),(35,'2018-07-18 19:31:12.625279','57','Mejoramiento en los Procesos de Administración, con Procesamiento de Datos.',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(36,'2018-07-18 19:31:40.562407','54','Proceso de Acreditación de ABET',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(37,'2018-07-18 19:32:14.485276','52','Plataforma de Evaluación de Eficiencia Docente',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(38,'2018-07-18 19:32:25.487329','45','Proceso de Evaluación Docente',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(39,'2018-07-18 19:32:41.901882','44','Proceso de Administración y Gestion Empresarial',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(40,'2018-07-18 19:33:05.556459','43','Control de Asistencia Georeferenciado con Doble Autentificación',2,'[{\"changed\": {\"fields\": [\"name\", \"description\"]}}]',38,6),(41,'2018-07-18 19:33:20.416504','40','Gestión de Instituciones',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(42,'2018-07-18 19:33:54.320927','35','Sistema de administración de licencias de software para departamentos de tecnología',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',38,6),(43,'2018-07-18 20:52:57.535025','112','Sudo TIC',2,'[{\"changed\": {\"fields\": [\"vacancy_total\", \"vacancy_available\"]}}]',38,6),(44,'2018-07-23 07:35:58.243938','1','Session_start object',2,'[{\"changed\": {\"fields\": [\"period\"]}}]',8,6),(45,'2018-07-23 22:29:26.668146','10','Minuta object',3,'',30,6),(46,'2018-07-24 21:04:47.916066','113','Week object',1,'[{\"added\": {}}]',22,6),(47,'2018-07-24 21:05:37.592918','114','Week object',1,'[{\"added\": {}}]',22,6),(48,'2018-07-26 22:06:06.257804','13','Rebeca Yañez Fuentes',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',24,6),(49,'2018-07-26 22:07:10.660093','10','Carlos Mañan Piña',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',24,6),(50,'2018-07-26 23:05:23.863021','106','Plataforma integrada de eventos registrables',2,'[{\"changed\": {\"fields\": [\"guide_teacher\"]}}]',38,6),(51,'2018-07-26 23:05:50.115907','106','Plataforma integrada de eventos registrables',2,'[]',38,6),(52,'2018-07-26 23:07:28.207352','15','Por Por Definir ',3,'',24,6),(53,'2018-07-26 23:11:20.293947','7','Marcelo Cavieres Davis',2,'[{\"changed\": {\"fields\": [\"name\", \"user_type\"]}}]',24,6),(54,'2018-07-26 23:11:31.217849','8','Marcelo Cavieres',2,'[{\"changed\": {\"fields\": [\"user_type\"]}}]',24,6),(55,'2018-07-26 23:15:11.261892','9','Carlos Abarzua Castro',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',24,6),(56,'2018-07-27 17:47:44.984136','11','Carlos Beyzaga Medel ',3,'',24,6),(57,'2018-07-27 17:48:11.737156','4','Haydee Vidal Zambrano',2,'[{\"changed\": {\"fields\": [\"name\", \"user_type\"]}}]',24,6),(58,'2018-08-01 20:17:24.688901','3','Solicitud_Practica object',3,'',48,6),(59,'2018-08-01 20:20:08.300539','4','Haydee Vidal Zambrano',3,'',24,6),(60,'2018-08-01 20:20:56.181013','8','hvidal',3,'',4,6),(61,'2018-08-08 15:48:55.562513','15','Minuta object',2,'[{\"changed\": {\"fields\": [\"external_visit\"]}}]',30,6),(62,'2018-08-24 18:50:23.218639','4','Solicitud_Practica object',3,'',48,6),(63,'2018-08-24 18:52:48.206230','5','Solicitud_Practica object',3,'',48,6),(64,'2018-11-29 00:24:45.456267','1','ProjectHitoEvaluation object',3,'',19,6),(65,'2018-11-29 00:25:04.224805','137','ProjectEvaluation object',3,'',10,6),(66,'2018-11-29 00:25:04.227660','136','ProjectEvaluation object',3,'',10,6),(67,'2018-11-29 00:25:04.229034','135','ProjectEvaluation object',3,'',10,6),(68,'2018-11-29 00:25:04.231466','134','ProjectEvaluation object',3,'',10,6),(69,'2018-11-29 00:25:04.232784','133','ProjectEvaluation object',3,'',10,6),(70,'2018-11-29 00:25:04.235176','132','ProjectEvaluation object',3,'',10,6),(71,'2018-11-29 00:25:04.236494','131','ProjectEvaluation object',3,'',10,6),(72,'2018-11-29 00:25:04.240637','130','ProjectEvaluation object',3,'',10,6),(73,'2018-11-29 00:25:04.243035','129','ProjectEvaluation object',3,'',10,6),(74,'2018-11-29 00:25:04.244372','128','ProjectEvaluation object',3,'',10,6),(75,'2018-11-29 00:25:04.245710','127','ProjectEvaluation object',3,'',10,6),(76,'2018-11-29 00:25:04.248192','126','ProjectEvaluation object',3,'',10,6),(77,'2018-11-29 00:25:04.249453','125','ProjectEvaluation object',3,'',10,6),(78,'2018-11-29 00:25:04.251707','124','ProjectEvaluation object',3,'',10,6),(79,'2018-11-29 00:25:04.252989','123','ProjectEvaluation object',3,'',10,6),(80,'2018-11-29 00:25:04.255325','122','ProjectEvaluation object',3,'',10,6),(81,'2018-11-29 00:25:04.256678','121','ProjectEvaluation object',3,'',10,6),(82,'2018-11-29 00:25:04.259132','120','ProjectEvaluation object',3,'',10,6),(83,'2018-11-29 00:25:04.260429','119','ProjectEvaluation object',3,'',10,6),(84,'2018-11-29 00:25:04.261685','118','ProjectEvaluation object',3,'',10,6),(85,'2018-11-29 00:25:04.263919','117','ProjectEvaluation object',3,'',10,6),(86,'2018-11-29 00:25:04.265246','116','ProjectEvaluation object',3,'',10,6),(87,'2018-11-29 00:25:04.267712','115','ProjectEvaluation object',3,'',10,6),(88,'2018-11-29 00:25:04.269028','114','ProjectEvaluation object',3,'',10,6),(89,'2018-11-29 00:25:04.271443','113','ProjectEvaluation object',3,'',10,6),(90,'2018-11-29 00:25:04.272709','112','ProjectEvaluation object',3,'',10,6),(91,'2018-11-29 00:25:04.275070','111','ProjectEvaluation object',3,'',10,6),(92,'2018-11-29 00:25:04.276351','110','ProjectEvaluation object',3,'',10,6),(93,'2018-11-29 00:25:04.277689','109','ProjectEvaluation object',3,'',10,6),(94,'2018-11-29 00:25:04.280171','108','ProjectEvaluation object',3,'',10,6),(95,'2018-11-29 00:25:04.281571','107','ProjectEvaluation object',3,'',10,6),(96,'2018-11-29 00:25:04.283900','106','ProjectEvaluation object',3,'',10,6),(97,'2018-11-29 00:25:04.285163','105','ProjectEvaluation object',3,'',10,6),(98,'2018-11-29 00:25:04.287657','104','ProjectEvaluation object',3,'',10,6),(99,'2018-11-29 00:25:04.289035','103','ProjectEvaluation object',3,'',10,6),(100,'2018-11-29 00:25:04.291599','102','ProjectEvaluation object',3,'',10,6),(101,'2018-11-29 00:25:04.292848','101','ProjectEvaluation object',3,'',10,6),(102,'2018-11-29 00:25:04.295246','100','ProjectEvaluation object',3,'',10,6),(103,'2018-11-29 00:25:04.296530','99','ProjectEvaluation object',3,'',10,6),(104,'2018-11-29 00:25:04.297828','98','ProjectEvaluation object',3,'',10,6),(105,'2018-11-29 00:25:04.300271','97','ProjectEvaluation object',3,'',10,6),(106,'2018-11-29 00:25:04.301639','96','ProjectEvaluation object',3,'',10,6),(107,'2018-11-29 00:25:04.304021','95','ProjectEvaluation object',3,'',10,6),(108,'2018-11-29 00:25:04.305362','94','ProjectEvaluation object',3,'',10,6),(109,'2018-11-29 00:25:04.307950','93','ProjectEvaluation object',3,'',10,6),(110,'2018-11-29 00:25:04.309235','92','ProjectEvaluation object',3,'',10,6),(111,'2018-11-29 00:25:04.311539','91','ProjectEvaluation object',3,'',10,6),(112,'2018-11-29 00:25:04.312847','90','ProjectEvaluation object',3,'',10,6),(113,'2018-11-29 00:25:04.315206','89','ProjectEvaluation object',3,'',10,6),(114,'2018-11-29 00:25:04.316564','88','ProjectEvaluation object',3,'',10,6),(115,'2018-11-29 00:25:04.317871','87','ProjectEvaluation object',3,'',10,6),(116,'2018-11-29 00:25:04.320281','86','ProjectEvaluation object',3,'',10,6),(117,'2018-11-29 00:25:04.321561','85','ProjectEvaluation object',3,'',10,6),(118,'2018-11-29 00:25:04.323862','84','ProjectEvaluation object',3,'',10,6),(119,'2018-11-29 00:25:04.325167','83','ProjectEvaluation object',3,'',10,6),(120,'2018-11-29 00:25:04.327602','82','ProjectEvaluation object',3,'',10,6),(121,'2018-11-29 00:25:04.328923','81','ProjectEvaluation object',3,'',10,6),(122,'2018-11-29 00:25:04.331398','80','ProjectEvaluation object',3,'',10,6),(123,'2018-11-29 00:25:04.332821','79','ProjectEvaluation object',3,'',10,6),(124,'2018-11-29 00:25:04.335246','78','ProjectEvaluation object',3,'',10,6),(125,'2018-11-29 00:25:04.336521','77','ProjectEvaluation object',3,'',10,6),(126,'2018-11-29 00:25:04.337845','76','ProjectEvaluation object',3,'',10,6),(127,'2018-11-29 00:25:04.340274','75','ProjectEvaluation object',3,'',10,6),(128,'2018-11-29 00:25:04.341527','74','ProjectEvaluation object',3,'',10,6),(129,'2018-11-29 00:25:04.343795','73','ProjectEvaluation object',3,'',10,6),(130,'2018-11-29 00:25:04.345065','72','ProjectEvaluation object',3,'',10,6),(131,'2018-11-29 00:25:04.347368','71','ProjectEvaluation object',3,'',10,6),(132,'2018-11-29 00:25:04.348670','70','ProjectEvaluation object',3,'',10,6),(133,'2018-11-29 00:25:04.350979','69','ProjectEvaluation object',3,'',10,6),(134,'2018-11-29 00:25:04.352378','68','ProjectEvaluation object',3,'',10,6),(135,'2018-11-29 00:25:04.353656','67','ProjectEvaluation object',3,'',10,6),(136,'2018-11-29 00:25:04.356022','66','ProjectEvaluation object',3,'',10,6),(137,'2018-11-29 00:25:04.357352','65','ProjectEvaluation object',3,'',10,6),(138,'2018-11-29 00:25:04.359710','64','ProjectEvaluation object',3,'',10,6),(139,'2018-11-29 00:25:04.361027','63','ProjectEvaluation object',3,'',10,6),(140,'2018-11-29 00:25:04.363392','62','ProjectEvaluation object',3,'',10,6),(141,'2018-11-29 00:25:04.364688','61','ProjectEvaluation object',3,'',10,6),(142,'2018-11-29 00:25:04.367168','60','ProjectEvaluation object',3,'',10,6),(143,'2018-11-29 00:25:04.368492','59','ProjectEvaluation object',3,'',10,6),(144,'2018-11-29 00:25:04.369850','58','ProjectEvaluation object',3,'',10,6),(145,'2018-11-29 00:25:04.372329','57','ProjectEvaluation object',3,'',10,6),(146,'2018-11-29 00:25:04.373632','56','ProjectEvaluation object',3,'',10,6),(147,'2018-11-29 00:25:04.375972','55','ProjectEvaluation object',3,'',10,6),(148,'2018-11-29 00:25:04.377303','54','ProjectEvaluation object',3,'',10,6),(149,'2018-11-29 00:25:04.379689','53','ProjectEvaluation object',3,'',10,6),(150,'2018-11-29 00:25:04.380967','52','ProjectEvaluation object',3,'',10,6),(151,'2018-11-29 00:25:04.383318','51','ProjectEvaluation object',3,'',10,6),(152,'2018-11-29 00:25:04.384659','50','ProjectEvaluation object',3,'',10,6),(153,'2018-11-29 00:25:04.387039','49','ProjectEvaluation object',3,'',10,6),(154,'2018-11-29 00:25:04.388349','48','ProjectEvaluation object',3,'',10,6),(155,'2018-11-29 00:25:04.389684','47','ProjectEvaluation object',3,'',10,6),(156,'2018-11-29 00:25:04.392044','46','ProjectEvaluation object',3,'',10,6),(157,'2018-11-29 00:25:04.393365','45','ProjectEvaluation object',3,'',10,6),(158,'2018-11-29 00:25:04.395778','44','ProjectEvaluation object',3,'',10,6),(159,'2018-11-29 00:25:04.397084','43','ProjectEvaluation object',3,'',10,6),(160,'2018-11-29 00:25:04.399461','42','ProjectEvaluation object',3,'',10,6),(161,'2018-11-29 00:25:04.400769','41','ProjectEvaluation object',3,'',10,6),(162,'2018-11-29 00:25:04.403204','40','ProjectEvaluation object',3,'',10,6),(163,'2018-11-29 00:25:04.404708','39','ProjectEvaluation object',3,'',10,6),(164,'2018-11-29 00:25:04.407129','38','ProjectEvaluation object',3,'',10,6);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(2,'auth','group'),(3,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(11,'core','activityevaluation'),(7,'core','carreer'),(21,'core','evaluation'),(23,'core','hito'),(20,'core','memory'),(12,'core','memoryevaluation'),(18,'core','objetiveevaluation'),(14,'core','presentationevaluation'),(16,'core','progressevaluation'),(10,'core','projectevaluation'),(19,'core','projecthitoevaluation'),(17,'core','rubric'),(28,'core','session'),(8,'core','session_start'),(25,'core','student'),(9,'core','subject'),(13,'core','subtopic'),(15,'core','technique'),(26,'core','techniqueevaluation'),(27,'core','topic'),(24,'core','userprofile'),(22,'core','week'),(49,'practicas','ausencia'),(55,'practicas','comments'),(50,'practicas','empresa'),(59,'practicas','eval_final'),(46,'practicas','justificativo'),(57,'practicas','obs_director'),(51,'practicas','practica'),(47,'practicas','publication'),(54,'practicas','respuestasolicitud'),(53,'practicas','retro'),(48,'practicas','solicitud_practica'),(45,'practicas','task'),(52,'practicas','week_practica'),(35,'projects','abet'),(31,'projects','abetevaluation'),(41,'projects','abetrubric'),(42,'projects','criteria'),(34,'projects','criteriaevaluation'),(37,'projects','crit_info'),(32,'projects','feedback'),(44,'projects','glosa'),(63,'projects','historicalminutaevidence'),(30,'projects','minuta'),(62,'projects','minutacomment'),(61,'projects','minutaevidence'),(56,'projects','minutaretro'),(40,'projects','minutavalidation'),(29,'projects','outcome'),(33,'projects','outcomeevaluation'),(39,'projects','postulation'),(38,'projects','project'),(43,'projects','tag'),(36,'projects','teacher_postulation'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-06-05 19:06:20.149353'),(2,'auth','0001_initial','2018-06-05 19:06:20.485377'),(3,'admin','0001_initial','2018-06-05 19:06:20.608626'),(4,'admin','0002_logentry_remove_auto_add','2018-06-05 19:06:20.634105'),(5,'contenttypes','0002_remove_content_type_name','2018-06-05 19:06:20.729985'),(6,'auth','0002_alter_permission_name_max_length','2018-06-05 19:06:20.754029'),(7,'auth','0003_alter_user_email_max_length','2018-06-05 19:06:20.782965'),(8,'auth','0004_alter_user_username_opts','2018-06-05 19:06:20.809144'),(9,'auth','0005_alter_user_last_login_null','2018-06-05 19:06:20.851699'),(10,'auth','0006_require_contenttypes_0002','2018-06-05 19:06:20.858446'),(11,'auth','0007_alter_validators_add_error_messages','2018-06-05 19:06:20.879747'),(12,'auth','0008_alter_user_username_max_length','2018-06-05 19:06:20.917687'),(13,'core','0001_initial','2018-06-05 19:06:21.484582'),(14,'projects','0001_initial','2018-06-05 19:06:24.061090'),(15,'core','0002_auto_20180605_1412','2018-06-05 19:06:26.589971'),(16,'practicas','0001_initial','2018-06-05 19:06:27.846400'),(17,'sessions','0001_initial','2018-06-05 19:06:27.880307'),(18,'practicas','0002_comments','2018-06-06 21:10:14.529711'),(19,'projects','0002_auto_20180606_0048','2018-06-06 21:10:14.649953'),(20,'projects','0003_auto_20180606_1159','2018-06-06 21:10:15.119200'),(21,'projects','0004_auto_20180606_2247','2018-06-07 04:53:39.439903'),(22,'projects','0005_auto_20180606_2248','2018-06-07 04:53:39.551897'),(23,'projects','0006_minutavalidation_minuta','2018-06-07 21:08:15.397576'),(24,'projects','0007_auto_20180607_1505','2018-06-07 21:08:15.539465'),(25,'projects','0008_auto_20180611_0115','2018-06-11 15:09:29.216840'),(26,'projects','0009_auto_20180611_1427','2018-06-11 19:27:14.960147'),(27,'projects','0010_auto_20180612_1414','2018-06-12 18:28:10.389376'),(28,'projects','0011_minuta_last_mod','2018-06-12 22:58:10.660769'),(29,'projects','0012_auto_20180612_1709','2018-06-12 22:58:10.739840'),(30,'projects','0013_minuta_author','2018-06-13 20:26:18.661591'),(31,'projects','0014_auto_20180613_1218','2018-06-13 20:26:18.802438'),(32,'projects','0015_minutaretro','2018-06-13 20:26:18.918686'),(33,'projects','0016_minutaretro_read','2018-06-14 18:33:10.105477'),(34,'practicas','0003_solicitud_practica_step','2018-06-25 15:59:39.637254'),(35,'practicas','0004_auto_20180606_1718','2018-06-25 15:59:39.967194'),(36,'practicas','0005_eval_final','2018-06-25 15:59:40.101169'),(37,'practicas','0006_auto_20180614_0909','2018-06-25 15:59:41.351081'),(38,'practicas','0007_eval_final_status','2018-06-25 15:59:41.456654'),(39,'practicas','0008_obs_director','2018-06-25 15:59:41.581368'),(40,'practicas','0009_auto_20180614_1415','2018-06-25 15:59:41.717090'),(41,'projects','0017_feedback_author_s','2018-06-25 15:59:41.929357'),(42,'projects','0018_project_cloud','2018-07-04 22:26:58.450296'),(43,'projects','0019_auto_20180626_1619','2018-07-04 22:26:58.536251'),(44,'core','0002_pregunta_alternativa_pregunta_text_pregunta_vf','2018-07-09 01:27:19.429585'),(45,'core','0003_auto_20180318_1756','2018-07-09 01:27:19.525096'),(46,'core','0004_merge_20180614_1436','2018-07-09 01:27:19.529176'),(47,'core','0005_auto_20180618_1450','2018-07-09 01:27:19.558236'),(48,'practicas','0010_auto_20180625_1229','2018-07-09 01:27:19.995421'),(49,'projects','0018_remove_feedback_project','2018-07-09 01:27:20.158770'),(50,'projects','0020_merge_20180630_1043','2018-07-09 01:27:20.162888'),(51,'projects','0021_feedback_project','2018-07-10 18:02:33.918017'),(52,'core','0006_auto_20180713_0421','2018-07-13 08:38:53.182059'),(53,'projects','0022_auto_20180711_1611','2018-07-13 08:38:54.105083'),(54,'projects','0023_auto_20180712_1023','2018-07-13 08:38:54.307702'),(55,'projects','0024_auto_20180713_0309','2018-07-13 08:38:54.540910'),(56,'projects','0025_auto_20180713_1307','2018-07-13 17:15:17.062791'),(57,'projects','0026_auto_20180713_1633','2018-07-13 20:34:06.770428'),(58,'core','0007_session_start_period','2018-07-17 22:37:31.211833'),(59,'core','0008_auto_20180721_1907','2018-07-23 07:34:20.884857'),(60,'projects','0027_auto_20180721_1851','2018-07-23 07:34:20.971498'),(61,'projects','0028_project_share_teacher','2018-07-24 19:34:49.639156'),(62,'projects','0029_historicalminutaevidence','2018-07-24 19:34:49.815586'),(63,'projects','0030_auto_20180724_1437','2018-07-24 19:34:50.071125'),(64,'projects','0031_auto_20180726_0050','2018-07-26 16:21:27.182410'),(65,'core','0009_auto_20180726_1911','2018-07-26 23:27:56.900375'),(66,'core','0010_auto_20180726_1914','2018-07-26 23:27:56.990900'),(67,'core','0011_auto_20180726_1955','2018-07-27 00:33:22.269617'),(68,'core','0012_hito_ponderation','2018-08-13 16:29:22.049565'),(69,'core','0013_rubric_desem','2018-08-13 16:29:22.191236'),(70,'core','0014_userprofile_max_projects','2018-08-13 16:29:22.334476'),(71,'core','0015_hito_final','2018-08-13 16:29:22.470354'),(72,'core','0016_auto_20180810_1246','2018-08-13 16:29:22.924630'),(73,'core','0017_rubric_pond_desem','2018-08-13 16:29:23.060978'),(74,'projects','0022_postulation_last_mod_user','2018-08-13 16:29:23.238827'),(75,'projects','0031_merge_20180730_1831','2018-08-13 16:29:23.243538'),(76,'projects','0032_merge_20180802_2234','2018-08-13 16:29:23.246934'),(77,'projects','0033_auto_20180802_2247','2018-08-13 16:29:23.331842'),(78,'projects','0034_project_desem_grade','2018-08-28 03:00:28.952720'),(79,'projects','0035_auto_20180831_1223','2018-08-31 17:15:04.095466'),(80,'core','0018_auto_20180905_1336','2018-11-29 00:23:21.026361'),(81,'core','0019_projecthitoevaluation_status','2018-11-29 00:23:21.181586'),(82,'core','0020_projectevaluation_status','2018-11-29 00:23:21.342848');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('02bxiwqsmy1hsw0xi5lj2b02eahww7k8','NWEwNjhlOGJmZjgzNWE2OWQxMDY0NWMwMzFhOTkxMjQxZTcwYjJlODp7InVzZXJuYW1lIjoiZi5mYWxjb25zb3RvIiwicHJvamVjdCI6MzUsIm5hbWUiOiIgRlJBTkNJU0NPIEouIEZBTENPTiBTT1RPIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjEwMzQsInRva2VuIjoiZDE4NGMzMjRiOWE3NTdkNTBlMmFkMDM5MDg1NTkzNjlkMWUyYzA4YyIsIl9hdXRoX3VzZXJfaGFzaCI6ImU5YzkwMjFkM2ZkOGEwOGYyMzE1MmNjMjUwNDgxYjY4MDk2NGE4NGUiLCJ0eXBlIjoiQUwiLCJydXQiOiIxODYyNjAzNi04In0=','2018-09-07 18:52:59.419376'),('0h1ymvdrieuahc98bebdocu4pvpqwo4m','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-08-22 15:43:47.644964'),('1gtgvyjc2b0y624oszlxnknbopl5xltr','NmEwZjRjYjY1ZDliZjhlNzAxMTY5ZmMzNjQwY2QzYWQ0ZTQ0MTFkOTp7ImgiOjB9','2018-07-12 05:35:51.154973'),('2nhir592qu1hly0f36xhvg1po2lhihco','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-06-19 19:34:11.725398'),('3jui2p3oyxhsduajqgwppb5t27vsu3ix','MDVmY2JkNmU0YTcwZWUxNmJjYTYxMmFlN2NiMWNlYzFhZDE5NjBmMDp7InVzZXJuYW1lIjoicm9kcmlnby5jYWJhbGxlcm8udml2YW5jbyIsIm5hbWUiOiJSb2RyaWdvIENhYmFsbGVybyBWaXZhbmNvIiwiX2F1dGhfdXNlcl9pZCI6IjciLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjMzOSwidG9rZW4iOiIwNjhmNzQwYjUyMzgwNTI3YmNmZDRjYmNhYmYzOTBlYWQxODliY2NlIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2M2MmRiYTA3N2NiMzE0YmYzNTc1ZmZlNjllZjI2OGRmODgxMWFlZSIsInR5cGUiOiJTQSIsInJ1dCI6IjE2NjYyMDcyLTQifQ==','2018-07-23 19:34:11.761470'),('4u91xhtalliibyvxb7exmkcfmfd7vkv6','NGMwMGM5Njg4YjVkNDZiNzM2ZTAwNmQzZDZjYWJjZWVhZTBjYjJjODp7Il9hdXRoX3VzZXJfaGFzaCI6ImNlNTM5MzhjOWI0NGFlNmE1MjMzMzZhYTg1YTgwNGJjNzE5ODRiOTIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2018-07-24 17:40:35.625366'),('6bt4p44yr42ypi9vljwrjc5ekmijz09p','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-08-16 14:01:25.034247'),('6rubp8799arrvz3qcxqlwfuubz120kjs','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-07-27 17:16:24.213559'),('72cdzdwdzkrsti7nfgy428y79go6fd9j','NmEwZjRjYjY1ZDliZjhlNzAxMTY5ZmMzNjQwY2QzYWQ0ZTQ0MTFkOTp7ImgiOjB9','2018-07-27 15:56:49.285305'),('7x0v3t9icq6im3cg5rwz4wdwqrsumdko','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-06-25 18:51:19.935740'),('7y6o77h2klb8fheedfu6w1yvye4an2mf','NWEwNjhlOGJmZjgzNWE2OWQxMDY0NWMwMzFhOTkxMjQxZTcwYjJlODp7InVzZXJuYW1lIjoiZi5mYWxjb25zb3RvIiwicHJvamVjdCI6MzUsIm5hbWUiOiIgRlJBTkNJU0NPIEouIEZBTENPTiBTT1RPIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjEwMzQsInRva2VuIjoiZDE4NGMzMjRiOWE3NTdkNTBlMmFkMDM5MDg1NTkzNjlkMWUyYzA4YyIsIl9hdXRoX3VzZXJfaGFzaCI6ImU5YzkwMjFkM2ZkOGEwOGYyMzE1MmNjMjUwNDgxYjY4MDk2NGE4NGUiLCJ0eXBlIjoiQUwiLCJydXQiOiIxODYyNjAzNi04In0=','2018-06-27 15:30:26.782238'),('7zvbxcj7xzk27ctx1g8wjujcs5kmfv3x','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-07-23 19:32:18.420769'),('81he546u2zua3jwl2fmus34vzyb8goxp','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-07-23 19:31:52.673149'),('8adccmrjxr5885eg3dl0d5dcon1ycjcq','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-07-12 17:13:16.509141'),('8ii59qb7tfpc7r2ow8brjrdjwws5uu7a','NWEwNjhlOGJmZjgzNWE2OWQxMDY0NWMwMzFhOTkxMjQxZTcwYjJlODp7InVzZXJuYW1lIjoiZi5mYWxjb25zb3RvIiwicHJvamVjdCI6MzUsIm5hbWUiOiIgRlJBTkNJU0NPIEouIEZBTENPTiBTT1RPIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjEwMzQsInRva2VuIjoiZDE4NGMzMjRiOWE3NTdkNTBlMmFkMDM5MDg1NTkzNjlkMWUyYzA4YyIsIl9hdXRoX3VzZXJfaGFzaCI6ImU5YzkwMjFkM2ZkOGEwOGYyMzE1MmNjMjUwNDgxYjY4MDk2NGE4NGUiLCJ0eXBlIjoiQUwiLCJydXQiOiIxODYyNjAzNi04In0=','2018-09-14 02:33:17.213386'),('8rjzytma75gag23hvnmhwiqhdbj61hk9','MDVmY2JkNmU0YTcwZWUxNmJjYTYxMmFlN2NiMWNlYzFhZDE5NjBmMDp7InVzZXJuYW1lIjoicm9kcmlnby5jYWJhbGxlcm8udml2YW5jbyIsIm5hbWUiOiJSb2RyaWdvIENhYmFsbGVybyBWaXZhbmNvIiwiX2F1dGhfdXNlcl9pZCI6IjciLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjMzOSwidG9rZW4iOiIwNjhmNzQwYjUyMzgwNTI3YmNmZDRjYmNhYmYzOTBlYWQxODliY2NlIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2M2MmRiYTA3N2NiMzE0YmYzNTc1ZmZlNjllZjI2OGRmODgxMWFlZSIsInR5cGUiOiJTQSIsInJ1dCI6IjE2NjYyMDcyLTQifQ==','2018-09-06 18:19:20.396905'),('8z7ctggurqqvp2kmjxnnnri99pr70ijf','NGMwMGM5Njg4YjVkNDZiNzM2ZTAwNmQzZDZjYWJjZWVhZTBjYjJjODp7Il9hdXRoX3VzZXJfaGFzaCI6ImNlNTM5MzhjOWI0NGFlNmE1MjMzMzZhYTg1YTgwNGJjNzE5ODRiOTIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2018-07-24 17:54:04.104402'),('a0lob72laxziic1a6uqikw66lt6yg66g','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-09-29 22:46:33.919266'),('aff1me8yqzld3nk82w4neemcg5x8n68p','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-07-11 19:53:17.521345'),('b78t6ejxefc9lgyc7kd2gy57ahsdp8v0','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-06-28 20:09:27.201189'),('bahyx3wfsa6odybpkz8kl95sa8kkrbz9','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-07-23 17:47:07.059075'),('cgc5mfklg18cm6zot6l46fqivwm15fd3','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-07-18 22:35:39.952012'),('dr5at7oqy1ipq0j5wasvzrh1nd5cjepd','ZDNiNTVjMmQyNGM3NTIzNWMyMDgwZTAwMjI0NWYxODE1NTcwZWJhZjp7InVzZXJuYW1lIjoiY2FybG9zLm5laXJhLmNhcnJhc2NvIiwibmFtZSI6IkNhcmxvcyBOZWlyYSBDYXJyYXNjbyIsIl9hdXRoX3VzZXJfaWQiOiIxOCIsImgiOjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidXNlcl9wayI6MTgsInRva2VuIjoiY2RlNjU2OTI2MTc5NGRkMWQwMmM4ZTQzNzI5MjQzNWE5ZTIyMDAwMyIsIl9hdXRoX3VzZXJfaGFzaCI6IjZjYzU3YjRiYTY1MGEzYTA4NGQ4ZDdhYjJkMTY2NmFiMzYyMjRhMjEiLCJ0eXBlIjoiRE8iLCJydXQiOiIxNDA2MTcwMC04In0=','2018-07-26 20:26:30.883356'),('e3ecggz003262ghgjjbe17jen4e3t9oz','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-08-22 16:14:17.537390'),('f0x8phdg6l1jrcn1qj41j22xzjyxryhi','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-10-18 15:53:00.268138'),('f49bmxnbd95cg1bx4qev2j3hd304vwm9','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-08-15 20:19:33.392102'),('fmglcedun3uhkkjdoi1j0sb2as0ddos4','NGMwMGM5Njg4YjVkNDZiNzM2ZTAwNmQzZDZjYWJjZWVhZTBjYjJjODp7Il9hdXRoX3VzZXJfaGFzaCI6ImNlNTM5MzhjOWI0NGFlNmE1MjMzMzZhYTg1YTgwNGJjNzE5ODRiOTIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2018-06-19 19:42:46.011995'),('hrxv5tkl63vqtb84dp37drx2q6zk0ot0','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-06-19 22:04:31.122039'),('ki00zm1qzrrogaku9hb4ah13qq7k8sfu','NGMwMGM5Njg4YjVkNDZiNzM2ZTAwNmQzZDZjYWJjZWVhZTBjYjJjODp7Il9hdXRoX3VzZXJfaGFzaCI6ImNlNTM5MzhjOWI0NGFlNmE1MjMzMzZhYTg1YTgwNGJjNzE5ODRiOTIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2018-07-04 19:15:59.820393'),('l5titim8g16h1yd1pmfa5jyokjct2bma','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-07-23 23:11:56.213983'),('m2li68g515otomonrbezs3mfzab0qqlz','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-07-05 06:42:52.106989'),('mmangriepsb5xxgtwapj04amtjx4xt7z','NWEwNjhlOGJmZjgzNWE2OWQxMDY0NWMwMzFhOTkxMjQxZTcwYjJlODp7InVzZXJuYW1lIjoiZi5mYWxjb25zb3RvIiwicHJvamVjdCI6MzUsIm5hbWUiOiIgRlJBTkNJU0NPIEouIEZBTENPTiBTT1RPIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjEwMzQsInRva2VuIjoiZDE4NGMzMjRiOWE3NTdkNTBlMmFkMDM5MDg1NTkzNjlkMWUyYzA4YyIsIl9hdXRoX3VzZXJfaGFzaCI6ImU5YzkwMjFkM2ZkOGEwOGYyMzE1MmNjMjUwNDgxYjY4MDk2NGE4NGUiLCJ0eXBlIjoiQUwiLCJydXQiOiIxODYyNjAzNi04In0=','2018-06-22 22:45:22.994546'),('nmdqiuq52y6u81f4cuk6ppm5zeyuiyq0','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-06-25 17:28:19.632713'),('o0teas2khpni63jd6t7kzozohrkjuxkt','ZWY5YzRlOTBjMmIzMGFiMGEzYTIwMDZkZTZmMzFiNzMwOTdlZTY0MDp7InVzZXJuYW1lIjoiRWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-11-12 14:41:45.508311'),('oqz97ttdllhg02khaiyjhhfok8tb9bj6','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-06-25 23:42:25.571635'),('p55b241b5xne6dgrjye6ycqb57oy9qzv','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-07-27 15:57:10.220831'),('pwugjod125ua248pxrlgklcqc4pqklaq','NGMwMGM5Njg4YjVkNDZiNzM2ZTAwNmQzZDZjYWJjZWVhZTBjYjJjODp7Il9hdXRoX3VzZXJfaGFzaCI6ImNlNTM5MzhjOWI0NGFlNmE1MjMzMzZhYTg1YTgwNGJjNzE5ODRiOTIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2018-12-13 00:22:48.704655'),('rpjtdt8ukpd63k2dtz52qbz55wwdzpmk','NWEwNjhlOGJmZjgzNWE2OWQxMDY0NWMwMzFhOTkxMjQxZTcwYjJlODp7InVzZXJuYW1lIjoiZi5mYWxjb25zb3RvIiwicHJvamVjdCI6MzUsIm5hbWUiOiIgRlJBTkNJU0NPIEouIEZBTENPTiBTT1RPIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjEwMzQsInRva2VuIjoiZDE4NGMzMjRiOWE3NTdkNTBlMmFkMDM5MDg1NTkzNjlkMWUyYzA4YyIsIl9hdXRoX3VzZXJfaGFzaCI6ImU5YzkwMjFkM2ZkOGEwOGYyMzE1MmNjMjUwNDgxYjY4MDk2NGE4NGUiLCJ0eXBlIjoiQUwiLCJydXQiOiIxODYyNjAzNi04In0=','2018-08-15 20:22:29.955002'),('rqfkn2t38dk5knnm0rlzadfevu7egfx7','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-08-20 23:38:06.884264'),('rtml5gb1cbncyenaqdw0shwq6u8tgmdn','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-07-27 15:58:36.249526'),('t2ih3yvtw3252pchot0qvvf2vvggn8aq','NGMwMGM5Njg4YjVkNDZiNzM2ZTAwNmQzZDZjYWJjZWVhZTBjYjJjODp7Il9hdXRoX3VzZXJfaGFzaCI6ImNlNTM5MzhjOWI0NGFlNmE1MjMzMzZhYTg1YTgwNGJjNzE5ODRiOTIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2018-07-17 21:46:25.109438'),('tcdijos2ckkfijk0msr08vqcl6pefdtp','MmNkMTI5MmFjYjk3MzlmYWEzYjM0NTYxMGIwYmUwZTYzNmM1NzBiNTp7InVzZXJuYW1lIjoiYWxlamFuZHJvLnNwaWNoaWdlci5zdHVhcmRvIiwibmFtZSI6IkFsZWphbmRybyBTcGljaGlnZXIgU3R1YXJkbyIsIl9hdXRoX3VzZXJfaWQiOiIzIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoxMywidG9rZW4iOiI3N2E1NmZjZTAxYmQ0MGRiNWU4MjA0YTAyZDk2ZTIzMDU4ODg5ODAzIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTUyMDgyM2Q0YzVjNzhjYmUxMWM5ZjZhNWYxZjAzYmM2YTQ1ZDliYSIsInR5cGUiOiJETyIsInJ1dCI6IjE0MTM3OTgxLUsifQ==','2018-06-22 22:33:30.728844'),('uvl6olg3p2qgq6jc58e5ta1hzcig64vb','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-07-23 19:32:58.383507'),('v4kf1q43oko4ioog7r1n4p0wd1wdulxi','NGMwMGM5Njg4YjVkNDZiNzM2ZTAwNmQzZDZjYWJjZWVhZTBjYjJjODp7Il9hdXRoX3VzZXJfaGFzaCI6ImNlNTM5MzhjOWI0NGFlNmE1MjMzMzZhYTg1YTgwNGJjNzE5ODRiOTIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2018-07-23 19:31:30.617501'),('volyd9iev8iilftkivluirbcer17koz7','MmNkMTI5MmFjYjk3MzlmYWEzYjM0NTYxMGIwYmUwZTYzNmM1NzBiNTp7InVzZXJuYW1lIjoiYWxlamFuZHJvLnNwaWNoaWdlci5zdHVhcmRvIiwibmFtZSI6IkFsZWphbmRybyBTcGljaGlnZXIgU3R1YXJkbyIsIl9hdXRoX3VzZXJfaWQiOiIzIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoxMywidG9rZW4iOiI3N2E1NmZjZTAxYmQ0MGRiNWU4MjA0YTAyZDk2ZTIzMDU4ODg5ODAzIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTUyMDgyM2Q0YzVjNzhjYmUxMWM5ZjZhNWYxZjAzYmM2YTQ1ZDliYSIsInR5cGUiOiJETyIsInJ1dCI6IjE0MTM3OTgxLUsifQ==','2018-06-19 22:02:17.873224'),('vwpujyx780x293zvkb015t3q0qhrdhcx','ZDNiNTVjMmQyNGM3NTIzNWMyMDgwZTAwMjI0NWYxODE1NTcwZWJhZjp7InVzZXJuYW1lIjoiY2FybG9zLm5laXJhLmNhcnJhc2NvIiwibmFtZSI6IkNhcmxvcyBOZWlyYSBDYXJyYXNjbyIsIl9hdXRoX3VzZXJfaWQiOiIxOCIsImgiOjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidXNlcl9wayI6MTgsInRva2VuIjoiY2RlNjU2OTI2MTc5NGRkMWQwMmM4ZTQzNzI5MjQzNWE5ZTIyMDAwMyIsIl9hdXRoX3VzZXJfaGFzaCI6IjZjYzU3YjRiYTY1MGEzYTA4NGQ4ZDdhYjJkMTY2NmFiMzYyMjRhMjEiLCJ0eXBlIjoiRE8iLCJydXQiOiIxNDA2MTcwMC04In0=','2018-08-20 23:25:08.244381'),('wbizodv7sn3rbfsi6jzte5on9oozdcok','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-10-16 12:38:50.376363'),('wra7ff53rvvvy69g1k7e64atbjbz3lvl','NWEwNjhlOGJmZjgzNWE2OWQxMDY0NWMwMzFhOTkxMjQxZTcwYjJlODp7InVzZXJuYW1lIjoiZi5mYWxjb25zb3RvIiwicHJvamVjdCI6MzUsIm5hbWUiOiIgRlJBTkNJU0NPIEouIEZBTENPTiBTT1RPIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjEwMzQsInRva2VuIjoiZDE4NGMzMjRiOWE3NTdkNTBlMmFkMDM5MDg1NTkzNjlkMWUyYzA4YyIsIl9hdXRoX3VzZXJfaGFzaCI6ImU5YzkwMjFkM2ZkOGEwOGYyMzE1MmNjMjUwNDgxYjY4MDk2NGE4NGUiLCJ0eXBlIjoiQUwiLCJydXQiOiIxODYyNjAzNi04In0=','2018-07-23 19:18:33.746545'),('wru2o6qo340i52nsn4d41vhvu9f6486u','NTlmNzlmNjM3MDViNjQ3YzUzNTY4Nzk1MzM1MWVmMmNmMmRhNGZiYTp7InVzZXJuYW1lIjoiZWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-06-21 16:00:43.089260'),('wu6x5mthqxytk3ng5v5edsarvtz2f1ew','MmNkMTI5MmFjYjk3MzlmYWEzYjM0NTYxMGIwYmUwZTYzNmM1NzBiNTp7InVzZXJuYW1lIjoiYWxlamFuZHJvLnNwaWNoaWdlci5zdHVhcmRvIiwibmFtZSI6IkFsZWphbmRybyBTcGljaGlnZXIgU3R1YXJkbyIsIl9hdXRoX3VzZXJfaWQiOiIzIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoxMywidG9rZW4iOiI3N2E1NmZjZTAxYmQ0MGRiNWU4MjA0YTAyZDk2ZTIzMDU4ODg5ODAzIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTUyMDgyM2Q0YzVjNzhjYmUxMWM5ZjZhNWYxZjAzYmM2YTQ1ZDliYSIsInR5cGUiOiJETyIsInJ1dCI6IjE0MTM3OTgxLUsifQ==','2018-08-01 20:53:58.902407'),('y30n5nqzy5pbmhdhpf8k9eoa7kbd1ow9','MmNkMTI5MmFjYjk3MzlmYWEzYjM0NTYxMGIwYmUwZTYzNmM1NzBiNTp7InVzZXJuYW1lIjoiYWxlamFuZHJvLnNwaWNoaWdlci5zdHVhcmRvIiwibmFtZSI6IkFsZWphbmRybyBTcGljaGlnZXIgU3R1YXJkbyIsIl9hdXRoX3VzZXJfaWQiOiIzIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoxMywidG9rZW4iOiI3N2E1NmZjZTAxYmQ0MGRiNWU4MjA0YTAyZDk2ZTIzMDU4ODg5ODAzIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTUyMDgyM2Q0YzVjNzhjYmUxMWM5ZjZhNWYxZjAzYmM2YTQ1ZDliYSIsInR5cGUiOiJETyIsInJ1dCI6IjE0MTM3OTgxLUsifQ==','2018-06-19 21:13:25.992151'),('y59i2bg5wqo3iuoqmwrj0h4pxdifeny2','NWEwNjhlOGJmZjgzNWE2OWQxMDY0NWMwMzFhOTkxMjQxZTcwYjJlODp7InVzZXJuYW1lIjoiZi5mYWxjb25zb3RvIiwicHJvamVjdCI6MzUsIm5hbWUiOiIgRlJBTkNJU0NPIEouIEZBTENPTiBTT1RPIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJoIjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfcGsiOjEwMzQsInRva2VuIjoiZDE4NGMzMjRiOWE3NTdkNTBlMmFkMDM5MDg1NTkzNjlkMWUyYzA4YyIsIl9hdXRoX3VzZXJfaGFzaCI6ImU5YzkwMjFkM2ZkOGEwOGYyMzE1MmNjMjUwNDgxYjY4MDk2NGE4NGUiLCJ0eXBlIjoiQUwiLCJydXQiOiIxODYyNjAzNi04In0=','2018-07-09 19:41:27.910630'),('y5yuwc35h7wllhg00kzwfry3x3xm0h8d','ZDU0MjA2YzdkZTEwMTc4MTQ2NGY5YjdlZjk0NzhlODc0ZTViMzc2Yjp7InVzZXJuYW1lIjoiaHZpZGFsIiwibmFtZSI6IkhheWRlZSBWaWRhbCBaYW1icmFubyIsImgiOjAsInVzZXJfcGsiOjMsInRva2VuIjoiN2U1M2QzYjY5MTIyNDBmZGEzOWIyYTMyYTRmZjcxYTgwYzNkMzgzMyIsInR5cGUiOiJTRSIsInJ1dCI6IjEyNDEyNDEtNSJ9','2018-08-15 20:20:24.933821'),('ynfey7ctz7f5vdi1wms1gttfnngz06kt','ZWY5YzRlOTBjMmIzMGFiMGEzYTIwMDZkZTZmMzFiNzMwOTdlZTY0MDp7InVzZXJuYW1lIjoiRWR1YXJkby5xdWlyb2dhIiwibmFtZSI6IkVkdWFyZG8gUXVpcm9nYSBBZ3VpbGVyYSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiaCI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX3BrIjoyLCJ0b2tlbiI6IjlhNmNiNzFjN2JmMjBjMzE2NGY3M2UwYjViNzRlYjA0OGM1YzA5YjUiLCJfYXV0aF91c2VyX2hhc2giOiIyOTNmNDYxOGRkZTFlYzViMGViOWZlZjExM2FiMWQyY2U5ZDliZDcyIiwidHlwZSI6IkRDIiwicnV0IjoiMTUwMjEwODUtMiJ9','2018-08-09 16:40:51.617031');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_ausencia`
--

DROP TABLE IF EXISTS `practicas_ausencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_ausencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `cantidad_dias` int(11) DEFAULT NULL,
  `practica_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practicas_ausencia_practica_id_48bbabbd_fk_practicas_practica_id` (`practica_id`),
  CONSTRAINT `practicas_ausencia_practica_id_48bbabbd_fk_practicas_practica_id` FOREIGN KEY (`practica_id`) REFERENCES `practicas_practica` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_ausencia`
--

LOCK TABLES `practicas_ausencia` WRITE;
/*!40000 ALTER TABLE `practicas_ausencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_ausencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_comments`
--

DROP TABLE IF EXISTS `practicas_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` varchar(200) DEFAULT NULL,
  `texto` longtext,
  `fecha_pub` date NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practicas_comments_task_id_1aad3364_fk_practicas_task_id` (`task_id`),
  CONSTRAINT `practicas_comments_task_id_1aad3364_fk_practicas_task_id` FOREIGN KEY (`task_id`) REFERENCES `practicas_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_comments`
--

LOCK TABLES `practicas_comments` WRITE;
/*!40000 ALTER TABLE `practicas_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_empresa`
--

DROP TABLE IF EXISTS `practicas_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `rut` varchar(20) DEFAULT NULL,
  `tipo` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_empresa`
--

LOCK TABLES `practicas_empresa` WRITE;
/*!40000 ALTER TABLE `practicas_empresa` DISABLE KEYS */;
INSERT INTO `practicas_empresa` VALUES (2,'Cubesoa','76432613-K','Inactiva'),(3,'webtic','72456321-4','AC'),(4,'kibernum','96616770-K','Inactiva');
/*!40000 ALTER TABLE `practicas_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_empresa_funcionarios`
--

DROP TABLE IF EXISTS `practicas_empresa_funcionarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_empresa_funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `userprofile_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `practicas_empresa_funcionarios_empresa_id_97ea0255_uniq` (`empresa_id`,`userprofile_id`),
  KEY `practicas_empresa_userprofile_id_d01cfa78_fk_core_userprofile_id` (`userprofile_id`),
  CONSTRAINT `practicas_empresa_fu_empresa_id_9ab512cd_fk_practicas_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `practicas_empresa` (`id`),
  CONSTRAINT `practicas_empresa_userprofile_id_d01cfa78_fk_core_userprofile_id` FOREIGN KEY (`userprofile_id`) REFERENCES `core_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_empresa_funcionarios`
--

LOCK TABLES `practicas_empresa_funcionarios` WRITE;
/*!40000 ALTER TABLE `practicas_empresa_funcionarios` DISABLE KEYS */;
INSERT INTO `practicas_empresa_funcionarios` VALUES (2,2,8),(3,3,6);
/*!40000 ALTER TABLE `practicas_empresa_funcionarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_eval_final`
--

DROP TABLE IF EXISTS `practicas_eval_final`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_eval_final` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `presentacion_personal` varchar(1) NOT NULL,
  `asistencia` varchar(1) NOT NULL,
  `puntualidad` varchar(1) NOT NULL,
  `capacidad_adaptacion` varchar(1) NOT NULL,
  `relacion_compa` varchar(1) NOT NULL,
  `colaboracion` varchar(1) NOT NULL,
  `interes` varchar(1) NOT NULL,
  `proactividad` varchar(1) NOT NULL,
  `disposicion_trabajo` varchar(1) NOT NULL,
  `respeto_superior` varchar(1) NOT NULL,
  `aceptacion_instruccion` varchar(1) NOT NULL,
  `nivel_conocimientos` varchar(1) NOT NULL,
  `estudio` varchar(1) NOT NULL,
  `actitud_situaciones_nuevas` varchar(1) NOT NULL,
  `funciones_realizadas` longtext,
  `observaciones` longtext,
  `practica_id` int(11) NOT NULL,
  `status` varchar(20),
  PRIMARY KEY (`id`),
  UNIQUE KEY `practica_id` (`practica_id`),
  CONSTRAINT `practicas_eval_fin_practica_id_f9847d45_fk_practicas_practica_id` FOREIGN KEY (`practica_id`) REFERENCES `practicas_practica` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_eval_final`
--

LOCK TABLES `practicas_eval_final` WRITE;
/*!40000 ALTER TABLE `practicas_eval_final` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_eval_final` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_justificativo`
--

DROP TABLE IF EXISTS `practicas_justificativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_justificativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `observacion` longtext,
  `estado` int(11) NOT NULL,
  `ausencia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ausencia_id` (`ausencia_id`),
  CONSTRAINT `practicas_justific_ausencia_id_6a5e157c_fk_practicas_ausencia_id` FOREIGN KEY (`ausencia_id`) REFERENCES `practicas_ausencia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_justificativo`
--

LOCK TABLES `practicas_justificativo` WRITE;
/*!40000 ALTER TABLE `practicas_justificativo` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_justificativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_obs_director`
--

DROP TABLE IF EXISTS `practicas_obs_director`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_obs_director` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext,
  `eval_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `practicas_obs_director_eval_id_6d70b6c3_uniq` (`eval_id`),
  CONSTRAINT `practicas_obs_direct_eval_id_6d70b6c3_fk_practicas_eval_final_id` FOREIGN KEY (`eval_id`) REFERENCES `practicas_eval_final` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_obs_director`
--

LOCK TABLES `practicas_obs_director` WRITE;
/*!40000 ALTER TABLE `practicas_obs_director` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_obs_director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_practica`
--

DROP TABLE IF EXISTS `practicas_practica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_practica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `horas_diarias` int(11) NOT NULL,
  `dias_semana` int(11) NOT NULL,
  `cant_semanas` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `status` varchar(200) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `solicitud_id` (`solicitud_id`),
  CONSTRAINT `practic_solicitud_id_e916d09f_fk_practicas_solicitud_practica_id` FOREIGN KEY (`solicitud_id`) REFERENCES `practicas_solicitud_practica` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_practica`
--

LOCK TABLES `practicas_practica` WRITE;
/*!40000 ALTER TABLE `practicas_practica` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_practica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_publication`
--

DROP TABLE IF EXISTS `practicas_publication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `dias_semana` int(11) DEFAULT NULL,
  `horas_dias` int(11) DEFAULT NULL,
  `tipo` varchar(200) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practicas_publicatio_empresa_id_4d2e4988_fk_practicas_empresa_id` (`empresa_id`),
  CONSTRAINT `practicas_publicatio_empresa_id_4d2e4988_fk_practicas_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `practicas_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_publication`
--

LOCK TABLES `practicas_publication` WRITE;
/*!40000 ALTER TABLE `practicas_publication` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_respuestasolicitud`
--

DROP TABLE IF EXISTS `practicas_respuestasolicitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_respuestasolicitud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT NULL,
  `observacion` longtext,
  `date` date NOT NULL,
  `author` varchar(200) DEFAULT NULL,
  `emisor` varchar(200) NOT NULL,
  `solicitud_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practic_solicitud_id_46f14d51_fk_practicas_solicitud_practica_id` (`solicitud_id`),
  CONSTRAINT `practic_solicitud_id_46f14d51_fk_practicas_solicitud_practica_id` FOREIGN KEY (`solicitud_id`) REFERENCES `practicas_solicitud_practica` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_respuestasolicitud`
--

LOCK TABLES `practicas_respuestasolicitud` WRITE;
/*!40000 ALTER TABLE `practicas_respuestasolicitud` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_respuestasolicitud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_retro`
--

DROP TABLE IF EXISTS `practicas_retro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_retro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(20) NOT NULL,
  `author_name` varchar(250) DEFAULT NULL,
  `description` longtext NOT NULL,
  `tipo` varchar(2) DEFAULT NULL,
  `tarea_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practicas_retro_tarea_id_1ee9f277_fk_practicas_task_id` (`tarea_id`),
  CONSTRAINT `practicas_retro_tarea_id_1ee9f277_fk_practicas_task_id` FOREIGN KEY (`tarea_id`) REFERENCES `practicas_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_retro`
--

LOCK TABLES `practicas_retro` WRITE;
/*!40000 ALTER TABLE `practicas_retro` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_retro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_solicitud_practica`
--

DROP TABLE IF EXISTS `practicas_solicitud_practica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_solicitud_practica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `created` datetime(6) DEFAULT NULL,
  `observacion` longtext,
  `carrera` varchar(250) DEFAULT NULL,
  `date` date NOT NULL,
  `alumno_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `encargado_id` int(11) DEFAULT NULL,
  `step` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practicas_solicitud_practi_alumno_id_e0d80923_fk_core_student_id` (`alumno_id`),
  KEY `practicas_solicitud__empresa_id_e82121a5_fk_practicas_empresa_id` (`empresa_id`),
  KEY `practicas_solicitud_encargado_id_bad6e4af_fk_core_userprofile_id` (`encargado_id`),
  CONSTRAINT `practicas_solicitud__empresa_id_e82121a5_fk_practicas_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `practicas_empresa` (`id`),
  CONSTRAINT `practicas_solicitud_encargado_id_bad6e4af_fk_core_userprofile_id` FOREIGN KEY (`encargado_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `practicas_solicitud_practi_alumno_id_e0d80923_fk_core_student_id` FOREIGN KEY (`alumno_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_solicitud_practica`
--

LOCK TABLES `practicas_solicitud_practica` WRITE;
/*!40000 ALTER TABLE `practicas_solicitud_practica` DISABLE KEYS */;
INSERT INTO `practicas_solicitud_practica` VALUES (1,'Solicitudes/Doc1.docx','2018-10-29 20:00:18.330046','','1','2018-10-29',18,3,6,'DS');
/*!40000 ALTER TABLE `practicas_solicitud_practica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_task`
--

DROP TABLE IF EXISTS `practicas_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext,
  `status` int(11) NOT NULL,
  `semana_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practicas_task_semana_id_adeb005b_fk_practicas_week_practica_id` (`semana_id`),
  CONSTRAINT `practicas_task_semana_id_adeb005b_fk_practicas_week_practica_id` FOREIGN KEY (`semana_id`) REFERENCES `practicas_week_practica` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_task`
--

LOCK TABLES `practicas_task` WRITE;
/*!40000 ALTER TABLE `practicas_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas_week_practica`
--

DROP TABLE IF EXISTS `practicas_week_practica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas_week_practica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `tematica` varchar(250) DEFAULT NULL,
  `practica_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `practicas_week_pra_practica_id_1d3bb9c8_fk_practicas_practica_id` (`practica_id`),
  CONSTRAINT `practicas_week_pra_practica_id_1d3bb9c8_fk_practicas_practica_id` FOREIGN KEY (`practica_id`) REFERENCES `practicas_practica` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas_week_practica`
--

LOCK TABLES `practicas_week_practica` WRITE;
/*!40000 ALTER TABLE `practicas_week_practica` DISABLE KEYS */;
/*!40000 ALTER TABLE `practicas_week_practica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_abet`
--

DROP TABLE IF EXISTS `projects_abet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_abet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime(6) DEFAULT NULL,
  `last_mod` datetime(6) DEFAULT NULL,
  `carreer` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_abet_author_id_085ccc63_fk_auth_user_id` (`author_id`),
  CONSTRAINT `projects_abet_author_id_085ccc63_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_abet`
--

LOCK TABLES `projects_abet` WRITE;
/*!40000 ALTER TABLE `projects_abet` DISABLE KEYS */;
INSERT INTO `projects_abet` VALUES (3,'2018-05-15 13:28:43.384000','2018-05-15 13:28:43.384000',1,2);
/*!40000 ALTER TABLE `projects_abet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_abetevaluation`
--

DROP TABLE IF EXISTS `projects_abetevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_abetevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(6) DEFAULT NULL,
  `final_score` int(11) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_abetevaluatio_author_id_91ebce98_fk_core_userprofile_id` (`author_id`),
  KEY `projects_abetevaluati_project_id_d14858df_fk_projects_project_id` (`project_id`),
  CONSTRAINT `projects_abetevaluati_project_id_d14858df_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `projects_abetevaluatio_author_id_91ebce98_fk_core_userprofile_id` FOREIGN KEY (`author_id`) REFERENCES `core_userprofile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_abetevaluation`
--

LOCK TABLES `projects_abetevaluation` WRITE;
/*!40000 ALTER TABLE `projects_abetevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_abetevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_abetrubric`
--

DROP TABLE IF EXISTS `projects_abetrubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_abetrubric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(140) NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `carreer` int(11) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `nota_suf` int(11) DEFAULT NULL,
  `nota_enpr` int(11) DEFAULT NULL,
  `nota_inci` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_abetrubric_author_id_a16f7fe7_fk_core_userprofile_id` (`author_id`),
  CONSTRAINT `projects_abetrubric_author_id_a16f7fe7_fk_core_userprofile_id` FOREIGN KEY (`author_id`) REFERENCES `core_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_abetrubric`
--

LOCK TABLES `projects_abetrubric` WRITE;
/*!40000 ALTER TABLE `projects_abetrubric` DISABLE KEYS */;
INSERT INTO `projects_abetrubric` VALUES (2,'ABET inicial','2018-07-24 19:37:10.694523',1,'AINS2401',7,5,3,1);
/*!40000 ALTER TABLE `projects_abetrubric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_abetrubric_outcomes`
--

DROP TABLE IF EXISTS `projects_abetrubric_outcomes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_abetrubric_outcomes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abetrubric_id` int(11) NOT NULL,
  `outcome_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_abetrubric_outcomes_abetrubric_id_d0d20e0a_uniq` (`abetrubric_id`,`outcome_id`),
  KEY `projects_abetrubric_o_outcome_id_0dd2fb69_fk_projects_outcome_id` (`outcome_id`),
  CONSTRAINT `projects_abetru_abetrubric_id_1be261fc_fk_projects_abetrubric_id` FOREIGN KEY (`abetrubric_id`) REFERENCES `projects_abetrubric` (`id`),
  CONSTRAINT `projects_abetrubric_o_outcome_id_0dd2fb69_fk_projects_outcome_id` FOREIGN KEY (`outcome_id`) REFERENCES `projects_outcome` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_abetrubric_outcomes`
--

LOCK TABLES `projects_abetrubric_outcomes` WRITE;
/*!40000 ALTER TABLE `projects_abetrubric_outcomes` DISABLE KEYS */;
INSERT INTO `projects_abetrubric_outcomes` VALUES (5,2,14),(6,2,15);
/*!40000 ALTER TABLE `projects_abetrubric_outcomes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_crit_info`
--

DROP TABLE IF EXISTS `projects_crit_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_crit_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suficient` longtext NOT NULL,
  `inprocess` longtext NOT NULL,
  `inscipient` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_crit_info`
--

LOCK TABLES `projects_crit_info` WRITE;
/*!40000 ALTER TABLE `projects_crit_info` DISABLE KEYS */;
INSERT INTO `projects_crit_info` VALUES (1,'listoko','medio medio','malito'),(2,'bueno','medio','malo'),(3,'1. Comprende como aplicar técnicas formales al desarrollo y mantenimiento de software. \n2. Representa comportamientos y estructuras del software empleando notaciones estándares propios de la profesión (por ejemplo, UML,  máquinas de estados finitos)\n3. Mide y analiza las principales propiedades del software de acuerdo con los requerimientos que lo definen (atributos de calidad interna, externa y en uso)\n4. Mide y analiza las principales propiedades de un proyecto de desarrollo de software (por ejemplo, esfuerzo, costo, tiempo, etc.)','1. Demuestra conocimientos sobre las técnicas formales aunque estas no las aplica de manera sistemática al desarrollo de software.\n2. Conoce estándares de representación del software pero no demuestra uso sistematizado.\n3. Reconoce las principales propiedades de un producto software, esencialmente los aspectos relacionados a los requerimientos no funcionales.\n4. Reconoce las principales propiedades de un proyecto de desarrollo de software.','1. No demuestra conocimiento de técnicas formales para desarrollar productos software.\n2. Conoce sobre estándares relacionados al software pero no los aplica al desarrollo de software.\n3. Aunque reconoce los requerimientos de un producto software no demuestra capacidad para definir atributos (de calidad) de un producto software a partir de los requerimientos.\n4. No demuestra un conocimiento sobre que puede ser medido sobre un proyecto de desarrollo de software.'),(4,'1. Emplea de manera apropiada al menos dos lenguajes de programación para la construcción de software.\n2. Aplica principios fundamentales de programación para la construcción de componentes y productos software.\n3. Aplica principios de diseño al desarrollo de estructura y algoritmos de los componentes que conforman un producto software.\n4. Aplica técnicas y principios de inteligencia artificial a la solución de problemas no procedurales.','1. Demuestra dominio en al menos un lenguaje de programación aplicándolo de manera efectiva en la construcción de software complejo y demuestra dominio de los elementos fundamentales de almenos un lenguaje adicional.\n2. Comprende los principios fundamentales de programación y su aplicación no es sistemática durante la construcción de software.\n3. Comprende los principios de diseño al desarrollo de estructura y algoritmos de componentes,  su aplicación no es sistemática durante el diseño de software.\n4. Comprende las técnicas de inteligencia artificial.','1. Demuestra conocimientos básicos en al menos dos lenguajes de programación aunque no posee las habilidades suficientes para aplicarlos al desarrollo de un software complejo.\n2. Conoce los principios fundamentales de programación. Las habilidades adquiridas no le permite una aplicación sistemática durante la construcción de software.\n3. Conoce los principios de diseño requeridos para el desarrollo de estructura y algoritmos de componentes. Las habilidades requerida para la aplicación no es sistemática durante el diseño.\n4. Conoce los fundamentos de las técnicas de inteligencia artificial.'),(5,'1. Identifica y recolecta los requerimientos de software que representan completamente las necesidades del usuario. \n2. Diseña sistemas software acorde con todos los requerimientos comprometidos.','1. Identifica y recolecta requerimientos que definen las principales necesidades del usuario, aunque no demuestran ser completos.\n2. Elabora diseños para satisfacer los principales requerimientos del software.','1. Se centra fundamentalmente en los requerimientos funcionales, aunque estos no demuestran una cobertura de las necesidades del usuario.\n2. Elabora diseños básicos no alineados o poco alineados con los requerimientos establecidos al software.'),(6,'1. Diseña pruebas que verifiquen los requerimientos funcionales y no funcionales (performance, usabilidad, disponibilidad, etc.)\n2. Diseña pruebas unitarias sobre los componentes del software.\n3. Diseña modelos de aseguramiento de la calidad de procesos del software, estableciendo de manera correcta el perfil objetivo de los procesos, de acuerdo con al menos un modelo de aseguramiento de calidad de procesos.','1. Diseña pruebas que verifiquen fundamentalmente los requerimientos funcionales pero no cubren totalmente los no funcionales (performance, usabilidad, disponibilidad, etc.)\n2. Diseña pruebas unitarias sobre algunos de los componentes del software.\n3. Diseña modelos de aseguramiento de la calidad de procesos del software, pero no demuestra un completo entendimiento del perfil objetivo de los procesos.','1. Diseña pruebas que verifiquen fundamentalmente los requerimientos funcionales.\n2. Diseña pruebas unitarias esencialmente sobre componentes básicos del software, sin centrar el foco en aquellos que concentran la lógica central del procesamiento.\n3. No demuestra un entendimiento del perfil objetivo de los procesos y de los modelos de aseguramiento de la calidad.'),(7,'1. Implementa pruebas funcionales y no funcionales al software de acuerdo con el diseño elaborado.\n2. Implementa pruebas unitarias sobre los componentes del software de acuerdo con el diseño elaborado.\n3. Realiza depuraciones sobre los programas construidos.\n4. Mide los principales atributos de un proyecto de desarrollo de software (costo, duración, etc.)\n5. Mide los principales atributos relacionados a procesos del desarrollo de software.','1. Implementa pruebas funcionales pero tiene dificultad para desarrollar las pruebas no funcionales al software de acuerdo con el diseño elaborado.\n2. Implementa pruebas unitarias sobre algunos de los componentes del software de acuerdo con el diseño elaborado.\n3. Realiza depuraciones sobre los programas construidos.\n4. Mide solo algunos atributos de un proyecto de desarrollo de software (costo, duración, etc.), pero que no permiten asegurar un control completo de la ejecución del proyecto.\n5. Mide algunos de los atributos relacionados a procesos del desarrollo de software, de acuerdo con el modelo de calidad seleccionado, centrado en los niveles más bajos del modelo de calidad.','1. Demuestra niveles básicos de implementa pruebas funcionales.\n2. Estructura la implementación de pruebas unitarias sobre los componentes del software pero no logra completar implementaciones de los casos de prueba.\n3. No demuestra habilidad para depurar los programas construidos.\n4. No es capaz de medir los atributos de un proyecto de desarrollo de software (costo, duración, etc.) para asegurar el control.\n5. No reconoce los principales atributos de calidad de los procesos.'),(8,'1. Analiza e interpreta los resultados generados durante las pruebas funcionales del software.\n2. Analiza e interpreta los resultados de las pruebas no funcionales (performance) realizadas al software.\n3. Analiza y concluye sobre los resultades de las mediciones de los atributos de un proyecto de desarrollo de software.\n4. Analizar y concluye sobre los resultados de las mediciones de realizadas a los procesos.','1. Demuestra capacidad de análisis de los resultados generados durante las pruebas funcionales del software, pero no sintetiza los resultados en un informe de pruebas.\n2. Analiza los resultados de las pruebas no funcionales (performance) realizadas al software, pero no sintetiza los resultados en un informe de pruebas.\n3. Analiza los resultades de las mediciones de los atributos de un proyecto de desarrollo de software, pero no sintetiza y aplica al control del proyecto.\n4. Analizar los resultados de las mediciones de realizadas a los procesos y sintetiza un conjunto de acciones para asegurar la mejora.','1. No es capaz de analizar e interpretar los resultados generados durante las pruebas funcionales del software.\n2. No es capaz de analizar los resultados de las pruebas no funcionales (performance) realizadas al software.\n3. No es capaz de analizar los resultades de las mediciones de los atributos de un proyecto de desarrollo de software.\n4. No es capaz analizar los resultados de las mediciones de realizadas a los procesos y sintetizar acciones para asegurar la mejora.'),(9,'1. Diseño componentes software al nivel detallado o bajo nivel, empleando estándares, principios y buenas prácticas de diseño propios de la profesión.\n2. Diseña la arquitectura de un producto software satisfaciendo los principales atributos de calidad.','1. Diseña componentes software al nivel detallado. La aplicación de estándares no es sistematica en el diseño.\n2. Diseña algunos aspectos de la arquitectura no cubriendo todos los atributos de calidad.','1. Desarrolla diseños incompletos de los componentes del software. No demuestra conocimiento de los estándares para el diseño (UML), los principios de diseño y buenas prácticas (patrones).\n2. No reconoce los atributos de calidad y el diseño de desiciones arquitecturales que aseguren los atributos de calidad del producto.'),(10,'1. Diseña un proyecto de desarrollo de software de acuerdo con las restricciones de tiempo establecidas.\n2. Sustenta el diseño de un proyecto de desarrollo de software asegurando su aprobación para la ejecución.','1. Diseña un proyecto de desarrollo de software de acuerdo con las restricciones de tiempo establecidas. Los objetivos establecidos no se asegura sean cumplidos completamente en las restricciones de tiempo.\n2. Sustenta el diseño de un proyecto de desarrollo de software asegurando su aprobación para la ejecución.','1. Diseña un proyecto de desarrollo de software aunque no refleja en el diseño el tratamiento correcto de las restricciones de tiempo, estableciendo un alcance apropiado. Los objetivos establecidos no se asegura sean cumplidos en las restricciones de tiempo.\n2. No es capaz de sustenta el diseño del proyecto de desarrollo de software.'),(11,'1. Modela y establece procesos relacionados al desarrollo de software. Asegura que las variaciones necesarias a los procesos y metodología se encuentren completamente reflejados en el Plan del Proyecto o las necesidades de la organización en la que trabaja.\n2. Adopta y adapta procesos de acuerdo con los requerimientos y características propias del proyecto.','1. Selecciona/adopta procesos para el desarrollo del proyecto, asegurando que la metodología sea la apropiada, pero no establece con claridad las variaciones que se requieren de acuerdo con las condiciones del proyecto.','1. No demuestra habilidades para modelar procesos para el desarrollo del software.\n2. Aunque adopta procesos, no se asegura que estos sean los apropiados de acuerdo con los requerimientos del proyecto y la organización.'),(12,'1. Lidera equipo de proyecto asegurando que en conjunto se logren los objetivos establecidos.\n2. Demuestra capacidad para el trabajo con alumnos de otras carreras (en especial con alumnos de Ing. de Sistemas de Información)\n3. Demuestra capacidad para trabajar de manera coordinada con otros equipos de proyectos.','1. Se constituye en miembro de un equipo de proyecto estableciendo liderazgo desde el rol que le toca desempeñar. Desarrolla su propio Plan de Trabajo para asegurar el logro de los compromisos asumidos.\n2. Es capaz de trabajar con alumnos de otras carreras (en especial con alumnos de Ing. de Sistemas de Información).','1. Aunque participa en equipos de proyecto no demuestra ser proactivo. Trabaja de acuerdo con un Plan de Trabajo o asignaciones elaboradas por terceros.\n2. Trabaja de manera individual, dificultándose poder colaborar con alumnos de otras carreras (en especial con alumnos de Ing. de Sistemas de Información)'),(13,'1. Demuestra conocimientos sólidos en almenos un dominio de aplicación por medio de la fundamentación teórica de las soluciones desarrolladas con conocimientos propio del dominio de aplicación de su proyecto.\n2. Demuestra a través de análisis de fuentes propias del dominio el sustento teórico del tema objeto de estudio en el proyecto.','1. Demuestra conocimientos sólidos en almenos un dominio de aplicación por medio de lafundamentación teórica de las soluciones desarrolladas con conocimientos propio del dominio de aplicación de su proyecto.\n2. Las fuentes utilizadas aunque son empleadas en el fundamento teórico no permiten la cobertura completa del tema objeto de estudio en el dominio de aplicación.','1. Demuestra conocimientos incipientes los que no le permiten abordar la solución de problemas de ingeniería dentro del dominio de aplicación donde actúa en el proyecto.\n2. El trabajo con fuentes es incipiente o casi nulo, por lo que no puede fundamentar consistentemente el problema de ingeniería que se busca abordar dentro del dominio de aplicación.'),(14,'1. Identifica y sustenta las necesidades, oportunidades o problemas de negocio a ser resueltas a través de la práctica de la ingeniería por medio de un proyecto.\n2. Identifica aspectos normativos, regulatorios, leyes, estándares u otras restricciones o condiciones que deben ser cubiertas al abordar la necesidad, oportunidad o problema de negocio.\n3. Identifica las causas que originan la necesidad u oportunidad de desarrollo de solución de ingeniería.','1. Identifica necesidades, oportunidades o problemas de negocio que pueden ser resueltas a través de la práctica de la ingeniería.\n2. Las causas identificadas no sustentan completamente el problema, oportunidad o necesidad de negocio identificados.\n3. Los aspectos normativos, regultatorios, leyes, estándares u otras restricciones o condiciones no son completas de acuerdo con el problema, oportunidad o necesidad de negocio cubierta.','1. No se identifica con claridad la necesidad, oportunidad o problema a ser cubierto a través de un proyecto de ingeniería.\n2. No hay claridad sobre la identificación de las causas que motivan el tratamiento de la necesidad, oportunidad o problema.\n3. No es suficiente la identificación de restricciones (norma, leyes, regulaciones o estándares propios del dominio) aplicables al problema o necesidad de negocio.'),(15,'1. Formula y documenta de manera apropiada restricciones sobre los productos software (trade-offs) de acuerdo con los Requerimientos No Funcionales (performance, usabilidad, seguridad, mantenibilidad, etc.)\n2. Formula y documenta de manera apropiada los atributos principales de los riesgos identificados en el proyecto.','1. Formula las restricciones que definen el problema a ser resuelto a través de una solución software (requerimientos funcionales y no funcionales) y los documenta. \n2. No se documenta de manera completa el problema, necesidad u oportunidad identicado.\n3. Las fuentes empleadas para documentar el problema no es suficiente para demostrar una completa comprensión del problema o necesidad a ser resuelto y las causas que lo originan.','1. No es capaz de formular la necesidad u oportunidad de negocio que sirve de base para establecer un proyecto típico de ingeniería.\n2. No hay suficiencia, claridad y precisión en las restricciones que establecen las condiciones bajo las que debe darse una solución al problema u oportundiad de negocio.\n3. No hay claridad en la documentación de las causas que producen la necesidad u oportunidad de negocio que debe ser cubierta por el proyecto de ingeniería.'),(16,'1. Actúa de manera proactiva resolviendo problemas ante riesgos no identificados en su proyecto.\n2. Ejecuta planes de contingencias ante la materialización de los riesgos identificados en el proyecto.\n3. Desarrolla soluciones sustentadas en base al problema o necesidad identificado y documento, las restricciones, causas que dan origen al problema o necesidad, eliminando dichas causas y entregando una solución de ingeniería apropiada para cada caso problema.','1. Demuestra ser reactivo ante los problemas que se presentan durante el desarrollo del proyecto, activando planes de contingencia definidos.\n2. Desarrolla soluciones al problema o necesidad identificado y documentado, las restricciones y causas que originan el problema o necesidad de negocio. La solución no demuestra una completa cobertura del problema y las restricciones identificadas y documentadas.','1. No demuestra capacidad de reacción y atención a problemas que se presentan durante un proyecto de ingeniería.\n2. No desarrolla soluciones a problemas o necesidades de negocio que cumplan con los requerimientos identificados y documentados, o da soluciones incompletas concentradas esencialmente en problemas de menor nivel que no resuelven las causas principales que generan el problema o necesidad de negocio.'),(17,'1. Demuestra comprensión del código de ética propio de su profesión.\n2. Emite juicios sobre situaciones donde el código de ética puede verse vulnerado.\n3. Demuestra respecto por los derechos de propiedad intelectual.','1. Conoce el código de ética de su profesión.\n2. Demuestra respeto por los derechos de propiedad intelectual.','1. Demuestra desconocimiento del código de ética de la profesión.\n2. No demuestra respeto por los derechos de propiedad intelectual en su actuación profesional (cursos, proyectos, etc.)'),(18,'1. Cumple con las responsabilidades asumidas y los planes comprometidos en los proyectos en los que participa.\n2. Ante eventos que ponen en riesgo el cumplimiento de los compromisos asumidos negocia con los interesados los niveles de cumplimientos de los objetivos establecidos.','1. Cumple con las responsabilidades asumidas y los planes comprometidos en los proyectos en los que participa.\n2. Es reactivo ante eventos que ponen el riesgo el cumplimiento de los compromisos asumidos, aunque participa de la negociación cuando se ven afectados los compromisos o hitos establecidos, lo realiza ante solicitud de los interesados afectados.','1. No demuestra ser responsable ante los compromisos asumidos.\n2. No toma acciones cuando suceden eventos que afectan el cumplimiento de los compromisos, hitos, plazos.'),(19,'1. Se comunica empleando códigos apropiados de acuerdo con la audiencia a la que se dirige.\n2. Emplea los medios audiovisuales apropiados de acuerdo con el mensaje que se busca transmitir.\n3. Enfoca su comunicación hacia los objetivos específicos a los que se orienta cada presentación de resultados.\n4. Escucha a otros de manera objetiva antes de emitir su juicio crítico y objetivos.','1. No es sistemático en el empleo de códigos de comunicación apropiados de acuerdo con la audiencia a la que se dirige.\n2. Emplea medios audiovisuales para transmitir el mensaje de acuerdo con el público objetivo, pero debe mejorar la efectividad del uso de los medios.\n3. Cubre solo parte de los objetivos específicos a los que se orienta la presentación que realiza, sobre los resultados que demuestren el logro de los objetivos.\n4. Escucha y emite juicios críticos.','1. No realiza un empleo de códigos de comunicación apropiados de acuerdo con la audiencia a la que se dirige.\n2. No hace un uso apropiado de medios audiovisuales para transmitir el mensaje de acuerdo con el público objetivo.\n3. En las presentaciones que realiza no se centra el los objetivos específicos a los que se orienta la presentación que realiza, sobre los resultados que demuestren el logro de los objetivos.\n4. No escucha antes de emitir juicios críticos sobre los puntos de vista de terceros.'),(20,'1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la efectividad en la transmisión del mensaje.\n3. Demuestra capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza de forma sistemática un lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo, aunque se advierten errores menores de redacción.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la comunicar la idea principal del mensaje.\n3. debe mejorar la capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos donde se advierten problemas serios de ortografía y redacción.\n2. Prepara presentaciones de los resultados del trabajo pero estas no aseguran comunicar la idea principal del mensaje.\n3. No posee capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo o esta capacidad es incipiente.\n4. El lenguaje y medios utilizados no es acorde con el público objetivo al que se dirige.'),(21,'1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la efectividad en la transmisión del mensaje.\n3. Demuestra capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza de forma sistemática un lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo, aunque se advierten errores menores de redacción.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la comunicar la idea principal del mensaje.\n3. debe mejorar la capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos donde se advierten problemas serios de ortografía y redacción.\n2. Prepara presentaciones de los resultados del trabajo pero estas no aseguran comunicar la idea principal del mensaje.\n3. No posee capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo o esta capacidad es incipiente.\n4. El lenguaje y medios utilizados no es acorde con el público objetivo al que se dirige.'),(22,'1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la efectividad en la transmisión del mensaje.\n3. Demuestra capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza de forma sistemática un lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo, aunque se advierten errores menores de redacción.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la comunicar la idea principal del mensaje.\n3. debe mejorar la capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos donde se advierten problemas serios de ortografía y redacción.\n2. Prepara presentaciones de los resultados del trabajo pero estas no aseguran comunicar la idea principal del mensaje.\n3. No posee capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo o esta capacidad es incipiente.\n4. El lenguaje y medios utilizados no es acorde con el público objetivo al que se dirige.'),(23,'1. Analiza la factibilidad de los proyectos que formula, incluyendo factores tales como tiempos, recursos y costos.','1. Analiza la factibilidad de los proyectos que formula, aunque no incluye todos los factores que afectan la ejecución del proyecto (costos, tiempos, recursos)','1. No es capaz de determinar la factibilidad de los proyectos que formula, demostrando que no puede identificar factores relacionados con costos, tiempos, esfuerzo, recursos, entre otros.'),(24,'1. Comprende el impacto que genera las soluciones software que produce en el dominio de aplicación al que aplica.\n2. Reconoce el papel que juegan los productos software en el contexto económico actual.','1. Demuestra poco conocimiento del impacto de las soluciones software en el dominio de aplicación donde realiza su proyecto.\n2. Demuestra poco entendimiento del papel que juegan las soluciones que produce en el contexto económico actual en la empresa u organización para la que se desarrolla el trabajo.','1. No demuestra conocimiento del impacto de las soluciones software en el dominio de aplicación donde realiza su proyecto.\n2. No demuestra entendimiento del papel que juegan las soluciones que produce en el contexto económico actual en la empresa u organización para la que se desarrolla el trabajo.'),(25,'1. Comprende el impacto que generan las soluciones software en la sociedad actual, modificando o acentuando hábitos nuevos de consumo de la tecnología.\n2. Comprende el impacto de las soluciones software en asegurar mejorar la calidad de vida o de las condiciones de trabajo de las personas y de los negocios.','1. Demuestra poca comprensión del impacto que generan las soluciones software en la sociedad actual, modificando o acentuando hábitos nuevos de consumo de la tecnología.\n2. Demuestra poca comprensión del impacto de las soluciones software en asegurar mejorar la calidad de vida o de las condiciones de trabajo de las personas y de los negocios.','1. No demuestra comprensión del impacto que generan las soluciones software en la sociedad actual, modificando o acentuando hábitos nuevos de consumo de la tecnología.\n2. No demuestra comprensión del impacto de las soluciones software en asegurar mejorar la calidad de vida o de las condiciones de trabajo de las personas y de los negocios.'),(26,'1. Demuestra capacidad para asimilar nuevas tecnologías (frameworks, lenguajes de programación o variantes, etc.).\n2. Demuestra capacidad para asimilar nuevos conocimientos metodológicos y de procesos.\n3. Demuestra capacidad para asimilar conocimientos en almenos un dominio de aplicación.\n4. Demuestra capacidad para asimilar nuevas técnicas y principios propios de la ingeniería de software en la medida que estos emergen.\n5. Demuestra dominio de técnicas o métodos para la investigación.','1. Demuestra habilidades básicas para asimilar nuevas tecnologías (frameworks, lenguajes de programación o variantes, etc.).\n2. Demuestra habilidades básicas para asimilar nuevos conocimientos metodológicos y de procesos.\n3. Demuestra habilidades básicas para asimilar conocimientos en almenos un dominio de aplicación.\n4. Demuestra habilidades básicas para asimilar nuevas técnicas y principios propios de la ingeniería de software en la medida que estos emergen.\n5. Demuestra conocimientos básicos sobre técnicas o métodos para la investigación.','1. No demuestra capacidad para asimilar nuevas tecnologías (frameworks, lenguajes de programación o variantes, etc.).\n2. No demuestra capacidad para asimilar nuevos conocimientos metodológicos y de procesos.\n3. No demuestra las habilidades para poder asimilar conocimientos en almenos un dominio de aplicación.\n4. No demuestra capacidad para asimilar nuevas técnicas y principios propios de la ingeniería de software en la medida que estos emergen.\n5. El conocimiento y habilidades de técnicas o métodos para la investigación no le permiten poder estructurar y conducir la investigación sobre un tema de interés particular.'),(27,'1. Identifica áreas de interés dentro de la profesión donde le interesaría desarrollarse luego de graduarse.\n2. Define un plan de crecimiento profesional alineado con las áreas de interés dentro de la profesión.','1. Aunque reconoce la necesidad de mantenerse actualizado no demuestra capacidad para identificar áreas de interés dentro de la profesión donde podría desarrollarse luego de graduarse.\n2. Demuestra dificultad para preparar un plan de crecimiento profesional alineado con las áreas de interés dentro de la profesión.','1. No demuestra capacidad para poder identificar áreas de interés dentro de la profesión donde le interesaría desarrollarse luego de graduarse.\n2. No es capaz de elaborar un plan de crecimiento profesional alineado con las áreas de interés dentro de la profesión.'),(28,'1. Comprende las competencias que definen el perfil profesional de la carrera.\n2. Comprende las diferencias entre su carreras y otras carreras dentro del campo de la computación e ingeniería.\n3. Reconoce el papel e importancia de las sociedades profesionales, en especial la IEEE Computer Society y ACM.','1. Conoce las competencias que definen el perfil profesional de la carrera.\n2. Conoce las diferencias entre su carreras y otras carreras dentro del campo de la computación e ingeniería.\n3. Conoce las principales sociedades profesionales de su profesión (por ejemplo, IEEE Computer Society y ACM)','1. No demuestra conocimiento sobre el perfil profesional de la carrera.\n2. No demuestra conocimiento sobre su carrera y otras carreras dentro del campo de computación o afines.\n3. No reconoce el papel y la importancia de las sociedades profesionales, en especial la IEEE Computer Society y ACM, para su profesión.'),(29,'1. Conoce y emplea métodos modernos para el trabajo colaborativo en equipo de proyectos.\n2. Conoce y emplea métodos y herramientas que faciliten la comunicación con los diferentes interesados en un equipo de proyecto','1. Conoce sobre métodos modernos para el trabajo colaborativo en equipo de proyectos.\n2. Conoce sobre métodos y herramientas que faciliten la comunicación con los diferentes interesados en un equipo de proyecto','1. No demuestra conocimiento y habilidades en métodos modernos para el trabajo colaborativo en equipo de proyectos.\n2. No demuestra conocimiento y habilidades en la aplicación de métodos y herramientas que faciliten la comunicación con los diferentes interesados en un equipo de proyecto'),(30,'1. Utiliza la metodología y proceso de acuerdo con las características propias del proyecto.\n2. Adapta la metodología o proceso de acuerdo con las características propias del proyecto y los estándares y restricciones establecidos.','1. Utiliza solo algunos elementos de las metodologías y procesos para el desarrollo de proyectos, más no se definen los criterios utilizados para excluir aspectos de los procesos y metodología utilizados.','1. Demuestra dificultad para aplicar un enfoque metodológico al desarrollo de proyectos.\n2. Cuando aplica metodología o proceso no toma en consideración las características propias del proyecto, no sustentando el porqué de la metodología o procesos utilizados.'),(31,'1. Conoce diversas herramientas de apoyo a los procesos de desarrollo de software.\n2. Aplica de manera informada las herramientas apropiadas a los procesos establecidos en un proyecto típico de desarrollo de software.','1. Conoce diversas herramientas de apoyo a los procesos de desarrollo de software.\n2. Aplica herramientas solo en algunas áreas de procesos establecidos en un proyecto típico de desarrollo de software.','1. Conoce diversas herramientas de apoyo a los procesos de desarrollo de software.\n2. No demuestra habilidades prácticas de aplicación de herramientas a los procesos definidos en un proyecto típico de desarrollo de software.'),(32,'1. Comprende como aplicar técnicas formales al desarrollo y mantenimiento de software. \n2. Representa comportamientos y estructuras del software empleando notaciones estándares propios de la profesión (por ejemplo, UML,  máquinas de estados finitos)\n3. Mide y analiza las principales propiedades del software de acuerdo con los requerimientos que lo definen (atributos de calidad interna, externa y en uso)\n4. Mide y analiza las principales propiedades de un proyecto de desarrollo de software (por ejemplo, esfuerzo, costo, tiempo, etc.)','1. Demuestra conocimientos sobre las técnicas formales aunque estas no las aplica de manera sistemática al desarrollo de software.\n2. Conoce estándares de representación del software pero no demuestra uso sistematizado.\n3. Reconoce las principales propiedades de un producto software, esencialmente los aspectos relacionados a los requerimientos no funcionales.\n4. Reconoce las principales propiedades de un proyecto de desarrollo de software.','1. No demuestra conocimiento de técnicas formales para desarrollar productos software.\n2. Conoce sobre estándares relacionados al software pero no los aplica al desarrollo de software.\n3. Aunque reconoce los requerimientos de un producto software no demuestra capacidad para definir atributos (de calidad) de un producto software a partir de los requerimientos.\n4. No demuestra un conocimiento sobre que puede ser medido sobre un proyecto de desarrollo de software.'),(33,'1. Emplea de manera apropiada al menos dos lenguajes de programación para la construcción de software.\n2. Aplica principios fundamentales de programación para la construcción de componentes y productos software.\n3. Aplica principios de diseño al desarrollo de estructura y algoritmos de los componentes que conforman un producto software.\n4. Aplica técnicas y principios de inteligencia artificial a la solución de problemas no procedurales.','1. Demuestra dominio en al menos un lenguaje de programación aplicándolo de manera efectiva en la construcción de software complejo y demuestra dominio de los elementos fundamentales de almenos un lenguaje adicional.\n2. Comprende los principios fundamentales de programación y su aplicación no es sistemática durante la construcción de software.\n3. Comprende los principios de diseño al desarrollo de estructura y algoritmos de componentes,  su aplicación no es sistemática durante el diseño de software.\n4. Comprende las técnicas de inteligencia artificial.','1. Demuestra conocimientos básicos en al menos dos lenguajes de programación aunque no posee las habilidades suficientes para aplicarlos al desarrollo de un software complejo.\n2. Conoce los principios fundamentales de programación. Las habilidades adquiridas no le permite una aplicación sistemática durante la construcción de software.\n3. Conoce los principios de diseño requeridos para el desarrollo de estructura y algoritmos de componentes. Las habilidades requerida para la aplicación no es sistemática durante el diseño.\n4. Conoce los fundamentos de las técnicas de inteligencia artificial.'),(34,'1. Identifica y recolecta los requerimientos de software que representan completamente las necesidades del usuario. \n2. Diseña sistemas software acorde con todos los requerimientos comprometidos.','1. Identifica y recolecta requerimientos que definen las principales necesidades del usuario, aunque no demuestran ser completos.\n2. Elabora diseños para satisfacer los principales requerimientos del software.','1. Se centra fundamentalmente en los requerimientos funcionales, aunque estos no demuestran una cobertura de las necesidades del usuario.\n2. Elabora diseños básicos no alineados o poco alineados con los requerimientos establecidos al software.'),(35,'1. Diseña pruebas que verifiquen los requerimientos funcionales y no funcionales (performance, usabilidad, disponibilidad, etc.)\n2. Diseña pruebas unitarias sobre los componentes del software.\n3. Diseña modelos de aseguramiento de la calidad de procesos del software, estableciendo de manera correcta el perfil objetivo de los procesos, de acuerdo con al menos un modelo de aseguramiento de calidad de procesos.','1. Diseña pruebas que verifiquen fundamentalmente los requerimientos funcionales pero no cubren totalmente los no funcionales (performance, usabilidad, disponibilidad, etc.)\n2. Diseña pruebas unitarias sobre algunos de los componentes del software.\n3. Diseña modelos de aseguramiento de la calidad de procesos del software, pero no demuestra un completo entendimiento del perfil objetivo de los procesos.','1. Diseña pruebas que verifiquen fundamentalmente los requerimientos funcionales.\n2. Diseña pruebas unitarias esencialmente sobre componentes básicos del software, sin centrar el foco en aquellos que concentran la lógica central del procesamiento.\n3. No demuestra un entendimiento del perfil objetivo de los procesos y de los modelos de aseguramiento de la calidad.'),(36,'1. Implementa pruebas funcionales y no funcionales al software de acuerdo con el diseño elaborado.\n2. Implementa pruebas unitarias sobre los componentes del software de acuerdo con el diseño elaborado.\n3. Realiza depuraciones sobre los programas construidos.\n4. Mide los principales atributos de un proyecto de desarrollo de software (costo, duración, etc.)\n5. Mide los principales atributos relacionados a procesos del desarrollo de software.','1. Implementa pruebas funcionales pero tiene dificultad para desarrollar las pruebas no funcionales al software de acuerdo con el diseño elaborado.\n2. Implementa pruebas unitarias sobre algunos de los componentes del software de acuerdo con el diseño elaborado.\n3. Realiza depuraciones sobre los programas construidos.\n4. Mide solo algunos atributos de un proyecto de desarrollo de software (costo, duración, etc.), pero que no permiten asegurar un control completo de la ejecución del proyecto.\n5. Mide algunos de los atributos relacionados a procesos del desarrollo de software, de acuerdo con el modelo de calidad seleccionado, centrado en los niveles más bajos del modelo de calidad.','1. Demuestra niveles básicos de implementa pruebas funcionales.\n2. Estructura la implementación de pruebas unitarias sobre los componentes del software pero no logra completar implementaciones de los casos de prueba.\n3. No demuestra habilidad para depurar los programas construidos.\n4. No es capaz de medir los atributos de un proyecto de desarrollo de software (costo, duración, etc.) para asegurar el control.\n5. No reconoce los principales atributos de calidad de los procesos.'),(37,'1. Analiza e interpreta los resultados generados durante las pruebas funcionales del software.\n2. Analiza e interpreta los resultados de las pruebas no funcionales (performance) realizadas al software.\n3. Analiza y concluye sobre los resultades de las mediciones de los atributos de un proyecto de desarrollo de software.\n4. Analizar y concluye sobre los resultados de las mediciones de realizadas a los procesos.','1. Demuestra capacidad de análisis de los resultados generados durante las pruebas funcionales del software, pero no sintetiza los resultados en un informe de pruebas.\n2. Analiza los resultados de las pruebas no funcionales (performance) realizadas al software, pero no sintetiza los resultados en un informe de pruebas.\n3. Analiza los resultades de las mediciones de los atributos de un proyecto de desarrollo de software, pero no sintetiza y aplica al control del proyecto.\n4. Analizar los resultados de las mediciones de realizadas a los procesos y sintetiza un conjunto de acciones para asegurar la mejora.','1. No es capaz de analizar e interpretar los resultados generados durante las pruebas funcionales del software.\n2. No es capaz de analizar los resultados de las pruebas no funcionales (performance) realizadas al software.\n3. No es capaz de analizar los resultades de las mediciones de los atributos de un proyecto de desarrollo de software.\n4. No es capaz analizar los resultados de las mediciones de realizadas a los procesos y sintetizar acciones para asegurar la mejora.'),(38,'1. Diseño componentes software al nivel detallado o bajo nivel, empleando estándares, principios y buenas prácticas de diseño propios de la profesión.\n2. Diseña la arquitectura de un producto software satisfaciendo los principales atributos de calidad.','1. Diseña componentes software al nivel detallado. La aplicación de estándares no es sistemática en el diseño.\n2. Diseña algunos aspectos de la arquitectura no cubriendo todos los atributos de calidad.','1. Desarrolla diseños incompletos de los componentes del software. No demuestra conocimiento de los estándares para el diseño (UML), los principios de diseño y buenas prácticas (patrones).\n2. No reconoce los atributos de calidad y el diseño de desiciones arquitecturales que aseguren los atributos de calidad del producto.'),(39,'1. Diseña un proyecto de desarrollo de software de acuerdo con las restricciones de tiempo establecidas.\n2. Sustenta el diseño de un proyecto de desarrollo de software asegurando su aprobación para la ejecución.','1. Diseña un proyecto de desarrollo de software de acuerdo con las restricciones de tiempo establecidas. Los objetivos establecidos no se asegura sean cumplidos completamente en las restricciones de tiempo.\n2. Sustenta el diseño de un proyecto de desarrollo de software asegurando su aprobación para la ejecución.','1. Diseña un proyecto de desarrollo de software aunque no refleja en el diseño el tratamiento correcto de las restricciones de tiempo, estableciendo un alcance apropiado. Los objetivos establecidos no se asegura sean cumplidos en las restricciones de tiempo.\n2. No es capaz de sustenta el diseño del proyecto de desarrollo de software.'),(40,'1. Modela y establece procesos relacionados al desarrollo de software. Asegura que las variaciones necesarias a los procesos y metodología se encuentren completamente reflejados en el Plan del Proyecto o las necesidades de la organización en la que trabaja.\n2. Adopta y adapta procesos de acuerdo con los requerimientos y características propias del proyecto.','1. Selecciona/adopta procesos para el desarrollo del proyecto, asegurando que la metodología sea la apropiada, pero no establece con claridad las variaciones que se requieren de acuerdo con las condiciones del proyecto.','1. No demuestra habilidades para modelar procesos para el desarrollo del software.\n2. Aunque adopta procesos, no se asegura que estos sean los apropiados de acuerdo con los requerimientos del proyecto y la organización.'),(41,'1. Lidera equipo de proyecto asegurando que en conjunto se logren los objetivos establecidos.\n2. Demuestra capacidad para el trabajo con alumnos de otras carreras (en especial con alumnos de Ing. de Sistemas de Información)\n3. Demuestra capacidad para trabajar de manera coordinada con otros equipos de proyectos.','1. Se constituye en miembro de un equipo de proyecto estableciendo liderazgo desde el rol que le toca desempeñar. Desarrolla su propio Plan de Trabajo para asegurar el logro de los compromisos asumidos.\n2. Es capaz de trabajar con alumnos de otras carreras (en especial con alumnos de Ing. de Sistemas de Información).','1. Aunque participa en equipos de proyecto no demuestra ser proactivo. Trabaja de acuerdo con un Plan de Trabajo o asignaciones elaboradas por terceros.\n2. Trabaja de manera individual, dificultándose poder colaborar con alumnos de otras carreras (en especial con alumnos de Ing. de Sistemas de Información)'),(42,'1. Demuestra conocimientos sólidos en al menos un dominio de aplicación por medio de la fundamentación teórica de las soluciones desarrolladas con conocimientos propio del dominio de aplicación de su proyecto.\n2. Demuestra a través de análisis de fuentes propias del dominio el sustento teórico del tema objeto de estudio en el proyecto.','1. Demuestra conocimientos sólidos en al menos un dominio de aplicación por medio de la fundamentación teórica de las soluciones desarrolladas con conocimientos propio del dominio de aplicación de su proyecto.\n2. Las fuentes utilizadas aunque son empleadas en el fundamento teórico no permiten la cobertura completa del tema objeto de estudio en el dominio de aplicación.','1. Demuestra conocimientos incipientes los que no le permiten abordar la solución de problemas de ingeniería dentro del dominio de aplicación donde actúa en el proyecto.\n2. El trabajo con fuentes es incipiente o casi nulo, por lo que no puede fundamentar consistentemente el problema de ingeniería que se busca abordar dentro del dominio de aplicación.'),(43,'1. Identifica y sustenta las necesidades, oportunidades o problemas de negocio a ser resueltas a través de la práctica de la ingeniería por medio de un proyecto.\n2. Identifica aspectos normativos, regulatorios, leyes, estándares u otras restricciones o condiciones que deben ser cubiertas al abordar la necesidad, oportunidad o problema de negocio.\n3. Identifica las causas que originan la necesidad u oportunidad de desarrollo de solución de ingeniería.','1. Identifica necesidades, oportunidades o problemas de negocio que pueden ser resueltas a través de la práctica de la ingeniería.\n2. Las causas identificadas no sustentan completamente el problema, oportunidad o necesidad de negocio identificados.\n3. Los aspectos normativos, regultarorios, leyes, estándares u otras restricciones o condiciones no son completas de acuerdo con el problema, oportunidad o necesidad de negocio cubierta.','1. No se identifica con claridad la necesidad, oportunidad o problema a ser cubierto a través de un proyecto de ingeniería.\n2. No hay claridad sobre la identificación de las causas que motivan el tratamiento de la necesidad, oportunidad o problema.\n3. No es suficiente la identificación de restricciones (norma, leyes, regulaciones o estándares propios del dominio) aplicables al problema o necesidad de negocio.'),(44,'1. Formula y documenta de manera apropiada restricciones sobre los productos software (trade-offs) de acuerdo con los Requerimientos No Funcionales (performance, usabilidad, seguridad, mantenibilidad, etc.)\n2. Formula y documenta de manera apropiada los atributos principales de los riesgos identificados en el proyecto.','1. Formula las restricciones que definen el problema a ser resuelto a través de una solución software (requerimientos funcionales y no funcionales) y los documenta. \n2. No se documenta de manera completa el problema, necesidad u oportunidad identicado.\n3. Las fuentes empleadas para documentar el problema no es suficiente para demostrar una completa comprensión del problema o necesidad a ser resuelto y las causas que lo originan.','1. No es capaz de formular la necesidad u oportunidad de negocio que sirve de base para establecer un proyecto típico de ingeniería.\n2. No hay suficiencia, claridad y precisión en las restricciones que establecen las condiciones bajo las que debe darse una solución al problema u oportundiad de negocio.\n3. No hay claridad en la documentación de las causas que producen la necesidad u oportunidad de negocio que debe ser cubierta por el proyecto de ingeniería.'),(45,'1. Actúa de manera proactiva resolviendo problemas ante riesgos no identificados en su proyecto.\n2. Ejecuta planes de contingencias ante la materialización de los riesgos identificados en el proyecto.\n3. Desarrolla soluciones sustentadas en base al problema o necesidad identificado y documento, las restricciones, causas que dan origen al problema o necesidad, eliminando dichas causas y entregando una solución de ingeniería apropiada para cada caso problema.','1. Demuestra ser reactivo ante los problemas que se presentan durante el desarrollo del proyecto, activando planes de contingencia definidos.\n2. Desarrolla soluciones al problema o necesidad identificado y documentado, las restricciones y causas que originan el problema o necesidad de negocio. La solución no demuestra una completa cobertura del problema y las restricciones identificadas y documentadas.','1. No demuestra capacidad de reacción y atención a problemas que se presentan durante un proyecto de ingeniería.\n2. No desarrolla soluciones a problemas o necesidades de negocio que cumplan con los requerimientos identificados y documentados, o da soluciones incompletas concentradas esencialmente en problemas de menor nivel que no resuelven las causas principales que generan el problema o necesidad de negocio.'),(46,'1. Demuestra comprensión del código de ética propio de su profesión.\n2. Emite juicios sobre situaciones donde el código de ética puede verse vulnerado.\n3. Demuestra respecto por los derechos de propiedad intelectual.','1. Conoce el código de ética de su profesión.\n2. Demuestra respeto por los derechos de propiedad intelectual.','1. Demuestra desconocimiento del código de ética de la profesión.\n2. No demuestra respeto por los derechos de propiedad intelectual en su actuación profesional (cursos, proyectos, etc.)'),(47,'1. Cumple con las responsabilidades asumidas y los planes comprometidos en los proyectos en los que participa.\n2. Ante eventos que ponen en riesgo el cumplimiento de los compromisos asumidos negocia con los interesados los niveles de cumplimientos de los objetivos establecidos.','1. Cumple con las responsabilidades asumidas y los planes comprometidos en los proyectos en los que participa.\n2. Es reactivo ante eventos que ponen el riesgo el cumplimiento de los compromisos asumidos, aunque participa de la negociación cuando se ven afectados los compromisos o hitos establecidos, lo realiza ante solicitud de los interesados afectados.','1. No demuestra ser responsable ante los compromisos asumidos.\n2. No toma acciones cuando suceden eventos que afectan el cumplimiento de los compromisos, hitos, plazos.'),(48,'1. Se comunica empleando códigos apropiados de acuerdo con la audiencia a la que se dirige.\n2. Emplea los medios audiovisuales apropiados de acuerdo con el mensaje que se busca transmitir.\n3. Enfoca su comunicación hacia los objetivos específicos a los que se orienta cada presentación de resultados.\n4. Escucha a otros de manera objetiva antes de emitir su juicio crítico y objetivos.','1. No es sistemático en el empleo de códigos de comunicación apropiados de acuerdo con la audiencia a la que se dirige.\n2. Emplea medios audiovisuales para transmitir el mensaje de acuerdo con el público objetivo, pero debe mejorar la efectividad del uso de los medios.\n3. Cubre solo parte de los objetivos específicos a los que se orienta la presentación que realiza, sobre los resultados que demuestren el logro de los objetivos.\n4. Escucha y emite juicios críticos.','1. No realiza un empleo de códigos de comunicación apropiados de acuerdo con la audiencia a la que se dirige.\n2. No hace un uso apropiado de medios audiovisuales para transmitir el mensaje de acuerdo con el público objetivo.\n3. En las presentaciones que realiza no se centra el los objetivos específicos a los que se orienta la presentación que realiza, sobre los resultados que demuestren el logro de los objetivos.\n4. No escucha antes de emitir juicios críticos sobre los puntos de vista de terceros.'),(49,'1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la efectividad en la transmisión del mensaje.\n3. Demuestra capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza de forma sistemática un lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos acordes con los estándares establecidos para el desarrollo del trabajo, aunque se advierten errores menores de redacción.\n2. Prepara presentaciones de los resultados del trabajo que aseguren la comunicar la idea principal del mensaje.\n3. debe mejorar la capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo.\n4. Utiliza lenguaje y medios acorde con el público objetivo al que se dirige.','1. Prepara informes técnicos donde se advierten problemas serios de ortografía y redacción.\n2. Prepara presentaciones de los resultados del trabajo pero estas no aseguran comunicar la idea principal del mensaje.\n3. No posee capacidad de síntesis de las ideas y conceptos básicos de acuerdo con el público objetivo o esta capacidad es incipiente.\n4. El lenguaje y medios utilizados no es acorde con el público objetivo al que se dirige.'),(50,'1. Analiza la factibilidad de los proyectos que formula, incluyendo factores tales como tiempos, recursos y costos.','1. Analiza la factibilidad de los proyectos que formula, aunque no incluye todos los factores que afectan la ejecución del proyecto (costos, tiempos, recursos)','1. No es capaz de determinar la factibilidad de los proyectos que formula, demostrando que no puede identificar factores relacionados con costos, tiempos, esfuerzo, recursos, entre otros.'),(51,'1. Comprende el impacto que genera las soluciones software que produce en el dominio de aplicación al que aplica.\n2. Reconoce el papel que juegan los productos software en el contexto económico actual.','1. Demuestra poco conocimiento del impacto de las soluciones software en el dominio de aplicación donde realiza su proyecto.\n2. Demuestra poco entendimiento del papel que juegan las soluciones que produce en el contexto económico actual en la empresa u organización para la que se desarrolla el trabajo.','1. No demuestra conocimiento del impacto de las soluciones software en el dominio de aplicación donde realiza su proyecto.\n2. No demuestra entendimiento del papel que juegan las soluciones que produce en el contexto económico actual en la empresa u organización para la que se desarrolla el trabajo.'),(52,'1. Comprende el impacto que generan las soluciones software en la sociedad actual, modificando o acentuando hábitos nuevos de consumo de la tecnología.\n2. Comprende el impacto de las soluciones software en asegurar mejorar la calidad de vida o de las condiciones de trabajo de las personas y de los negocios.','1. Demuestra poca comprensión del impacto que generan las soluciones software en la sociedad actual, modificando o acentuando hábitos nuevos de consumo de la tecnología.\n2. Demuestra poca comprensión del impacto de las soluciones software en asegurar mejorar la calidad de vida o de las condiciones de trabajo de las personas y de los negocios.','1. No demuestra comprensión del impacto que generan las soluciones software en la sociedad actual, modificando o acentuando hábitos nuevos de consumo de la tecnología.\n2. No demuestra comprensión del impacto de las soluciones software en asegurar mejorar la calidad de vida o de las condiciones de trabajo de las personas y de los negocios.'),(53,'1. Demuestra capacidad para asimilar nuevas tecnologías (frameworks, lenguajes de programación o variantes, etc.).\n2. Demuestra capacidad para asimilar nuevos conocimientos metodológicos y de procesos.\n3. Demuestra capacidad para asimilar conocimientos en almenos un dominio de aplicación.\n4. Demuestra capacidad para asimilar nuevas técnicas y principios propios de la ingeniería de software en la medida que estos emergen.\n5. Demuestra dominio de técnicas o métodos para la investigación.','1. Demuestra habilidades básicas para asimilar nuevas tecnologías (frameworks, lenguajes de programación o variantes, etc.).\n2. Demuestra habilidades básicas para asimilar nuevos conocimientos metodológicos y de procesos.\n3. Demuestra habilidades básicas para asimilar conocimientos en almenos un dominio de aplicación.\n4. Demuestra habilidades básicas para asimilar nuevas técnicas y principios propios de la ingeniería de software en la medida que estos emergen.\n5. Demuestra conocimientos básicos sobre técnicas o métodos para la investigación.','1. No demuestra capacidad para asimilar nuevas tecnologías (frameworks, lenguajes de programación o variantes, etc.).\n2. No demuestra capacidad para asimilar nuevos conocimientos metodológicos y de procesos.\n3. No demuestra las habilidades para poder asimilar conocimientos en almenos un dominio de aplicación.\n4. No demuestra capacidad para asimilar nuevas técnicas y principios propios de la ingeniería de software en la medida que estos emergen.\n5. El conocimiento y habilidades de técnicas o métodos para la investigación no le permiten poder estructurar y conducir la investigación sobre un tema de interés particular.'),(54,'1. Identifica áreas de interés dentro de la profesión donde le interesaría desarrollarse luego de graduarse.\n2. Define un plan de crecimiento profesional alineado con las áreas de interés dentro de la profesión.','1. Aunque reconoce la necesidad de mantenerse actualizado no demuestra capacidad para identificar áreas de interés dentro de la profesión donde podría desarrollarse luego de graduarse.\n2. Demuestra dificultad para preparar un plan de crecimiento profesional alineado con las áreas de interés dentro de la profesión.','1. No demuestra capacidad para poder identificar áreas de interés dentro de la profesión donde le interesaría desarrollarse luego de graduarse.\n2. No es capaz de elaborar un plan de crecimiento profesional alineado con las áreas de interés dentro de la profesión.'),(55,'1. Comprende las competencias que definen el perfil profesional de la carrera.\n2. Comprende las diferencias entre su carreras y otras carreras dentro del campo de la computación e ingeniería.\n3. Reconoce el papel e importancia de las sociedades profesionales, en especial la IEEE Computer Society y ACM.','1. Conoce las competencias que definen el perfil profesional de la carrera.\n2. Conoce las diferencias entre su carreras y otras carreras dentro del campo de la computación e ingeniería.\n3. Conoce las principales sociedades profesionales de su profesión (por ejemplo, IEEE Computer Society y ACM)','1. No demuestra conocimiento sobre el perfil profesional de la carrera.\n2. No demuestra conocimiento sobre su carrera y otras carreras dentro del campo de computación o afines.\n3. No reconoce el papel y la importancia de las sociedades profesionales, en especial la IEEE Computer Society y ACM, para su profesión.'),(56,'1. Conoce y emplea métodos modernos para el trabajo colaborativo en equipo de proyectos.\n2. Conoce y emplea métodos y herramientas que faciliten la comunicación con los diferentes interesados en un equipo de proyecto','1. Conoce sobre métodos modernos para el trabajo colaborativo en equipo de proyectos.\n2. Conoce sobre métodos y herramientas que faciliten la comunicación con los diferentes interesados en un equipo de proyecto','1. No demuestra conocimiento y habilidades en métodos modernos para el trabajo colaborativo en equipo de proyectos.\n2. No demuestra conocimiento y habilidades en la aplicación de métodos y herramientas que faciliten la comunicación con los diferentes interesados en un equipo de proyecto'),(57,'1. Utiliza la metodología y proceso de acuerdo con las características propias del proyecto.\n2. Adapta la metodología o proceso de acuerdo con las características propias del proyecto y los estándares y restricciones establecidos.','1. Utiliza solo algunos elementos de las metodologías y procesos para el desarrollo de proyectos, más no se definen los criterios utilizados para excluir aspectos de los procesos y metodología utilizados.','1. Demuestra dificultad para aplicar un enfoque metodológico al desarrollo de proyectos.\n2. Cuando aplica metodología o proceso no toma en consideración las características propias del proyecto, no sustentando el porqué de la metodología o procesos utilizados.'),(58,'1. Conoce diversas herramientas de apoyo a los procesos de desarrollo de software.\n2. Aplica de manera informada las herramientas apropiadas a los procesos establecidos en un proyecto típico de desarrollo de software.','1. Conoce diversas herramientas de apoyo a los procesos de desarrollo de software.\n2. Aplica herramientas solo en algunas áreas de procesos establecidos en un proyecto típico de desarrollo de software.','1. Conoce diversas herramientas de apoyo a los procesos de desarrollo de software.\n2. No demuestra habilidades prácticas de aplicación de herramientas a los procesos definidos en un proyecto típico de desarrollo de software.');
/*!40000 ALTER TABLE `projects_crit_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_criteria`
--

DROP TABLE IF EXISTS `projects_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext NOT NULL,
  `criteria_info_id` int(11) DEFAULT NULL,
  `outcome_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `criteria_info_id` (`criteria_info_id`),
  KEY `projects_criteria_outcome_id_9a95d8bc_fk_projects_outcome_id` (`outcome_id`),
  CONSTRAINT `projects_crit_criteria_info_id_f2bf83c5_fk_projects_crit_info_id` FOREIGN KEY (`criteria_info_id`) REFERENCES `projects_crit_info` (`id`),
  CONSTRAINT `projects_criteria_outcome_id_9a95d8bc_fk_projects_outcome_id` FOREIGN KEY (`outcome_id`) REFERENCES `projects_outcome` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_criteria`
--

LOCK TABLES `projects_criteria` WRITE;
/*!40000 ALTER TABLE `projects_criteria` DISABLE KEYS */;
INSERT INTO `projects_criteria` VALUES (32,'Aplica  conocimientos matemáticos en las propuestas de soluciones en Ingeniería de Software',32,14),(33,'Aplica  teorías y principios de ciencias de la computación al desarrollo y mantenimiento de software',33,14),(34,'Aplica conocimientos de ingeniería en la propuesta de soluciones a problemas en Ingeniería de Software',34,14),(35,'Diseña  proyectos o experimentos para la implementación de una solución de software.',35,15),(36,'Realiza experimentos o proyectos para la implementación de soluciones de software',36,15),(37,'Analiza e interpreta datos/resultados de la ejecución de las tareas propias del desarrollo de proyectos de software.',37,15),(38,'Diseña  sistemas y componentes software relacionados al desarrollo software y considerando aspectos económicos, sociales, políticos, éticos.',38,16),(39,'Diseña proyectos considerando aspectos económicos, sociales, políticos, éticos',39,16),(40,'Diseña  procesos relacionados al desarrollo y mantenimiento de productos software tomando en cuenta aspectos económicos',40,16),(41,'Participa en equipos multidisciplinarios con eficacia, eficiencia y objetividad.',41,17),(42,'Conoce al menos un sector empresarial o dominio de aplicación de los sistemas de software',42,17),(43,'Identifica de debilidades o problemas de negocio o tecnológicos y las oportunidades de su solución.',43,18),(44,'Define las variables que inducen el problema o debilidad y las oportunidades que se pierden con ellas',44,18),(45,'Analiza variables que inducen problemas y plantea soluciones  y acciones de mitigación',45,18),(46,'Muestra ética en el desempeño profesional',46,19),(47,'Muestra responsabilidad en el cumplimiento del rol de Ingeniero de Software',47,19),(48,'Comunica oralmente  ideas y/o resultados con objetividad a públicos de diferentes especialidades y niveles jerárquicos',48,20),(49,'Comunica en forma escrita ideas y/o resultados con objetividad a públicos de diferentes especialidades y niveles jerárquicos',49,20),(50,'Comprende el impacto de las soluciones de ingeniería de software en el contexto global.',50,21),(51,'Comprende el impacto de las soluciones de ingeniería de software en el contexto económico.',51,21),(52,'Comprende el impacto de las soluciones de ingeniería de software en la sociedad.',52,21),(53,'Actualiza conceptos y conocimientos necesarios para el desarrollo profesional',53,22),(54,'Reconoce la necesidad del aprendizaje permanente para el desempeño profesional',54,22),(55,'Comprende las competencias profesionales del Ingeniero de Software necesarias para atender las demandas del mundo contemporáneo.',55,23),(56,'Analiza  nuevos métodos de colaboración y comunicación en el mundo contemporáneo.',56,23),(57,'Utiliza técnicas de última generación  (metodologías y procesos) para la práctica de la Ingeniería de Software',57,24),(58,'Utiliza  herramientas  de última generación necesarias para la práctica de la Ingeniería de Software.',58,24);
/*!40000 ALTER TABLE `projects_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_criteriaevaluation`
--

DROP TABLE IF EXISTS `projects_criteriaevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_criteriaevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluation` varchar(2) DEFAULT NULL,
  `criteria_id` int(11) DEFAULT NULL,
  `outcome_eval_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_criteriaev_criteria_id_7b02d8d7_fk_projects_criteria_id` (`criteria_id`),
  KEY `projec_outcome_eval_id_d93e13cf_fk_projects_outcomeevaluation_id` (`outcome_eval_id`),
  CONSTRAINT `projec_outcome_eval_id_d93e13cf_fk_projects_outcomeevaluation_id` FOREIGN KEY (`outcome_eval_id`) REFERENCES `projects_outcomeevaluation` (`id`),
  CONSTRAINT `projects_criteriaev_criteria_id_7b02d8d7_fk_projects_criteria_id` FOREIGN KEY (`criteria_id`) REFERENCES `projects_criteria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_criteriaevaluation`
--

LOCK TABLES `projects_criteriaevaluation` WRITE;
/*!40000 ALTER TABLE `projects_criteriaevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_criteriaevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_feedback`
--

DROP TABLE IF EXISTS `projects_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `read` int(11) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `description` longtext NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `postulation_id` int(11) DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `receipt_student_id` int(11) DEFAULT NULL,
  `t_postulation_id` int(11) DEFAULT NULL,
  `minuta_id` int(11),
  `author_s_id` int(11),
  `project_id` int(11),
  PRIMARY KEY (`id`),
  KEY `projects_feedback_author_id_55795f71_fk_core_userprofile_id` (`author_id`),
  KEY `projects_feed_postulation_id_dc7e9601_fk_projects_postulation_id` (`postulation_id`),
  KEY `projects_feedback_receipt_id_3582fda0_fk_core_userprofile_id` (`receipt_id`),
  KEY `projects_feedback_receipt_student_id_9cc5bed3_fk_core_student_id` (`receipt_student_id`),
  KEY `pro_t_postulation_id_3b423cbd_fk_projects_teacher_postulation_id` (`t_postulation_id`),
  KEY `projects_feedback_minuta_id_21a8a5ff_fk_projects_minuta_id` (`minuta_id`),
  KEY `projects_feedback_author_s_id_d112f2c1_fk_core_student_id` (`author_s_id`),
  KEY `projects_feedback_project_id_33136ce5_fk_projects_project_id` (`project_id`),
  CONSTRAINT `pro_t_postulation_id_3b423cbd_fk_projects_teacher_postulation_id` FOREIGN KEY (`t_postulation_id`) REFERENCES `projects_teacher_postulation` (`id`),
  CONSTRAINT `projects_feed_postulation_id_dc7e9601_fk_projects_postulation_id` FOREIGN KEY (`postulation_id`) REFERENCES `projects_postulation` (`id`),
  CONSTRAINT `projects_feedback_author_id_55795f71_fk_core_userprofile_id` FOREIGN KEY (`author_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `projects_feedback_author_s_id_d112f2c1_fk_core_student_id` FOREIGN KEY (`author_s_id`) REFERENCES `core_student` (`id`),
  CONSTRAINT `projects_feedback_minuta_id_21a8a5ff_fk_projects_minuta_id` FOREIGN KEY (`minuta_id`) REFERENCES `projects_minuta` (`id`),
  CONSTRAINT `projects_feedback_project_id_33136ce5_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `projects_feedback_receipt_id_3582fda0_fk_core_userprofile_id` FOREIGN KEY (`receipt_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `projects_feedback_receipt_student_id_9cc5bed3_fk_core_student_id` FOREIGN KEY (`receipt_student_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_feedback`
--

LOCK TABLES `projects_feedback` WRITE;
/*!40000 ALTER TABLE `projects_feedback` DISABLE KEYS */;
INSERT INTO `projects_feedback` VALUES (22,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Intercambio de juguetes usados ','2018-06-01 19:07:22.788000',2,7,NULL,19,NULL,NULL,NULL,NULL),(23,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Simulador de Entrega de Licencias de conducir de un Mapa 3D ','2018-06-01 19:58:08.290000',2,8,NULL,20,NULL,NULL,NULL,NULL),(24,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Simulador de Entrega de Licencias de conducir de un Mapa 3D ','2018-06-01 19:58:08.315000',2,9,NULL,21,NULL,NULL,NULL,NULL),(25,1,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Plataforma de Solicitud de Cargo ','2018-06-01 20:21:33.076000',2,10,NULL,22,NULL,NULL,NULL,NULL),(26,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Plataforma de Solicitud de Cargo ','2018-06-01 20:21:33.111000',2,11,NULL,23,NULL,NULL,NULL,NULL),(27,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Gestion de Instituciones ','2018-06-01 20:43:13.175000',2,12,NULL,24,NULL,NULL,NULL,NULL),(28,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: MultiSede ','2018-06-01 20:52:50.276000',2,13,NULL,25,NULL,NULL,NULL,NULL),(29,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: MultiSede ','2018-06-01 21:06:03.657000',2,14,NULL,26,NULL,NULL,NULL,NULL),(30,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-01 21:15:51.485000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(31,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-01 21:15:51.493000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(32,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-01 21:15:51.502000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(33,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-01 21:15:51.518000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(34,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Proceso de Administracion y Gestion Empresarial ','2018-06-01 23:06:05.624000',1,15,NULL,28,NULL,NULL,NULL,NULL),(35,0,'AP','Tu asignación a un proyecto a sido aprobada!<br> Nombre del proyecto: Proceso de Administracion y Gestion Empresarial ','2018-06-01 23:06:05.637000',1,15,NULL,28,NULL,NULL,NULL,NULL),(36,1,'AP','Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: NSAgro <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-04 23:40:50.412000',1,NULL,1,NULL,NULL,NULL,NULL,NULL),(37,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Proceso de Acreditacion de ABET ','2018-06-05 01:17:33.491000',2,16,NULL,17,NULL,NULL,NULL,NULL),(38,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-05 01:20:09.576000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(39,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-05 02:19:11.363000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(40,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-05 02:19:11.371000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(41,1,'AP','Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: Mejoramiento en los Procesos de Administracion, con Procesamiento de Datos. <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-05 03:40:45.955000',1,NULL,1,NULL,NULL,NULL,NULL,NULL),(42,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Plataforma de generacion de certificados ','2018-06-05 03:46:31.416000',2,17,NULL,36,NULL,NULL,NULL,NULL),(43,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-05 04:23:48.525000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(44,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-05 04:23:48.779000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(45,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: MultiSede ','2018-06-05 15:10:50.740000',2,18,NULL,37,NULL,NULL,NULL,NULL),(46,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-05 15:11:40.423000',1,NULL,2,NULL,NULL,NULL,NULL,NULL),(48,1,'AP','El alumno Francisco J. Falcon Soto  ha aceptado la solicitud a reunion.','2018-06-25 19:41:31.133854',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(50,1,'AP','El alumno Francisco J. Falcon Soto  ha aceptado la solicitud a reunion.','2018-06-25 20:05:39.200534',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(109,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto:  Testeo de prototipo ','2018-06-28 17:05:36.597798',5,49,NULL,58,NULL,NULL,NULL,NULL),(110,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto:  Testeo de prototipo ','2018-06-28 17:05:36.624941',5,50,NULL,59,NULL,NULL,NULL,NULL),(111,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-28 17:05:56.085809',1,NULL,5,NULL,NULL,NULL,NULL,NULL),(113,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: testeo 2 ','2018-06-28 17:11:32.453180',5,51,NULL,61,NULL,NULL,NULL,NULL),(114,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: testeo 2 ','2018-06-28 17:11:32.484047',5,52,NULL,62,NULL,NULL,NULL,NULL),(115,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-06-28 17:13:54.218778',1,NULL,5,NULL,NULL,NULL,NULL,NULL),(146,1,'AP','Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: Integrador de gestion de entidades financieras <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-10 18:04:17.434152',1,NULL,1,NULL,NULL,NULL,NULL,104),(147,1,'AP','Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: Gestion automatizada de proyectos <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-10 18:48:27.267496',1,NULL,1,NULL,NULL,NULL,NULL,105),(151,1,'AP','El alumno Jorge I. Gomez Parga  ha aceptado la solicitud a reunion.','2018-07-11 15:02:50.504175',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(152,1,'AP','El alumno Juan C. Villalobos Jara  ha aceptado la solicitud a reunion.','2018-07-11 15:03:36.202537',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(153,1,'AP','El alumno Katherine L. Espinoza Pavez  ha aceptado la solicitud a reunion.','2018-07-11 15:04:07.055750',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(155,1,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Registro Clinico Electronico  ','2018-07-12 19:37:57.341146',1,NULL,12,NULL,33,NULL,NULL,107),(156,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Registro Clinico Electronico  ','2018-07-12 19:37:57.448646',1,69,NULL,80,NULL,NULL,NULL,107),(157,0,'AP','Tu asignacion a un proyecto a sido aprobada!<br> Nombre del proyecto: Registro Clinico Electronico  <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 19:37:57.462725',1,69,NULL,80,NULL,NULL,NULL,107),(158,1,'AP','Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: Registro Clinico Electronico  <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 19:37:57.476956',1,NULL,1,NULL,NULL,NULL,NULL,107),(159,1,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Gestion de proyectos ABB ','2018-07-12 19:43:48.819674',1,NULL,12,NULL,34,NULL,NULL,108),(160,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Gestion de proyectos ABB ','2018-07-12 19:43:48.860042',1,70,NULL,81,NULL,NULL,NULL,108),(161,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Gestion de proyectos ABB ','2018-07-12 19:43:48.885097',1,71,NULL,82,NULL,NULL,NULL,108),(162,0,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Gestion de proyectos ABB ','2018-07-12 19:43:48.913601',1,72,NULL,83,NULL,NULL,NULL,108),(163,0,'AP','Tu asignacion a un proyecto a sido aprobada!<br> Nombre del proyecto: Gestion de proyectos ABB <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 19:43:48.928436',1,70,NULL,81,NULL,NULL,NULL,108),(164,0,'AP','Tu asignacion a un proyecto a sido aprobada!<br> Nombre del proyecto: Gestion de proyectos ABB <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 19:43:48.938230',1,71,NULL,82,NULL,NULL,NULL,108),(165,0,'AP','Tu asignacion a un proyecto a sido aprobada!<br> Nombre del proyecto: Gestion de proyectos ABB <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 19:43:48.952139',1,72,NULL,83,NULL,NULL,NULL,108),(166,1,'AP','Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: Gestion de proyectos ABB <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 19:43:48.959657',1,NULL,1,NULL,NULL,NULL,NULL,108),(167,1,'AP','Felicitaciones! <br>Tu postulacion al proyecto: Registro Clinico Electronico  						ha sido Aprobada!','2018-07-12 20:20:49.348923',12,NULL,12,NULL,33,NULL,NULL,NULL),(168,1,'AP','Felicitaciones! <br>Tu postulacion al proyecto: Gestion de proyectos ABB 						ha sido Aprobada!','2018-07-12 20:20:52.739180',12,NULL,12,NULL,34,NULL,NULL,NULL),(169,1,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Gestion de estacionamientos para personas en situacion de discapacidad ','2018-07-12 20:21:26.098986',1,NULL,12,NULL,35,NULL,NULL,109),(170,1,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Gestion de estacionamientos para personas en situacion de discapacidad ','2018-07-12 20:21:26.109158',1,73,NULL,85,NULL,NULL,NULL,109),(171,1,'AP','Tu asignacion a un proyecto a sido aprobada!<br> Nombre del proyecto: Gestion de estacionamientos para personas en situacion de discapacidad <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 20:21:26.122617',1,73,NULL,85,NULL,NULL,NULL,109),(172,1,'AP','Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: Gestion de estacionamientos para personas en situacion de discapacidad <br> con la siguiente glosa: <br> <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-12 20:21:26.131227',1,NULL,1,NULL,NULL,NULL,NULL,109),(173,1,'AP','Felicitaciones! <br>Tu postulacion al proyecto: Gestion de estacionamientos para personas en situacion de discapacidad 						ha sido Aprobada!','2018-07-12 20:21:50.998599',12,NULL,12,NULL,35,NULL,NULL,NULL),(174,1,'RE','Su Postulacion a sido RECHAZADA con el siguiente comentario: \nEstá reservado para otro docente','2018-07-12 20:52:01.237243',1,NULL,12,NULL,36,NULL,NULL,NULL),(175,1,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 09/07/2018 - 19:00','2018-07-13 14:39:42.949061',1,NULL,NULL,77,NULL,6,NULL,NULL),(176,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 09/07/2018 - 19:00','2018-07-13 14:39:42.959100',1,NULL,NULL,78,NULL,6,NULL,NULL),(177,1,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 09/07/2018 - 19:00','2018-07-13 14:39:42.968312',1,NULL,NULL,79,NULL,6,NULL,NULL),(178,1,'AP','El alumno Adriana M. Coca Silva  ha aceptado la solicitud a reunion.','2018-07-13 14:40:58.846214',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(179,1,'AP','El alumno Jose L. Pozo Saavedra  ha aceptado la solicitud a reunion.','2018-07-13 14:41:17.586017',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(180,1,'AP','El alumno Juan C. Venegas Zurita  ha aceptado la solicitud a reunion.','2018-07-13 14:41:33.020526',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(181,1,'RE','Su propuesta ha sido RECHAZADA con el siguiente comentario: Faltan fundamentos temáticos del perfil de egreso','2018-07-18 20:29:53.857797',1,NULL,2,NULL,NULL,NULL,NULL,111),(182,1,'AP','Su propuesta ha sido APROBADA con la siguiente glosa: <p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>','2018-07-18 20:45:09.039048',1,NULL,2,NULL,NULL,NULL,NULL,112),(183,1,'AP','Felicitaciones! <br>Tu postulacion al proyecto: Sudo TIC                     ha sido Aprobada! <br>Tus otras postulaciones han sido canceladas.','2018-07-18 20:54:32.175860',2,74,NULL,87,NULL,NULL,NULL,NULL),(184,1,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 16/07/2018 - 19:00','2018-07-18 20:57:19.768643',1,NULL,NULL,87,NULL,7,NULL,NULL),(185,1,'AP','El alumno Diego I. Espinoza Maldonado  ha aceptado la solicitud a reunion.','2018-07-18 20:57:37.000414',NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),(186,1,'ME','El docente Eduardo Quiroga Aguilera  ha enviado el siguiente mensaje sobre el proyecto Sudo TIC: Procurar mantener el formato (x,y,z) en el objetivo general y específicos\n\nDe la exposición presentada, queda es posible interpretar que la idea fuerza de la problemática es \"Reunión de trabajo ineficiente\". Así como también:\n\nReuniones de trabajo son:\n-- ineficientes\n-- poco claras\n\nPérdida\n-- tiempo\n-- dinero\n\nEn relación a esos antecedentes la componente X pudiese ser:\n\n+ Aumentar la eficiencia de las reuniones\n+ Aumentar la claridad de reuniones\n\n- Disminuir el tiempo perdido\n- Disminuir el dinero invertido\n\nAhora queda el desafío de ¿cómo comprobar esos objetivos? si se mantiene la apreciación de subjetividad se deben reformular\n','2018-07-18 21:11:00.521060',1,NULL,NULL,87,NULL,NULL,NULL,112),(187,1,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 18/07/2018 - 18:30','2018-07-18 21:32:15.944443',1,NULL,NULL,87,NULL,8,NULL,NULL),(188,1,'RE','Se ha rechazado la solicitud de reunion con el siguiente comentario: <br> Estimado director, junto con saludar, el comento que por temas laborales no puedo asistir a la reunión propuesta, ya que tengo una entrevista laboral, favor solicito su colaboración para poder modificar el día y hora solicitado,\n\nquedo atento a comentarios,\n\nsaludos cordiales,\n','2018-07-18 21:33:36.408971',NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),(189,1,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 18/07/2018 - 17:39','2018-07-18 21:38:12.088443',1,NULL,NULL,87,NULL,9,NULL,NULL),(190,1,'AP','El alumno Diego I. Espinoza Maldonado  ha aceptado la solicitud a reunion.','2018-07-18 21:38:52.974094',NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),(191,1,'AS','Se te ha asignado un proyecto!<br> Nombre del proyecto: Propuesta de gestión de existencias ','2018-07-23 00:18:46.969656',1,NULL,12,NULL,37,NULL,NULL,113),(193,1,'AP','El alumno Francisco J. Falcon Soto  ha aceptado la solicitud a reunion.','2018-07-23 22:28:20.755077',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(196,1,'AS','El docente Eduardo Quiroga Aguilera  ha compartido el proyecto Integrador de gestión de entidades financieras contigo.','2018-07-27 16:44:14.430560',NULL,NULL,12,NULL,NULL,NULL,NULL,104),(197,1,'AS','El docente Eduardo Quiroga Aguilera  ha compartido el proyecto Gestión automatizada de proyectos contigo.','2018-07-27 16:44:39.110544',NULL,NULL,12,NULL,NULL,NULL,NULL,105),(198,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:09:37.043722',1,NULL,NULL,77,NULL,11,NULL,NULL),(199,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:09:37.053095',1,NULL,NULL,78,NULL,11,NULL,NULL),(200,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:09:37.063989',1,NULL,NULL,79,NULL,11,NULL,NULL),(201,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:10:06.389394',1,NULL,NULL,74,NULL,12,NULL,NULL),(202,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:10:06.397412',1,NULL,NULL,75,NULL,12,NULL,NULL),(203,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:10:06.407802',1,NULL,NULL,76,NULL,12,NULL,NULL),(204,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:12:17.998147',12,NULL,NULL,85,NULL,13,NULL,NULL),(205,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:12:42.352672',12,NULL,NULL,81,NULL,14,NULL,NULL),(206,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:12:42.363648',12,NULL,NULL,82,NULL,14,NULL,NULL),(207,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:12:42.371313',12,NULL,NULL,83,NULL,14,NULL,NULL),(208,0,'MI','Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: 30/07/2018 - 19:00','2018-07-30 21:13:03.178142',12,NULL,NULL,80,NULL,15,NULL,NULL),(209,1,'AS','El docente Carlos Neira Carrasco  ha compartido el proyecto Registro Clínico Electrónico contigo.','2018-07-30 21:13:09.416758',NULL,NULL,1,NULL,NULL,NULL,NULL,107),(210,1,'AS','El docente Carlos Neira Carrasco  ha compartido el proyecto Gestión de estacionamientos para personas en situación de discapacidad contigo.','2018-07-30 21:13:18.828462',NULL,NULL,1,NULL,NULL,NULL,NULL,109),(211,1,'AS','El docente Carlos Neira Carrasco  ha compartido el proyecto Gestión de proyectos ABB contigo.','2018-07-30 21:13:32.916033',NULL,NULL,1,NULL,NULL,NULL,NULL,108);
/*!40000 ALTER TABLE `projects_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_glosa`
--

DROP TABLE IF EXISTS `projects_glosa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_glosa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carreer` int(11) DEFAULT NULL,
  `description` longtext,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_glosa_author_id_7516c3fb_fk_core_userprofile_id` (`author_id`),
  CONSTRAINT `projects_glosa_author_id_7516c3fb_fk_core_userprofile_id` FOREIGN KEY (`author_id`) REFERENCES `core_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_glosa`
--

LOCK TABLES `projects_glosa` WRITE;
/*!40000 ALTER TABLE `projects_glosa` DISABLE KEYS */;
INSERT INTO `projects_glosa` VALUES (2,1,'<p>\n\n\n\n\n<style type=\"text/css\">\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px \'Helvetica Neue\'; color: #454545}\n</style>\n\n\n</p><p class=\"p1\">Se acepta proyecto, solicitando al equipo (profesor - estudiante) mantener validación constante de las 3 lineas de formación de la carrera; gestión de proyecto, arquitectura y desarrollo de software. También se deben fortalecer las habilidades transversales, tales como; responsabilidad social, comunicación oral y escrita, trabajo en equipo, liderazgo e innovación, y manejo del idioma inglés.</p>',NULL);
/*!40000 ALTER TABLE `projects_glosa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_historicalminutaevidence`
--

DROP TABLE IF EXISTS `projects_historicalminutaevidence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_historicalminutaevidence` (
  `id` int(11) NOT NULL,
  `author_type` varchar(2) NOT NULL,
  `topic` varchar(200) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `last_mod` datetime(6) DEFAULT NULL,
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `history_date` datetime(6) NOT NULL,
  `history_change_reason` varchar(100) DEFAULT NULL,
  `history_type` varchar(1) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `history_user_id` int(11) DEFAULT NULL,
  `minuta_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  KEY `projects_historicalminu_history_user_id_39b5be4f_fk_auth_user_id` (`history_user_id`),
  KEY `projects_historicalminutaevidence_b80bb774` (`id`),
  KEY `projects_historicalminutaevidence_4f331e2f` (`author_id`),
  KEY `projects_historicalminutaevidence_3b9b4938` (`minuta_id`),
  CONSTRAINT `projects_historicalminu_history_user_id_39b5be4f_fk_auth_user_id` FOREIGN KEY (`history_user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_historicalminutaevidence`
--

LOCK TABLES `projects_historicalminutaevidence` WRITE;
/*!40000 ALTER TABLE `projects_historicalminutaevidence` DISABLE KEYS */;
INSERT INTO `projects_historicalminutaevidence` VALUES (2,'DO','Objetivos de proyecto','Procurar mantener el formato (x,y,z) en el objetivo general y específicos\r\n\r\nDe la exposición presentada, es posible interpretar que la idea fuerza de la problemática es \"Reunión de trabajo ineficiente\". Así como también:\r\n\r\nReuniones de trabajo son:\r\n-- ineficientes\r\n-- poco claras\r\n\r\nPérdida\r\n-- tiempo\r\n-- dinero\r\n\r\nEn relación a esos antecedentes la componente X pudiese ser:\r\n\r\n+ Aumentar la eficiencia de las reuniones\r\n+ Aumentar la claridad de reuniones\r\n\r\n- Disminuir el tiempo perdido\r\n- Disminuir el dinero invertido\r\n\r\nAhora queda el desafío de ¿cómo comprobar esos objetivos? si se mantiene la apreciación de subjetividad se deben reformular. No olvidar las componentes (y, z)','2018-07-13 20:36:10.782318',1,'2018-07-31 20:19:57.477131',NULL,'~',2,2,6),(8,'DO','Conclusiones','<p>No se presentan con claridad</p><p>Debe recordar los pilares de la carrera</p><p>Evidenciar el cumplimiento de los indicadores detallados en los casos de éxito</p>','2018-07-31 20:19:57.483382',2,'2018-07-31 20:19:57.485084',NULL,'+',2,2,6),(8,'DO','Conclusiones','<p>No se presentan con claridad</p><p>Debe recordar los pilares de la carrera</p><p>Evidenciar el cumplimiento de los indicadores detallados en los casos de éxito</p>','2018-07-31 20:19:57.483382',3,'2018-07-31 20:19:57.492800',NULL,'~',2,2,6);
/*!40000 ALTER TABLE `projects_historicalminutaevidence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_minuta`
--

DROP TABLE IF EXISTS `projects_minuta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_minuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reunion_date` datetime(6),
  `reunion_theme` varchar(400) NOT NULL,
  `data_file` varchar(100) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `external_visit` varchar(500) NOT NULL,
  `last_mod` datetime(6) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `author_type` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_minuta_project_id_d01bd119_fk_projects_project_id` (`project_id`),
  KEY `projects_minuta_author_id_181a6696_fk_auth_user_id` (`author_id`),
  CONSTRAINT `projects_minuta_author_id_181a6696_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `projects_minuta_project_id_d01bd119_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_minuta`
--

LOCK TABLES `projects_minuta` WRITE;
/*!40000 ALTER TABLE `projects_minuta` DISABLE KEYS */;
INSERT INTO `projects_minuta` VALUES (6,'2018-07-09 23:00:00.000000','Retroalimentación','','2018-07-13 14:39:42.000000','AP',105,'Sin Registrar','2018-07-13 14:39:42.000000',2,'DO'),(7,'2018-07-16 23:00:00.000000','Objetivos de Proyecto','Minutas/Reunión_01.txt','2018-07-18 20:57:19.760388','AP',112,'','2018-07-18 20:57:19.760366',2,'DO'),(8,'2018-07-18 22:30:00.000000','Tabla de Homologación','','2018-07-18 21:32:15.937222','EM',112,'','2018-07-18 21:32:15.937176',2,'DO'),(9,'2018-07-18 21:39:00.000000','Prioridades','','2018-07-18 21:38:12.079169','AP',112,'','2018-07-18 21:38:12.079146',2,'DO'),(11,'2018-07-30 23:00:00.000000','avances de proyecto','','2018-07-30 21:09:37.031399','EM',105,'','2018-07-30 21:09:37.031373',2,'DO'),(12,'2018-07-30 23:00:00.000000','','','2018-07-30 21:10:06.374835','EM',104,'','2018-07-30 21:10:06.374811',2,'DO'),(13,'2018-07-30 23:00:00.000000','avances de proyecto','','2018-07-30 21:12:17.987249','EM',109,'','2018-07-30 21:12:17.987225',18,'DO'),(14,'2018-07-30 23:00:00.000000','','','2018-07-30 21:12:42.337387','EM',108,'','2018-07-30 21:12:42.337363',18,'DO'),(15,'2018-07-30 23:00:00.000000','','','2018-07-30 21:13:03.000000','EM',107,';','2018-07-30 21:13:03.000000',18,'DO');
/*!40000 ALTER TABLE `projects_minuta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_minuta_invited_stud`
--

DROP TABLE IF EXISTS `projects_minuta_invited_stud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_minuta_invited_stud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minuta_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_minuta_invited_stud_minuta_id_7cea8c9c_uniq` (`minuta_id`,`student_id`),
  KEY `projects_minuta_invited_s_student_id_1e0fac50_fk_core_student_id` (`student_id`),
  CONSTRAINT `projects_minuta_invited_minuta_id_816190e0_fk_projects_minuta_id` FOREIGN KEY (`minuta_id`) REFERENCES `projects_minuta` (`id`),
  CONSTRAINT `projects_minuta_invited_s_student_id_1e0fac50_fk_core_student_id` FOREIGN KEY (`student_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_minuta_invited_stud`
--

LOCK TABLES `projects_minuta_invited_stud` WRITE;
/*!40000 ALTER TABLE `projects_minuta_invited_stud` DISABLE KEYS */;
INSERT INTO `projects_minuta_invited_stud` VALUES (1,15,80);
/*!40000 ALTER TABLE `projects_minuta_invited_stud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_minuta_invited_user`
--

DROP TABLE IF EXISTS `projects_minuta_invited_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_minuta_invited_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minuta_id` int(11) NOT NULL,
  `userprofile_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_minuta_invited_user_minuta_id_1f6ed255_uniq` (`minuta_id`,`userprofile_id`),
  KEY `projects_minuta_i_userprofile_id_98b2be92_fk_core_userprofile_id` (`userprofile_id`),
  CONSTRAINT `projects_minuta_i_userprofile_id_98b2be92_fk_core_userprofile_id` FOREIGN KEY (`userprofile_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `projects_minuta_invited_minuta_id_68bff66f_fk_projects_minuta_id` FOREIGN KEY (`minuta_id`) REFERENCES `projects_minuta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_minuta_invited_user`
--

LOCK TABLES `projects_minuta_invited_user` WRITE;
/*!40000 ALTER TABLE `projects_minuta_invited_user` DISABLE KEYS */;
INSERT INTO `projects_minuta_invited_user` VALUES (2,11,12),(3,12,12),(4,13,1),(5,14,1),(6,15,1);
/*!40000 ALTER TABLE `projects_minuta_invited_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_minutacomment`
--

DROP TABLE IF EXISTS `projects_minutacomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_minutacomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_type` varchar(2) NOT NULL,
  `comment` longtext NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `minuta_id` int(11),
  PRIMARY KEY (`id`),
  KEY `projects_minutacomment_author_id_97188584_fk_auth_user_id` (`author_id`),
  KEY `projects_minutacomment_minuta_id_3c3261ff_fk_projects_minuta_id` (`minuta_id`),
  CONSTRAINT `projects_minutacomment_author_id_97188584_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `projects_minutacomment_minuta_id_3c3261ff_fk_projects_minuta_id` FOREIGN KEY (`minuta_id`) REFERENCES `projects_minuta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_minutacomment`
--

LOCK TABLES `projects_minutacomment` WRITE;
/*!40000 ALTER TABLE `projects_minutacomment` DISABLE KEYS */;
INSERT INTO `projects_minutacomment` VALUES (1,'DO','No olviden dejar en el drive la minuta elaborada a partir de la retroalimentación','2018-07-18 21:15:02.316870',2,7),(2,'DO','No olviden completar las minutas y los archivos de presentación en el Drive','2018-07-18 21:54:11.228455',2,9);
/*!40000 ALTER TABLE `projects_minutacomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_minutaevidence`
--

DROP TABLE IF EXISTS `projects_minutaevidence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_minutaevidence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(200) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `last_mod` datetime(6) DEFAULT NULL,
  `minuta_id` int(11),
  `author_id` int(11) DEFAULT NULL,
  `author_type` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_minutaregistro_minuta_id_cb14141f_fk_projects_minuta_id` (`minuta_id`),
  KEY `projects_minutaevidence_author_id_71174926_fk_auth_user_id` (`author_id`),
  CONSTRAINT `projects_minutaevidence_author_id_71174926_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `projects_minutaregistro_minuta_id_cb14141f_fk_projects_minuta_id` FOREIGN KEY (`minuta_id`) REFERENCES `projects_minuta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_minutaevidence`
--

LOCK TABLES `projects_minutaevidence` WRITE;
/*!40000 ALTER TABLE `projects_minutaevidence` DISABLE KEYS */;
INSERT INTO `projects_minutaevidence` VALUES (2,'Objetivos de proyecto','Procurar mantener el formato (x,y,z) en el objetivo general y específicos\r\n\r\nDe la exposición presentada, es posible interpretar que la idea fuerza de la problemática es \"Reunión de trabajo ineficiente\". Así como también:\r\n\r\nReuniones de trabajo son:\r\n-- ineficientes\r\n-- poco claras\r\n\r\nPérdida\r\n-- tiempo\r\n-- dinero\r\n\r\nEn relación a esos antecedentes la componente X pudiese ser:\r\n\r\n+ Aumentar la eficiencia de las reuniones\r\n+ Aumentar la claridad de reuniones\r\n\r\n- Disminuir el tiempo perdido\r\n- Disminuir el dinero invertido\r\n\r\nAhora queda el desafío de ¿cómo comprobar esos objetivos? si se mantiene la apreciación de subjetividad se deben reformular. No olvidar las componentes (y, z)','2018-07-13 20:36:10.782318',6,2,'DO'),(3,'Objetivos del proyecto','Procurar mantener el formato (x,y,z) en el objetivo general y específicos\r\nDe la exposición presentada, queda es posible interpretar que la idea fuerza de la problemática es \"Reunión de trabajo ineficiente\". Así como también:\r\n\r\nReuniones de trabajo son:\r\n-- ineficientes\r\n-- poco claras\r\n\r\nPérdida\r\n-- tiempo\r\n-- dinero\r\n\r\nEn relación a esos antecedentes la componente X pudiese ser:\r\n\r\n+ Aumentar la eficiencia de las reuniones\r\n+ Aumentar la claridad de reuniones\r\n\r\n- Disminuir el tiempo perdido\r\n- Disminuir el dinero invertido\r\n\r\nAhora queda el desafío de ¿cómo comprobar esos objetivos? si se mantiene la apreciación de subjetividad se deben reformular','2018-07-18 21:08:14.235528',7,2,'DO'),(4,'Prioridades','Se deben ajustar las entregas funcionales','2018-07-18 21:53:08.284651',9,2,'DO'),(5,'Gestión de Comunicaciones','Las minutas deben quedar registradas en la carpeta destinada a estos fines','2018-07-18 21:53:08.288939',9,2,'DO'),(6,'Aspectos de Forma','Las diapositivas deben mantener los colores institucionales y deben contener las directrices de biblioteca','2018-07-18 21:53:08.295251',9,2,'DO'),(7,'Aspectos de fondo','Las conclusiones deben tratar el cumplimiento de los objetivos y de los lineamientos de especialidad de la carrera','2018-07-18 21:53:08.299162',9,2,'DO'),(8,'Conclusiones','<p>No se presentan con claridad</p><p>Debe recordar los pilares de la carrera</p><p>Evidenciar el cumplimiento de los indicadores detallados en los casos de éxito</p>','2018-07-31 20:19:57.483382',6,2,'DO');
/*!40000 ALTER TABLE `projects_minutaevidence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_minutaretro`
--

DROP TABLE IF EXISTS `projects_minutaretro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_minutaretro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(1000) DEFAULT NULL,
  `minuta_id` int(11) DEFAULT NULL,
  `read` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_minutaretro_minuta_id_f0e5bc57_fk_projects_minuta_id` (`minuta_id`),
  CONSTRAINT `projects_minutaretro_minuta_id_f0e5bc57_fk_projects_minuta_id` FOREIGN KEY (`minuta_id`) REFERENCES `projects_minuta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_minutaretro`
--

LOCK TABLES `projects_minutaretro` WRITE;
/*!40000 ALTER TABLE `projects_minutaretro` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_minutaretro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_minutavalidation`
--

DROP TABLE IF EXISTS `projects_minutavalidation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_minutavalidation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(6) NOT NULL,
  `last_mod` datetime(6) NOT NULL,
  `validation_status` varchar(2) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `comment` varchar(400) NOT NULL,
  `minuta_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_minutavalidation_student_id_d14415a7_fk_core_student_id` (`student_id`),
  KEY `projects_minutavalidati_minuta_id_e85eb303_fk_projects_minuta_id` (`minuta_id`),
  CONSTRAINT `projects_minutavalidati_minuta_id_e85eb303_fk_projects_minuta_id` FOREIGN KEY (`minuta_id`) REFERENCES `projects_minuta` (`id`),
  CONSTRAINT `projects_minutavalidation_student_id_d14415a7_fk_core_student_id` FOREIGN KEY (`student_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_minutavalidation`
--

LOCK TABLES `projects_minutavalidation` WRITE;
/*!40000 ALTER TABLE `projects_minutavalidation` DISABLE KEYS */;
INSERT INTO `projects_minutavalidation` VALUES (8,'2018-07-13 14:39:42.953700','2018-07-13 14:40:58.840275','AP',77,'Yo Adriana M. Coca Silva  confirmo mi disponibilidad para asistir a la reunion solicitada.',6),(9,'2018-07-13 14:39:42.963024','2018-07-13 14:41:17.580085','AP',78,'Yo Jose L. Pozo Saavedra  confirmo mi disponibilidad para asistir a la reunion solicitada.',6),(10,'2018-07-13 14:39:42.972320','2018-07-13 14:41:33.014581','AP',79,'Yo Juan C. Venegas Zurita  confirmo mi disponibilidad para asistir a la reunion solicitada.',6),(11,'2018-07-18 20:57:19.772870','2018-07-18 20:57:36.993544','AP',87,'Yo Diego I. Espinoza Maldonado  confirmo mi disponibilidad para asistir a la reunion solicitada.',7),(12,'2018-07-18 21:32:15.948635','2018-07-18 21:33:36.402840','RE',87,'Estimado director, junto con saludar, el comento que por temas laborales no puedo asistir a la reunión propuesta, ya que tengo una entrevista laboral, favor solicito su colaboración para poder modificar el día y hora solicitado,\n\nquedo atento a comentarios,\n\nsaludos cordiales,\n',8),(13,'2018-07-18 21:38:12.092639','2018-07-18 21:38:52.967636','AP',87,'Yo Diego I. Espinoza Maldonado  confirmo mi disponibilidad para asistir a la reunion solicitada.',9),(15,'2018-07-30 21:09:37.049054','2018-07-30 21:09:37.049097','NR',77,'',11),(16,'2018-07-30 21:09:37.057058','2018-07-30 21:09:37.057075','NR',78,'',11),(17,'2018-07-30 21:09:37.069066','2018-07-30 21:09:37.069083','NR',79,'',11),(18,'2018-07-30 21:10:06.393527','2018-07-30 21:10:06.393547','NR',74,'',12),(19,'2018-07-30 21:10:06.401254','2018-07-30 21:10:06.401272','NR',75,'',12),(20,'2018-07-30 21:10:06.412136','2018-07-30 21:10:06.412154','NR',76,'',12),(21,'2018-07-30 21:12:18.003389','2018-07-30 21:12:18.003411','NR',85,'',13),(22,'2018-07-30 21:12:42.359263','2018-07-30 21:12:42.359283','NR',81,'',14),(23,'2018-07-30 21:12:42.367324','2018-07-30 21:12:42.367343','NR',82,'',14),(24,'2018-07-30 21:12:42.375611','2018-07-30 21:12:42.375630','NR',83,'',14),(25,'2018-07-30 21:13:03.183126','2018-07-30 21:13:03.183144','NR',80,'',15);
/*!40000 ALTER TABLE `projects_minutavalidation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_outcome`
--

DROP TABLE IF EXISTS `projects_outcome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_outcome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `letter` varchar(1) NOT NULL,
  `description` longtext NOT NULL,
  `abet_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_outcome_abet_id_945c25e8_fk_projects_abet_id` (`abet_id`),
  CONSTRAINT `projects_outcome_abet_id_945c25e8_fk_projects_abet_id` FOREIGN KEY (`abet_id`) REFERENCES `projects_abet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_outcome`
--

LOCK TABLES `projects_outcome` WRITE;
/*!40000 ALTER TABLE `projects_outcome` DISABLE KEYS */;
INSERT INTO `projects_outcome` VALUES (14,'a','Propone soluciones a problemas en Ingeniería de Software aplicando principios de matemáticas, ciencias, computación e ingeniería. ',3),(15,'b','Diseña y conduce experimentos en base al análisis e interpretación de datos relevantes en la implementación de soluciones de software',3),(16,'c','Diseña sistemas, componentes o procesos para la implementación de un sistema de software teniendo en cuenta restricciones económicas, sociales, políticas, éticas, de salud y seguridad y otras propias del entorno empresarial.',3),(17,'d','Participa en equipos multidisciplinarios liderando o desarrollando sus tareas eficientemente con profesionales de diferentes especialidades o dominios de aplicación',3),(18,'e','Identifica, formula y resuelve problemas de negocio o tecnológicos dentro del ciclo de vida de un sistema de software',3),(19,'f','Propone soluciones a problemas de Ingeniería de Software con responsabilidad profesional y ética.',3),(20,'g','Comunica ideas o resultados de manera oral o escrita con claridad y efectividad a públicos de diferentes especialidades y niveles jerárquicos',3),(21,'h','Identifica el impacto de las soluciones de Ingeniería de Software en el contexto global, económico y del entorno de la sociedad.',3),(22,'i','Actualiza los conocimientos en metodologías, técnicas, herramientas  por ser necesarios para mantener la vigencia en el desarrollo de sistemas de software',3),(23,'j','Analiza hechos del mundo contemporáneo identificando el impacto en el desempeño profesional del Ingeniero de Software',3),(24,'k','Utiliza técnicas, herramientas, metodologías, procesos, frameworks, middlewares y lenguajes de programación de última generación necesarios para la práctica de la ingeniería de software.',3);
/*!40000 ALTER TABLE `projects_outcome` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_outcomeevaluation`
--

DROP TABLE IF EXISTS `projects_outcomeevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_outcomeevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) NOT NULL,
  `abet_evaluation_id` int(11) DEFAULT NULL,
  `outcome_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projec_abet_evaluation_id_aba70785_fk_projects_abetevaluation_id` (`abet_evaluation_id`),
  KEY `projects_outcomeevalu_outcome_id_07b23af8_fk_projects_outcome_id` (`outcome_id`),
  CONSTRAINT `projec_abet_evaluation_id_aba70785_fk_projects_abetevaluation_id` FOREIGN KEY (`abet_evaluation_id`) REFERENCES `projects_abetevaluation` (`id`),
  CONSTRAINT `projects_outcomeevalu_outcome_id_07b23af8_fk_projects_outcome_id` FOREIGN KEY (`outcome_id`) REFERENCES `projects_outcome` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_outcomeevaluation`
--

LOCK TABLES `projects_outcomeevaluation` WRITE;
/*!40000 ALTER TABLE `projects_outcomeevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_outcomeevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_postulation`
--

DROP TABLE IF EXISTS `projects_postulation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_postulation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime(6) DEFAULT NULL,
  `last_mod` datetime(6) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `last_mod_user_id` int(11),
  PRIMARY KEY (`id`),
  KEY `projects_postulation_project_id_00ec0d48_fk_projects_project_id` (`project_id`),
  KEY `projects_postulation_student_id_0f11361a_fk_core_student_id` (`student_id`),
  KEY `projects_postul_last_mod_user_id_fff5a951_fk_core_userprofile_id` (`last_mod_user_id`),
  CONSTRAINT `projects_postul_last_mod_user_id_fff5a951_fk_core_userprofile_id` FOREIGN KEY (`last_mod_user_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `projects_postulation_project_id_00ec0d48_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `projects_postulation_student_id_0f11361a_fk_core_student_id` FOREIGN KEY (`student_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_postulation`
--

LOCK TABLES `projects_postulation` WRITE;
/*!40000 ALTER TABLE `projects_postulation` DISABLE KEYS */;
INSERT INTO `projects_postulation` VALUES (7,'2018-06-01 19:07:22.784000','2018-06-01 19:07:22.783000','AP',36,19,NULL),(8,'2018-06-01 19:58:08.285000','2018-06-01 19:58:08.285000','AP',38,20,NULL),(9,'2018-06-01 19:58:08.312000','2018-06-01 19:58:08.312000','AP',38,21,NULL),(10,'2018-06-01 20:21:33.072000','2018-06-01 20:21:33.072000','AP',39,22,NULL),(11,'2018-06-01 20:21:33.107000','2018-06-01 20:21:33.107000','AP',39,23,NULL),(12,'2018-06-01 20:43:13.171000','2018-06-01 20:43:13.171000','AP',40,24,NULL),(13,'2018-06-01 20:52:50.272000','2018-06-01 20:52:50.272000','AP',41,25,NULL),(14,'2018-06-01 21:06:03.652000','2018-06-01 21:06:03.652000','AP',42,26,NULL),(15,'2018-06-01 23:06:05.617000','2018-06-01 23:06:05.617000','AP',44,28,NULL),(16,'2018-06-05 01:17:33.487000','2018-06-05 01:17:33.487000','AP',54,17,NULL),(17,'2018-06-05 03:46:31.413000','2018-06-05 03:46:31.412000','AP',58,36,NULL),(18,'2018-06-05 15:10:50.735000','2018-06-05 15:10:50.735000','AP',59,37,NULL),(49,'2018-06-28 17:05:36.592406','2018-06-28 17:05:36.592334','AP',88,58,NULL),(50,'2018-06-28 17:05:36.620713','2018-06-28 17:05:36.620648','AP',88,59,NULL),(51,'2018-06-28 17:11:32.449070','2018-06-28 17:11:32.449005','AP',89,61,NULL),(52,'2018-06-28 17:11:32.477133','2018-06-28 17:11:32.477066','AP',89,62,NULL),(69,'2018-07-12 19:37:57.441712','2018-07-12 19:37:57.441637','AP',107,80,NULL),(70,'2018-07-12 19:43:48.856211','2018-07-12 19:43:48.856135','AP',108,81,NULL),(71,'2018-07-12 19:43:48.880987','2018-07-12 19:43:48.880911','AP',108,82,NULL),(72,'2018-07-12 19:43:48.909946','2018-07-12 19:43:48.909860','AP',108,83,NULL),(73,'2018-07-12 20:21:26.103786','2018-07-12 20:21:26.103721','AP',109,85,NULL),(74,'2018-07-18 20:51:44.938846','2018-07-18 20:54:32.162549','AP',112,87,NULL);
/*!40000 ALTER TABLE `projects_postulation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_project`
--

DROP TABLE IF EXISTS `projects_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `project_image` varchar(100),
  `vacancy_total` int(10) unsigned DEFAULT NULL,
  `vacancy_available` int(10) unsigned DEFAULT NULL,
  `section` int(10) unsigned DEFAULT NULL,
  `description` longtext NOT NULL,
  `year` int(10) unsigned DEFAULT NULL,
  `period` varchar(2) DEFAULT NULL,
  `carreer` int(11) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `guide_teacher_id` int(11) DEFAULT NULL,
  `cloud` varchar(300) NOT NULL,
  `desem_grade` int(11) NOT NULL,
  `final_grade` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_project_author_id_9499cc7e_fk_core_userprofile_id` (`author_id`),
  KEY `projects_projec_guide_teacher_id_e11abaf8_fk_core_userprofile_id` (`guide_teacher_id`),
  CONSTRAINT `projects_projec_guide_teacher_id_e11abaf8_fk_core_userprofile_id` FOREIGN KEY (`guide_teacher_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `projects_project_author_id_9499cc7e_fk_core_userprofile_id` FOREIGN KEY (`author_id`) REFERENCES `core_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_project`
--

LOCK TABLES `projects_project` WRITE;
/*!40000 ALTER TABLE `projects_project` DISABLE KEYS */;
INSERT INTO `projects_project` VALUES (35,'Sistema de administración de licencias de software para departamentos de tecnología','2018-06-01 18:10:34.000000','',0,1,NULL,'Sistema de administracion de licencias de software, permite la optimizacion de los recursos de infraestructura tecnologica, almacenando datos y procesamiento de solicitudes.',2018,'10',1,'EC',1,1,'1',0,0),(36,'Intercambio de juguetes usados','2018-06-01 19:07:22.000000','',0,1,NULL,'Facilitar la Adquisicion de juguetes nuevos, a traves de la implementacion de una red social para ninos.',2018,'10',1,'EC',2,2,'1',0,0),(38,'Simulador de Entrega de Licencias de conducir de un Mapa 3D','2018-06-01 19:58:08.123000','',1,3,NULL,'Se basa en la entrega de licencia de conducir en un simulador de mapa 3d el cual pueda realizar diferentes \"actos\"\npara dar una mejor evaluacion con respecto a los conductores.',2018,'10',1,'AC',2,2,'1',0,0),(39,'Plataforma de Solicitud de Cargo','2018-06-01 20:21:32.920000','',0,2,NULL,'\nBuscar en el mercado plataformas que se dedican a la postulacion y seleccion de cargos. ',2018,'10',1,'EC',2,2,'1',0,0),(40,'Gestión de Instituciones','2018-06-01 20:43:13.000000','',0,1,NULL,'Integrar nuevas herramientas  a un sistema  para la gestion de instituciones.',2018,'10',1,'EC',2,2,'1',0,0),(41,'MultiSede','2018-06-01 20:52:50.127000','',0,1,NULL,'Disenar, desarrollar e integrar modulos a un aplicativo web que centralice el manejo de la informacion de las organizaciones sin fines de lucro.',2018,'10',1,'EC',2,2,'1',0,0),(42,'MultiSede','2018-06-01 21:06:03.506000','',0,1,NULL,'Herramienta para el manejo de instituciones reducir el trabajo necesario que conlleva administrar la informacion y usuarios pertenecientes o no a estas instituciones y/o empresas.\n',2018,'10',1,'EC',2,2,'1',0,0),(43,'Control de Asistencia Georeferenciado con Doble Autentificación','2018-06-01 22:33:11.000000','',0,1,NULL,'Desarrollo de un sistema de control de asistencia georeferenciado\r\ncon doble autenticacion a traves de una aplicacion web para\r\nmejorar el proceso de registro y control de asistencia.',2018,'10',1,'EC',1,1,'1',0,0),(44,'Proceso de Administración y Gestion Empresarial','2018-06-01 23:06:05.000000','',0,1,NULL,'Desarrollar una sistema adaptable para apoyar los procesos de administracion y gestion empresarial, implementando un software ERP.',2018,'10',1,'EC',1,1,'1',0,0),(45,'Proceso de Evaluación Docente','2018-06-01 23:24:53.000000','',0,1,NULL,'Monitorear el proceso de Evaluacion docente por medio de la plataforma web.',2018,'10',1,'EC',1,1,'1',0,0),(52,'Plataforma de Evaluación de Eficiencia Docente','2018-06-02 00:30:32.000000','',0,1,NULL,'Proyecto que busca evaluar la eficiencia del docente a traves de los alumnos.',2018,'10',1,'EC',1,1,'1',0,0),(53,'NSAgro','2018-06-04 23:40:50.200000','',0,1,NULL,'Construir un Aplicativo web de seguimiento de ventas del vendedor, a traves de una plataforma que permita tener informacion detallada.',2018,'10',1,'EC',1,1,'1',0,0),(54,'Proceso de Acreditación de ABET','2018-06-05 01:17:33.000000','',0,1,NULL,'Establecer una mejora para el proceso de acreditacion de los programas de curso para abet.',2018,'10',1,'EC',2,2,'1',0,0),(57,'Mejoramiento en los Procesos de Administración, con Procesamiento de Datos.','2018-06-05 03:40:45.000000','',0,1,NULL,'Desarrollar un sistema adaptable para apoyar los procesos de administracion y gestion empresarial.',2018,'10',1,'EC',1,1,'1',0,0),(58,'Plataforma de generación de certificados','2018-06-05 03:46:31.000000','',0,1,NULL,'generacion de certificaciones enfocada en conferencias o workshops para dar evidencias',2018,'10',1,'EC',2,2,'1',0,0),(59,'MultiSede','2018-06-05 15:10:50.587000','',0,1,NULL,'Herramienta para el manejo de instituciones reducir el trabajo necesario que conlleva administrar la informacion y usuarios pertenecientes o no a estas instituciones y/o empresas.',2018,'10',1,'EC',2,2,'1',0,0),(88,' Testeo de prototipo','2018-06-28 17:05:36.567431','',0,2,NULL,'esta es una prueba\n',2018,'10',1,'EC',5,5,'1',0,0),(89,'testeo 2','2018-06-28 17:11:32.428807','',0,2,NULL,'es una prueba',2018,'10',1,'EC',5,5,'1',0,0),(104,'Integrador de gestión de entidades financieras','2018-07-10 18:04:17.000000','',0,3,NULL,'Proyecto de software asociado a la integracion de plataformas que contienen informacion financiera sensible para la empresa Tanner',2018,'15',1,'EC',1,1,'https://drive.google.com/open?id=1F2sYOu5eS7XSMXvXfDHuPqI5QbVn6BA_',0,0),(105,'Gestión automatizada de proyectos','2018-07-10 18:48:27.000000','',3,0,NULL,'Proyecto de software orientado al servicio que pretende mejorar el rendimiento de los indicadores base de proyectos',2018,'15',1,'EC',1,1,'https://drive.google.com/open?id=1LLa484rmR3bQ_8UqzMcnbkNucpC38yqF',0,0),(106,'Plataforma integrada de eventos registrables','2018-07-12 02:50:34.000000','',2,2,NULL,'Servicio multiplataforma dedicado a la integracion de plataformas de seguimiento',2018,'20',1,'ED',1,NULL,'',0,0),(107,'Registro Clínico Electrónico','2018-07-12 19:37:57.000000','',1,0,NULL,'Una de las principales dificultades que enfrentamos como usuarios del sistema de salud en Chile, ya sea publico o privado es que en muy pocas ocasiones contamos con la informacion disponible de nuestro historial clinico. Ya sea porque lo extraviamos o por que se encuentra repartido en los distintos prestadores de salud en donde nos hemos atendido. Teniendo que incurrir eventualmente en nuevas consultas medicas solo para conseguir ordenes de examenes, diagnosticos, recetas o resultado de examenes y/o procedimientos realizados en alguna oportunidad de nuestra vida.',2018,'15',1,'EC',1,12,'',0,0),(108,'Gestión de proyectos ABB','2018-07-12 19:43:48.000000','',3,0,NULL,'El presente proyecto tiene como objetivo reducir en al menos un 80% el tiempo de confeccion de entregables, documentos asociados a un proyectos necesarios para cumplir con ciertos hitos, relativos a proyectos y crear un sistema de visualizacion global de proyectos denominado como Global Gantt , utilizados en el area de operaciones de Power Grid-Grids Automation de la empresa ABB S.A Chile, mediante una herramienta personalizada que permita automatizar los entregables externos dependiendo la informacion de la Gantt, la generacion de estandares de trabajo y de documentacion, mediante la intervencion de un software ya existente, como lo es Microsoft Project, un Add-in y un motor de base de datos sincronizado en la nube.',2018,'15',1,'EC',1,12,'',0,0),(109,'Gestión de estacionamientos para personas en situación de discapacidad','2018-07-12 20:21:26.000000','',1,0,NULL,'Esta propuesta nace como una solucion a un problema real de una persona, la cual debe lidiar regularmente con la dificultad de encontrar estacionamientos acondicionados especialmente para las personas en situacion de discapacidad, que por lo demas, deben existir segun la LEY No 18.290, por lo tanto, sabiendo que hay un problema real, con una ley que intenta dar un tipo de solucion,para la dificultad  que presentan estas personas, se ha decidido emprender el desafio de construir una aplicacion que facilite la visibilidad de los estacionamientos habilitados especialmente para ellos.',2018,'15',1,'EC',1,12,'',0,0),(110,'Sistema de retroalimentación inmediata','2018-07-12 20:39:35.000000','',1,1,NULL,'En la actualidad se requiere agilizar los procesos de retroalimentacion de distintas reuniones con el respectivo respaldo auditable. Es relevante un sector centralizado con estandares de usabilidad y navegabilidad',2018,'20',1,'ED',1,NULL,'',0,0),(111,'Identity','2018-07-18 20:28:49.525403','',2,2,NULL,'Sistema de control de participación en actividades extra programáticas ',2018,'20',1,'RE',2,2,'',0,0),(112,'Sudo TIC','2018-07-18 20:44:28.000000','',1,0,NULL,'Sistema de administración de tecnología, el proyecto contempla las tres áreas del perfil de egreso, arquitectura, desarrollo y gestión de proyecto.',2018,'20',1,'EC',2,2,'',0,0),(113,'Propuesta de gestión de existencias','2018-07-23 00:18:46.955309','img/logo_unab.png',1,1,NULL,'Control de existencias, activos fijos y contabilidad el ingreso de información se realiza sólo una vez. Por ejemplo, al adquirir un activo fijo, éste será automáticamente registrado en la cuenta de activos. Si compro un producto a un proveedor, se registrará su existencia y su cuenta asociada, quedando todo registrado en libros contables.',2018,'15',1,'ED',1,12,'',0,0);
/*!40000 ALTER TABLE `projects_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_project_shared_teachers`
--

DROP TABLE IF EXISTS `projects_project_shared_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_project_shared_teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `userprofile_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_project_shared_teachers_project_id_65b2c94d_uniq` (`project_id`,`userprofile_id`),
  KEY `projects_project__userprofile_id_fc49e5a7_fk_core_userprofile_id` (`userprofile_id`),
  CONSTRAINT `projects_project__userprofile_id_fc49e5a7_fk_core_userprofile_id` FOREIGN KEY (`userprofile_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `projects_project_shar_project_id_902ab978_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_project_shared_teachers`
--

LOCK TABLES `projects_project_shared_teachers` WRITE;
/*!40000 ALTER TABLE `projects_project_shared_teachers` DISABLE KEYS */;
INSERT INTO `projects_project_shared_teachers` VALUES (1,104,12),(2,105,12),(3,107,1),(5,108,1),(4,109,1);
/*!40000 ALTER TABLE `projects_project_shared_teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_project_tags`
--

DROP TABLE IF EXISTS `projects_project_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_project_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_project_tags_project_id_5891719a_uniq` (`project_id`,`tag_id`),
  KEY `projects_project_tags_tag_id_c949773d_fk_projects_tag_id` (`tag_id`),
  CONSTRAINT `projects_project_tags_project_id_9bbfa17b_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `projects_project_tags_tag_id_c949773d_fk_projects_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `projects_tag` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_project_tags`
--

LOCK TABLES `projects_project_tags` WRITE;
/*!40000 ALTER TABLE `projects_project_tags` DISABLE KEYS */;
INSERT INTO `projects_project_tags` VALUES (211,35,31),(212,36,32),(213,36,33),(214,36,34),(217,38,38),(218,38,39),(215,38,40),(216,38,41),(219,39,42),(220,39,43),(221,39,44),(222,39,45),(223,40,46),(224,40,47),(225,41,48),(226,41,49),(227,42,50),(228,42,51),(229,42,52),(230,42,53),(231,43,54),(232,43,55),(233,44,56),(234,44,57),(235,44,58),(236,45,59),(237,45,60),(238,53,61),(239,53,62),(240,58,66),(249,88,67),(250,89,69),(255,104,70),(256,104,71),(257,104,72),(258,105,73),(259,105,74),(260,105,75),(261,106,73),(262,106,76),(263,106,77),(264,107,78),(265,107,79),(266,107,80),(267,108,81),(268,108,82),(269,108,83),(270,109,84),(271,109,85),(272,109,86),(275,110,73),(273,110,87),(274,110,88),(276,111,27),(278,111,54),(279,111,73),(277,111,89),(280,112,27),(282,112,28),(281,112,90),(284,113,70),(283,113,91),(285,113,92),(286,113,93);
/*!40000 ALTER TABLE `projects_project_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_tag`
--

DROP TABLE IF EXISTS `projects_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_tag`
--

LOCK TABLES `projects_tag` WRITE;
/*!40000 ALTER TABLE `projects_tag` DISABLE KEYS */;
INSERT INTO `projects_tag` VALUES (16,'yonnis'),(17,'tag'),(18,'v2'),(19,'docente'),(20,'list'),(21,'alumno'),(22,'titulo'),(23,'mtv'),(24,'propuestas'),(25,'tga'),(26,'licencias'),(27,'tecnologia'),(28,'desarrollo'),(29,'gestion'),(30,'arquitectura'),(31,'django'),(32,'Álbum'),(33,' Desarrollo'),(34,'Social '),(35,'Sistema'),(36,'Nuevas Herramientas'),(37,'Gestion de reuniones'),(38,'Conductor '),(39,'Escuela de Conducir '),(40,'Pruebas Teorícas'),(41,'Simulación '),(42,'Plataformas'),(43,'Empresa'),(44,'Cargo'),(45,'Solicitud'),(46,'Herraminetas'),(47,'Registrar'),(48,'Diseño'),(49,'Gestión de Finanzas'),(50,'Información '),(51,'Instituciones '),(52,'Herramientas '),(53,'Administración '),(54,'Control '),(55,'Registro '),(56,'Gestión Empresarial'),(57,'Software '),(58,'Data Real '),(59,'Web '),(60,'Evaluación '),(61,'Aplicación '),(62,'Ventas '),(63,'Registro de Datos'),(64,'Inventario'),(65,'Inventarios '),(66,'certificados'),(67,'testeo'),(68,'teas'),(69,'terergt'),(70,'integración'),(71,'servicios'),(72,'financieros'),(73,'seguimiento'),(74,'estándares'),(75,'agilidad'),(76,'métricas'),(77,'eventos'),(78,'Ficha Clínica'),(79,'RCE'),(80,'Historia Clínica'),(81,'Proyectos'),(82,'Automatización'),(83,'Costos'),(84,'Inclusión'),(85,'Colaboración'),(86,' Movilidad'),(87,'Retroalimentación'),(88,'Centralizada'),(89,'innovación'),(90,'scrum'),(91,'pyme'),(92,'recursos'),(93,'centralizado');
/*!40000 ALTER TABLE `projects_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_teacher_postulation`
--

DROP TABLE IF EXISTS `projects_teacher_postulation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_teacher_postulation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime(6) DEFAULT NULL,
  `last_mod` datetime(6) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_teacher_postu_author_id_d9374769_fk_core_userprofile_id` (`author_id`),
  KEY `projects_teacher_post_project_id_d5e40839_fk_projects_project_id` (`project_id`),
  KEY `projects_teacher_post_teacher_id_2f087bc3_fk_core_userprofile_id` (`teacher_id`),
  CONSTRAINT `projects_teacher_post_project_id_d5e40839_fk_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects_project` (`id`),
  CONSTRAINT `projects_teacher_post_teacher_id_2f087bc3_fk_core_userprofile_id` FOREIGN KEY (`teacher_id`) REFERENCES `core_userprofile` (`id`),
  CONSTRAINT `projects_teacher_postu_author_id_d9374769_fk_core_userprofile_id` FOREIGN KEY (`author_id`) REFERENCES `core_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_teacher_postulation`
--

LOCK TABLES `projects_teacher_postulation` WRITE;
/*!40000 ALTER TABLE `projects_teacher_postulation` DISABLE KEYS */;
INSERT INTO `projects_teacher_postulation` VALUES (33,'2018-07-12 19:37:57.336914','2018-07-12 20:20:49.339730','AP',1,107,12),(34,'2018-07-12 19:43:48.813151','2018-07-12 20:20:52.729774','AP',1,108,12),(35,'2018-07-12 20:21:26.095128','2018-07-12 20:21:50.989420','AP',1,109,12),(36,'2018-07-12 20:51:14.216836','2018-07-12 20:52:01.233590','RE',12,110,12),(37,'2018-07-23 00:18:46.966890',NULL,'AS',1,113,12);
/*!40000 ALTER TABLE `projects_teacher_postulation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-29  0:48:41
