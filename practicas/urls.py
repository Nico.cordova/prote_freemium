from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
from practicas import views as practicas_view


urlpatterns = [
    url(r'^index/$', practicas_view.index, name="index_practicas"),
    url(r'^solicitar_practica/$', practicas_view.solicitar_practica, name="solicitar_practica"),
    url(r'^crear_solicitud/$', practicas_view.crear_solicitud, name="crear_solicitud"),
    url(r'^dsa_solicitudes_pendientes/$', practicas_view.dsa_solicitudes_pendientes, name="dsa_solicitudes_pendientes"),
    url(r'^dc_solicitudes_pendientes/$', practicas_view.dc_solicitudes_pendientes, name="dc_solicitudes_pendientes"),
    url(r'^aprobar_solicitud_dsa/$', practicas_view.aprobar_solicitud_dsa, name="aprobar_solicitud_dsa"),
    url(r'^aprobar_solicitud_se/$', practicas_view.aprobar_solicitud_se, name="aprobar_solicitud_se"),
    url(r'^rechazar_solicitud_dsa/$', practicas_view.rechazar_solicitud_dsa, name="rechazar_solicitud_dsa"),
    url(r'^rechazar_solicitud_se/$', practicas_view.rechazar_solicitud_se, name="rechazar_solicitud_se"),
    url(r'^detalles_solicitud/(?P<pk>\d+)$',practicas_view.detalles_solicitud, name="detalles_solicitud"),
    url(r'^semana_a_semana/(?P<pk>\d+)$',practicas_view.semana_a_semana, name="semana_a_semana"),
    url(r'^formulario_final/(?P<pk>\d+)$',practicas_view.formulario_final, name="formulario_final"),
    url(r'^retroalimentar_tareas/(?P<pk>\d+)$',practicas_view.retroalimentar_tareas, name="retroalimentar_tareas"),
    url(r'^get_encargados/$', practicas_view.get_encargados, name="get_encargados"),
    url(r'^aprobar_solicitud_dc/$', practicas_view.aprobar_solicitud_dc, name="aprobar_solicitud_dc"),
    url(r'^configurar_practica/$', practicas_view.configurar_practica, name="configurar_practica"),
    url(r'^tematica_semana/$', practicas_view.tematica_semana, name="tematica_semana"),
    url(r'^tarea_semana/$', practicas_view.tarea_semana, name="tarea_semana"),
    url(r'^delete_task/$', practicas_view.delete_task, name="delete_task"),
    url(r'^retro_al_tarea/$', practicas_view.retro_al_tarea, name="retro_al_tarea"),
    url(r'^get_task/$', practicas_view.get_task, name="get_task"),
    url(r'^edit_task/$', practicas_view.edit_task, name="edit_task"),
    url(r'^crear_ausencia/$', practicas_view.crear_ausencia, name="crear_ausencia"),
    url(r'^upload_justificativo/$', practicas_view.upload_justificativo, name="upload_justificativo"),
    url(r'^get_justificativos/$', practicas_view.get_justificativos, name="get_justificativos"),
    url(r'^crud_empresas/$', practicas_view.crud_empresas, name="crud_empresas"),
    url(r'^create_final/$', practicas_view.create_final, name="create_final"),
    url(r'^listado_practicas/$', practicas_view.lista_practicas, name="listado_practicas"),
    url(r'^perfil_practicante/(?P<pk>\d+)$', practicas_view.perfil_practicante, name="perfil_practicante"),
    url(r'^perfil_practicante/(?P<pk>\d+)/semana/(?P<pk_sem>\d+)/comentar_tarea/(?P<pk_tar>\d+)$', practicas_view.comentar_tarea, name="comentar_tarea"),
    url(r'^agregar_comentario/$', practicas_view.agregar_comentario, name="agregar_comentario"),
    url(r'^eliminar_comentario/(?P<pk>\d+)$', practicas_view.eliminar_comentario, name="eliminar_comentario"),
    url(r'^view_informe_dc/(?P<pk>\d+)$', practicas_view.view_informe_dc, name="view_informe_dc"),
    url(r'^get_comentario/$', practicas_view.get_comentario, name="get_comentario"),
    url(r'^edit_comentario/$', practicas_view.edit_comentario, name="edit_comentario"),
    url(r'^publicar_practica/$', practicas_view.publicar_practica, name="publicar_practica"),
    url(r'^practicas_publicadas/$', practicas_view.practicas_publicadas, name="practicas_publicadas"),
    url(r'^postular_practicas/$', practicas_view.postular_practicas, name="postular_practicas"),
    url(r'^obt_sgt/$', practicas_view.obt_sgt, name="obt_sgt"),
    url(r'^edit_solicitud/$', practicas_view.edit_solicitud, name="edit_solicitud"),
    url(r'^terminar_practica/$', practicas_view.terminar_practica, name="terminar_practica"),
    url(r'^practicas_terminadas/$', practicas_view.practicas_terminadas, name="practicas_terminadas"),
    url(r'^obs_director_final/$', practicas_view.obs_director_final, name="obs_director_final"),
    url(r'^solicitud_convalidar/$', practicas_view.solicitud_convalidar, name="solicitud_convalidar"),
]
