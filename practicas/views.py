# -*- coding:UTF-8 -*-
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from core.models import UserProfile, Student
from projects.models import Project, Tag, Postulation, Feedback
from django.views.decorators.csrf import csrf_protect
from datetime import datetime
from string import maketrans
from operator import itemgetter
from unidecode import unidecode
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import PermissionDenied
from core import models
from models import *
from .forms import CommentForm
import requests
import json
import ast
import sys
import os
import math
import datetime

@login_required(login_url='/')
def index(request):
	template_name = 'Practicas/index_practicas.html'
	data = {}
	if request.session["type"] == "EE":
		user = UserProfile.objects.get(rut=request.session["rut"])
		practicas = Solicitud_Practica.objects.filter(encargado=user)
		data["practicas"] = []
		for x in practicas:
			aux = []
			pendientes = 0
			terminadas = 0
			try:
				if x.practica:
					aux.append(x)
					for z in x.practica.week_practica_set.all():
						pendientes = len(Task.objects.filter(status=0,semana=z)) + pendientes
						terminadas = len(Task.objects.filter(status=1,semana=z)) + terminadas
					if len(Retro.objects.filter(author=x.alumno.rut)) != 0:
						aux.append("ok")
					else:
						aux.append("not")
				aux.append(pendientes)
				aux.append(terminadas)
				data["practicas"].append(aux)
			except:
				print "no Practica"

	if request.session["type"] == "SE":
		data["SA"] = len(RespuestaSolicitud.objects.filter(tipo=1,author=request.session["rut"]))
		data["SR"] = len(RespuestaSolicitud.objects.filter(tipo=0,author=request.session["rut"]))
		solicitudes = Solicitud_Practica.objects.all()
		data["solicitudes"] = [r for r in solicitudes]
		for x in solicitudes:
			if x.step != "SE":
				data["solicitudes"].remove(x)
		data["SP"] = len(data["solicitudes"])
		data["ST"] = data["SA"] + data["SR"] + data["SP"]

	if request.session["type"] == "DS":
		data["SA"] = len(RespuestaSolicitud.objects.filter(tipo=1,author=request.session["rut"]))
		data["SR"] = len(RespuestaSolicitud.objects.filter(tipo=0,author=request.session["rut"]))
		data["SP"] = 0
		solicitudes = Solicitud_Practica.objects.all()
		data["solicitudes"] = [r for r in solicitudes]
		for x in solicitudes:
			if x.step != "DS":
				data["solicitudes"].remove(x)
			else:
				data["SP"] += 1
		data["ST"] = data["SA"] + data["SR"] + data["SP"]
		return render(request, template_name, data)
	
	if request.session["type"] == "AL":
		alumno = Student.objects.get(rut=request.session["rut"])
		if len(Solicitud_Practica.objects.filter(alumno=alumno)) != 0:
			data["solicitud"] = Solicitud_Practica.objects.get(alumno=alumno)
		try:
			data["solicitud"] = Solicitud_Practica.objects.get(alumno=alumno)				
		except:
			print "Hola"
	return render(request, template_name, data)

@login_required(login_url='/')
def solicitar_practica(request):
    if request.session["type"] == "AL":
        template_name = 'Practicas/solicitar_practica.html'
        data = {}
        empresas = Empresa.objects.all()
        data["empresas"] = empresas
        alumno = Student.objects.get(rut=request.session["rut"])
        if len(Solicitud_Practica.objects.filter(alumno=alumno)) != 0:
            data["solicitud"] = 0
        return render(request, template_name, data)
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

@login_required(login_url='/')
def dsa_solicitudes_pendientes(request):
    if request.session["type"] == "DS":
        template_name = 'Practicas/dsa_solicitudes_pendientes.html'
        data = {}
        solicitudes = Solicitud_Practica.objects.all()
        data["solicitudes"] = [r for r in solicitudes]
        for x in solicitudes:
            aux = x.respuestasolicitud_set.all()
            if len(aux) != 0:
                for y in aux:
                    if y.emisor == "dsa":
                        data["solicitudes"].remove(x)
        return render(request, template_name, data)
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

@login_required(login_url='/')
def dc_solicitudes_pendientes(request):
	if request.session["type"] == "DC":
		template_name = 'Practicas/dc_solicitudes_pendientes.html'
		data = {}
		solicitudes = Solicitud_Practica.objects.all()
		data["aux"] = [r for r in solicitudes]
		data["solicitudes"] = []
		for x in solicitudes:
			if x.step == "DC":
				data["solicitudes"].append(x)
		return render(request, template_name, data)
	else:
		return HttpResponseRedirect(reverse_lazy('index'))

@login_required(login_url='/')
def detalles_solicitud(request,pk):
    if request.session["type"] == "DC":
        template_name = 'Practicas/detalles_solicitud.html'
        data = {}
        data["solicitud"] = Solicitud_Practica.objects.get(pk=int(pk))
        try:
            solicitud = data["solicitud"]
            for x in solicitud.respuestasolicitud_set.all():
                if x.emisor == "DS":
                    data["dsa"] = "completed"
                    data["dsa_obj"] = x
                if x.emisor == "SE":
                    data["se"] = "completed"
                    data["se_obj"] = x
        except:
            print "Hola"

        return render(request, template_name, data)
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

@login_required(login_url='/')
def crear_solicitud(request):
    data = {}
    print request.POST
    print request.FILES
    if int(request.POST["empresa_solicitar"]) != 0:
        empresa = Empresa.objects.get(pk=request.POST["empresa_solicitar"])
        if request.POST["encargado_empresa_solicitar"] != 0:
            profile = UserProfile.objects.get(pk=int(request.POST["encargado_empresa_solicitar"]))
    else:
        if len(Empresa.objects.filter(rut=request.POST["rut_nueva_empresa"])) != 0:
			empresa = Empresa.objects.get(rut=request.POST["rut_nueva_empresa"])
			if len(UserProfile.objects.filter(rut=request.POST["rut_encargado_nueva_empresa"])) != 0:
				profile = UserProfile.objects.get(rut=request.POST["rut_encargado_nueva_empresa"])
			else:
				header = {"Content-Type": 'application/x-www-form-urlencoded',
				  'Authorization': "Token " + request.session["token"]}
				post_data = {'nombre': request.POST["nombre_encargado_nueva_empresa"].split(" ")[0],
							'rut': request.POST["rut_encargado_nueva_empresa"],
							'email':request.POST["mail_encargado_nueva_empresa"],
							'username':request.POST["mail_encargado_nueva_empresa"].split("@")[0],
							'type':"EE",
							"apellidos":request.POST["nombre_encargado_nueva_empresa"].split(" ")[1]}
				response = requests.post(
					'http://supreme.webtic.cl/get/create_user/', headers=header, data=post_data)
				content = ast.literal_eval(response.content)
				if content["respuesta"] == 1:
					profile = UserProfile.objects.create(api_user=content["user_api"],rut=request.POST["rut_encargado_nueva_empresa"],name=request.POST["nombre_encargado_nueva_empresa"])
					profile.save()
					empresa.funcionarios.add(profile)
					empresa.save()
				else:
					return HttpResponseRedirect(reverse_lazy('solicitar_practica'))

        else:
			empresa = Empresa.objects.create(rut=request.POST["rut_nueva_empresa"],nombre=request.POST["nombre_nueva_empresa"])
			empresa.save()
			header = {"Content-Type": 'application/x-www-form-urlencoded',
				  'Authorization': "Token " + request.session["token"]}
			post_data = {'nombre': request.POST["nombre_encargado_nueva_empresa"],
						'rut': request.POST["rut_encargado_nueva_empresa"],
						'email':request.POST["mail_encargado_nueva_empresa"],
						'username':request.POST["mail_encargado_nueva_empresa"].split("@")[0],
						'type':"EE",
						"apellidos":""}
			response = requests.post(
				'http://supreme.webtic.cl/get/create_user/', headers=header, data=post_data)
			content = ast.literal_eval(response.content)
			if content["respuesta"] == 1:
				profile = UserProfile.objects.create(api_user=content["user_api"],rut=request.POST["rut_encargado_nueva_empresa"],name=request.POST["nombre_encargado_nueva_empresa"])
				profile.save()
				empresa.funcionarios.add(profile)
				empresa.save()
			else:
				return HttpResponseRedirect(reverse_lazy('solicitar_practica'))
    alumno = Student.objects.get(rut=request.session["rut"])
    solicitud = Solicitud_Practica.objects.create(alumno=alumno,
                                                  carrera = alumno.carreers.split("-")[0],
                                                 observacion=request.POST["observacion_solicitar"],
                                                 file=request.FILES["file_solicitar"],
                                                 empresa=empresa,
												 encargado=profile)
    solicitud.save()
    return HttpResponseRedirect(reverse_lazy('solicitar_practica'))

@login_required(login_url='/')
def aprobar_solicitud_dsa(request):
	data = {}
	solicitud = Solicitud_Practica.objects.get(pk=request.POST["pk_solicitud_aprobar"])
	solicitud.step = "SE"
	solicitud.save()
	aux = RespuestaSolicitud.objects.create(observacion=request.POST["comentario"],
										   emisor="DS",
											author=request.session["rut"],
										   tipo=1,
										   solicitud=solicitud)
	aux.save()
	return HttpResponseRedirect(reverse_lazy('index'))

@login_required(login_url='/')
def aprobar_solicitud_dc(request):
	data = {}
	print request.POST
	solicitud = Solicitud_Practica.objects.get(pk=request.POST["pk_solicitud_aprobar"])
	solicitud.step == "FF"
	solicitud.save()
	aux = RespuestaSolicitud.objects.create(observacion=request.POST["comentario"],
                                           emisor="DC",
                                            author=request.session["rut"],
                                           tipo=1,
                                           solicitud=solicitud)
	aux.save()
	practica = Practica.objects.create(solicitud=solicitud,status="AC")
	practica.save()
	return HttpResponseRedirect(reverse_lazy('dc_solicitudes_pendientes'))

@login_required(login_url='/')
def aprobar_solicitud_se(request):
	data = {}
	solicitud = Solicitud_Practica.objects.get(pk=request.POST["pk_solicitud_aprobar"])
	solicitud.step = "DC"
	solicitud.save()
	aux = RespuestaSolicitud.objects.create(observacion=request.POST["comentario"],
										   emisor="SE",
										   author=request.session["rut"],
										   tipo=1,
										   solicitud=solicitud)
	aux.save()
	return HttpResponseRedirect(reverse_lazy('index'))

@login_required(login_url='/')
def rechazar_solicitud_dsa(request):
	data = {}
	solicitud = Solicitud_Practica.objects.get(pk=request.POST["pk_solicitud_rechazar"])
	solicitud.step="AL"
	solicitud.save()
	aux = RespuestaSolicitud.objects.create(observacion=request.POST["comentario"],
										   emisor="DS",
										   author=request.session["rut"],
										   tipo=0,
										   solicitud=solicitud)
	aux.save()
	return HttpResponseRedirect(reverse_lazy('index'))


@login_required(login_url='/')
def rechazar_solicitud_se(request):
	data = {}
	solicitud = Solicitud_Practica.objects.get(pk=request.POST["pk_solicitud_rechazar"])
	solicitud.step="AL"
	solicitud.save()
	aux = RespuestaSolicitud.objects.create(observacion=request.POST["comentario"],
										   emisor="SE",
										   author=request.session["rut"],
										   tipo=0,
										   solicitud=solicitud)
	aux.save()
	return HttpResponseRedirect(reverse_lazy('index'))

@login_required(login_url='/')
def semana_a_semana(request,pk):
	data = {}
	template_name = "Practicas/semana_a_semana.html"
	solicitud = Solicitud_Practica.objects.get(pk=pk)
	data["solicitud"] = solicitud
	data["semanas"] = solicitud.practica.week_practica_set.all()
	return render(request, template_name, data)

@login_required(login_url='/')
def get_encargados(request):
	if int(request.POST["pk"]) != 0:
		empresa = Empresa.objects.get(pk=int(request.POST["pk"]))
		html = '<option value="0" selected> El encargado no aparece en la lista </option>'
		for x in empresa.funcionarios.all():
			html += '<option value="' + str(x.pk) +'">'+str(x)+'</option>'
		response = {"html":html}
	else:
		response = {"respuesta":1}
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def configurar_practica(request):
	data = {}
	print request.POST
	practica = Practica.objects.get(pk=int(request.POST["pk_practica"]))
	practica.horas_diarias = request.POST["horas_dia"]
	practica.dias_semana = request.POST["semanas"]
	practica.fecha_inicio = request.POST["fecha_inicio"]
	print int(request.POST["horas_dia"])*int(request.POST["semanas"])
	total = int(math.ceil(360.0/(float(request.POST["horas_dia"])*float(request.POST["semanas"]))))
	practica.cant_semanas = total
	practica.save()
	aux = str(practica.fecha_inicio)
	for x in range(1,total+1):
		date_1 = datetime.datetime.strptime(aux, "%Y-%m-%d")
		semana = Week_Practica.objects.create(number=x,practica=practica)
		end_date = date_1 + datetime.timedelta(days=7)
		semana.fecha_inicio = aux
		semana.fecha_termino = end_date
		aux = str(end_date).split(" ")[0]
		semana.save()
	return HttpResponseRedirect(reverse_lazy('index'))


@login_required(login_url='/')
def tematica_semana(request):
	semana = Week_Practica.objects.get(pk=request.POST["pk_semana"])
	semana.tematica = request.POST["tematica_semana"]
	semana.save()
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def tarea_semana(request):
	semana = Week_Practica.objects.get(pk=request.POST["pk_semana"])
	task = Task.objects.create(semana=semana,
							  name=request.POST["tarea"])
	task.save()
	response = {"pk":task.pk}
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def delete_task(request):
	print request.POST
	task = Task.objects.get(pk=int(request.POST["pk"]))
	task.delete()
	response = {}
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def get_task(request):
	print request.POST
	response = {}
	task = Task.objects.get(pk=request.POST["pk_semana"])
	response["task"] = task.name
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)
@login_required(login_url='/')
def edit_task(request):
	print request.POST
	response = {}
	task = Task.objects.get(pk=request.POST["pk_semana"])
	task.name = request.POST["edit_tarea"]
	task.save()
	response["html"] = '<td>'+str(task.name)+'</td><td class="text-center" style="vertical-align:middle;"><button data-toggle="modal" data-target="#exampleModal" data-pk="'+str(task.pk)+'" class="btn btn-blue btn-sm"><i class="fa fa-pencil"></i> Editar</button><button data-pk="'+str(task.pk)+'" class="btn eliminar_tarea btn-danger btn-sm"><i class="fa fa-times"></i> Eliminar</button></td>'
	response["pk"] = task.pk
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def upload_justificativo(request):
	response = {}
	ausencia = Ausencia.objects.get(pk=int(request.POST["pk_ausencia"]))
	justificativo = Justificativo.objects.create(ausencia=ausencia,
												file=request.FILES["archivo"],
												observacion=request.POST["retro"])
	justificativo.save()
	return HttpResponseRedirect(reverse_lazy('index_practicas'))

@login_required(login_url='/')
def get_justificativos(request):
	response = {}
	print request.POST
	practica = Practica.objects.get(pk=int(request.POST["pk"]))
	ausencias = practica.ausencia_set.all()
	justificativos = []
	for x in ausencias:
		try:
			print justificativos.append(x.justificativo)
		except:
			print "nada"
	html = ""
	for x in justificativos:
		html += "<tr><td style='vertical-align:middle;' class='col-md-9 text-center'> Desde " + str(x.ausencia.fecha_inicio) + " Hasta " + str(x.ausencia.fecha_termino) + "</td><td class='text-center col-md-3'><button data-file='"+str(x.file.url)+"' data-dismiss='modal' data-toggle='modal' data-target='#ArchivoModal' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i>Ver Detalles</button></td></tr>"
	response["html"] = html
	return HttpResponse(
			json.dumps(response),
			content_type="application/json"
			)

@login_required(login_url='/')
def retro_al_tarea(request):
	print request.session["type"]
	if request.session["type"] == "EE":
		tarea = Task.objects.get(pk=int(request.POST["pk_tarea"]))
		retro = Retro.objects.create(author=request.session["rut"],tipo=request.session["type"],
									description=request.POST["retro_tarea"],
									tarea=tarea)
		retro.save()
		tarea.status = request.POST["status_tarea"]
		tarea.save()
		solicitud = tarea.semana.practica.solicitud.pk
		return HttpResponseRedirect(reverse_lazy('retroalimentar_tareas',kwargs={'pk':solicitud}))
	else:
		tarea = Task.objects.get(pk=int(request.POST["pk_tarea"]))
		retro = Retro.objects.create(author=request.session["rut"],tipo=request.session["type"],
									description=request.POST["retro_tarea"],
									tarea=tarea)
		retro.save()
		return HttpResponseRedirect(reverse_lazy('index_practicas'))

@login_required(login_url='/')
def retroalimentar_tareas(request,pk):
	data = {}
	template_name = "Practicas/retroalimentar_tareas.html"
	solicitud = Solicitud_Practica.objects.get(pk=pk)
	data["solicitud"] = solicitud
	return render(request, template_name, data)

@login_required(login_url='/')
def practicas_publicadas(request):
	data = {}
	template_name = "Practicas/practicas_publicadas.html"
	pub = Publication.objects.all()
	data["publication"] = pub
	return render(request, template_name, data)

@login_required(login_url='/')
def publicar_practica(request):
	data = {}
	template_name = "Practicas/publicar_practica.html"
	user = UserProfile.objects.get(rut=request.session["rut"])
	empresas = user.empresa_set.all()
	data["empresas"] = empresas
	if request.POST:
		empresa = Empresa.objects.get(pk=request.POST["empresa"])
		publication = Publication.objects.create(empresa = empresa,description=request.POST["descripcion_practica"],horas_dias=request.POST["horas_dia"],dias_semana=request.POST["dias_semana"])
		publication.save()
		data["ok"] = 1
	return render(request, template_name, data)

@login_required(login_url='/')
def crear_ausencia(request):
	print request.POST
	practica = Practica.objects.get(pk=int(request.POST["pk"]))
	date_format = "%Y-%m-%d"
	fecha_inicio1 = datetime.datetime.strptime(str(request.POST["fecha_inicio1"]), date_format)
	print datetime.datetime.today() > fecha_inicio1
	if request.POST["tipo_notificacion"] == "1":
		ausencia = Ausencia.objects.create(practica=practica,
										  fecha_inicio=request.POST["fecha_inicio1"],
										  cantidad_dias=1)
		ausencia.save()
	else:
		date_format = "%Y-%m-%d"
		a = datetime.datetime.strptime(str(request.POST["fecha_inicio"]), date_format)
		b = datetime.datetime.strptime(str(request.POST["fecha_termino"]), date_format)
		delta = b - a
		ausencia = Ausencia.objects.create(practica=practica,
										  fecha_inicio=request.POST["fecha_inicio"],
										  fecha_termino=request.POST["fecha_termino"],
										  cantidad_dias=int(delta.days))
		ausencia.save()
	return HttpResponseRedirect(reverse_lazy('index_practicas'))

@login_required(login_url='/')
def edit_solicitud(request):
	print request.POST
	solicitud = Solicitud_Practica.objects.get(pk=request.POST["pk"])
	solicitud.file = request.FILES["file_solicitar"]
	solicitud.observacion = request.POST["obs_edit"]
	solicitud.step = "DS"
	solicitud.save()
	respuesta = RespuestaSolicitud.objects.create(solicitud=solicitud,
												 tipo=1,
												 emisor = "AL")
	respuesta.save()
	return HttpResponseRedirect(reverse_lazy('index_practicas'))

@login_required(login_url='/')
def solicitud_convalidar(request):
	print request.POST, "POST"
	print request.FILES, "FILES"
	return HttpResponseRedirect(reverse_lazy('index_practicas'))

@login_required(login_url='/')
def crud_empresas(request):
	template_name = 'Practicas/crud_empresa.html'
	data = {}
	if request.POST:

		if len(Empresa.objects.filter(rut=request.POST["rut_empresa"])) == 0:
			empresa = Empresa.objects.create(nombre=request.POST["nombre_empresa"],
											rut=request.POST["rut_empresa"],
											tipo="AC")
			empresa.save()
			data["empresa_existe"] = "none"
		else:
			empresa = Empresa.objects.get(rut=request.POST["rut_empresa"])
			data["empresa_existe"] = "ok"

		if len(UserProfile.objects.filter(rut=request.POST["rut_encargado_empresa"])) == 0:
			header = {"Content-Type": 'application/x-www-form-urlencoded',
				  'Authorization': "Token " + request.session["token"]}
			post_data = {'nombre': request.POST["nombre_encargado_empresa"],
							'rut': request.POST["rut_encargado_empresa"],
							'email':request.POST["correo_encargado_empresa"],
							'username':request.POST["correo_encargado_empresa"].split("@")[0],
							'type':"EE",
							"apellidos":""}
			response = requests.post(
					'http://supreme.webtic.cl/get/create_user/', headers=header, data=post_data)
			content = ast.literal_eval(response.content)
			if content["respuesta"] == 1:
				profile = UserProfile.objects.create(api_user=content["user_api"],rut=request.POST["rut_encargado_empresa"],name=request.POST["nombre_encargado_empresa"])
				profile.save()
				empresa.funcionarios.add(profile)
				empresa.save()
				print profile, "Profile"
				data["encargado_existe_en_empresa"] = "none"
		else:
			profile = UserProfile.objects.get(rut=request.POST["rut_encargado_empresa"])
			if profile in empresa.funcionarios.all():
				data["encargado_existe_en_empresa"] = "ok"
			else:
				empresa.funcionarios.add(profile)
				empresa.save()
				data["encargado_existe_en_empresa"] = "none"
	empresas = Empresa.objects.all()
	data["empresas"] = empresas
	return render(request, template_name, data)

@login_required(login_url='/')
def lista_practicas(request):
	template_name = 'Practicas/listado_practicas.html'
	data = {}
	practicas = Practica.objects.all()
	data['practicas'] = practicas
	return render(request, template_name, data)

@login_required(login_url='/')
def practicas_terminadas(request):
	template_name = 'Practicas/practicas_terminadas.html'
	data = {}
	practicas = Practica.objects.all()
	data["practicas"] = practicas
	return render(request, template_name, data)

@login_required(login_url='/')
def view_informe_dc(request,pk):
	template_name = 'Practicas/view_informe_dc.html'
	data = {}
	practica = Practica.objects.get(pk=pk)
	data["practica"] = practica
	return render(request, template_name, data)

@login_required(login_url='/')
def obs_director_final(request):
	data = {}
	practica = Practica.objects.get(pk=request.POST["pk"])
	obs = Obs_director(eval=practica.eval_final,text=request.POST["obs_director"])
	obs.save()
	return HttpResponseRedirect(reverse_lazy('index_practicas'))

@login_required(login_url='/')
def formulario_final(request,pk):
	template_name = 'Practicas/formulario_final.html'
	data = {}
	practica = Practica.objects.get(pk=pk)
	data["practica"] = practica
	return render(request, template_name, data)

@login_required(login_url='/')
def perfil_practicante(request, pk):
	template_name = 'Practicas/perfil_practicante.html'
	data = {}
	user = Student.objects.get(pk = pk)
	solicitud = Solicitud_Practica.objects.get(alumno = user)
	semana = solicitud.practica.week_practica_set.all()
	data["solicitud"] = solicitud
	data["semana"] = semana
	data["pk"] = user.pk
	return render(request, template_name, data)

@login_required(login_url='/')
@csrf_protect
def comentar_tarea(request, **kwargs):
	template_name = 'Practicas/comentarios_tarea.html'
	data = {}

	print kwargs['pk'] + kwargs['pk_sem'] + kwargs['pk_tar']

	user = Student.objects.get(pk = int(kwargs['pk']))
	practicante = Solicitud_Practica.objects.get(alumno = user)
	semanas = practicante.practica.week_practica_set.all()

	data["practicante"] = practicante
	for semana in semanas:
		if semana.pk == int(kwargs['pk_sem']):
			data["semana"] = semana
			for task in semana.task_set.all():
				if task.pk == int(kwargs['pk_tar']):
					data["task"] = task

	return render(request, template_name, data)

@login_required(login_url='/')
def agregar_comentario(request):
	print request.POST 
	task = Task.objects.get(pk = int(request.POST["pk_tar"]))
	comentario = Comments.objects.create(task = task,
										texto = request.POST["comentario_nuevo"])
	comentario.save()
	pk = request.POST["pk"]
	print request.POST
	return redirect('perfil_practicante',pk)

@login_required(login_url='/')
def eliminar_comentario(request, pk):
	template_name = 'Practicas/eliminar_comentario.html'
	data = {}
	comentario = Comments.objects.get(pk = pk)

	pk_practicante = comentario.task.semana.practica.solicitud.alumno.pk
	pk_semana = comentario.task.semana.pk
	pk_tarea = comentario.task.pk

	data["comentario"] = comentario
	# data["pk_practicante"] = pk_practicante
	# data["pk_semana"] = pk_semana
	# data["pk_tarea"] = pk_tarea

	if request.method == 'POST':
		comentario.delete()
		return redirect('comentar_tarea', pk_practicante, pk_semana, pk_tarea)

	return render(request, template_name, data)

@login_required(login_url='/')
def create_final(request):
	print request.POST
	practica = Practica.objects.get(pk=request.POST["pk"])
	practica.eval_final.presentacion_personal = request.POST["6"]
	practica.eval_final.asistencia = request.POST["7"]
	practica.eval_final.puntialidad = request.POST["8"]
	practica.eval_final.capacidad_adaptacion = request.POST["1"]
	practica.eval_final.relacion_compa = request.POST["2"]
	practica.eval_final.colaboracion = request.POST["3"]
	practica.eval_final.interes = request.POST["4"]
	practica.eval_final.proactividad = request.POST["5"]
	practica.eval_final.disposicion_trabajo = request.POST["9"]
	practica.eval_final.respeto_superior = request.POST["10"]
	practica.eval_final.aceptacion_instruccion = request.POST["11"]
	practica.eval_final.nivel_conocimientos = request.POST["12"]
	practica.eval_final.estudio = request.POST["13"]
	practica.eval_final.actitud_situaciones_nuevas = request.POST["14"]
	practica.eval_final.funciones_realizadas = request.POST["funciones_realizadas"]
	practica.eval_final.observaciones = request.POST["observaciones"]
	practica.eval_final.status = "completado"
	practica.eval_final.save()
	return HttpResponseRedirect(reverse_lazy('index_practicas'))

@login_required(login_url='/')
def get_comentario(request):
	print request.POST
	response = {}
	comentario = Comments.objects.get(pk=request.POST["pk_tarea"])
	response["comentario"] = comentario.texto
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def terminar_practica(request):
	print request.POST
	response = {}
	response["ok"] = 1
	practica = Practica.objects.get(pk=request.POST["pk"])
	semanas = practica.week_practica_set.all()
	aux = []
	for x in semanas:
		tasks = x.task_set.all()
		aux.append(len(x.task_set.all()))
		for i in tasks:
			if i.status == 0:
				response["ok"] = 0
	if response["ok"] == 1 and 0 not in aux:
		eval = Eval_Final.objects.create(practica=practica)
		eval.save()
	else:
		response["ok"] = 0
 	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def edit_comentario(request):
	print request.POST
	response = {}
	comentario = Comments.objects.get(pk=request.POST["pk_tarea"])
	comentario.texto = request.POST["edit_comentario"]
	comentario.save()
	response["html"] = '<div class="social-comment"><div class="pull-right social-action dropdown"><a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a><ul class="dropdown-menu dropdown-menu-right btn-primary"><li><a href="#">Editar</a></li><li><a href="#"">Eliminar</a></li></ul></div><div class="media-body"><a><strong>"' + str(comentario.autor) + '": </strong></a>"' + str(comentario.texto) + '"<br/><small class="text-muted">"' + str(comentario.fecha_pub) + '"</small><br><br><br></div></div>'
	response["pk"] = comentario.pk
	return HttpResponse(
		json.dumps(response),
		content_type="application/json"
		)

@login_required(login_url='/')
def postular_practicas(request):
	data = {}
	template_name = 'Practicas/postular_practicas.html'
	data["publications"] = Publication.objects.all()[:6]
	return render(request, template_name, data)

@login_required(login_url='/')
def obt_sgt(request):
	response = {}
	html = ""
	print request.POST["indice"]
	if int(request.POST["indice"]) <= len(Publication.objects.all()): 
		for x in Publication.objects.all()[int(request.POST["indice"])-1:int(request.POST["indice"])+5]:
			html += '<div class=" col-sm-4 col-md-4 col-xs-4 anti">\
				<div class="card">\
					<header style="background-color: #1c84c6 " class="card-heading ">\
						<h2 style="color: white" class="card-title">Práctica desarrollo</h2>\
						<h2 class="pull-right">'+ str(x.pk) +'</h2>\
					</header>\
					<div class="card-body">\
						<div class="row">\
							<div style="margin-top: 30px;" class="col-md-4 col-xs-4 col-sm-4 ">\
								<img src="/Static/img/CUBESOA.png" alt="" class="img-circle ">\
							</div>\
							<div class="col-md-8 col-xs-8 col-sm-8 text-center">\
								<h1><strong> cubesoa </strong></h1>\
								<a href="http://www.cubesoa.com/">http://www.cubesoa.com/</a>\
								<h6> aqui va una sinopsis o resumen de las acciones que se necesitan que hagas para esta practica maldito madafaka </h6>\
							</div>\
						</div>\
					</div>\
					<div class="card-footer border-top text-center">\
						<button class="btn btn-info  " data-toggle="modal" data-target="#detalle">detalles</button>\
						<button class="btn btn-info postular" data-toggle="modal" data-target="#msn_m">postular</button>\
					</div>\
				</div>\
			</div>'
		response["a"] = int(request.POST["indice"]) + 6
		response["html"] = html
	else:
		response["a"] = 0
	return HttpResponse(
			json.dumps(response),
			content_type="application/json"
			)
