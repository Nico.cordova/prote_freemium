from django.contrib import admin
from practicas.models import *

# Register your models here.
admin.site.register(Empresa)
admin.site.register(Solicitud_Practica)
admin.site.register(RespuestaSolicitud)
admin.site.register(Practica)
admin.site.register(Retro)
admin.site.register(Week_Practica)
admin.site.register(Task)
admin.site.register(Ausencia)
admin.site.register(Justificativo)
admin.site.register(Comments)
admin.site.register(Publication)
admin.site.register(Eval_Final)
admin.site.register(Obs_director)
