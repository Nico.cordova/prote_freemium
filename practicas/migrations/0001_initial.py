# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-11-29 14:29
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ausencia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField(blank=True, null=True)),
                ('fecha_termino', models.DateField(blank=True, null=True)),
                ('cantidad_dias', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('autor', models.CharField(blank=True, max_length=200, null=True)),
                ('texto', models.TextField(blank=True, max_length=1000, null=True)),
                ('fecha_pub', models.DateField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=200, null=True)),
                ('rut', models.CharField(blank=True, max_length=20, null=True)),
                ('tipo', models.CharField(choices=[('AC', 'Activa'), ('IC', 'Inactiva'), ('CI', 'Informacion Completa')], default='Inactiva', max_length=200)),
                ('funcionarios', models.ManyToManyField(to='core.UserProfile')),
            ],
        ),
        migrations.CreateModel(
            name='Eval_Final',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('presentacion_personal', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('asistencia', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('puntualidad', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('capacidad_adaptacion', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('relacion_compa', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('colaboracion', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('interes', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('proactividad', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('disposicion_trabajo', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('respeto_superior', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('aceptacion_instruccion', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('nivel_conocimientos', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('estudio', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('actitud_situaciones_nuevas', models.CharField(choices=[('D', 'Deficiente'), ('R', 'Regular'), ('B', 'Bueno')], default='B', max_length=1)),
                ('funciones_realizadas', models.TextField(blank=True, max_length=2000, null=True)),
                ('observaciones', models.TextField(blank=True, max_length=2000, null=True)),
                ('status', models.CharField(blank=True, max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Justificativo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to='Justificativos')),
                ('observacion', models.TextField(blank=True, max_length=2000, null=True)),
                ('estado', models.IntegerField(default=0)),
                ('ausencia', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='practicas.Ausencia')),
            ],
        ),
        migrations.CreateModel(
            name='Obs_director',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(blank=True, max_length=2000, null=True)),
                ('eval', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='practicas.Eval_Final')),
            ],
        ),
        migrations.CreateModel(
            name='Practica',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas_diarias', models.IntegerField(default=0)),
                ('dias_semana', models.IntegerField(default=0)),
                ('cant_semanas', models.IntegerField(default=0)),
                ('fecha_inicio', models.DateField(blank=True, null=True)),
                ('status', models.CharField(choices=[('EC', 'En Curso'), ('RE', 'Reprobada'), ('AP', 'Aprobada')], default='En Curso', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Publication',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(blank=True, max_length=2000, null=True)),
                ('dias_semana', models.IntegerField(blank=True, null=True)),
                ('horas_dias', models.IntegerField(blank=True, null=True)),
                ('tipo', models.CharField(choices=[('AC', 'Activa'), ('IC', 'Inactiva'), ('CI', 'Informacion Completa')], default='Inactiva', max_length=200)),
                ('empresa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='practicas.Empresa')),
            ],
        ),
        migrations.CreateModel(
            name='RespuestaSolicitud',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.IntegerField(blank=True, null=True)),
                ('observacion', models.TextField(blank=True, max_length=200, null=True)),
                ('date', models.DateField(blank=True, default=datetime.date.today)),
                ('author', models.CharField(blank=True, max_length=200, null=True)),
                ('emisor', models.CharField(choices=[('DS', 'Direccion de Servicios Academicos'), ('SE', 'Secretaria'), ('DC', 'Director de Carrera'), ('FF', 'Final'), ('AL', 'Alumno')], default='DS', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Retro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=20)),
                ('author_name', models.CharField(blank=True, max_length=250, null=True)),
                ('description', models.TextField(max_length=2000)),
                ('tipo', models.CharField(blank=True, max_length=2, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Solicitud_Practica',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to='Solicitudes')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('observacion', models.TextField(blank=True, max_length=1000, null=True)),
                ('carrera', models.CharField(blank=True, max_length=250, null=True)),
                ('step', models.CharField(choices=[('DS', 'Direccion de Servicios Academicos'), ('SE', 'Secretaria'), ('DC', 'Director de Carrera'), ('FF', 'Final'), ('AL', 'Alumno')], default='DS', max_length=200)),
                ('date', models.DateField(blank=True, default=datetime.date.today)),
                ('alumno', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Student')),
                ('empresa', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='practicas.Empresa')),
                ('encargado', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.UserProfile')),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, max_length=2000, null=True)),
                ('status', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Week_Practica',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(blank=True, null=True)),
                ('fecha_inicio', models.DateField(blank=True, null=True)),
                ('fecha_termino', models.DateField(blank=True, null=True)),
                ('tematica', models.CharField(blank=True, max_length=250, null=True)),
                ('practica', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='practicas.Practica')),
            ],
        ),
        migrations.AddField(
            model_name='task',
            name='semana',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='practicas.Week_Practica'),
        ),
        migrations.AddField(
            model_name='retro',
            name='tarea',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='practicas.Task'),
        ),
        migrations.AddField(
            model_name='respuestasolicitud',
            name='solicitud',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='practicas.Solicitud_Practica'),
        ),
        migrations.AddField(
            model_name='practica',
            name='solicitud',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='practicas.Solicitud_Practica'),
        ),
        migrations.AddField(
            model_name='eval_final',
            name='practica',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='practicas.Practica'),
        ),
        migrations.AddField(
            model_name='comments',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='practicas.Task'),
        ),
        migrations.AddField(
            model_name='ausencia',
            name='practica',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='practicas.Practica'),
        ),
    ]
