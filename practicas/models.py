from __future__ import unicode_literals
from django.shortcuts import get_object_or_404
from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
from random import randint
import os
from datetime import date
from core.models import *
#from simple_history.models import HistoricalRecords



EMISOR_TYPES_CHOICES = (

    ('DS', 'Direccion de Servicios Academicos'),
    ('SE', 'Secretaria'),
    ('DC', 'Director de Carrera'),
    ('FF', 'Final'),
    ('AL', 'Alumno'),

)

EMISOR_TYPES_DEFAULT = 'DS'

EMPRESA_TYPES_CHOICES = (

    ('AC', 'Activa'),
    ('IC', 'Inactiva'),
    ('CI', 'Informacion Completa'),

)

EMPRESA_TYPES_DEFAULT = 'Inactiva'

RESPUESTAS_TYPES_CHOICES = (

    ('D', 'Deficiente'),
    ('R', 'Regular'),
    ('B', 'Bueno'),

)

RESPUESTAS_TYPES_DEFAULT = 'B'


STATUS_TYPES_CHOICES = (

    ('EC', 'En Curso'),
    ('RE', 'Reprobada'),
    ('AP', 'Aprobada'),

)

STATUS_TYPES_DEFAULT = 'En Curso'

PROFILE_TYPES_CHOICES = (
    
    ('DV', 'Desarrollo'),
    ('GE', 'Gestion'),
    ('AR', 'Arquitectura'),

)

PROFILE_TYPES_DEFAULT = 'Desarrollo'


class Empresa(models.Model):
    nombre = models.CharField(max_length=200,null=True,blank=True)
    rut = models.CharField(max_length=20,null=True,blank=True)
    funcionarios = models.ManyToManyField(UserProfile)
    tipo = models.CharField(
            max_length=200,
            choices=EMPRESA_TYPES_CHOICES,
            default=EMPRESA_TYPES_DEFAULT,
        )

class Solicitud_Practica(models.Model):
	alumno = models.ForeignKey(Student, null=True,blank=True)
	file = models.FileField(upload_to="Solicitudes")
	empresa = models.ForeignKey(Empresa,null=True,blank=True)
	encargado = models.ForeignKey(UserProfile,null=True,blank=True)
	created = models.DateTimeField(auto_now_add=True,null=True)
	observacion = models.TextField(null=True,blank=True,max_length=1000)
	carrera = models.CharField(null=True,blank=True,max_length=250)
	step = models.CharField(
			max_length=200,
			choices=EMISOR_TYPES_CHOICES,
			default=EMISOR_TYPES_DEFAULT,
		)
	date = models.DateField(default=date.today, blank=True)
	#history = HistoricalRecords()

class RespuestaSolicitud(models.Model):
    solicitud = models.ForeignKey(Solicitud_Practica)
    tipo = models.IntegerField(null=True,blank=True)
    observacion = models.TextField(max_length=200,null=True,blank=True)
    date = models.DateField(default=date.today, blank=True)
    author = models.CharField(max_length=200,null=True,blank=True)
    emisor = models.CharField(
            max_length=200,
            choices=EMISOR_TYPES_CHOICES,
            default=EMISOR_TYPES_DEFAULT,
        )

class Practica(models.Model):
	solicitud = models.OneToOneField(Solicitud_Practica)
	horas_diarias = models.IntegerField(default=0)
	dias_semana = models.IntegerField(default=0)
	cant_semanas = models.IntegerField(default=0)
	fecha_inicio = models.DateField(null=True,blank=True)
   	status = models.CharField(
            max_length=200,
            choices=STATUS_TYPES_CHOICES,
            default=STATUS_TYPES_DEFAULT,
        )

class Week_Practica(models.Model):
	practica = models.ForeignKey(Practica)
	number = models.IntegerField(null=True,blank=True)
	fecha_inicio = models.DateField(null=True,blank=True)
	fecha_termino = models.DateField(null=True,blank=True)
	tematica = models.CharField(null=True,blank=True,max_length=250)

class Task(models.Model):
	semana = models.ForeignKey(Week_Practica)
	name = models.TextField(null=True,blank=True,max_length=2000)
	status = models.IntegerField(default=0)

class Comments(models.Model):
    task = models.ForeignKey(Task)
    autor = models.CharField(max_length = 200, null = True, blank = True)
    texto = models.TextField(null=True,blank=True,max_length=1000)
    fecha_pub = models.DateField(default=timezone.now)


class Retro(models.Model):
	author = models.CharField(max_length=20)
	author_name = models.CharField(max_length=250,null=True,blank=True)
	description = models.TextField(max_length=2000)
	tarea = models.ForeignKey(Task)
	tipo = models.CharField(max_length=2,blank=True,null=True)

class Ausencia(models.Model):
	practica = models.ForeignKey(Practica)
	fecha_inicio = models.DateField(null=True,blank=True)
	fecha_termino = models.DateField(null=True,blank=True)
	cantidad_dias = models.IntegerField(null=True,blank=True)

class Justificativo(models.Model):
	ausencia = models.OneToOneField(Ausencia)
	file = models.FileField(upload_to="Justificativos")
	observacion = models.TextField(max_length=2000,null=True,blank=True)
	estado = models.IntegerField(default=0)
	
class Publication(models.Model):
	empresa = models.ForeignKey(Empresa)
	description = models.TextField(max_length=2000,null=True,blank=True)
	dias_semana = models.IntegerField(null=True,blank=True)
	horas_dias = models.IntegerField(null=True,blank=True)
	tipo = models.CharField(
            max_length=200,
            choices=EMPRESA_TYPES_CHOICES,
            default=EMPRESA_TYPES_DEFAULT,
        )

class Eval_Final(models.Model):
	practica = models.OneToOneField(Practica)
	presentacion_personal = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	asistencia = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	puntualidad = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	capacidad_adaptacion = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	relacion_compa = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	colaboracion = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	interes = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	proactividad = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	disposicion_trabajo = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	respeto_superior = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	aceptacion_instruccion = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	nivel_conocimientos = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	estudio = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	actitud_situaciones_nuevas = models.CharField(max_length=1,choices=RESPUESTAS_TYPES_CHOICES,default=RESPUESTAS_TYPES_DEFAULT)
	funciones_realizadas = models.TextField(max_length=2000,null=True,blank=True)
	observaciones = models.TextField(max_length=2000,null=True,blank=True)
	status = models.CharField(max_length=20,null=True,blank=True)

	
class Obs_director(models.Model):
	eval = models.OneToOneField("Eval_Final")
	text = models.TextField(max_length=2000,null=True,blank=True)
	
	
