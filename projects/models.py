from __future__ import unicode_literals
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from simple_history.models import HistoricalRecords
from django.utils.encoding import python_2_unicode_compatible
# Create your models here.
@python_2_unicode_compatible
class Tag(models.Model):
	value = models.CharField(max_length=30)

	def __str__(self):
		return self.value

@python_2_unicode_compatible
class Project(models.Model):
    en_revision = 'ER'
    aceptado = 'AC'
    aprobado = 'AP'
    reprobado = 'RP'
    rechazado = 'RE'
    en_curso = 'EC'
    terminado = 'TE'
    espera_docente = 'ED'
    caducado = 'CA'

    PROJECT_STATUS_CHOICES = (
        (en_revision, 'Revision'),
        (aceptado, 'Aceptado'),
        (rechazado, 'Rechazado'),
        (en_curso, 'En Curso'),
        (aprobado, 'Aprobado'),
        (reprobado, 'Reprobado'),
        (terminado, 'Terminado'),
        (caducado, 'Caducado'),
        (espera_docente, 'En espera de Docente')
    )

    primer_semestre = '10'
    segundo_semestre = '20'
    primer_trimestre = '05'
    segundo_trimestre = '15'
    tercer_trimestre = '25'
    anual = '00'

    PERIOD_CHOICES = (
        (primer_semestre, 'Primer Semestre'),
        (segundo_semestre, 'Segundo Semestre'),
        (primer_trimestre, 'Primer Trimestre'),
        (segundo_trimestre, 'Segundo Trimestre'),
        (tercer_trimestre, 'Tercer Trimestre'),
    )

    name = models.CharField(max_length=150)
    date = models.DateTimeField(blank=True, null=True)
    project_image = models.ImageField(upload_to="project_img", default='img/logo_unab.png', blank=True, null=True,)
    vacancy_total = models.PositiveIntegerField(null=True)
    vacancy_available = models.PositiveIntegerField(null=True)
    section = models.PositiveIntegerField(blank=True,null=True)
    cloud = models.CharField(max_length=300, blank=True)
    desem_grade = models.IntegerField(default=0)
    description = models.TextField(max_length=5000)
    year = models.PositiveIntegerField(null=True,blank=True)
    author = models.ForeignKey("core.UserProfile", blank=True, null=True,related_name="author")
    final_grade = models.IntegerField(default=0)
    shared_teachers = models.ManyToManyField("core.UserProfile", blank=True,related_name="share_teacher")
    guide_teacher = models.ForeignKey("core.UserProfile", blank=True, null=True,related_name="guide_teacher")
    period = models.CharField(blank=True, null=True, max_length=2, choices=PERIOD_CHOICES, default="00")
    tags = models.ManyToManyField(Tag, blank=True)
    carreer = models.IntegerField(blank=True, null=True)
    status = models.CharField(
        max_length=2,
        choices=PROJECT_STATUS_CHOICES,
        default=en_revision,
    )

    def __str__(self):
        return self.name

class Glosa(models.Model):
    author = models.ForeignKey('core.UserProfile',null=True,blank=True)
    carreer = models.IntegerField(blank=True, null=True)
    description = models.TextField(max_length=5000,blank=True,null=True)


class Postulation(models.Model):
	POSTULATION_STATUS_CHOICES = (
		("PE","Pendiente"),
		("AP","Aprobada"),
		("RE","Rechazada"),
		("PR","Pre-Postulation")
	)

	project = models.ForeignKey(Project,blank=True,null=True)
	student = models.ForeignKey("core.Student",blank=True,null=True)
	creation_date = models.DateTimeField(default=timezone.now,blank=True,null=True)
	last_mod = models.DateTimeField(blank=True,null=True)
	last_mod_user = models.ForeignKey("core.UserProfile", blank = True, null = True)
	status = models.CharField(
        max_length=2,
        choices=POSTULATION_STATUS_CHOICES,
        default="PE",
    )


class Teacher_Postulation(models.Model):
    POSTULATION_STATUS_CHOICES = (
        ("PE","Pendiente"),
        ("AP","Aprobada"),
        ("RE","Rechazada"),
        ("AS","Asignacion")
        )

    author = models.ForeignKey("core.UserProfile",blank=True,null=True,related_name="autor")
    project = models.ForeignKey(Project,blank=True,null=True)
    teacher = models.ForeignKey("core.UserProfile",blank=True,null=True,related_name="teacher")
    creation_date = models.DateTimeField(default=timezone.now,blank=True,null=True)
    last_mod = models.DateTimeField(blank=True,null=True)
    status = models.CharField(
        max_length=2,
        choices=POSTULATION_STATUS_CHOICES,
        default="PE",
    )

## FEEDBACK DE POSTULACION DE PROYECTOS o PROPUESTAS DE PROYECTOS
@python_2_unicode_compatible
class Feedback(models.Model):

    FEEDBACK_STATUS_CHOICES = (
        ("AP","Aprobada"),
        ("ME","Mensaje"),
        ("RE","Rechazada"),
        ("AS","Asignacion"),
        ("MI","Minuta"),
        ("RP","Reprobado")
        )

    author = models.ForeignKey("core.UserProfile",blank=True,null=True,related_name="m_author")
    author_s = models.ForeignKey("core.Student",blank=True,null=True,related_name="author_student")
    read = models.IntegerField(default=0,null=True,blank=True)
    status = models.CharField(max_length=2,choices=FEEDBACK_STATUS_CHOICES,null=True,blank=True)
    description = models.TextField(max_length=300)
    postulation = models.ForeignKey(Postulation,null=True,blank=True)
    minuta = models.ForeignKey("Minuta",null=True,blank=True)
    t_postulation = models.ForeignKey(Teacher_Postulation,null=True,blank=True)
    project = models.ForeignKey(Project,blank=True,null=True)
    date = models.DateTimeField(default=timezone.now,blank=True,null=True)
    receipt = models.ForeignKey("core.UserProfile",blank=True,null=True,related_name="m_receipt")
    receipt_student = models.ForeignKey("core.Student",blank=True,null=True,related_name="ms_receipt")

    def __str__(self):
        return self.description

class Minuta(models.Model):
    MINUTA_STATUS_CHOICES = (
        ("AT","Atrasada"),
        ("RM","Revisando Minuta"),
        ("EM","Espera Minuta"),
        ("ER","Espera Reunion"),
        ("PE","Pendientes"),
        ("AP","Aprobada"),
        ("RE","Rechazada"),
        )

    AUTHOR_TYPE_CHOICES = (
        ("AL",'Alumno'),
        ("DO",'Docente')
    )
    project = models.ForeignKey(Project,null=True,blank=True)
    author = models.ForeignKey(User,null=True,default=None)
    author_type = models.CharField(
                    max_length=2,
                    choices=AUTHOR_TYPE_CHOICES,
                    default="AL"
                )
    reunion_date = models.DateTimeField(default=timezone.now,blank=True,null=True)
    reunion_theme = models.CharField(max_length=400,blank=True)
    invited_stud = models.ManyToManyField("core.Student",blank=True)
    last_mod = models.DateTimeField(default=timezone.now,blank=True)
    external_visit = models.CharField(max_length=500,default="")
    invited_user = models.ManyToManyField("core.UserProfile",blank=True)
    data_file = models.FileField(upload_to="Minutas/",blank=True,null=True)
    date = models.DateTimeField(default=timezone.now,blank=True,null=True)
    status = models.CharField(
        max_length=2,
        choices=MINUTA_STATUS_CHOICES,
        default="PE")

class MinutaEvidence(models.Model):
    AUTHOR_TYPE_CHOICES = (
        ("AL",'Alumno'),
        ("DO",'Docente')
    )
    author = models.ForeignKey(User,null=True,default=None)
    author_type = models.CharField(
                    max_length=2,
                    choices=AUTHOR_TYPE_CHOICES,
                    default="AL"
                )
    minuta = models.ForeignKey(Minuta,null=True,blank=True)
    topic = models.CharField(max_length=200,null=True,default="")
    description = models.CharField(max_length=2000,null=True,default="")
    last_mod = models.DateTimeField(default=timezone.now,blank=True,null=True)
    history = HistoricalRecords()

class MinutaComment(models.Model):
    AUTHOR_TYPE_CHOICES = (
        ("AL",'Alumno'),
        ("DO",'Docente')
    )
    minuta = models.ForeignKey(Minuta,null=True,blank=True)
    author = models.ForeignKey(User,null=True,blank=True)
    author_type = models.CharField(
                    max_length=2,
                    choices=AUTHOR_TYPE_CHOICES,
                    default="AL"
                )
    comment = models.TextField(max_length=200)
    date = models.DateTimeField(default=timezone.now,null=True,blank=True)
    

# class MinutaHistoric(models.Model):
#     AUTHOR_TYPE_CHOICES = (
#         ("AL",'Alumno'),
#         ("DO",'Docente')
#     )
#     registro = models.ForeignKey(MinutaEvidence,default=None,blank=True)
#     topic = models.CharField(max_length=200,null=True,default="")
#     description = models.CharField(max_length=200,null=True,default="")
#     date = models.DateTimeField(default=timezone.now,blank=True,null=True)
#     author = models.ForeignKey(User,null=True,blank=True)
#     author_type = models.CharField(
#                     max_length=2,
#                     choices=AUTHOR_TYPE_CHOICES,
#                     default="AL"
#                 )

class MinutaValidation(models.Model):
    VALIDATION_STATUS_CHOICES = (
        ("PE","Pendiente"),
        ("NR","No Responde"),
        ("AP","Aprobada"),
        ("RE","Rechazada")
    )
    student = models.ForeignKey("core.Student",null=True,default=None)
    date = models.DateTimeField(default=timezone.now)
    minuta = models.ForeignKey(Minuta,null=True,blank=True)
    last_mod = models.DateTimeField(default=timezone.now)
    comment = models.CharField(max_length=400,default="")
    validation_status = models.CharField(
        max_length=2,
        choices=VALIDATION_STATUS_CHOICES,
        default="PE"
    )



class MinutaRetro(models.Model):
    minuta = models.ForeignKey(Minuta,blank=True,null=True)
    comment = models.CharField(max_length=1000,blank=True,null=True)
    read = models.IntegerField(default=0)

class ProjectHistoric(models.Model):
    HISTORIC_STATUS_CHOICES = (
		("AP","Aprobado"),
		("RE","Reprobado"),
	)
    status = models.CharField(max_length=2,choices=HISTORIC_STATUS_CHOICES,null=True,blank=True)
    students = models.ManyToManyField("core.Student")    
    project = models.ForeignKey(Project,blank=True)
    evaluationComment = models.CharField(max_length=1000,blank=True)
