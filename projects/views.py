#!/usr/bin/python
# -*- coding:utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from core.models import Session_start, ProjectHitoEvaluation, ProjectEvaluation, Subject, UserProfile, Student, Carreer, Rubric, Session, Hito, ObjetiveEvaluation, ActivityEvaluation, ProgressEvaluation, MemoryEvaluation, TechniqueEvaluation, PresentationEvaluation
from projects.models import ProjectHistoric, MinutaEvidence, MinutaComment, MinutaRetro, Glosa, Project, Teacher_Postulation, Tag, Postulation, Feedback, Minuta, MinutaValidation
import datetime
from string import maketrans
from operator import itemgetter
import math
from unidecode import unidecode
from django.utils.encoding import smart_unicode
from django.utils import timezone
from django.db.models import Q
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import PermissionDenied
import requests
import json
import ast
import sys
import os

# Create your views here.


@login_required(login_url='/')
def abet_rubric_delete(request, pk):
    rubric = get_object_or_404(AbetRubric, pk=pk)
    rubric.delete()

    return HttpResponseRedirect(reverse_lazy('abet_rubric_list'))


@login_required(login_url='/')
def abet_rubric_deactivate(request):
	rubricpk = request.POST['rubric_pk']
	rubric = get_object_or_404(AbetRubric, pk=rubricpk)
	rubric.subject = None
	rubric.save()
	response = {}
	response["message"] = "activado con exito"
	return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


@login_required(login_url='/')
def abet_rubric_details(request, pk):
    template_name = 'projects/abet_rubric_details.html'
    rubric = get_object_or_404(AbetRubric, pk=pk)

    if Carreer.objects.filter(career_pk=int(rubric.carreer)).exists():
        str_f = Carreer.objects.get(career_pk=int(rubric.carreer)).name
    else:
        post_data = {"carreer": int(rubric.carreer)}
        header = {"Content-Type": 'application/x-www-form-urlencoded',
                "Authorization": "Token " + request.session["token"]}
        response = requests.post(
            "http://supreme.webtic.cl/get/Carreer_Data/", headers=header, data=post_data)
        content = response.content
        content = json.loads(content)
        str_f = content["nombre"]

        n_career = Carreer.objects.create(
            career_pk=int(rubric.carreer), name=str_f)
        n_career.save()

    abet = Abet.objects.get(carreer=rubric.carreer)
    letters = []
    for outcome in rubric.outcomes.all():
        letters.append(outcome.letter)
    rubric_outcomes = []
    for outcome in abet.outcome_set.all():
        if outcome.letter not in letters:
            rubric_outcomes.append(outcome)

    return render(request, template_name, {'rubric': rubric, 'carreer': str_f, 'outcomes': rubric_outcomes})


@login_required(login_url='/')
def abet_rubric_add(request):
	html = "projects/abet_rubric_add.html"
	data = {}
	post_data = {"carreers": 0}

	header = {"Content-Type": 'application/json',
			"Authorization": "Token " + request.session["token"]}
	response = requests.post(
		"http://supreme.webtic.cl/get/get_carrers/", headers=header, data=post_data)
	content = response.content
	content = ast.literal_eval(content)
	carreers = []
	for dictionary in content["carreers"]:
		values = dictionary.values()
		if Abet.objects.filter(carreer=int(values[0][1])).exists() == True:
			carreers.append([values[0][0], values[0][1]])

	data["carreers"] = carreers

	return render(request, html, data)


@login_required(login_url='/')
def abet_rubric_list(request):
	html = "projects/abet_rubric_list.html"
	rubrics = AbetRubric.objects.all()
	profile = UserProfile.objects.get(user=request.user)
	abets_privados = []
	abets_activos = []
	for rubric in rubrics:
		if Carreer.objects.filter(career_pk=int(rubric.carreer)).exists():
			str_f = Carreer.objects.get(career_pk=int(rubric.carreer)).name
		else:
			post_data = {"carreer": int(rubric.carreer)}
			header = {"Content-Type": 'application/x-www-form-urlencoded',
					"Authorization": "Token " + request.session["token"]}
			response = requests.post(
				"http://supreme.webtic.cl/get/Carreer_Data/", headers=header, data=post_data)
			content = response.content
			content = json.loads(content)
			str_f = content["nombre"]

			n_career = Carreer.objects.create(career_pk=int(rubric.carreer), name=str_f)
			n_career.save()

		if rubric.author == profile and rubric.subject == None:
			abets_privados.append([str_f, rubric])
		elif rubric.subject != None:
			abets_activos.append(rubric)
	data = {
		"abets_privados": abets_privados,
		"abets_activos": abets_activos}
	return render(request, html, data)


@login_required(login_url='/')
def add_abet(request):
    html = "projects/create_abet.html"
    data = {}
    post_data = {"carreers": 0}

    header = {"Content-Type": 'application/json',
              "Authorization": "Token " + request.session["token"]}
    response = requests.post(
        "http://supreme.webtic.cl/get/get_carrers/", headers=header, data=post_data)
    content = response.content
    content = ast.literal_eval(content)
    carreers = []
    for dictionary in content["carreers"]:
		values = dictionary.values()
		if Abet.objects.filter(carreer=int(values[0][1])).exists() == False:
			carreers.append([values[0][0], values[0][1]])
    data["carreers"] = carreers

    abets = Abet.objects.all()

    abets_f = []
    for abet in abets:
        profile = UserProfile.objects.get(user=abet.author)
        if Carreer.objects.filter(career_pk=int(abet.carreer)).exists():
            str_f = Carreer.objects.get(career_pk=int(abet.carreer)).name
        else:
            post_data = {"carreer": int(abet.carreer)}
            header = {"Content-Type": 'application/x-www-form-urlencoded',
                    "Authorization": "Token " + request.session["token"]}
            response = requests.post(
                "http://supreme.webtic.cl/get/Carreer_Data/", headers=header, data=post_data)
            content = response.content
            content = json.loads(content)
            str_f = content["nombre"]
            n_career = Carreer.objects.create(
                career_pk=int(abet.carreer), name=str_f)
            n_career.save()
        abets_f.append([abet, str_f, profile])
    data["abets"] = abets_f
    return render(request, html, data)


@login_required(login_url='/')
def approve_postulation(request):
    s_postulation = request.POST["pk"]
    perfil = UserProfile.objects.get(user=request.user)
    s_postulation = Postulation.objects.get(pk=int(s_postulation))

    s_postulation.last_mod = datetime.datetime.now()
    s_postulation.status = "AP"
    s_postulation.last_mod_user = request.user
    s_postulation.save()
    student = s_postulation.student
    project = s_postulation.project
    project.vacancy_available -= 1
    project.save()
    if project.vacancy_available == 0:
        project.status = "EC"
        postulations = Postulation.objects.filter(project=project)
        description = "Tu postulacion al proyecto: {0} \
                    ha sido rechazada! <br>Esto debido a que los cupos de el proyecto se han agotado.".format(unidecode(project.name))
        for postulation in postulations:
            if postulation != s_postulation:
                postulation.status = "RE"
                postulation.last_mod = datetime.datetime.now()
                postulation.last_mod_user = ""
                postulation.save()
                feedback = Feedback.objects.create(
                        status="RE",
                        receipt_student=student,
                        description=description,
                        postulation=postulation,
                        author=perfil,
						last_mod=timezone.now(),
                        date=timezone.now()
                    )
                feedback.save()
    else:
        project.status = "AC"
    project.save()
    description = "Felicitaciones! <br>Tu postulacion al proyecto: {0} \
                    ha sido Aprobada! <br>Tus otras postulaciones han sido canceladas.".format(unidecode(s_postulation.project.name))

    feedback = Feedback.objects.create(
        status="AP",
        receipt_student=student,
        description=description,
        postulation=s_postulation,
        author=perfil,
        date=timezone.now()
    )
    feedback.save()

    student.project = s_postulation.project
    student.save()

    postulations = Postulation.objects.filter(student=student)
    for postulation in postulations:
        if postulation != s_postulation:
            postulation.status = "RE"
            postulation.last_mod = datetime.datetime.now()
            postulation.last_mod_user = ""
            postulation.save()

    data = {"mensaje": "Exito"}

    return JsonResponse(data)


@login_required(login_url='/')
def reject_postulation(request):
    postulation = request.POST["pk"]
    description = "Su Postulacion a sido RECHAZADA con el siguiente comentario: \n"
    description += request.POST["description"]
    postulation = Postulation.objects.get(pk=int(postulation))
    perfil = UserProfile.objects.get(user=request.user)
    postulation.last_mod = timezone.now()
    postulation.status = "RE"
    postulation.save()

    feedback = Feedback.objects.create(
        receipt_student=postulation.student,
        description=description,
        postulation=postulation,
        author=perfil,
        date=timezone.now(),
        status="RE"
    )
    feedback.save()

    data = {"mensaje": "Exito"}

    return JsonResponse(data)


@login_required(login_url='/')
def approve_teacher_postulation(request):
	t_postulation = request.POST["pk"]
	perfil = UserProfile.objects.get(user=request.user)
	t_postulation = Teacher_Postulation.objects.get(pk=int(t_postulation))
	t_postulation.last_mod = timezone.now()
	t_postulation.status = "AP"

	t_postulation.save()
	teacher = t_postulation.teacher
	project = t_postulation.project
	project.guide_teacher = teacher
	project.status = "AC"
	project.save()
	postulations = Teacher_Postulation.objects.filter(project=project)
	description = "Tu postulacion al proyecto: {0} \
				ha sido rechazada! <br>Esto debido a que se ha asignado un profesor guía.".format(unidecode(project.name))

	for postulation in postulations:
		if postulation != t_postulation:
			postulation.status = "RE"
			postulation.save()
			feedback = Feedback.objects.create(
					status="RE",
					receipt=teacher,
					description=description,
					t_postulation=postulation,
					author=perfil,
					last_mod=timezone.now(),
					date=timezone.now()
				)
			feedback.save()
		else:
			pass

	if request.POST["type"] == "SA":
		description = "Felicitaciones! <br>Tu asignación al proyecto: {0} \
						ha sido Aprobada! ".format(unidecode(postulation.project.name))
	else:
		description = "Felicitaciones! <br>Tu postulacion al proyecto: {0} \
						ha sido Aprobada!".format(unidecode(postulation.project.name))

	feedback = Feedback.objects.create(
        status="AP",
        receipt=teacher,
        description=description,
        t_postulation=postulation,
        author=perfil,
        date=timezone.now()
    )
	feedback.save()

	if len(teacher.guide_teacher.all()) >= 15:

		postulations = Teacher_Postulation.objects.filter(teacher=teacher)
		for postulation in postulations:
			if postulation != t_postulation:
				postulation.status = "RE"
				postulation.save()

	data = {"mensaje": "Exito"}
	return JsonResponse(data)


@login_required(login_url='/')
def reject_teacher_postulation(request):
    postulation = request.POST["pk"]
    description = "Su Postulacion a sido RECHAZADA con el siguiente comentario: \n"
    description += request.POST["description"]
    postulation = Teacher_Postulation.objects.get(pk=int(postulation))
    perfil = UserProfile.objects.get(user=request.user)
    postulation.last_mod = timezone.now()
    postulation.status = "RE"
    postulation.save()

    feedback = Feedback.objects.create(
        receipt=postulation.teacher,
        description=description,
        t_postulation=postulation,
        author=perfil,
        date=timezone.now(),
        status="RE"
    )
    feedback.save()

    data = {"mensaje": "Exito"}

    return JsonResponse(data)


def get_careers_all(token):
    post_data = {"carreers": 0}

    header = {"Content-Type": 'application/json',
              "Authorization": "Token " + token}
    response = requests.post(
        "http://supreme.webtic.cl/get/get_carrers/", headers=header, data=post_data)
    content = response.content
    content = ast.literal_eval(content)
    return content["carreers"]


@login_required(login_url='/')
def get_careers_teacher(request, rut, token):
    post_data = {"rut": rut}

    header = {"Content-Type": 'application/x-www-form-urlencoded',
              "Authorization": "Token " + token}
    response = requests.post(
        "http://supreme.webtic.cl/get/get_careers_teacher/", headers=header, data=post_data)
    content = response.content
    content = ast.literal_eval(content)
    return content["carreers"]


@login_required(login_url='/')
def delete_abet(request):
	pk = request.POST["pk"]

	abet = Abet.objects.get(pk=int(pk))
	abet.delete()

	data = {"mensaje": "ok"}

	return JsonResponse(data)


@login_required(login_url='/')
def project_propose(request):
    html = "projects/project_propose.html"
    carreers = []
    list_careers = []
    if request.session['type'] == 'DO':
        list_careers = get_careers_teacher(
            request, request.session['rut'], request.session['token'])
    else:
        list_careers = get_careers_all(request.session['token'])

    for dictionary in list_careers:
        values = dictionary.values()
        carreers.append([values[0][0], values[0][1]])

    projects = Project.objects.filter(
        author=UserProfile.objects.get(user=request.user))

    projects = projects.all()
    cant_prop = len(projects.all())
    prop_er = 0
    prop_ap = 0
    prop_re = 0

    for project in projects:
        if project.status == "ER" or project.status == "ED":
            prop_er += 1
        elif project.status == "AC":
            prop_ap += 1
        elif project.status == "RE":
            prop_re += 1
        else:
            prop_ap += 1

    return render(request, html, {"carreers": carreers,
                                  "cant_prop": cant_prop,
                                  "prop_er": prop_er,
                                  "prop_re": prop_re,
                                  "prop_ap": prop_ap,
                                  "periods": Project.PERIOD_CHOICES,
                                  "tags": Tag.objects.all()})


@login_required(login_url='/')
def my_projects(request):
    html = "projects/my_projects.html"
    perfil = UserProfile.objects.get(user=request.user)
    display_proposed = "block"
    display_revision = "none"
    display_approved = "none"
    display_rejected = "none"
    if request.GET:
        if str(request.GET['filter']) == 'revision':
            display_proposed = "none"
            display_revision = "block"
            display_approved = "none"
            display_rejected = "none"
        if str(request.GET['filter']) == 'approved':
            display_proposed = "none"
            display_revision = "none"
            display_approved = "block"
            display_rejected = "none"
        if str(request.GET['filter']) == 'rejected':
            display_proposed = "none"
            display_revision = "none"
            display_approved = "none"
            display_rejected = "block"

    projects = Project.objects.filter(
        Q(author=perfil) | Q(guide_teacher=perfil))
    carreers = []
    list_careers = []
    if request.session['type'] == 'DO':
        list_careers = get_careers_teacher(
            request, request.session['rut'], request.session['token'])
    else:
        list_careers = get_careers_all(request.session['token'])

    for dictionary in list_careers:
        values = dictionary.values()
        carreers.append([values[0][0], values[0][1]])

    projects = projects.all()
    cant_prop = len(projects.filter(author=perfil))
    prop_er = 0
    prop_ap = 0
    prop_re = 0

    projects_f = []
    for project in projects:
        if project.status == "ER" and project.author == perfil:
            prop_er += 1
        elif project.status == "AC" and project.author == perfil:
            prop_ap += 1
        elif project.status == "RE" and project.author == perfil:
            prop_re += 1
        else:
            if project.author == request.user:
                prop_ap += 1
            else:
                pass
        projects_f.append(project)

    return render(request, html, {"carreers": carreers,
                                  "projects": projects,
                                  "cant_prop": cant_prop,
                                  "prop_er": prop_er,
                                  "prop_re": prop_re,
                                  "prop_ap": prop_ap,
                                  "display_proposed": display_proposed,
                                  "display_revision": display_revision,
                                  "display_approved": display_approved,
                                  "display_rejected": display_rejected,
                                  "periods": Project.PERIOD_CHOICES,
                                  })


@login_required(login_url='/')
def get_teachers(request):
    carreer_pk = request.POST["pk"]
    post_data = {
        "carreer": carreer_pk
    }
    header = {"Content-Type": 'application/x-www-form-urlencoded',
              "Authorization": "Token " + request.session["token"]}

    response = requests.post(
        "http://supreme.webtic.cl/get/get_teachers_carreer/", headers=header, data=post_data)
    content = response.content
    content = ast.literal_eval(content)

    html = "<option value='Null'>Sin profesor asignado</option>"
    values = sorted(content["teachers"], key=itemgetter(0))

    for teacher in values:
        html += "<option value='%s'>%s</option>" % (teacher[1], teacher[0])

    response = {
        "html": html
    }

    return JsonResponse(response)


@login_required(login_url='/')
def create_postulation(request):
    project = get_object_or_404(Project, pk=request.POST["projectpk"])
    student = get_object_or_404(Student, user=request.user)
    n_postulation = Postulation.objects.create(
        project=project,
        student=student,
        creation_date=timezone.now(),
    )
    n_postulation.save()
    response = {"mensaje": "exito"}

    return JsonResponse(response)


@login_required(login_url='/')
def create_teacher_postulation(request):
    project = get_object_or_404(Project, pk=request.POST["projectpk"])
    teacher = get_object_or_404(UserProfile, user=request.user)
    n_postulation = Teacher_Postulation.objects.create(
        project=project,
		author=teacher,
        teacher=teacher,
        creation_date=timezone.now(),
    )
    n_postulation.save()
    response = {"mensaje": "exito"}

    return JsonResponse(response)


@login_required(login_url='/')
def list_active_projects(request):
    html = "projects/list_active_projects.html"
    projects = Project.objects.all()
    carreers = []
    list_careers = []
    if request.session['type'] == 'DO':
        list_careers = get_careers_teacher(
            request, request.session['rut'], request.session['token'])
    else:
        list_careers = get_careers_all(request.session['token'])

    for dictionary in list_careers:
        values = dictionary.values()
        carreers.append([values[0][0], values[0][1]])

    projects = projects.all()
    cant_prop = len(projects.all())
    proj_en_revision = 0
    proj_aceptado = 0
    proj_rechazado = 0
    proj_en_curso = 0

    projects_f = []
    for project in projects:
        if project.status == 'ER' or project.status == "ED":
            proj_en_revision += 1
        if project.status == 'AC':
            proj_aceptado += 1
        if project.status == 'RE':
            proj_rechazado += 1
        if project.status == 'EC':
            proj_en_curso += 1

        students = []
        students = Student.objects.filter(project=project)
        if students.count() == 0:
            students = ''
        projects_f.append({
            "pk": project.pk,
            "name": project.name,
            "date": project.date,
            "guide_teacher": project.guide_teacher,
            "status": project.status,
            'subject': Subject.objects.get(code=project.student_set.all()[0].actual_subject).name if project.student_set.exists() else "Sin Asignatura",
            "students": students
        })

    return render(request, html, {"carreers": carreers,
                                  "projects": projects_f,
                                  "cant_prop": cant_prop,
                                  "proj_en_revision": proj_en_revision,
                                  "proj_aceptado": proj_aceptado,
                                  "proj_rechazado": proj_rechazado,
                                  "proj_en_curso": proj_en_curso,
                                  })


@login_required(login_url='/')
def list_evaluated_projects(request):
    html = "projects/list_evaluated_projects.html"
    projects = Project.objects.all()
    carreers = []
    list_careers = []
    if request.session['type'] == 'DO':
        list_careers = get_careers_teacher(
            request.session['rut'], request.session['token'])
    else:
        list_careers = get_careers_all(request.session['token'])

    for dictionary in list_careers:
        values = dictionary.values()
        carreers.append([values[0][0], values[0][1]])

    projects = projects.all()
    cant_prop = 0
    proj_aprobado = 0
    proj_reprobado = 0

    projects_f = []
    for project in projects:

        students = []
        students = Student.objects.filter(project=project)

        if students.count() == 0:
            students = ''
        if project.status == 'AP':
            proj_aprobado += 1
            cant_prop += 1
            projects_f.append({
                "pk": project.pk,
                "name": project.name,
                "date": project.date,
                "guide_teacher": project.guide_teacher,
                "status": project.status,
                "students": students
            })
        if project.status == 'RP':
            proj_reprobado += 1
            cant_prop += 1
            projects_f.append({
                "pk": project.pk,
                "name": project.name,
                "date": project.date,
                "guide_teacher": project.guide_teacher,
                "status": project.status,
                "students": students
            })

    return render(request, html, {"carreers": carreers,
                                  "projects": projects_f,
                                  "cant_prop": cant_prop,
                                  "proj_aprobado": proj_aprobado,
                                  "proj_reprobado": proj_reprobado,
                                  })


@login_required(login_url='/')
def get_students(request):
	carreer_pk = request.POST["pk"]
	print "ESTA ES LA PK, ", carreer_pk
	cycle = request.POST["cycle"]
	post_data = {
		'cycle': cycle,
		"carreer": carreer_pk
	}

	print "Antes de el post"
	header = {"Content-Type": 'application/x-www-form-urlencoded',
			"Authorization": "Token " + request.session["token"]}
	response = requests.post(
		"http://supreme.webtic.cl/get/get_students_carreer/", headers=header, data=post_data)
	content = response.content
	content = ast.literal_eval(content)
	print "Despues del post"
	html = ""

	values = sorted(content["students"], key=itemgetter(0))

	for student in values:
		try:
			o_student = Student.objects.get(rut=student[1])
			if o_student.project == None:
				html += "<option value='%s;%s'>%s</option>" % (
				    o_student.rut, o_student.actual_subject, o_student.name)

		except:
			html += "<option value='%s;%s'>%s</option>" % (
			    student[1], student[2], student[0])

	response = {
		"html": html
	}

	return JsonResponse(response)


@login_required(login_url='/')
def project_management(request):
	html = "projects/project_management.html"

	projects_er = Project.objects.filter(status="ER")
	careers_no_glosa = []
	for project in projects_er:
		try:
			Glosa.objects.get(carreer=project.carreer)
		except Glosa.DoesNotExist:
			if Carreer.objects.filter(career_pk=int(project.carreer)).exists():
				n_carreer = Carreer.objects.get(career_pk=int(project.carreer))
			else:
				post_data = {"carreer": int(project.carreer)}
				header = {"Content-Type": 'application/x-www-form-urlencoded',
						"Authorization": "Token " + request.session["token"]}
				response = requests.post(
					"http://supreme.webtic.cl/get/Carreer_Data/", headers=header, data=post_data)
				content = response.content
				print content
				content = json.loads(content)
				str_f = content["nombre"]
				n_career = Carreer.objects.create(
				    career_pk=int(project.carreer), name=str_f)
				n_career.save()
			if n_career not in careers_no_glosa:
				careers_no_glosa.append(n_career)

	data = {
		"projects_er": projects_er,
	}
	if not careers_no_glosa:
		data["careers_no_glosa"] = "0"
	else:
		data["careers_no_glosa"] = careers_no_glosa

	return render(request, html, data)


@login_required(login_url='/')
def create_feedback(request):
    if request.POST["feedbacks"]:
        perfil = UserProfile.objects.get(user=request.user)
        feedbacks = json.loads(request.POST['feedbacks'])
        for pk in feedbacks.keys():
            project = Project.objects.get(pk=int(pk))
            glosa = Glosa.objects.get(carreer=project.carreer)
            receipt = project.author
            status = feedbacks[pk]["type"]
            if status == "AP":
                description = "Su propuesta ha sido APROBADA con la siguiente glosa: "
                description += glosa.description
            elif status == "RE":
                description = "Su propuesta ha sido RECHAZADA con el siguiente comentario: "
                description += feedbacks[pk]["description"]

            feedback = Feedback.objects.create(
                receipt=receipt,
                status=status,
                description=description,
                project=project,
                author=perfil,
                date=timezone.now()
            )
            feedback.save()
        response = {
            "mensaje": "Enviado con exito"
        }
    else:
        response = {
            "mensaje": "Falta agregar feedbacks"
        }

    return JsonResponse(response)


@login_required(login_url='/')
def update_projects(request):
	approved = request.POST["aprobados_pk"]
	rechazados = request.POST["rechazados_pk"]

	approved = approved.split("-")[:-1]
	rechazados = rechazados.split("-")[:-1]

	for project_pk in approved:
		if project_pk != "":
			project = Project.objects.get(pk=int(project_pk))
			if project.vacancy_total == 0:
				project.status = "EC"
			else:
				project.status = "AC"
			project.save()

			if Postulation.objects.filter(project=project, status="PR").exists():
				postulations = Postulation.objects.filter(project=project, status="PR")
				for postulation in postulations:
					postulation.status = "AP"
					postulation.save()
					student = postulation.student
					student.project = project
					student.save()

	for project_pk in rechazados:
		if project_pk != "":
			project = Project.objects.get(pk=int(project_pk))
			project.status = "RE"
			project.save()

	return HttpResponseRedirect(reverse_lazy("project_management"))


@login_required(login_url='/')
def create_project(request):
    carrera = request.POST["carrera"]
    periodo = request.POST["periodo"]
    anio = request.POST["anio"]
    cupos = request.POST["cupos"]
    try:
        estudiantes = request.POST.getlist("estudiantes[]")
    except:
        estudiantes = request.POST["estudiantes"]

    profesor = request.POST["profesor"]
    titulo = request.POST["titulo"]
    descripcion = request.POST["descripcion"]

    tags = request.POST["tags"]
    tags_f = []
    for tag in tags.split(","):
        if tag != "":
            tags_f.append(tag.split("<")[0])

    author = UserProfile.objects.get(user=request.user)
    if profesor != "Null":
        try:
            teacher = UserProfile.objects.get(rut=profesor)
            if teacher.get_amount_projects() >= 15:
                return JsonResponse({'mensaje': 'Docente ya tiene el máximo de proyectos posibles.'})

        except UserProfile.DoesNotExist:
            header = {"Content-Type": 'application/x-www-form-urlencoded',
                'Authorization': "Token " + request.session["token"]}
            post_data = {'rut': profesor}
            response = requests.post(
                'http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
            content = ast.literal_eval(response.content)
            content = content['user_info']
            if content['user_first_name'][0] == " ":
                first = unidecode(content['user_first_name'])[1:].split(" ")
            else:
                first = unidecode(content['user_first_name']).split(" ")

            if content["user_last_name"][0] == " ":
                last = unidecode(content['user_last_name'])[1:].split(" ")
            else:
                last = unidecode(content['user_last_name']).split(" ")

            full_name = ""
            for name in first:
                full_name += name.capitalize()+" "

            for name in last:
                full_name += name.capitalize()+" "

            teacher = UserProfile.objects.create(rut=profesor, name=full_name)
            teacher.save()
    else:
        teacher = None

    status = ""
    if request.session["type"] == "DC" and teacher != None and teacher == author:
        status = "AC"
    elif teacher == None or (teacher != None and teacher != author):
        status = "ED"
    else:
        status = "ER"

    n_project = Project.objects.create(
        status=status,
        date=timezone.now(),
        name=titulo,
        vacancy_available=int(cupos),
        vacancy_total=int(cupos),
        description=descripcion,
        author=author,
        guide_teacher=teacher,
        period=periodo,
        year=anio,
        carreer=int(carrera),
    )
    n_project.save()

    if teacher != None and teacher != author:
        n_postulation = Teacher_Postulation.objects.create(
            project=n_project,
            status="AS",
            author=author,
            teacher=teacher,
        )
        n_postulation.save()

        n_feedback = Feedback.objects.create(
            status="AS",
            project=n_project,
            description="Se te ha asignado un proyecto!<br> Nombre del proyecto: %s " % (
                n_project.name),
            receipt=teacher,
            t_postulation=n_postulation,
            author=author,
        )
        n_feedback.save()

    for student in estudiantes:
        if student != "Null" and n_project.vacancy_total != 0:

            try:
                student_rut = student.split(";")[0]
                student_code = student.split(";")[1]
                student_obj = Student.objects.get(rut=student_rut)

            except:
                student_rut = student.split(";")[0]
                student_code = student.split(";")[1]
                header = {"Content-Type": 'application/x-www-form-urlencoded',
                    'Authorization': "Token " + request.session["token"]}
                post_data = {'rut': student_rut,
                            'period': periodo}

                response = requests.post(
                    'http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
                content = ast.literal_eval(response.content)
                content = content['user_info']

                if content['user_first_name'][0] == " ":
                    first = content['user_first_name'][1:].split(" ")
                else:
                    first = content['user_first_name'].split(" ")

                if content["user_last_name"][0] == " ":
                    last = content['user_last_name'][1:].split(" ")
                else:
                    last = content['user_last_name'].split(" ")
                full_name = ""
                for name in first:
                    full_name += name.capitalize()+" "

                for name in last:
                    full_name += name.capitalize()+" "

                student_obj = Student.objects.create(
                    rut=student_rut,
                    actual_subject=student_code,
                    name=full_name,
                    carreers=carrera)
                student_obj.save()

            if status == "AC":
                student_obj.project = n_project
                student_obj.save()

            else:
                n_postulation = Postulation.objects.create(
                    status="PR",
                    student=student_obj,
                    project=n_project,
                    last_mod=timezone.now(),
                )
                n_postulation.save()

                n_feedback = Feedback.objects.create(
                    status="AS",
                    project=n_project,
                    description="Se te ha asignado un proyecto!<br> Nombre del proyecto: %s " % (
                        n_project.name),
                    receipt_student=student_obj,
                    postulation=n_postulation,
                    author=author,
                )
                n_feedback.save()

            n_project.vacancy_available -= 1
            n_project.save()

    if n_project.vacancy_available == 0 and request.session["type"] == "DC":
        postulations = Postulation.objects.filter(
            project=n_project, status="PR")
        for postulation in postulations:
            postulation.status = "AP"
            postulation.save()
            postulation.student.project = n_project
            postulation.student.save()
            n_project.vacancy_available -= 1
            n_project.save()

            glosa = Glosa.objects.get(carreer=int(carrera))
            n_feedback = Feedback.objects.create(
                status="AP",
                project=n_project,
                description=unicode("Tu asignacion a un proyecto a sido aprobada!<br> Nombre del proyecto: %s <br> con la siguiente glosa: <br> %s" % (
                    n_project.name, glosa.description)),
                receipt_student=postulation.student,
                postulation=postulation,
                author=author,
            )
            n_feedback.save()
        glosa = Glosa.objects.get(carreer=int(carrera))
        n_project.status = "EC"
        n_project.save()
        n_feedback = Feedback.objects.create(
            status="AP",
            project=n_project,
            description="Tu propuesta de proyecto a sido aprobada!<br> Nombre del proyecto: %s <br> con la siguiente glosa: <br> %s" % (
                n_project.name, glosa.description),
            receipt=author,
            author=author,
        )

    for tag in tags_f:
        try:
            tag_obj = Tag.objects.get(value=tag)
            n_project.tags.add(tag_obj)
            n_project.save()
        except:
            tag_obj = Tag.objects.create(value=tag)
            tag_obj.save()
            n_project.tags.add(tag_obj)
            n_project.save()

    return JsonResponse({'mensaje': 'exito'})


@login_required(login_url='/')
def edit_project(request):
    if request.POST:
        try:
            project_details = {}
            project_pk = str(request.POST['info']).split('_')[1]
            project = get_object_or_404(Project, pk=project_pk)

            project_details['info'] = "project_" + str(project.pk)
            project_details['name'] = unidecode(project.name)
            project_details['period'] = project.period
            project_details['description'] = project.description
            project_details['vacancy_available'] = project.vacancy_available
            project_details['carreer'] = project.carreer
            project_details['guide_teacher'] = project.guide_teacher.rut
            project_details['year'] = project.year

            project_details['student_list'] = []
            for s in Student.objects.filter(project=project):
                project_details['student_list'].append(s.rut)

            project_details['tags'] = []
            for tag in project.tags.all():
                project_details['tags'].append(tag.value)

            return JsonResponse({
                "response": "ok",
                "data": project_details
            })
        except Exception, e:
            print e
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            raise PermissionDenied
    else:
        raise PermissionDenied


@login_required(login_url='/')
def update_project_detail(request):
    if request.POST:
        print request.POST
        try:
            project_pk = str(request.POST['info']).split('_')[1]

            title = request.POST["title"]
            descripcion = request.POST["descripcion"]
            cupos = request.POST["cupos"]
            year = request.POST["anio"]
            period = request.POST["periodo"]
            tags = request.POST["tags"]
            tags_f = []
            for tag in tags.split(","):
                if tag != "":
                    tags_f.append(tag.split("<")[0])
            author = UserProfile.objects.get(user=request.user)

            n_project = get_object_or_404(Project, pk=project_pk)

            n_project.vacancy_available = int(cupos)
            n_project.name = title
            n_project.year = year
            n_project.period = period
            print n_project

            n_project.save()

            for tag in tags_f:
                try:
                    tag_obj = Tag.objects.get(value=tag)
                except:
                    tag_obj = Tag.objects.create(value=tag)
                    tag_obj.save()
                    n_project.tags.add(tag_obj)
                    n_project.save()

            return HttpResponse(status=200)

        except Exception, e:
            print e
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            raise PermissionDenied
    else:
        raise PermissionDenied


@login_required(login_url='/')
def delete_project(request):
    if request.POST:
        try:
            project_pk = str(request.POST['info']).split('_')[1]
            project = get_object_or_404(Project, pk=project_pk)
            project.delete()

            return JsonResponse({
                "response": "ok",
                "data": {
                    "title": "Éxito!",
                    "message": "El proyecto se eliminó correctamente",
                    "type": "success"
                }
            })
        except Exception, e:
            print e
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            raise PermissionDenied
    else:
        raise PermissionDenied


@login_required(login_url='/')
def retro_modal(request):
    approved = request.POST["aprobados_pk"]
    rejected = request.POST["rechazados_pk"]
    approved = approved.split("-")[:-1]
    rejected = rejected.split("-")[:-1]
    input_str = '<label for="textArea" class="control-label">Comentarios:</label>\
							<textarea class="form-control retro" data-pk="%s" data-status="%s" rows="8" id="textArea"></textarea>'
    header = '<li role="presentation" class="{4}" style="width:8%"><a class="persistant-disabled" href="#stepper-step-{0}" data-toggle="tab" aria-controls="stepper-step-{1}" role="tab" title="{2}"><span class="round-tab">{3}</span></a></li>'
    content = '<div class = "tab-pane fade %s" role = "tabpanel" id = "stepper-step-%d" >\
                <div class = "p-20" >\
                    <header >\
                        <h3> %s </h3>\
                        <h2> %s </h2>\
                        <p> %s </p>\
                    </header >\
                    <section class = "stepper-body" >\
						<div class="form-group label-floating is-empty">\
                            %s\
						</div>\
                    </section >\
                </div >\
                <div class = "modal-footer" >\
                    <ul class = "list-inline pull-right" >\
                        %s\
                    </ul>\
                </div>\
            </div>'
    footer_i = '<li><a class="btn btn-primary next-step">Siguiente</a></li>'
    footer_m = '<li><a class="btn btn-default prev-step">Anterior</a></li><li><a class="btn btn-primary next-step">Siguiente</a></li>'
    footer_f = '<li><a class="btn btn-default prev-step">Anterior</a><button id="finish" type="button" class="btn btn-primary btn-flat">Completar</button></li>'
    i = 1
    header_f = ""
    content_f = ""
    if len(approved) > 0:
        for project_pk in approved:
            project = Project.objects.get(pk=int(project_pk))
            if len(approved)+len(rejected) == 1:
                header_f += header.format(i, i,
                                          unidecode(project.name), i, "active")
                content_f += content % ("in active", i, u"El siguiente proyecto pasará a estar ACEPTADO", unidecode(project.name),
                                        project.description[:250]+"...", "<h2 class='retro' data-pk='{0}' data-status='{1}'> Para confirmar continue, de lo contrario cierre este mensaje.</h2>".format(project_pk, "AP"), footer_f)
            else:
                if i == 1:
                    header_f += header.format(i, i,
                                              unidecode(project.name), i, "active")
                    content_f += content % ("in active", i, u"El siguiente proyecto pasará a estar ACEPTADO", unidecode(project.name),
                                            project.description[:250]+"...", "<h2 class='retro' data-pk='{0}' data-status='{1}'> Para confirmar continue, de lo contrario cierre este mensaje.</h2>".format(project_pk, "AP"), footer_i)
                elif i == len(approved)+len(rejected):
                    header_f += header.format(i, i,
                                              unidecode(project.name), i, "disabled")
                    content_f += content % ("", i, u"El siguiente proyecto pasará a estar ACEPTADO", unidecode(project.name),
                                            project.description[:250]+"...", "<h2 class='retro' data-pk='{0}' data-status='{1}'> Para confirmar continue, de lo contrario cierre este mensaje.</h2>".format(project_pk, "AP"), footer_f)
                else:
                    header_f += header.format(i, i,
                                              unidecode(project.name), i, "disabled")
                    content_f += content % ("", i, u"El siguiente proyecto pasará a estar ACEPTADO", unidecode(project.name),
                                            project.description[:250]+"...", "<h2 class='retro' data-pk='{0}' data-status='{1}'> Para confirmar continue, de lo contrario cierre este mensaje.</h2>".format(project_pk, "AP"), footer_m)
            i += 1

    if len(rejected) > 0:
        for project_pk in rejected:
            project_r = Project.objects.get(pk=int(project_pk))
            if len(rejected)+len(approved) == 1:
                header_f += header.format(i, i, project_r.name, i, "active")
                content_f += content % ("in active", i, "Explica por que RECHAZAS este proyecto: ", project_r.name,
                                        project_r.description[:250]+"...", input_str % (project_pk, "RE"), footer_f)
                i += 1
            else:
                if i == 1:
                    header_f += header.format(i, i,
                                              project_r.name, i, "active")
                    content_f += content % ("in active", i, "Explica por que RECHAZAS este proyecto: ", project_r.name,
                                            project_r.description[:250]+"...", input_str % (project_pk, "RE"), footer_i)
                    i += 1
                elif i == len(rejected)+len(approved):
                    header_f += header.format(i, i,
                                              project_r.name, i, "disabled")
                    content_f += content % ("", i, "Explica por que RECHAZAS este proyecto: ", project_r.name,
                                            project_r.description[:250]+"...", input_str % (project_pk, "RE"), footer_f)
                    i += 1
                else:
                    header_f += header.format(i, i,
                                              project_r.name, i, "disabled")
                    content_f += content % ("", i, "Explica por que RECHAZAS este proyecto: ", project_r.name,
                                            project_r.description[:250]+"...", input_str % (project_pk, "RE"), footer_m)
                    i += 1

    response = {
        "header": header_f,
        "content": content_f
    }

    return JsonResponse(response)


@login_required(login_url='/')
def criteria_delete(request):
    criteria = get_object_or_404(Criteria, pk=request.POST['criteria_pk'])
    criteria.delete()
    response = {}
    response["criteria_pk"] = request.POST['criteria_pk']
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required(login_url='/')
def outcome_delete(request):
    outcome = get_object_or_404(Outcome, pk=request.POST['outcome_pk'])
    outcome.delete()
    response = {}
    response["outcome_pk"] = request.POST['outcome_pk']
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required(login_url='/')
def abet_details(request, pk):
	template_name = 'projects/abet_details.html'
	abet_obj = get_object_or_404(Abet, pk=pk)
	if Carreer.objects.filter(career_pk=int(abet_obj.carreer)).exists():
		str_f = Carreer.objects.get(career_pk=int(abet_obj.carreer)).name
	else:
		post_data = {"carreer": int(abet_obj.carreer)}
		header = {"Content-Type": 'application/x-www-form-urlencoded',
				"Authorization": "Token " + request.session["token"]}
		response = requests.post(
			"http://supreme.webtic.cl/get/Carreer_Data/", headers=header, data=post_data)
		content = response.content
		content = json.loads(content)
		str_f = content["nombre"]
		n_career = Carreer.objects.create(
		    career_pk=int(abet_obj.carreer), name=str_f)
		n_career.save()

	outcomes = Outcome.objects.filter(abet=abet_obj).order_by("letter")
	return render(request, template_name, {'carreer': str_f, 'abet': abet_obj, 'outcomes': outcomes})


@login_required(login_url='/')
def abet_create(request):
    response = {}
    if request.POST['abet_carreer'] == '':
        abet = Abet.objects.create(
                                   author=request.user,
                                   creation_date=timezone.now(),
                                   last_mod=timezone.now(),
                                   )
        abet.save()

    else:
        carreer = request.POST['abet_carreer']
        abet = Abet.objects.create(
                                   author=request.user,
                                   creation_date=timezone.now(),
                                   last_mod=timezone.now(),
                                   carreer=carreer,
                                   )
        abet.save()

    response['pk'] = abet.pk
    response['mensaje'] = "Abet Creado"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required(login_url='/')
def outcome_create(request):
    response = {}
    outcome_desc = request.POST['outcome_desc']
    abet = get_object_or_404(Abet, pk=request.POST['abet_pk'])
    outcome = Outcome.objects.create(abet=abet,
                                     description=outcome_desc,
                                     )
    outcome.set_letter(abet)
    outcome.save()

    outcome_desc_response = outcome_desc[0:250] + "..."
    response['outcome_pk'] = outcome.pk
    response['mensaje'] = "Outcome Creado"
    response['outcome_letter'] = outcome.letter
    response['outcome_desc_response'] = outcome_desc_response

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required(login_url='/')
def criteria_create(request):
    response = {}
    suficient = request.POST['criteria_sufficient']
    inprocess = request.POST['criteria_inProcess']
    inscipient = request.POST['criteria_inscipient']
    outcome = get_object_or_404(Outcome, pk=request.POST['outcome_pk'])
    criteria_info = Crit_info.objects.create(suficient=suficient,
                                             inprocess=inprocess,
                                             inscipient=inscipient
                                            )
    criteria_info.save()

    criteria = Criteria.objects.create(outcome=outcome,
                                       description=request.POST['criteria_desc'],
                                       criteria_info=criteria_info
                                      )
    criteria.save()
    response['mensaje'] = "Criterio Creado"
    response['criteria_pk'] = criteria.pk
    response['criteria_desc'] = criteria.description
    response['criteria_suf'] = criteria_info.suficient
    response['criteria_inpr'] = criteria_info.inprocess
    response['criteria_ins'] = criteria_info.inscipient

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


@login_required(login_url='/')
def get_criteria_info(request):
    response = {}
    criteria = get_object_or_404(Criteria, pk=request.POST['criteria_pk'])
    response["criteria_desc"] = criteria.description
    response["criteria_suf"] = criteria.criteria_info.suficient
    response["criteria_inpr"] = criteria.criteria_info.inprocess
    response["criteria_ins"] = criteria.criteria_info.inscipient

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


@login_required(login_url='/')
def criteria_edit(request):
    response = {}
    criteria = get_object_or_404(Criteria, pk=request.POST['criteria_pk'])
    crit_info = criteria.criteria_info
    criteria.description = request.POST['criteria_desc']
    crit_info.suficient = request.POST['criteria_sufficient']
    crit_info.inprocess = request.POST['criteria_inProcess']
    crit_info.inscipient = request.POST['criteria_inscipient']

    criteria.save()
    crit_info.save()

    response["criteria_desc"] = criteria.description
    response["criteria_suf"] = criteria.criteria_info.suficient
    response["criteria_inpr"] = criteria.criteria_info.inprocess
    response["criteria_ins"] = criteria.criteria_info.inscipient
    response["criteria_pk"] = criteria.pk

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


@login_required(login_url='/')
def get_opt_rubric_outcomes(request):
    tipo = ""
    try:
        carreer = request.POST["carreer_pk"]
        rubric = get_object_or_404(AbetRubric, pk=request.POST["rubric"])
        tipo = "POST"
    except AttributeError:
        carreer = request[1]
        rubric = get_object_or_404(AbetRubric, pk=request[0])
        tipo = "GET"
    options = ''
    abet = get_object_or_404(Abet, carreer=carreer)
    outcomes_abet = abet.outcome_set.all()
    for outcome in outcomes_abet:
        if outcome not in rubric.outcomes.all():
            options += '<option value="%s">%s. %s</option>' % (
                outcome.pk,
                outcome.letter,
                outcome.description[:150]+"...")

    if tipo == "GET":
        return options

    else:
        response = {}
        response["outcomes"] = options
        return HttpResponse(
                json.dumps(response),
                content_type="application/json"
                )


@login_required(login_url='/')
def abet_rubric_add_outcomes(request):
    outcome_pks = request.POST.getlist('keys[]')
    outcomes_aux = []
    for key in outcome_pks:
        outcome = get_object_or_404(Outcome, pk=key)
        outcomes_aux.append(outcome)

    rubricpk = request.POST['rubric_pk']
    rubric = get_object_or_404(AbetRubric, pk=rubricpk)

    for outcome in outcomes_aux:
        rubric.outcomes.add(outcome)

    rubric.save()
    outcomes_html = ""

    for outcome in outcomes_aux:
        criterios_html = ""
        for criteria in outcome.criteria_set.all():
            aux = '<tr id="row_criteria_%d">\
                     <td>%s</td>\
                     <td>%s</td>\
                     <td>%s</td>\
                     <td>%s</td>\
                 </tr>' % (criteria.pk, criteria.description, criteria.criteria_info.suficient, criteria.criteria_info.inprocess, criteria.criteria_info.inscipient)
            criterios_html += aux
        aux = '<div class="panel panel-default animated fadeInRight">\
                <div class="panel-heading" role="tab" id="heading%d">\
                    <h4 class="panel-title">\
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse%d" aria-expanded="true" aria-controls="collapse%d">\
                            %s. %s\
                        </a>\
                    </h4>\
                </div>\
                <div id="collapse%d" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading%d">\
                    <div class="panel-body">\
                        <div class="row">\
                            <div class="col-sm-12 col-md-12">\
                                <button class="btn btn-block btn-danger remove_outcome" data-outcomepk="%d"> Eliminar Outcome</button> \
                            </div>\
                        </div>\
                        <table class="table table-striped table-bordered">\
                            <thead>\
                                <tr>\
                                    <th class="col-md-4" style="text-align:center;">Descripcion Criterio</th>\
                                    <th class="col-md-2" style="text-align:center;">Suficiente</th>\
                                    <th class="col-md-2" style="text-align:center;">En Proceso</th>\
                                    <th class="col-md-2" style="text-align:center;">Inscipiente</th>\
                                </tr>\
                            </thead>\
                            <tbody style="vertical-align:middle" id="content_table_criteria_%d">\
                            %s\
                            </tbody>\
                        </table>\
                    </div>\
                </div>\
            </div>' % (outcome.pk, outcome.pk, outcome.pk, outcome.letter, outcome.description[0:64], outcome.pk, outcome.pk, outcome.pk, outcome.pk, criterios_html)
        outcomes_html += aux
    arg = (rubric.pk, request.POST["carreer_pk"])
    options = get_opt_rubric_outcomes(arg)
    response = {}
    response["outcomes_opt"] = options
    response["outcomes"] = outcomes_html

    return HttpResponse(
                        json.dumps(response),
                        content_type="application/json"
                        )


@login_required(login_url='/')
def rubric_outcome_remove(request):
    rubric = get_object_or_404(AbetRubric, pk=request.POST['rubric_pk'])
    outcome = get_object_or_404(Outcome, pk=request.POST['outcome_pk'])
    rubric.outcomes.remove(outcome)
    rubric.save()

    arg = (rubric.pk, request.POST["carreer_pk"])

    options = get_opt_rubric_outcomes(arg)

    response = {}

    response["outcomes_opt"] = options
    response["outcome_pk"] = outcome.pk
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


@login_required(login_url='/')
def get_carreer_outcomes(request):
    carreer = request.POST['carreer_pk']
    options = ''
    abet = get_object_or_404(Abet, carreer=carreer)
    outcomes = abet.outcome_set.all()
    for outcome in outcomes:
        options += '<option value="%s">%s. %s</option>' % (
            outcome.pk,
            outcome.letter,
            outcome.description[:150]+"...")

    response = {}
    response['outcomes'] = options
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


@login_required(login_url='/')
def abet_rubric_activate(request):
    subject = request.POST['subject']
    rubricpk = request.POST['rubric_pk']
    rubric = get_object_or_404(AbetRubric, pk=rubricpk)
    rubric.subject = subject
    rubric.save()

    response = {}
    response["message"] = "activado con exito"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required(login_url='/')
def abet_rubric_create(request):
	profile = UserProfile.objects.get(user=request.user)
	name = request.POST["nombre"]
	subject = request.POST["subject"]
	if subject == "None":
		subject = None
	nota_suf = request.POST["nota_suf"]
	nota_enpr = request.POST["nota_enpr"]
	nota_inci = request.POST["nota_inci"]
	carreer = request.POST["carreer_pk"]
	rubric = AbetRubric.objects.create(
		name=name,
		author=profile,
		carreer=carreer,
		subject=subject,
		date=datetime.datetime.now(),
		nota_suf=nota_suf,
		nota_enpr=nota_enpr,
		nota_inci=nota_inci
	)
	rubric.save()

	response = {}

	response["rubricpk"] = rubric.pk

	return HttpResponse(
	json.dumps(response),
	content_type="application/json"
	)


@login_required(login_url='/')
def get_carreer_subjects(request):
    pk = request.POST["pk"]
    post_data = {"pk": int(pk)}
    header = {"Content-Type": 'application/x-www-form-urlencoded',
            "Authorization": "Token " + request.session["token"]}
    response = requests.post(
        "http://supreme.webtic.cl/get/get_course_director/", headers=header, data=post_data)
    content = response.content
    content = ast.literal_eval(content)
    html_abet = "<option value='None'>Sin Asignatura</option>"
    html_rubric = "<option value='None'>Sin Asignatura</option>"
    for ramo in content["subjects"]:
        if not Subject.objects.filter(code=str(ramo[1])).exists():
            n_subject = Subject.objects.create(
                name=str(ramo[0]),
                code=ramo[1],
                carreer=int(pk),
            )
            n_subject.save()

        if not AbetRubric.objects.filter(subject=ramo[1]).exists():
            html_abet += "<option value='%s'>%s</option>" % (
                str(ramo[1]), ramo[0]+" - "+str(ramo[1]))

        if not Rubric.objects.filter(subject=ramo[1]).exists():
            html_rubric += "<option value='%s'>%s</option>" % (
                str(ramo[1]), ramo[0]+" - "+str(ramo[1]))

    data = {
        "subjects_abet": html_abet,
        "subjects_rubric": html_rubric,
    }
    return JsonResponse(data)


@login_required(login_url='/')
def project_current(request):
	html = "projects/my_project_current.html"
	data = {}
	cant_advance = 0
	cant_regular = 0
	user_profile = UserProfile.objects.get(user=request.user)
	projects = Project.objects.filter(Q(guide_teacher=user_profile,status="EC")|Q(shared_teachers=user_profile,status="EC") )
	projects_f = []
	for project in projects.all():
		student = project.student_set.all()[0]
		if not Session_start.objects.filter(carreer=project.carreer,period=project.period).exists():
			projects_f.append([project, "Sin Inicio Configurado"])
		elif not Rubric.objects.filter(subject=student.actual_subject).exists():
			projects_f.append([project, "Sin Rúbrica Asignada"])
		else:
			beginning = Session_start.objects.get(carreer=project.carreer, period=project.period).beginning
			time_diff = datetime.date.today() - beginning
			time_diff = time_diff.total_seconds()
			sesion_actual = math.ceil(time_diff/60/60/24/7)
			rubric = Rubric.objects.get(subject=student.actual_subject)
			options = []
			for session in rubric.session_set.all():
				if not ProjectHitoEvaluation.objects.filter(session=session, project=project).exists() and not ProjectEvaluation.objects.filter(session=session, project=project).exists():
					try:
						if session.week and session.abet == 1:
							msg = "Semana %d: Evaluación Semanal y ABET" % (session.number)
							options.append([session.number, msg, ])
						elif session.week:
							msg = "Semana %d: Evaluación Semanal" % (session.number)
							options.append([session.number, msg, ])
					except:
						if session.hito and session.abet == 1:
							msg = "Semana %d: Evaluación Hito y ABET" % (session.number)
							options.append([session.number, msg, ])
						elif session.hito:
							msg = "Semana %d: Evaluación Hito" % (session.number)
							options.append([session.number, msg, ])

			cant_sesiones = len(rubric.session_set.all())
			print sesion_actual > cant_sesiones
			if not sesion_actual > cant_sesiones:
				print sesion_actual
				session = rubric.session_set.get(number=sesion_actual)
				if ProjectHitoEvaluation.objects.filter(session=session, project=project).exists():
					evaluado = 1
				elif ProjectEvaluation.objects.filter(session=session, project=project).exists():
					evaluado = 1
				else:
					evaluado = 0
				if project.student_set.all()[0].actual_subject[0] == "A":
					reg = "Advance"
					name = Subject.objects.get(code=project.student_set.all()[0].actual_subject).name
					cant_advance += 1
				else:
					reg = "Regular"
					name = Subject.objects.get(code=project.student_set.all()[0].actual_subject).name
					cant_regular += 1

				projects_f.append([project, evaluado, options, reg, name])
			else:
				pass

	data["projects"] = projects_f
	data["proj_advance"] = cant_advance
	data["proj_regular"] = cant_regular
	data["proj_todos"] = cant_regular+cant_advance
	return render(request, html, data)

@login_required(login_url='/')
def get_evaluation_review(request):
    data = {}
    if request.POST:
        info = str(request.POST['info']).split('-')
        project = get_object_or_404(Project, pk=info[2])
        session = get_object_or_404(Session, pk=info[1])
        if session.projecthitoevaluation_set.filter(project=project).exists():
            project_evaluation = session.projecthitoevaluation_set.get(project=project)
            data["grade1n"] = unicode("Técnica")
            data["grade1"] = project_evaluation.technique_qualification if project_evaluation.technique_qualification != 0 else "Formativo"
            data["grade2n"] = unicode("Presentación")
            data["grade2"] = project_evaluation.presentation_qualification if project_evaluation.presentation_qualification != 0 else "Formativo"
            data["grade3n"] = unicode("Memoria")
            data["grade3"] = project_evaluation.memory_qualification if project_evaluation.memory_qualification != 0 else "Formativo"
        elif session.projectevaluation_set.filter(project=project).exists():
            project_evaluation = session.projectevaluation_set.get(project=project)
            data["grade1n"] = unicode("Objetivo")
            data["grade1"] = project_evaluation.objetive_qualification if project_evaluation.objetive_qualification != 0 else "Formativo"
            data["grade2n"] = unicode("Actividad")
            data["grade2"] = project_evaluation.activity_qualification if project_evaluation.activity_qualification != 0 else "Formativo"
            data["grade3n"] = unicode("Progreso")
            data["grade3"] = project_evaluation.progress_qualification if project_evaluation.progress_qualification != 0 else "Formativo"

        data["comment_progress"] = unicode(project_evaluation.comment_progress)
        data["comment_activity"] = unicode(project_evaluation.comment_activity)
        data["comment_accomplishment"] = unicode(project_evaluation.comment_accomplishment)
        data['mensaje'] = "ok"
        return JsonResponse(data)
    else:
        raise PermissionDenied


@login_required(login_url='/')
def agendar_minuta(request):
	author = UserProfile.objects.get(user=request.user)
	tema = request.POST['about']
	date = str(request.POST['fecha'])
	print "FECHA ",date
	date = datetime.datetime.strptime(date,"%d/%m/%Y - %H:%M")
	print "Datetime ",date
	project = request.POST['projectpk']
	invitados_ext = request.POST['externos']
	project = Project.objects.get(pk=project)

	if request.session['type'] != "AL":
		author_type = "DO"
	else:
		author_type = "AL"

	n_minuta = Minuta.objects.create(
		status="PE",
		project=project,
		reunion_theme=tema,
		author=request.user,
		author_type=author_type,
		reunion_date=date,
		external_visit=invitados_ext,
	)
	n_minuta.save()

	try:
		invitados_int = request.POST.getlist('internos[]')
		for invitados in invitados_int:
			invitados = invitados.split(";")
			if invitados[-1]=="S":
				try:
					student_obj = Student.objects.get(rut=invitados[0])
					n_minuta.invited_stud.add(student_obj)
				except Student.DoesNotExist:
					student_rut = invitados[0]
					student_code = invitados[1]
					header = {"Content-Type": 'application/x-www-form-urlencoded','Authorization': "Token " + request.session["token"]}
					post_data = {'rut': student_rut,"period":datetime.date.today().year}

					response = requests.post('http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
					content = ast.literal_eval(response.content)
					content = content['user_info']
					if content['user_first_name'][0] == " ":
						first = content['user_first_name'][1:].split(" ")
					else:
						first = content['user_first_name'].split(" ")

					if content["user_last_name"][0] == " ":
						last = content['user_last_name'][1:].split(" ")
					else:
						last = content['user_last_name'].split(" ")
					full_name = ""
					for name in first:
						full_name += name.capitalize()+" "

					for name in last:
						full_name += name.capitalize()+" "

					student_obj = Student.objects.create(
						rut=student_rut,
						actual_subject=student_code,
						name=full_name,
						carreers=project.carreer)
					student_obj.save()
					n_minuta.invited_stud.add(student_obj)
			else:
				try:
					teacher = UserProfile.objects.get(rut=invitados[0])
					n_minuta.invited_user.add(teacher)

				except UserProfile.DoesNotExist:
					header = {"Content-Type": 'application/x-www-form-urlencoded','Authorization': "Token " + request.session["token"]}
					post_data = {'rut': invitados[0]}
					response = requests.post('http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
					content = ast.literal_eval(response.content)
					content = content['user_info']
					if content['user_first_name'][0] == " ":
						first = unidecode(content['user_first_name'])[1:].split(" ")
					else:
						first = unidecode(content['user_first_name']).split(" ")

					if content["user_last_name"][0] == " ":
						last = unidecode(content['user_last_name'])[1:].split(" ")
					else:
						last = unidecode(content['user_last_name']).split(" ")

					full_name = ""
					for name in first:
						full_name += name.capitalize()+" "

					for name in last:
						full_name += name.capitalize()+" "

					teacher = UserProfile.objects.create(rut=invitados[0],name=full_name)
					teacher.save()
					n_minuta.invited_user.add(teacher)

	except:
		invitados_int = request.POST['internos']
		invitados = invitados.split(";")
		if invitados[-1]=="S":
			try:
				student_obj = Student.objects.get(rut=invitados[0])
				n_minuta.invited_stud.add(student_obj)
			except Student.DoesNotExist:
				student_rut = invitados.split(";")[0]
				student_code = invitados.split(";")[1]
				header = {"Content-Type": 'application/x-www-form-urlencoded','Authorization': "Token " + request.session["token"]}
				post_data = {'rut': student_rut,"period":datetime.date.today().year}

				response = requests.post('http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
				content = ast.literal_eval(response.content)
				content = content['user_info']

				if content['user_first_name'][0] == " ":
					first = content['user_first_name'][1:].split(" ")
				else:
					first = content['user_first_name'].split(" ")

				if content["user_last_name"][0] == " ":
					last = content['user_last_name'][1:].split(" ")
				else:
					last = content['user_last_name'].split(" ")
				full_name = ""
				for name in first:
					full_name += name.capitalize()+" "

				for name in last:
					full_name += name.capitalize()+" "

				student_obj = Student.objects.create(
					rut=student_rut,
					actual_subject=student_code,
					name=full_name,
					carreers=project.carreer)
				student_obj.save()
				n_minuta.invited_stud.add(student_obj)
		else:
			try:
				teacher = UserProfile.objects.get(rut=invitados[0])
				n_minuta.invited_user.add(teacher)
			except UserProfile.DoesNotExist:
				header = {"Content-Type": 'application/x-www-form-urlencoded','Authorization': "Token " + request.session["token"]}
				post_data = {'rut': invitados[0]}
				response = requests.post('http://supreme.webtic.cl/get/get_user_info/', headers=header, data=post_data)
				content = ast.literal_eval(response.content)
				content = content['user_info']
				if content['user_first_name'][0] == " ":
					first = unidecode(content['user_first_name'])[1:].split(" ")
				else:
					first = unidecode(content['user_first_name']).split(" ")

				if content["user_last_name"][0] == " ":
					last = unidecode(content['user_last_name'])[1:].split(" ")
				else:
					last = unidecode(content['user_last_name']).split(" ")

				full_name = ""
				for name in first:
					full_name += name.capitalize()+" "

				for name in last:
					full_name += name.capitalize()+" "

				teacher = UserProfile.objects.create(rut=invitados[0],name=full_name)
				teacher.save()
				n_minuta.invited_user.add(teacher)


	description = unidecode("Hay una nueva solicitud de reunion en espera para tu proyecto! <br> La fecha y hora es: ")+request.POST["fecha"]
	description = smart_unicode(description)
	for student in project.student_set.all():
		n_feedback = Feedback.objects.create(
			status="MI",
			author=author,
			minuta=n_minuta,
			receipt_student=student,
			description=description,
		)
		n_feedback.save()

		n_validation = MinutaValidation.objects.create(
			student=student,
			minuta=n_minuta
		)
		n_validation.save()

	data = {'mensaje':'Exito.'}

	return JsonResponse(data)

@login_required(login_url='/')
def update_validation(request):
	validation = MinutaValidation.objects.get(pk=request.POST['pk_validation'])
	if request.POST["status"] == "RE":
		validation.comment = request.POST["comment"]
	else:
		validation.comment = "Yo %s confirmo mi disponibilidad para asistir a la reunion solicitada." % (validation.student.name)

	validation.last_mod = datetime.datetime.now()
	validation.validation_status = request.POST["status"]
	validation.save()

	description = ""
	if request.POST["status"] == 'RE':
		description = 'Se ha rechazado la solicitud de reunion con el siguiente comentario: <br> '+request.POST["comment"]
	else:
		description = "El alumno %s ha aceptado la solicitud a reunion."%(validation.student.name)
	feedback = Feedback.objects.create(
		status = request.POST['status'],
		description = description,
		receipt = validation.minuta.project.guide_teacher
	)
	feedback.save()

	data = {'mensaje':'Exito.'}

	return JsonResponse(data)

@login_required(login_url='/')
def approve_reunion(request):
    minutapk = request.POST["minutapk"]
    minuta = Minuta.objects.get(pk=minutapk)
    minuta.status = "ER"
    minuta.save()
    description = "La reunión ha sido aprobada para la fecha:  ", minuta.reunion_date
    for student in minuta.project.student_set.all():
        feedback = Feedback.objects.create(
            status = "AP",
            receipt_student = student,
            project = minuta.project,
            minuta = minuta,
            description = description,
        )
        feedback.save()

    feedback = Feedback.objects.create(
        status = "AP",
        receipt = minuta.project.guide_teacher,
        project = minuta.project,
        minuta = minuta,
        description = description, 
    )
    feedback.save()

    data = {}
    data['mensaje'] = "OK"
    return JsonResponse(data)

@login_required(login_url='/')
def reject_reunion(request):
	minutapk = request.POST["minutapk"]
	minuta = Minuta.objects.get(pk=minutapk)
	minuta.status = "RE"
	minuta.save()

	data = {
		'mensaje':"OK"
	}

	return JsonResponse(data)

@login_required(login_url='/')
def minuta_details(request):
	data = {}
	minuta = Minuta.objects.get(pk=request.POST["minutapk"])
	html = ""
	validations = minuta.minutavalidation_set.all()
	for validation in validations:
		if validation.validation_status == "AP":
			html+= '<div class="card">\
					<header class="card-heading bg-success">\
						<h2 class="card-title">%s</h2>\
					</header>\
				</div>'%(validation.student)
		elif validation.validation_status == "RE":
			html+='<div class="card">\
						<header class="card-heading card-salmon">\
						<h2 class="card-title">%s</h2>\
					</header>\
					<div id="comment_progress" class="card-body">\
						<p>\
							<h2>\
								Este es el comentario con el que el alumno rechazó la solicitud:\
							</h2>\
							<hr>\
						<br>\
							%s\
						</p>\
					</div>\
				</div>'%(validation.student,validation.comment)

	data['html'] = html

	return JsonResponse(data)


@login_required(login_url='/')
def update_minuta(request):
    data = {}
    minuta = Minuta.objects.get(pk=request.POST["minutapk"])
    i = 0
    while i < 200:
        try:
            tema = request.POST["topic-"+str(i)]
            description = request.POST['evidence-'+str(i)]
            if request.session['type'] != "AL":
                author_type = "DO"
            else:
                author_type = "AL"
            if "evidencepk-"+str(i) in request.POST.keys():
                evidence = MinutaEvidence.objects.get(pk=int(request.POST['evidencepk-'+str(i)]))
                evidence.topic = tema
                evidence.description = description
                evidence.save()
            else:
                n_evidence = MinutaEvidence.objects.create(
                    minuta = minuta,
                    topic = tema,
                    description = description,
                    author = request.user,
                    author_type = author_type,
                )
                n_evidence.save()
            i += 1
        except MultiValueDictKeyError:
            break
    if request.FILES:
        minuta.data_file = request.FILES["file"]

    if request.session["type"] == "DC":
        minuta.status = "AP"
        minuta.save()
    else:
        minuta.status = "RM"
        minuta.author = request.user
        minuta.last_mod = timezone.now()
        minuta.save()

    data["mensaje"] = "ok"
    print "Esta es la minuta: ",minuta.status
    return JsonResponse(data)

@login_required(login_url='/')
def minuta_detail(request):
    data = {}
    minuta = Minuta.objects.get(pk=request.POST['minutapk'])
    data['reunion_date'] = "{0}-{1}-{2} {3}:{4}".format(
        minuta.reunion_date.day,
        minuta.reunion_date.month,
        minuta.reunion_date.year,
        minuta.reunion_date.hour,
        minuta.last_mod.minute if len(str(minuta.last_mod.minute))!=1 else '0'+str(minuta.last_mod.minute))

    data['reunion_theme'] = minuta.reunion_theme
    data['last_mod'] = "{0}-{1}-{2} {3}:{4}".format(
        minuta.last_mod.day,
        minuta.last_mod.month,
        minuta.last_mod.year,
        minuta.last_mod.hour,
        minuta.last_mod.minute if len(str(minuta.last_mod.minute))!=1 else '0'+str(minuta.last_mod.minute))

    if minuta.author_type == "AL":
        data['author'] = minuta.author.student.name
    else:
        data['author'] = minuta.author.userprofile.name

    if minuta.data_file :
        data['file'] = minuta.data_file.url
    data['filename'] = minuta.data_file.name.split("/")[-1]
    data['minuta'] = ""
    plant = '<div class="col-xs-6 col-md-6 col-sm-6">\
                <div class="card">\
                    <header class="card-heading card-purple">\
                        <h2 class="card-title">%s</h2>\
                    </header>\
                    <div class="card-body p-0 max-h-200 scrollbar overflow-auto" style="margin-left:10px">\
                        <p>%s</p>\
                    </div>\
                </div>\
            </div>'
    for evidence in minuta.minutaevidence_set.all():
		data['minuta'] += plant % (evidence.topic,evidence.description)

    comments = MinutaComment.objects.filter(minuta=minuta).order_by('date')

    comment_html = ""
    for comment in comments:
        if comment.author_type == "AL":
            perfil = Student.objects.get(user=comment.author)
        else:
            perfil = UserProfile.objects.get(user=comment.author)

        comment_html += '<li class="list-group-item ">\
            <span class="pull-left"><img src="%s" alt="" class="img-circle max-w-40 m-r-10"></span>\
            <div class="list-group-item-body">\
                <div class="list-group-item-heading">%s</div>\
                <div class="list-group-item-text">%s<br><small>%s</small></div>\
            </div>\
        </li>' % (perfil.img.url,perfil.name,comment.comment,(comment.date - datetime.timedelta(hours=2)).strftime("%Y-%m-%d %H:%M:%S"))

    data["comment_html"] = comment_html
    return JsonResponse(data)

@login_required(login_url='/')
def approve_minuta(request):
	data = {}
	minuta = Minuta.objects.get(pk=request.POST["minutapk"])
	minuta.status = "AP"
	minuta.save()
	for student in minuta.project.student_set.all():
		feedback = Feedback.objects.create(
			status="AP",
			minuta=minuta,
			description="Felicitaciones! Su minuta ha sido aprobada por el profesor a cargo.",
			receipt_student=student
		)
		feedback.save()
	data["mensaje"] = "OK"

	return JsonResponse(data)

@login_required(login_url='/')
def reject_minuta(request):
	data = {}
	minuta = Minuta.objects.get(pk=request.POST['minutapk'])
	minuta.status = "EM"
	minuta.save()
	retro = MinutaRetro.objects.create(
		minuta=minuta,
		comment=request.POST['comment']
	)
	retro.save()

	data['mensaje'] = "OK"

	return JsonResponse(data)

@login_required(login_url='/')
def get_retro_minuta(request):
    data = {
    }

    retro = MinutaRetro.objects.get(pk=request.POST['retro_pk'])
    retro.read = 1
    retro.save()
    data["html"] = retro.comment
    return JsonResponse(data)

@login_required(login_url='/')
def update_retro_minuta(request):
    data = {
    }

    retro = MinutaRetro.objects.get(pk=request.POST['retro_pk'])
    retro.read = 1
    retro.save()
    data["mensaje"] = 'ok'
    return JsonResponse(data)

@login_required(login_url='/')
def create_glosa(request):
	template_name = "projects/glosa.html"
	post_data = {"username": request.session['username']}
	header = {"Content-Type": 'application/x-www-form-urlencoded',
              "Authorization": "Token " + request.session["token"]}

	response = requests.post("http://supreme.webtic.cl/get/get_careers/", headers=header, data=post_data)
	dic = {"\u00ed".encode('utf-8'):"í","\u00f3".encode('utf-8'):"ó","\u00e1".encode('utf-8'):"á"}
	content = response.content
	content = ast.literal_eval(content)
	glosa_carreers = []
	inicio_carreers = Carreer.objects.all()
	for career in content['careers']:
		if not Glosa.objects.filter(carreer=int(career['info'])).exists():
			str_f = career["career_name"]
			for key in dic:
				if key in str_f:
					str_f = str_f.replace(key,dic[key])
			glosa_carreers.append([career['info'],str_f])

	data = {"glosa_carreers":glosa_carreers,
            'periods': Project.PERIOD_CHOICES,
			'inicio_carreers':inicio_carreers}
	glosas = Glosa.objects.all()
	sessions = Session_start.objects.all()

	data["glosas"] = []
	data["sessions"] = []
	print "Sessions, ", sessions
	for glosa in glosas:
		if Carreer.objects.filter(career_pk=int(glosa.carreer)).exists():
			carreer = Carreer.objects.get(career_pk=glosa.carreer)
		else:

			header = {"Content-Type": 'application/x-www-form-urlencoded',
					'Authorization': "Token " + request.session["token"]}
			post_data = {'carreer': glosa.carreer}

			response = requests.post(
					'http://supreme.webtic.cl/get/Carreer_Data/', headers=header, data=post_data)

			content = json.loads(response.content)
			str_f = content["nombre"]
			carreer = Carreer.objects.create(name=str_f,career_pk=glosa.carreer)
			carreer.save()
		data['glosas'].append([glosa,carreer.name])

	for session in sessions:
		print "ENTRE AL FOR"
		if Carreer.objects.filter(career_pk=int(session.carreer)).exists():
			carreer = Carreer.objects.get(career_pk=session.carreer)
		else:
			dic = {"\u00ed":"i","\u00f3":"o","\u00e1":"a"}

			header = {"Content-Type": 'application/x-www-form-urlencoded',
					'Authorization': "Token " + request.session["token"]}
			post_data = {'carreer': session.carreer}

			response = requests.post(
					'http://supreme.webtic.cl/get/Carreer_Data/', headers=header, data=post_data)

			content = json.loads(response.content)
			str_f = content["nombre"]
			for key in dic:
				if key in str_f:
					str_f = str_f.replace(key,dic[key])
			carreer = Carreer.objects.create(name=str_f,career_pk=session.carreer)
			carreer.save()

		year = session.beginning.year
		print "Este es el session, ",session
		print "Este es el session, ",session.period
		if session.period == "15":
			data['sessions'].append([session,carreer.name,year,"Segundo Trimestre"])
		elif session.period == "05":
			data['sessions'].append([session,carreer.name,year,"Primer Trimestre"])
		elif session.period == "25":
			data['sessions'].append([session,carreer.name,year,"Tercer Trimestre"])
		elif session.period == "10":
			data['sessions'].append([session,carreer.name,year,"Primer Semestre"])
		elif session.period == "20":
			data['sessions'].append([session,carreer.name,year,"Segundo Semestre"])
		print data["sessions"]

	return render(request, template_name, data)

@login_required(login_url='/')
def save_glosa(request):
    carrera = request.POST["carrera"]
    descripcion = request.POST['descripcion']
    user = UserProfile.objects.get(user = request.user)
    glosa = Glosa.objects.create(carreer=int(carrera),
                                description=descripcion,
                                author=user)
    glosa.save()
    response = {"mensaje":"exito"}
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

@login_required(login_url='/')
def get_glosa(request):
    response = {}
    glosa_pk = str(request.POST['info']).split('_')[1]
    glosa = get_object_or_404(Glosa, pk=glosa_pk)

    carreer = Carreer.objects.get(career_pk=glosa.carreer)
    response["carrera"] = carreer.name
    response["descripcion"] = glosa.description
    response["autor"] = glosa.author.name
    return JsonResponse({
        "response": "ok",
        "data": response
        })

@login_required(login_url='/')
def edit_glosa(request):
    if request.POST:
        try:
            glosa_detail = {}
            glosa_pk = str(request.POST['info']).split('_')[1]
            glosa = get_object_or_404(Glosa, pk=glosa_pk)

            print glosa.description

            # glosa.carreer = request.POST['carreer']
            # glosa.description = request.POST['description']
            # glosa.author = request.POST['author']
            #
            # glosa.save()

            glosa_detail['info'] = "glosa_" + str(glosa.pk)
            glosa_detail['carreer'] = glosa.carreer
            glosa_detail['description'] = glosa.description
            # glosa_detail['author'] = glosa.author

            return JsonResponse({
                "response": "ok",
                "data": glosa_detail
            })
        except Exception as e:
            return HttpResponseRedirect(reverse_lazy('create_glosa'))

@login_required(login_url='/')
def update_glosa(request):
    if request.POST:
        try:
            glosa_pk = str(request.POST['info']).split('_')[1]

            descripcion = request.POST['descripcion']
            autor = UserProfile.objects.get(user=request.user)

            glosa_nueva = get_object_or_404(Glosa, pk=glosa_pk)
            glosa_nueva.description = descripcion

            glosa_nueva.save()

            return HttpResponse(status=200)
        except Exception as e:
            pass

@login_required(login_url='/')
def delete_glosa(request):
    if request.POST:
        try:
            glosa_pk = str(request.POST['info']).split('_')[1]
            glosa = get_object_or_404(Glosa, pk=glosa_pk)
            glosa.delete()

            return JsonResponse({
                "response": "ok",
                "data": {
                    "title": "Éxito!",
                    "message": "La glosa se eliminó correctamente",
                    "type": "success"
                }
            })

        except Exception, e:
            pass
            # print e
            # exc_type, exc_obj, exc_tb = sys.exc_info()
            # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            # raise PermissionDenied
    else:
        pass
        # raise PermissionDenied
        return HttpResponseRedirect(reverse_lazy('glosa'))

@login_required(login_url='/')
def get_session(request):
    pk = request.POST["pk"]
    session = Session_start.objects.get(pk = pk)

    response = {}

    response["carreer"] = session.carreer
    response["beginning"] = str(session.beginning)

    return HttpResponse(
        json.dumps(response),
        content_type = "application/json"
    )

@login_required(login_url='/')
def edit_session(request):
    pk = request.POST["pk"]
    session = Session_start.objects.get(pk = pk)

    session.carreer = request.POST["carreer"]
    session.beginning = request.POST["beginning"]

    session.save()
    response = {}
    response["pk"] = session.pk

    return HttpResponse(
        json.dumps(response),
        content_type = "application/json"
    )

@login_required(login_url='/')
def send_message(request):
    response = {}
    content = request.POST['message_content']
    project = Project.objects.get(pk=request.POST['projectpk'])
    if request.session['type'] == "AL":
        for student in project.student_set.all().exclude(user=request.user):
            feedback = Feedback.objects.create(
                status = "ME",
                author_s = request.user.student,
                description = "El alumno %s ha enviado el siguiente mensaje sobre el proyecto %s: %s"%(request.user.student,project,content),
                project = project,
                receipt_student = student
            )
            feedback.save()

        feedback = Feedback.objects.create(
            status = "ME",
            author_s = request.user.student,
            description = "El alumno %s ha enviado el siguiente mensaje sobre el proyecto %s: %s"%(request.user.student,project,content),
            project = project,
            receipt = project.guide_teacher
        )
        feedback.save()
    else:
        author = UserProfile.objects.get(user=request.user)
        for student in project.student_set.all():
            feedback = Feedback.objects.create(
                status = "ME",
                author = author,
                description = "El docente %s ha enviado el siguiente mensaje sobre el proyecto %s: %s"%(author,project,content),
                project = project,
                receipt_student = student
            )
            feedback.save()
    response["mensaje"] = "ok"
    return JsonResponse(response)

@login_required(login_url='/')
def project_report(request):
    template_name = "projects/project_resolution.html"
    data = {}
    counts = []
    # post_data = {"username": request.session['username']}
    # header = {"Content-Type": 'application/x-www-form-urlencoded',
    #           "Authorization": "Token " + request.session["token"]}
    carreers = Carreer.objects.all()
    periods = Project.PERIOD_CHOICES

    # data_carreers = requests.post("http://supreme.webtic.cl/get/get_careers/", headers = header, data = post_data)

    for carreer in carreers:
        quantity = Project.objects.filter(carreer = carreer.career_pk).count()
        counts.append([carreer.career_pk, carreer.name, quantity])

    data["counts"] = counts
    data["periods"] = periods
    data["carreers"] = carreers
    return render(request, template_name, data)

@login_required(login_url='/')
def get_projects(request):
    response = {}

    carreer_pk = str(request.POST['pk']).split('_')[1]
    print carreer_pk
    p_year = request.POST['year']
    p_period = request.POST['period']

    projects = Project.objects.filter(carreer = int(carreer_pk), year = int(p_year), period = p_period).values('pk', 'name', 'status', 'guide_teacher')

    users = UserProfile.objects.all()

    for project in projects:
        for user in users:
            if project['guide_teacher'] == user.pk:
                project['guide_teacher'] = user.name

    # print list(projects)

    response["projects"] = list(projects)

    return JsonResponse(response)

@login_required(login_url='/')
def get_students_report(request):
    response = {}
    data_student = []
    data_postulations = []
    carreer_pk = request.POST["carreer"]
    p_year = request.POST["year"]
    p_period = request.POST["period"]

    projects = Project.objects.filter(carreer = carreer_pk, year = p_year, period = p_period)
    students = Student.objects.filter(carreers = carreer_pk)
    postulations = Postulation.objects.all()
    users = UserProfile.objects.all()

    for project in projects:
        for student in students:
            if str(student.project) == project.name:
                data_student.append(student)

    if request.POST["option"] == "AA":
        for postulation in postulations:
            for student in data_student:
                if str(student.name) == str(postulation.student) and postulation.status == "AP":
                    data_postulations.append([postulation.pk, student.name, student.project.name, str(student.project.guide_teacher), postulation.status])

    elif request.POST["option"] == "AN":
        for postulation in postulations:
            for student in data_student:
                if str(student) == str(postulation.student) and postulation.status == "RE":
                    data_postulations.append([postulation.pk, student.name, student.project.name, str(student.project.guide_teacher), postulation.status])


    elif request.POST["option"] == "AP":
        for user in users:
            count_AP = Postulation.objects.filter(last_mod_user = user.pk, status = "AP").count()
            count_RE = Postulation.objects.filter(last_mod_user = user.pk, status = "RE").count()
            data_postulations.append([user.pk, user.name, count_AP, count_RE])

    elif request.POST["option"] == "AL":
        for postulation in postulations:
            for student in data_student:
                if str(student.name) == str(postulation.student):
                    data_postulations.append([postulation.pk, student.name, student.project.name, str(student.project.guide_teacher), postulation.status])

    response["data_postulations"] = data_postulations

    return JsonResponse(response)

@login_required(login_url='/')
def get_binnacle_report(request):
    response = {}
    data = []

    carreer_pk = request.POST["carreer"]
    p_year = request.POST["year"]
    p_period = request.POST["period"]

    projects = Project.objects.filter(carreer = carreer_pk, year = p_year, period = p_period)
    students = Student.objects.filter(carreers = carreer_pk)

    for student in students:
        for project in projects:
            if str(student.project) == str(project.name):
                if ProjectHitoEvaluation.objects.all():
                    data.append([project.pk, project.name, student.name, str(project.guide_teacher), ProjectHitoEvaluation.objects.filter(project = project.pk).count()])


    response["data"] = data

    return JsonResponse(response)

@login_required(login_url='/')
def send_comment(request):
    data = {}
    if request.session["type"] == "AL":
        perfil = Student.objects.get(user=request.user)
    else:
        perfil = UserProfile.objects.get(user=request.user)

    comment = request.POST["comment"]
    minuta_pk = request.POST['minuta_pk']
    minuta = Minuta.objects.get(pk=int(minuta_pk))
    n_comment = MinutaComment.objects.create(
        author = request.user,
        author_type = "AL" if request.session["type"] == "AL" else "DO",
        comment = comment,
        minuta = minuta,
    )
    n_comment.save()
    html = '<li class="list-group-item ">\
        <span class="pull-left"><img src="%s" alt="" class="img-circle max-w-40 m-r-10"></span>\
        <div class="list-group-item-body">\
            <div class="list-group-item-heading">%s</div>\
            <div class="list-group-item-text">%s<br><small>%s</small></div>\
        </div>\
    </li>' % (perfil.img.url,perfil.name,n_comment.comment,(n_comment.date - datetime.timedelta(hours=2)).strftime("%Y-%m-%d %H:%M:%S"))

    data["html"] = html
    return JsonResponse(data)

@login_required(login_url='/')
def get_info_minuta(request):
    data = {}
    minutapk = request.POST["minuta_pk"]
    minuta = Minuta.objects.get(pk=int(minutapk))
    evidences = minuta.minutaevidence_set.all()
    i = 0
    header = ""
    tabs = ""
    for evidence in evidences:
        header += '<li role="presentation">\
                        <a href="#tab-%d" data-toggle="tab" aria-expanded="true">Registro %d </a>\
                    </li>' % (i,i+1)
        tabs += '<div class="tab-pane animated %s fadeIn" id="tab-%d">\
                    <div class="col-md-12 col-sm-12 col-xs-12">\
                        <div class="form-control-wrapper">\
                            <div class="form-group is-empty">\
                                <label for="topic-%d" class="control-label">Tema %d:</label>\
                                <input name="topic-%d" id="topic-%d" class="form-control" value="%s" type="text"></input>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="col-md-12 col-sm-12 col-xs-12">\
                        <div class="form-control-wrapper">\
                            <div class="form-group is-empty">\
                                <label for="evidence-%d" class="control-label">Descripción %d:</label>\
                                <textarea name="evidence-%d" id="evidence-%d" class="form-control" type="text">%s</textarea>\
                            </div>\
                            <input value="%d" name="evidencepk-%d" style="display:none"></input>\
                        </div>\
                    </div>\
                </div>' % (("active in" if i == 0 else ""),i,i,i+1,i,i,unicode(evidence.topic),i,i+1,i,i,unicode(evidence.description),evidence.pk,i)
        i+=1
    data["header"] = header
    data["tabs"] = tabs
    data['cant'] = len(evidences)
    
    return JsonResponse(data)

@login_required(login_url='/')
def share_project(request):
    data = {}
    author = UserProfile.objects.get(user=request.user)
    project = Project.objects.get(pk=request.POST["project_pk"])
    if project.shared_teachers.count() < 3:
        teacher = request.POST["teacher_pk"]
        teacher = UserProfile.objects.get(pk=int(teacher))
        n_feedback = Feedback.objects.create(
            receipt=teacher,
            status="AS",
            description="El docente %s ha compartido el proyecto %s contigo." % (author,project.name),
            project=project,
        )
        n_feedback.save()
        project.shared_teachers.add(teacher)
        project.save()
        data["mensaje"] = "Éxito"
        data["nombre"] = teacher.name
        return JsonResponse(data)
    else:
        raise MemoryError


@login_required(login_url='/')
def update_pond(request):
    data = {
    }

    hito = Hito.objects.get(pk=request.POST["hitopk"])
    hito.tech_pond = int(request.POST["tech_pond"].strip("%"))
    hito.mem_pond = int(request.POST["mem_pond"].strip("%"))
    hito.pres_pond = int(request.POST["pres_pond"].strip("%"))
    hito.save()

    data["mensaje"] = "exito"
    return JsonResponse(data)

@login_required(login_url='/')
def update_hito_pond(request):
    data = {
    }

    rubric = Rubric.objects.get(pk=int(request.POST['rubricpk']))
    
    i = 1
    for session in rubric.session_set.all():
        try:
            data[str(i)]=[session.hito.ponderation,session.hito.pk]
            i+=1
        except Hito.DoesNotExist:
            print "No tiene Hito"

    data["desem"] = rubric.desem
    data['rubricpk'] = rubric.pk
    data['pond_desem'] = rubric.pond_desem 
    data["cant"] = i-1
    return JsonResponse(data)


@login_required(login_url='/')
def edit_hito_ponderation(request):
    print request.POST['desem'], type(request.POST["desem"])
    if request.POST['desem'] == "1":
        rubric = Rubric.objects.get(pk=int(request.POST['rubricpk']))
        if "%" in request.POST["desem_pond"]:
            rubric.pond_desem = int(request.POST['desem_pond'].strip("%"))
        else:
            rubric.pond_desem = int(request.POST['desem_pond'])

        rubric.save()

    else:
        hito = Hito.objects.get(pk=int(request.POST["hitopk"]))

        if "%" in request.POST["val"]:
            hito.ponderation = int(request.POST["val"].strip("%"))
        else:
            hito.ponderation = int(request.POST["val"])
        hito.save()
    data = {
        "mensaje":"Exito"
    }
    return JsonResponse(data)

@login_required(login_url='/')
def get_project_grades(request):
    project = Project.objects.get(pk=request.POST["projectpk"])
    evaluations = project.projecthitoevaluation_set.all().order_by("date")
    grades ='' 
    i = 1
    for evaluation in evaluations:
        print "ENTRE AL CICLO"
        grades +='<div class="col-md-3 col-sm-3">\
                      <div class="card">\
                          <header class="card-heading bg-success">\
                              <h2 class="card-title">Hito %d</h2>\
                          </header>\
                          <div id="nota_hito%d" data-nota="%d" data-pond="%d" class="card-body">\
                              <p>%d (%s)</p>\
                          </div>\
                      </div>\
                  </div>' % (i,i,evaluation.hito_qualification,evaluation.session.hito.ponderation,evaluation.hito_qualification,"%"+str(evaluation.session.hito.ponderation))
        i += 1

    print "ESTOS SON LOS GRADES:",grades
    data = {"html" : grades}
    return JsonResponse(data)

def approve_project(request):
    project = Project.objects.get(pk=int(request.POST["projectpk"]))
    if request.POST.get('desem',False):
        project.desem_grade = float(request.POST["desem"].replace(',','.'))
    project.status = "AP"
    project.final_grade = request.POST['final_grade'].replace(',','.')
    project.save()
    historic = ProjectHistoric.objects.create(
        status = "AP",
        project = project,
        evaluationComment = request.POST['comment']
    )
    historic.save()


    int_period = int(project.period)
    if int_period == 25 or int_period == 20:
        project.year += 1
        project.period = "05" if int_period == 25 else "10"
    else:
        project.period = str(int_period + 10)
    project.desem_grade = 0
    project.vacancy_available = project.vacancy_total
    project.status = "AC"
    project.save()
    
    for student in project.student_set.all():
        student.project = None
        student.save()
        historic.students.add(student)
        historic.save()
        stud_postulation = Postulation.objects.create(
            project = project,
            student = student,
            status = "PR"
        )
        stud_postulation.save()

        desc = "Se te ha asignado el proyecto: %s, para tu nuevo ciclo universitario." %(project.name)
        feedback = Feedback.objects.create(
            author = project.guide_teacher,
            status = "AS",
            description = desc, 
            postulation = stud_postulation,
            project = project,
            receipt_student = student
        )

        feedback.save()

    feedback = Feedback.objects.create(
        status = "AS",
        project = project,
        description = "Gracias a la aprobación del proyecto: %s, se ha generado una nueva versión para el siguiente periodo." % (project.name),
        receipt = project.guide_teacher,
    )
    feedback.save()
    data = {'mensaje':'exito'}
    return JsonResponse(data)

def fail_project(request):
    project = Project.objects.get(pk=int(request.POST["projectpk"]))
    if request.POST.get('desem',False):
        project.desem_grade = float(request.POST["desem"].replace(',','.'))
    project.status = "RP"
    project.save()
    for student in project.student_set.all():
        student.project = None
        student.save()
        
        feedback = Feedback.objects.create(
            author = project.guide_teacher,
            description = "Has reprobado tu proyecto, por lo que se te ha expulsado del mismo",
            project = project,
            status = "RP"
        )
        feedback.save()


    data = {'mensaje':'exito'}
    return JsonResponse(data)

def finish_project(request,pk):
    data = {}
    if request.user.userprofile:
        data["flag"] = True
        project = Project.objects.get(pk=pk)
        evaluations = project.projecthitoevaluation_set.all().order_by("date")
        grades ='' 
        i = 1
        final_score = 0
        for evaluation in evaluations:
            grades +='<div class="col-md-3 col-sm-3">\
                        <div class="card">\
                            <header class="card-heading bg-success">\
                                <h2 class="card-title">Hito %d</h2>\
                            </header>\
                            <div id="nota_hito%d" data-nota="%d" data-pond="%d" class="card-body">\
                                <p>%d (%s)</p>\
                            </div>\
                        </div>\
                    </div>' % (i,i,evaluation.hito_qualification,evaluation.session.hito.ponderation,evaluation.hito_qualification,"%"+str(evaluation.session.hito.ponderation))
            i += 1
            final_score += evaluation.hito_qualification*(float(evaluation.session.hito.ponderation)/100)
        
        data["final_score"] = "%.2f" % (final_score)
        data["grades"] = grades
        data["project"] = project
        data["hitos"] = i
        subject = project.student_set.all()[0].actual_subject
        rubric = Rubric.objects.get(subject=subject)
        data["rubric"] = rubric

    else:  
        data["flag"] = False

    template = "projects/finish_project.html"
    return render(request,template,data)

def evaluation_info(request,evaluation,tipo):
    data = {}
    template_name = 'evaluation_info.html'
    if tipo == "1":
        evaluacion = ProjectHitoEvaluation.objects.get(pk=evaluation)
        print "Entre al Hito: ", evaluacion,evaluacion.status
        print "Evaluacion tecnica: ",evaluacion.techniqueevaluation_set.all()
        print "Evaluacion de presentacion: ", evaluacion.presentationevaluation_set.all()
        print "Evaluacion Memoria: ", evaluacion.memoryevaluation_set.all()
    else:
        evaluacion = ProjectEvaluation.objects.get(pk=evaluation)
        print "Entre a la semana: ", evaluacion,evaluacion.status
        print "Objetivos de aprendizaje: ",evaluacion.objetiveevaluation_set.all()
        print "Avances de Producto: ", evaluacion.progressevaluation_set.all()
        print "Actividad semanal: ", evaluacion.activityevaluation_set.all()
        for nota in evaluacion.activityevaluation_set.all():
            print "Evaluacion de actividad, ",nota.nota

    data['tipo'] = tipo
    data['evaluacion'] = evaluacion
    
    if request.session['type'] == "DO" or request.session['type'] == "DC":
        perfil = UserProfile.objects.get(user=request.user)
        if evaluacion.project.guide_teacher == perfil or perfil in evaluacion.project.shared_teachers:
            data["editable"] = 1
        else:
            data["editable"] = 0
    else:
        data["editable"] = 0
    return render(request,template_name,data)

def update_evaluation(request):
	print "POST, ",request.POST
	finished_flag = True
	if request.POST.get('len_obj',False):
		len_obj = request.POST["len_obj"]
		len_act = request.POST["len_act"]
		len_pro = request.POST['len_pro']

		for x in range(int(len_obj)):
			pk = request.POST["objetivos["+str(x)+"][name]"]
			pk = pk.strip("objective")
			evaluation = ObjetiveEvaluation.objects.get(pk=int(pk))
			nota = float(request.POST["objetivos["+str(x)+"][value]"].replace(",","."))
			if nota == 0:
				finished_flag = False
			evaluation.nota = nota
			evaluation.save()
		
		for x in range(int(len_act)):
			pk = request.POST["actividades["+str(x)+"][name]"]
			pk = pk.strip('activity')
			evaluation = ActivityEvaluation.objects.get(pk=int(pk))
			print request.POST["actividades["+str(x)+"][value]"].replace(",",".")
			nota = float(request.POST["actividades["+str(x)+"][value]"].replace(",","."))
			evaluation.nota = nota
			if nota == 0:
				finished_flag = False
			evaluation.save()

		for x in range(int(len_pro)):
			pk = request.POST["avances["+str(x)+"][name]"]
			pk = pk.strip('progress')
			evaluation = ProgressEvaluation.objects.get(pk=int(pk))
			evaluation.nota = float(request.POST["avances["+str(x)+"][value]"].replace(",","."))
			evaluation.save()
			if evaluation.nota == 0:
				finished_flag = False

		project_eval = evaluation.project_evaluation

	else:
		len_mem = request.POST["len_mem"]
		len_tech = request.POST['len_tech']
		len_pre = request.POST["len_pre"]

		for x in range(int(len_mem)):
			pk = request.POST["memorias["+str(x)+"][name]"]
			pk = pk.strip("memory")
			evaluation = MemoryEvaluation.objects.get(pk=int(pk))
			evaluation.nota = float(request.POST["memorias["+str(x)+"][value]"].replace(",","."))
			evaluation.save()
			if evaluation.nota == 0:
				finished_flag = False
		
		for x in range(int(len_tech)):
			pk = request.POST["tecnicas["+str(x)+"][name]"]
			pk = pk.strip('technique')
			evaluation = TechniqueEvaluation.objects.get(pk=int(pk))
			evaluation.nota = float(request.POST["tecnicas["+str(x)+"][value]"].replace(",","."))
			evaluation.save()
			if evaluation.nota == 0:
				finished_flag = False

		for x in range(int(len_pre)):
			pk = request.POST["presentaciones["+str(x)+"][name]"]
			pk = pk.strip('presentation')
			evaluation = PresentationEvaluation.objects.get(pk=int(pk))
			evaluation.nota = float(request.POST["presentaciones["+str(x)+"][value]"].replace(",","."))
			evaluation.save()
			if evaluation.nota == 0:
				finished_flag = False

		project_eval = evaluation.project_hito_evaluation

	project_eval.comment_progress = request.POST['comment_progress']
	project_eval.comment_activity = request.POST['comment_activity']
	project_eval.comment_accomplishment = request.POST["comment_accomplishment"]
	if finished_flag:
		project_eval.status = "FI"
	project_eval.save()
		
	return JsonResponse({"message":"exito"})