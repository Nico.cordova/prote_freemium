from django.contrib import admin
from projects.models import MinutaEvidence,MinutaComment,MinutaValidation,Teacher_Postulation,Glosa,Feedback,Tag,Project,Postulation,Minuta
from simple_history.admin import SimpleHistoryAdmin

admin.site.register(Tag)
admin.site.register(Project)
admin.site.register(Postulation)
admin.site.register(Teacher_Postulation)
admin.site.register(Minuta)
admin.site.register(Feedback)
admin.site.register(Glosa)
admin.site.register(MinutaValidation)
admin.site.register(MinutaEvidence, SimpleHistoryAdmin)
admin.site.register(MinutaComment)