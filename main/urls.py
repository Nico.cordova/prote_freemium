"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
from core import views as core_views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', core_views.login_user, name="login"),
    url(r'^index/$', core_views.index, name="index"),
    url(r'^dashboard/', include('core.urls')),
    url(r'^projects/', include('projects.urls')),
    url(r'^practicas/', include('practicas.urls')),
#    url(r'^Lista_prote/$', core_views.Lista_prote, name="Lista_prote"),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
